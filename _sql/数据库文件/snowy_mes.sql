/*
 Navicat Premium Data Transfer

 Source Server         : 本机
 Source Server Type    : MySQL
 Source Server Version : 50738
 Source Host           : localhost:3306
 Source Schema         : snowy

 Target Server Type    : MySQL
 Target Server Version : 50738
 File Encoding         : 65001

 Date: 26/10/2022 16:58:33
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for dw_ass
-- ----------------------------
DROP TABLE IF EXISTS `dw_ass`;
CREATE TABLE `dw_ass`  (
                           `id` bigint(20) NOT NULL COMMENT '主键id',
                           `code` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号，唯一性校验',
                           `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                           `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                           `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                           `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                           `update_user` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
                           PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '装配工单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dw_ass
-- ----------------------------

-- ----------------------------
-- Table structure for dw_bom
-- ----------------------------
DROP TABLE IF EXISTS `dw_bom`;
CREATE TABLE `dw_bom`  (
                           `id` bigint(20) NOT NULL COMMENT '主键id',
                           `parent_id` bigint(20) NULL DEFAULT NULL COMMENT '父项产品id',
                           `subitem_id` bigint(20) NULL DEFAULT NULL COMMENT '子项产品id',
                           `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                           `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                           `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                           `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                           `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                           `unit_con` bigint(255) NULL DEFAULT NULL COMMENT '单位用量',
                           PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_german2_ci COMMENT = '物料表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dw_bom
-- ----------------------------

-- ----------------------------
-- Table structure for dw_cus_infor
-- ----------------------------
DROP TABLE IF EXISTS `dw_cus_infor`;
CREATE TABLE `dw_cus_infor`  (
                                 `id` bigint(20) NOT NULL COMMENT '主键id',
                                 `cus_infor_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户资料名称',
                                 `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '社会统一信用代码',
                                 `cus_sort_id` bigint(20) NULL DEFAULT NULL COMMENT '客户分类id',
                                 `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
                                 `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                 `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                 `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                 `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                                 PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '客户资料' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dw_cus_infor
-- ----------------------------

-- ----------------------------
-- Table structure for dw_cus_person
-- ----------------------------
DROP TABLE IF EXISTS `dw_cus_person`;
CREATE TABLE `dw_cus_person`  (
                                  `id` bigint(20) NOT NULL COMMENT '主键id',
                                  `cus_infor_id` bigint(20) NULL DEFAULT NULL COMMENT '客户资料id',
                                  `cus_person` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系人',
                                  `cus_phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系电话',
                                  `maior_cus_person` int(1) NULL DEFAULT NULL COMMENT '主联系人',
                                  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
                                  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                  `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                  `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '客户联系人' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dw_cus_person
-- ----------------------------

-- ----------------------------
-- Table structure for dw_cus_sort
-- ----------------------------
DROP TABLE IF EXISTS `dw_cus_sort`;
CREATE TABLE `dw_cus_sort`  (
                                `id` bigint(20) NOT NULL COMMENT '主键id',
                                `pid` bigint(20) NULL DEFAULT NULL COMMENT '父类id',
                                `pids` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父类id集合',
                                `sort_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户分类名称',
                                `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
                                `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                                `update_user` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
                                PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '客户分类' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dw_cus_sort
-- ----------------------------

-- ----------------------------
-- Table structure for dw_custom_code
-- ----------------------------
DROP TABLE IF EXISTS `dw_custom_code`;
CREATE TABLE `dw_custom_code`  (
                                   `encode` varchar(6) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义编码',
                                   `time_format` int(1) NOT NULL COMMENT '时间选项',
                                   `serial_num` int(1) NOT NULL COMMENT '流水号位数',
                                   `table_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '目标表单名称',
                                   `id` bigint(20) NOT NULL COMMENT '主键id',
                                   `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                   `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                   `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                                   `update_user` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
                                   `table_re_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '表名称',
                                   `free_field` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自由字段',
                                   PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '编号配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dw_custom_code
-- ----------------------------
INSERT INTO `dw_custom_code` VALUES ('CP', 2, 3, 'dw_pro', 1542046206739779586, '2022-06-29 15:24:00', 1265476890672672808, '2022-08-18 13:40:25', 1265476890672672808, '产品', '');
INSERT INTO `dw_custom_code` VALUES ('CPT', 2, 3, 'dw_pro_type', 1542062774647451650, '2022-06-29 16:29:50', 1265476890672672808, '2022-08-18 13:40:30', 1265476890672672808, '产品类型', '');
INSERT INTO `dw_custom_code` VALUES ('GX', 2, 3, 'dw_work_step', 1542065983416532994, '2022-06-29 16:42:35', 1265476890672672808, '2022-08-18 13:40:36', 1265476890672672808, '工序', '');
INSERT INTO `dw_custom_code` VALUES ('GY', 2, 3, 'dw_work_route', 1542066297486016513, '2022-06-29 16:43:50', 1265476890672672808, '2022-08-18 13:40:41', 1265476890672672808, '工艺路线', '');
INSERT INTO `dw_custom_code` VALUES ('GD', 2, 3, 'dw_work_order', 1542068533796634626, '2022-06-29 16:52:43', 1265476890672672808, '2022-08-18 13:40:48', 1265476890672672808, '工单表', '');
INSERT INTO `dw_custom_code` VALUES ('RW', 2, 3, 'dw_task', 1542070134766567425, '2022-06-29 16:59:05', 1265476890672672808, '2022-08-18 13:40:52', 1265476890672672808, '任务表', '');
INSERT INTO `dw_custom_code` VALUES ('CKD', 2, 3, 'dw_inv', 1542400369382318081, '2022-06-30 14:51:19', 1265476890672672808, '2022-08-18 13:40:57', 1265476890672672808, '出库单', '');
INSERT INTO `dw_custom_code` VALUES ('RKD', 2, 3, ' dw_inv', 1542400415490301954, '2022-06-30 14:51:30', 1265476890672672808, '2022-08-18 13:41:03', 1265476890672672808, '入库单', '');
INSERT INTO `dw_custom_code` VALUES ('SC', 2, 4, 'dw_pro_plan', 1550020760306331649, '2022-07-21 15:32:02', 1265476890672672808, '2022-08-18 13:41:12', 1265476890672672808, '生产计划', '');
INSERT INTO `dw_custom_code` VALUES ('SX', 2, 3, 'dw_sale_order', 1552121260994740225, '2022-07-27 10:38:40', 1265476890672672808, '2022-08-18 13:41:16', 1265476890672672808, '销售订单', '');
INSERT INTO `dw_custom_code` VALUES ('CK', 2, 3, 'dw_ware_house', 1552845456764162050, '2022-07-29 10:36:22', 1265476890672672808, '2022-08-18 13:41:21', 1265476890672672808, '仓库管理', '');
INSERT INTO `dw_custom_code` VALUES ('CGDD', 2, 3, 'dw_puor_order', 1553930681875931137, '2022-08-01 10:28:40', 1265476890672672808, '2022-08-18 13:41:26', 1265476890672672808, '采购订单', '');
INSERT INTO `dw_custom_code` VALUES ('CGMX', 2, 3, 'dw_puor_detail', 1553931829898878978, '2022-08-01 10:33:14', 1265476890672672808, '2022-08-18 13:41:32', 1265476890672672808, '采购细明', '');
INSERT INTO `dw_custom_code` VALUES ('ASS', 2, 4, 'dw_ass', 1566978849076428802, '2022-09-06 10:37:25', 1265476890672672808, NULL, NULL, '装配工单', '');

-- ----------------------------
-- Table structure for dw_field_config
-- ----------------------------
DROP TABLE IF EXISTS `dw_field_config`;
CREATE TABLE `dw_field_config`  (
                                    `id` bigint(20) NOT NULL COMMENT '主键',
                                    `table_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表名称',
                                    `field_index` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段索引',
                                    `field_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段名称',
                                    `field_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段类型',
                                    `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
                                    `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                    `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建用户',
                                    `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                    `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新用户',
                                    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字段配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dw_field_config
-- ----------------------------

-- ----------------------------
-- Table structure for dw_inv
-- ----------------------------
DROP TABLE IF EXISTS `dw_inv`;
CREATE TABLE `dw_inv`  (
                           `id` bigint(20) NOT NULL COMMENT '主键id',
                           `code` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号，唯一性校验',
                           `category` int(11) NOT NULL COMMENT '-1为出库，1为入库',
                           `out_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '出库类型',
                           `in_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '入库类型',
                           `time` datetime(0) NOT NULL COMMENT '出入库时间',
                           `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                           `war_hou_id` bigint(20) NULL DEFAULT NULL COMMENT '仓库id',
                           `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                           `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                           `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                           `update_user` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
                           PRIMARY KEY (`id`) USING BTREE,
                           INDEX `code`(`code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '出入库表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dw_inv
-- ----------------------------

-- ----------------------------
-- Table structure for dw_inv_detail
-- ----------------------------
DROP TABLE IF EXISTS `dw_inv_detail`;
CREATE TABLE `dw_inv_detail`  (
                                  `id` bigint(20) NOT NULL COMMENT '主键id',
                                  `inv_id` bigint(20) NOT NULL COMMENT '库单ID',
                                  `pro_id` bigint(20) NOT NULL COMMENT '产品ID',
                                  `war_hou_id` bigint(20) NULL DEFAULT NULL COMMENT '仓库id',
                                  `num` bigint(20) NOT NULL COMMENT '数量',
                                  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                  `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                                  `update_user` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
                                  PRIMARY KEY (`id`) USING BTREE,
                                  INDEX `inv_id_code`(`inv_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '出入库明细' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dw_inv_detail
-- ----------------------------

-- ----------------------------
-- Table structure for dw_pro
-- ----------------------------
DROP TABLE IF EXISTS `dw_pro`;
CREATE TABLE `dw_pro`  (
                           `id` bigint(20) NOT NULL COMMENT '主键',
                           `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '产品名称',
                           `code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '产品编码',
                           `pro_type_id` bigint(20) NULL DEFAULT NULL COMMENT '产品类型',
                           `remarks` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
                           `unit` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '库存位置',
                           `josn` json NULL COMMENT '配置字段值',
                           `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图片',
                           `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                           `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                           `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                           `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                           `work_route_id` bigint(20) NULL DEFAULT NULL COMMENT '工艺路线id',
                           PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '产品' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dw_pro
-- ----------------------------

-- ----------------------------
-- Table structure for dw_pro_model
-- ----------------------------
DROP TABLE IF EXISTS `dw_pro_model`;
CREATE TABLE `dw_pro_model`  (
                                 `id` bigint(20) NOT NULL COMMENT '产品型号主键',
                                 `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品型号名称',
                                 `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '产品型号编码',
                                 `pro_id` bigint(20) NULL DEFAULT NULL COMMENT '产品ID',
                                 `unit` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '库存单位',
                                 `pro_type_id` bigint(20) NULL DEFAULT NULL COMMENT '产品类型ID',
                                 `remarks` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                                 `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                 `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                 `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                 `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                                 PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '产品型号' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dw_pro_model
-- ----------------------------

-- ----------------------------
-- Table structure for dw_pro_plan
-- ----------------------------
DROP TABLE IF EXISTS `dw_pro_plan`;
CREATE TABLE `dw_pro_plan`  (
                                `id` bigint(20) NOT NULL COMMENT '主键id',
                                `code` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号，唯一性校验',
                                `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                                `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                                `update_user` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
                                PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '生产计划' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dw_pro_plan
-- ----------------------------

-- ----------------------------
-- Table structure for dw_pro_type
-- ----------------------------
DROP TABLE IF EXISTS `dw_pro_type`;
CREATE TABLE `dw_pro_type`  (
                                `id` bigint(20) NOT NULL COMMENT '主键',
                                `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
                                `code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '编码',
                                `pid` bigint(20) NULL DEFAULT NULL COMMENT '父级',
                                `pids` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所有父级',
                                `remarks` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
                                `sort` int(11) NULL DEFAULT NULL COMMENT '排序号',
                                `josn` json NULL COMMENT '配置字段值',
                                `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                                PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '产品类型' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dw_pro_type
-- ----------------------------

-- ----------------------------
-- Table structure for dw_puor_detail
-- ----------------------------
DROP TABLE IF EXISTS `dw_puor_detail`;
CREATE TABLE `dw_puor_detail`  (
                                   `id` bigint(20) NOT NULL COMMENT '主键',
                                   `purchase_order_id` bigint(20) NOT NULL COMMENT '采购订单id',
                                   `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '采购明细编号',
                                   `pro_id` bigint(20) NOT NULL COMMENT '产品id',
                                   `purchase_num` int(11) NOT NULL COMMENT '数量',
                                   `unit_price` double(10, 2) NOT NULL COMMENT '单价',
  `total_price` double(10, 2) NULL DEFAULT NULL COMMENT '总价',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '采购细明' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dw_puor_detail
-- ----------------------------

-- ----------------------------
-- Table structure for dw_puor_order
-- ----------------------------
DROP TABLE IF EXISTS `dw_puor_order`;
CREATE TABLE `dw_puor_order`  (
                                  `id` bigint(20) NOT NULL COMMENT '主键',
                                  `code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '采购订单编码',
                                  `arrival_time` datetime(0) NOT NULL COMMENT '到货时间',
                                  `order_source` bigint(20) NOT NULL COMMENT '订单来源',
                                  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                                  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                  `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                  `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '采购订单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dw_puor_order
-- ----------------------------

-- ----------------------------
-- Table structure for dw_sale_order
-- ----------------------------
DROP TABLE IF EXISTS `dw_sale_order`;
CREATE TABLE `dw_sale_order`  (
                                  `id` bigint(20) NOT NULL COMMENT '主键id',
                                  `code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '订单编号',
                                  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                  `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                  `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                                  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '销售订单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dw_sale_order
-- ----------------------------

-- ----------------------------
-- Table structure for dw_stock_balance
-- ----------------------------
DROP TABLE IF EXISTS `dw_stock_balance`;
CREATE TABLE `dw_stock_balance`  (
                                     `id` bigint(20) NOT NULL COMMENT '主键id',
                                     `war_hou_id` bigint(20) NULL DEFAULT NULL COMMENT '仓库id',
                                     `pro_id` bigint(20) NULL DEFAULT NULL COMMENT '产品id',
                                     `sto_num` bigint(20) NULL DEFAULT NULL COMMENT '库存数量',
                                     `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                     `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                     `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                     `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                                     PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '库存余额' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dw_stock_balance
-- ----------------------------

-- ----------------------------
-- Table structure for dw_supp_data
-- ----------------------------
DROP TABLE IF EXISTS `dw_supp_data`;
CREATE TABLE `dw_supp_data`  (
                                 `id` bigint(20) NOT NULL COMMENT '主键id',
                                 `supp_data_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '供应商资料名称',
                                 `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '社会统一信用代码',
                                 `supp_sort_id` bigint(20) NULL DEFAULT NULL COMMENT '供应商分类id',
                                 `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                                 `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                 `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                 `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                 `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                                 PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = ' 供应商资料' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dw_supp_data
-- ----------------------------

-- ----------------------------
-- Table structure for dw_supp_person
-- ----------------------------
DROP TABLE IF EXISTS `dw_supp_person`;
CREATE TABLE `dw_supp_person`  (
                                   `id` bigint(20) NOT NULL COMMENT '主键id',
                                   `supp_data_id` bigint(20) NULL DEFAULT NULL COMMENT '供应商资料id',
                                   `supp_person` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系人',
                                   `supp_phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系人电话',
                                   `chief_supp_person` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '主联系人',
                                   `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                                   `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                   `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                   `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                   `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                                   PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '供应商联系人' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dw_supp_person
-- ----------------------------

-- ----------------------------
-- Table structure for dw_supp_sort
-- ----------------------------
DROP TABLE IF EXISTS `dw_supp_sort`;
CREATE TABLE `dw_supp_sort`  (
                                 `id` bigint(20) NOT NULL COMMENT '主键id',
                                 `pid` bigint(20) NULL DEFAULT NULL COMMENT '父类id',
                                 `pids` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父类id集合',
                                 `sort_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '供应商分类名称',
                                 `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                                 `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                 `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                 `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                 `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                                 PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '供应商分类' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dw_supp_sort
-- ----------------------------

-- ----------------------------
-- Table structure for dw_task
-- ----------------------------
DROP TABLE IF EXISTS `dw_task`;
CREATE TABLE `dw_task`  (
                            `id` bigint(20) NOT NULL COMMENT 'id',
                            `sort_num` int(2) NULL DEFAULT NULL COMMENT '序号',
                            `code` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '编码',
                            `work_order_id` bigint(20) NULL DEFAULT NULL COMMENT '工单id',
                            `pro_id` bigint(20) NULL DEFAULT NULL COMMENT '产品id',
                            `pro_type_id` bigint(20) NULL DEFAULT NULL COMMENT '产品类型id',
                            `work_step_id` bigint(20) NULL DEFAULT NULL COMMENT '工序id',
                            `good_num` int(20) NULL DEFAULT NULL COMMENT '良品数',
                            `bad_num` int(20) NULL DEFAULT NULL COMMENT '不良品数',
                            `pla_num` int(20) NULL DEFAULT NULL COMMENT '计划数',
                            `pla_start_time` datetime(0) NULL DEFAULT NULL COMMENT '计划开始时间',
                            `pla_end_time` datetime(0) NULL DEFAULT NULL COMMENT '计划结束时间',
                            `fact_sta_time` datetime(0) NULL DEFAULT NULL COMMENT '实际开始时间',
                            `fact_end_time` datetime(0) NULL DEFAULT NULL COMMENT '实际结束时间',
                            `status` int(20) NULL DEFAULT NULL COMMENT '状态(未开始:-1;执行中:0;已结束:1;)',
                            `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                            `create_user` bigint(20) NULL DEFAULT NULL,
                            `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                            `update_user` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
                            `report_right` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '报工权限',
                            PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '任务表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dw_task
-- ----------------------------

-- ----------------------------
-- Table structure for dw_ware_house
-- ----------------------------
DROP TABLE IF EXISTS `dw_ware_house`;
CREATE TABLE `dw_ware_house`  (
                                  `id` bigint(20) NOT NULL COMMENT '主键id',
                                  `pid` bigint(20) NULL DEFAULT NULL COMMENT '父类id',
                                  `pids` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父类id集合',
                                  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '编号',
                                  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
                                  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                                  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                  `create_user` bigint(20) NULL DEFAULT NULL,
                                  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                                  `update_user` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '仓库管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dw_ware_house
-- ----------------------------

-- ----------------------------
-- Table structure for dw_work_order
-- ----------------------------
DROP TABLE IF EXISTS `dw_work_order`;
CREATE TABLE `dw_work_order`  (
                                  `id` bigint(20) NOT NULL COMMENT '主键id',
                                  `type` int(2) NULL DEFAULT NULL COMMENT '工单类型：1顺序工单；2流程工单',
                                  `work_order_no` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '工单编号',
                                  `pla_num` int(11) NULL DEFAULT NULL COMMENT '计划数',
                                  `pla_start_time` datetime(0) NULL DEFAULT NULL COMMENT '计划开始时间',
                                  `pla_end_time` datetime(0) NULL DEFAULT NULL COMMENT '计划结束时间',
                                  `fact_sta_time` datetime(0) NULL DEFAULT NULL COMMENT '实际开始时间',
                                  `fact_end_time` datetime(0) NULL DEFAULT NULL COMMENT '实际结束时间',
                                  `pro_id` bigint(20) NULL DEFAULT NULL COMMENT '产品id',
                                  `pro_model_id` bigint(20) NULL DEFAULT NULL COMMENT '产品型号id',
                                  `pro_type_id` bigint(20) NULL DEFAULT NULL COMMENT '产品类型id',
                                  `bill_id` bigint(20) UNSIGNED ZEROFILL NULL DEFAULT NULL COMMENT '关联单据id',
                                  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                                  `status` int(11) NULL DEFAULT NULL COMMENT '状态(未开始:-1;执行中:0;已结束:1;已取消:-2)',
                                  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                  `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                  `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                                  `next_person` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT '下单人',
                                  `order_time` datetime(0) NULL DEFAULT NULL COMMENT '下单时间',
                                  `person_charge` bigint(20) UNSIGNED ZEROFILL NULL DEFAULT NULL COMMENT '负责人',
                                  `enclosure` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附件',
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '工单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dw_work_order
-- ----------------------------

-- ----------------------------
-- Table structure for dw_work_order_bill
-- ----------------------------
DROP TABLE IF EXISTS `dw_work_order_bill`;
CREATE TABLE `dw_work_order_bill`  (
                                       `id` bigint(20) NOT NULL COMMENT '主键id',
                                       `type` bigint(1) NULL DEFAULT NULL COMMENT '单据类型：1：销售订单  2：生产计划 3：装配工单',
                                       `bill_id` bigint(20) NOT NULL COMMENT '单据ID',
                                       `pro_id` bigint(20) NOT NULL COMMENT '产品id',
                                       `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                       `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                       `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                                       `update_user` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
                                       PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '工单单据关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dw_work_order_bill
-- ----------------------------

-- ----------------------------
-- Table structure for dw_work_order_task
-- ----------------------------
DROP TABLE IF EXISTS `dw_work_order_task`;
CREATE TABLE `dw_work_order_task`  (
                                       `id` bigint(20) NOT NULL COMMENT '主键id',
                                       `work_order_id` bigint(20) NULL DEFAULT NULL COMMENT '工单id',
                                       `source_task_id` bigint(20) NULL DEFAULT NULL COMMENT '源节点任务',
                                       `target_task_id` bigint(20) NULL DEFAULT NULL COMMENT '目标节点任务',
                                       `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                       `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                       `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                       `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                                       PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '工单任务关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dw_work_order_task
-- ----------------------------

-- ----------------------------
-- Table structure for dw_work_report
-- ----------------------------
DROP TABLE IF EXISTS `dw_work_report`;
CREATE TABLE `dw_work_report`  (
                                   `id` bigint(20) NOT NULL COMMENT '报工表主键Id',
                                   `work_order_id` bigint(20) NULL DEFAULT NULL COMMENT '工单id',
                                   `task_id` bigint(20) NULL DEFAULT NULL COMMENT '任务id',
                                   `production_user` bigint(20) NULL DEFAULT NULL COMMENT '生产人员',
                                   `report_num` int(20) NULL DEFAULT NULL COMMENT '报工数',
                                   `good_num` int(20) NULL DEFAULT NULL COMMENT '良品数',
                                   `bad_num` int(20) NULL DEFAULT NULL COMMENT '不良品数',
                                   `start_time` datetime(0) NULL DEFAULT NULL COMMENT '开始时间',
                                   `end_time` datetime(0) NULL DEFAULT NULL COMMENT '结束时间',
                                   `work_time` int(11) NULL DEFAULT NULL COMMENT '工作时长',
                                   `distinction` int(1) NULL DEFAULT NULL COMMENT '区分该报工从哪里添加(1为从报工添加,0为从任务添加)',
                                   `approval` int(1) NULL DEFAULT NULL COMMENT '审批(1为已审批,0为未审批)',
                                   `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
                                   `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                   `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                   `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                                   `update_user` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
                                   PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '报工' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dw_work_report
-- ----------------------------

-- ----------------------------
-- Table structure for dw_work_route
-- ----------------------------
DROP TABLE IF EXISTS `dw_work_route`;
CREATE TABLE `dw_work_route`  (
                                  `id` bigint(20) NOT NULL COMMENT 'id',
                                  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编码',
                                  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '工艺路线名称',
                                  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                                  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                  `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                                  `update_user` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '工艺路线' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dw_work_route
-- ----------------------------

-- ----------------------------
-- Table structure for dw_work_step
-- ----------------------------
DROP TABLE IF EXISTS `dw_work_step`;
CREATE TABLE `dw_work_step`  (
                                 `id` bigint(20) NOT NULL COMMENT 'id\r\n',
                                 `code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '工序编号',
                                 `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '工序名称',
                                 `mul_bad_item` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '不良品项列表（多选）',
                                 `josn` json NULL COMMENT '配置字段值',
                                 `remarks` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
                                 `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                 `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                 `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                 `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                                 `report_right` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '报工权限',
                                 PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '工序' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dw_work_step
-- ----------------------------

-- ----------------------------
-- Table structure for dw_work_step_route
-- ----------------------------
DROP TABLE IF EXISTS `dw_work_step_route`;
CREATE TABLE `dw_work_step_route`  (
                                       `id` bigint(20) NOT NULL COMMENT '工序路线关系表主键id',
                                       `work_route_Id` bigint(20) NULL DEFAULT NULL COMMENT '工艺路线id',
                                       `work_step_Id` bigint(20) NULL DEFAULT NULL COMMENT '工序id',
                                       `sort_num` int(11) NULL DEFAULT NULL COMMENT '排序号',
                                       `source` bigint(20) NULL DEFAULT NULL COMMENT '源节点',
                                       `target` bigint(20) NULL DEFAULT NULL COMMENT '目标节点',
                                       `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                       `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                       `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                                       `update_user` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
                                       PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '工序路线关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dw_work_step_route
-- ----------------------------

-- ----------------------------
-- Table structure for sys_app
-- ----------------------------
DROP TABLE IF EXISTS `sys_app`;
CREATE TABLE `sys_app`  (
                            `id` bigint(20) NOT NULL COMMENT '主键id',
                            `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '应用名称',
                            `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '编码',
                            `active` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否默认激活（Y-是，N-否）',
                            `status` tinyint(4) NOT NULL COMMENT '状态（字典 0正常 1停用 2删除）',
                            `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                            `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                            `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                            `update_user` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
                            PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统应用表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_app
-- ----------------------------
INSERT INTO `sys_app` VALUES (1265476890672672821, '系统应用', 'system', 'N', 0, '2029-12-30 19:07:00', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_app` VALUES (1342445032647098369, '系统工具', 'system_tool', 'N', 0, '2030-01-01 20:20:12', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_app` VALUES (1527495028319117314, '生产管理', 'manufacturing', 'N', 0, '2022-05-20 11:42:49', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_app` VALUES (1549011192304033793, '客户关系', 'crm', 'N', 0, '2019-07-18 20:40:22', 1265476890672672808, '2022-07-18 21:32:40', 1265476890672672808);
INSERT INTO `sys_app` VALUES (1549019665616003074, '协同办公', 'teamwork', 'Y', 0, '2019-06-18 21:14:02', 1265476890672672808, '2022-07-18 21:25:01', 1265476890672672808);
INSERT INTO `sys_app` VALUES (1549021450137165826, '人力资源', 'hr', 'N', 0, '2022-07-18 21:21:08', 1265476890672672808, '2022-07-18 21:24:09', 1265476890672672808);
INSERT INTO `sys_app` VALUES (1557615934372339714, '高级体验', 'ht', 'N', 2, '2022-08-11 14:32:33', 1265476890672672808, '2022-09-15 10:09:47', 1265476890672672808);

-- ----------------------------
-- Table structure for sys_area
-- ----------------------------
DROP TABLE IF EXISTS `sys_area`;
CREATE TABLE `sys_area`  (
                             `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
                             `level_code` tinyint(3) UNSIGNED NULL DEFAULT NULL COMMENT '层级',
                             `parent_code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '父级行政代码',
                             `area_code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '行政代码',
                             `zip_code` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '邮政编码',
                             `city_code` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '区号',
                             `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '名称',
                             `short_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '简称',
                             `merger_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '组合名',
                             `pinyin` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '拼音',
                             `lng` decimal(10, 6) NULL DEFAULT NULL COMMENT '经度',
                             `lat` decimal(10, 6) NULL DEFAULT NULL COMMENT '纬度',
                             PRIMARY KEY (`id`) USING BTREE,
                             UNIQUE INDEX `uk_code`(`area_code`) USING BTREE,
                             INDEX `idx_parent_code`(`parent_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '中国行政地区表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_area
-- ----------------------------

-- ----------------------------
-- Table structure for sys_code_generate
-- ----------------------------
DROP TABLE IF EXISTS `sys_code_generate`;
CREATE TABLE `sys_code_generate`  (
                                      `id` bigint(20) NOT NULL COMMENT '主键',
                                      `author_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '作者姓名',
                                      `class_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '类名',
                                      `table_prefix` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '是否移除表前缀',
                                      `generate_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '生成位置类型',
                                      `table_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '数据库表名',
                                      `package_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '包名称',
                                      `bus_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '业务名',
                                      `table_comment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '功能名',
                                      `app_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所属应用',
                                      `menu_pid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单上级',
                                      `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                      `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                      `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                                      `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                      PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成基础配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_code_generate
-- ----------------------------

-- ----------------------------
-- Table structure for sys_code_generate_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_code_generate_config`;
CREATE TABLE `sys_code_generate_config`  (
                                             `id` bigint(20) NOT NULL COMMENT '主键',
                                             `code_gen_id` bigint(20) NULL DEFAULT NULL COMMENT '代码生成主表ID',
                                             `column_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '数据库字段名',
                                             `java_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'java类字段名',
                                             `data_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '物理类型',
                                             `column_comment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字段描述',
                                             `java_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'java类型',
                                             `effect_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '作用类型（字典）',
                                             `dict_type_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字典code',
                                             `whether_table` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列表展示',
                                             `whether_add_update` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '增改',
                                             `whether_retract` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列表是否缩进（字典）',
                                             `whether_required` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否必填（字典）',
                                             `query_whether` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否是查询条件',
                                             `query_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '查询方式',
                                             `column_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '主键',
                                             `column_key_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '主外键名称',
                                             `whether_common` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否是通用字段',
                                             `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                             `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                             `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                                             `update_user` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
                                             PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成详细配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_code_generate_config
-- ----------------------------
INSERT INTO `sys_code_generate_config` VALUES (1527495983397613569, 1527495981841526785, 'code', 'code', 'varchar', '产品编码', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'Code', 'N', '2022-05-20 11:46:37', 1265476890672672808, '2022-05-20 11:50:27', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527495984014176258, 1527495981841526785, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-05-20 11:46:37', 1265476890672672808, '2022-05-20 11:50:27', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527495984668487682, 1527495981841526785, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-05-20 11:46:37', 1265476890672672808, '2022-05-20 11:50:27', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527495985381519361, 1527495981841526785, 'id', 'id', 'bigint', '主键', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-05-20 11:46:37', 1265476890672672808, '2022-05-20 11:50:27', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527495986098745346, 1527495981841526785, 'name', 'name', 'varchar', '产品名称', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'Name', 'N', '2022-05-20 11:46:37', 1265476890672672808, '2022-05-20 11:50:27', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527495986753056769, 1527495981841526785, 'pro_type_id', 'proTypeId', 'bigint', '产品类型', 'Long', 'select', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'ProTypeId', 'N', '2022-05-20 11:46:37', 1265476890672672808, '2022-05-20 11:50:27', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527495987461894145, 1527495981841526785, 'remarks', 'remarks', 'varchar', '备注', 'String', 'input', NULL, 'Y', 'Y', 'N', 'N', 'Y', 'like', '', 'Remarks', 'N', '2022-05-20 11:46:38', 1265476890672672808, '2022-05-20 11:50:28', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527495988116205570, 1527495981841526785, 'update_time', 'updateTime', 'datetime', '更新时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-05-20 11:46:38', 1265476890672672808, '2022-05-20 11:50:28', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527495988757934082, 1527495981841526785, 'update_user', 'updateUser', 'bigint', '更新人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-05-20 11:46:38', 1265476890672672808, '2022-05-20 11:50:28', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527498140268146689, 1527498138657533954, 'code', 'code', 'varchar', '编码', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'Code', 'N', '2022-05-20 11:55:11', 1265476890672672808, '2022-05-20 14:59:36', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527498140905680897, 1527498138657533954, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-05-20 11:55:11', 1265476890672672808, '2022-05-20 14:59:36', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527498141547409410, 1527498138657533954, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-05-20 11:55:11', 1265476890672672808, '2022-05-20 14:59:36', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527498142184943617, 1527498138657533954, 'id', 'id', 'bigint', '主键', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-05-20 11:55:11', 1265476890672672808, '2022-05-20 14:59:37', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527498142818283522, 1527498138657533954, 'name', 'name', 'varchar', '名称', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'Name', 'N', '2022-05-20 11:55:11', 1265476890672672808, '2022-05-20 14:59:37', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527498143460012034, 1527498138657533954, 'pro_id', 'proId', 'bigint', '产品型号ID', 'Long', 'select', 'code_gen_query_type', 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'ProId', 'N', '2022-05-20 11:55:12', 1265476890672672808, '2022-05-20 14:59:37', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527498144097546241, 1527498138657533954, 'remarks', 'remarks', 'varchar', '备注', 'String', 'textarea', NULL, 'Y', 'Y', 'N', 'N', 'Y', 'like', '', 'Remarks', 'N', '2022-05-20 11:55:12', 1265476890672672808, '2022-05-20 14:59:37', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527498144739274753, 1527498138657533954, 'update_time', 'updateTime', 'datetime', '更新时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-05-20 11:55:12', 1265476890672672808, '2022-05-20 14:59:37', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527498145318088706, 1527498138657533954, 'update_user', 'updateUser', 'bigint', '更新人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-05-20 11:55:12', 1265476890672672808, '2022-05-20 14:59:37', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527499129060777986, 1527499124988108802, 'code', 'code', 'varchar', '编码', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Code', 'N', '2022-05-20 11:59:07', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1527499129757032450, 1527499124988108802, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-05-20 11:59:07', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1527499130465869826, 1527499124988108802, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-05-20 11:59:07', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1527499131170512898, 1527499124988108802, 'id', 'id', 'bigint', '主键', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-05-20 11:59:07', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1527499131891933186, 1527499124988108802, 'pro_model_id', 'proModelId', 'bigint', '产品型号id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'ProModelId', 'N', '2022-05-20 11:59:07', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1527499132852428801, 1527499124988108802, 'update_time', 'updateTime', 'datetime', '修改时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-05-20 11:59:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1527499133573849089, 1527499124988108802, 'update_user', 'updateUser', 'bigint', '修改人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-05-20 11:59:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1527523767136477186, 1527523765366480897, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-05-20 13:37:01', 1265476890672672808, '2022-05-20 13:37:39', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527523767841120258, 1527523765366480897, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-05-20 13:37:01', 1265476890672672808, '2022-05-20 13:37:40', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527523768533180418, 1527523765366480897, 'id', 'id', 'bigint', '主键', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-05-20 13:37:01', 1265476890672672808, '2022-05-20 13:37:40', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527523769187491842, 1527523765366480897, 'pro_inst_id', 'proInstId', 'bigint', '产品实例ID', 'Long', 'select', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'ProInstId', 'N', '2022-05-20 13:37:01', 1265476890672672808, '2022-05-20 13:37:40', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527523769896329218, 1527523765366480897, 'pro_step_id', 'proStepId', 'bigint', '产品步骤ID', 'Long', 'select', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'ProStepId', 'N', '2022-05-20 13:37:01', 1265476890672672808, '2022-05-20 13:37:40', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527523770613555202, 1527523765366480897, 'update_time', 'updateTime', 'datetime', '更新时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-05-20 13:37:02', 1265476890672672808, '2022-05-20 13:37:40', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527523771330781186, 1527523765366480897, 'update_user', 'updateUser', 'bigint', '修改人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-05-20 13:37:02', 1265476890672672808, '2022-05-20 13:37:40', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527528596401373185, 1527528594438438913, 'code', 'code', 'varchar', '编码', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'Code', 'N', '2022-05-20 13:56:12', 1265476890672672808, '2022-05-20 14:01:34', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527528597173125121, 1527528594438438913, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-05-20 13:56:12', 1265476890672672808, '2022-05-20 14:01:34', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527528598083289090, 1527528594438438913, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-05-20 13:56:13', 1265476890672672808, '2022-05-20 14:01:34', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527528598800515073, 1527528594438438913, 'id', 'id', 'bigint', '主键', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-05-20 13:56:13', 1265476890672672808, '2022-05-20 14:01:35', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527528599580655617, 1527528594438438913, 'name', 'name', 'varchar', '名称', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'Name', 'N', '2022-05-20 13:56:13', 1265476890672672808, '2022-05-20 14:01:35', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527528600302075905, 1527528594438438913, 'pid', 'pid', 'bigint', '父级', 'Long', 'select', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Pid', 'N', '2022-05-20 13:56:13', 1265476890672672808, '2022-05-20 14:01:35', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527528601015107585, 1527528594438438913, 'pids', 'pids', 'varchar', '所有父级', 'String', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'like', '', 'Pids', 'N', '2022-05-20 13:56:13', 1265476890672672808, '2022-05-20 14:01:35', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527528601795248129, 1527528594438438913, 'remarks', 'remarks', 'varchar', '备注', 'String', 'input', NULL, 'Y', 'Y', 'N', 'N', 'Y', 'like', '', 'Remarks', 'N', '2022-05-20 13:56:13', 1265476890672672808, '2022-05-20 14:01:35', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527528602516668417, 1527528594438438913, 'update_time', 'updateTime', 'datetime', '更新时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-05-20 13:56:14', 1265476890672672808, '2022-05-20 14:01:35', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527528603233894402, 1527528594438438913, 'update_user', 'updateUser', 'bigint', '更新人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-05-20 13:56:14', 1265476890672672808, '2022-05-20 14:01:35', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527560391012384769, 1527560389183668225, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-05-20 16:02:33', 1265476890672672808, '2022-05-20 16:08:10', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527560391675084802, 1527560389183668225, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-05-20 16:02:33', 1265476890672672808, '2022-05-20 16:08:10', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527560392312619010, 1527560389183668225, 'id', 'id', 'bigint', '主键', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-05-20 16:02:33', 1265476890672672808, '2022-05-20 16:08:10', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527560392971124738, 1527560389183668225, 'name', 'name', 'varchar', '步骤名称', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'Name', 'N', '2022-05-20 16:02:33', 1265476890672672808, '2022-05-20 16:08:10', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527560393642213377, 1527560389183668225, 'pro_model_id', 'proModelId', 'bigint', '产品型号id', 'Long', 'select', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'ProModelId', 'N', '2022-05-20 16:02:33', 1265476890672672808, '2022-05-20 16:08:10', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527560394292330497, 1527560389183668225, 'remarks', 'remarks', 'varchar', '备注', 'String', 'input', NULL, 'Y', 'Y', 'N', 'N', 'N', 'eq', '', 'Remarks', 'N', '2022-05-20 16:02:33', 1265476890672672808, '2022-05-20 16:08:10', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527560394967613442, 1527560389183668225, 'update_time', 'updateTime', 'datetime', '更新时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-05-20 16:02:34', 1265476890672672808, '2022-05-20 16:08:10', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527560395592564738, 1527560389183668225, 'update_user', 'updateUser', 'bigint', '更新人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-05-20 16:02:34', 1265476890672672808, '2022-05-20 16:08:11', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528914713331683330, 1528914711771402242, 'code', 'code', 'bigint', '不良品编号', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Code', 'N', '2022-05-24 09:44:08', 1265476890672672808, '2022-05-24 10:05:27', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528914713939857410, 1528914711771402242, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-05-24 09:44:08', 1265476890672672808, '2022-05-24 10:05:28', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528914714556420098, 1528914711771402242, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-05-24 09:44:08', 1265476890672672808, '2022-05-24 10:05:28', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528914715164594177, 1528914711771402242, 'id', 'id', 'bigint', '不良品项主键id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-05-24 09:44:09', 1265476890672672808, '2022-05-24 10:05:28', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528914715776962562, 1528914711771402242, 'name', 'name', 'varchar', '不良品名称', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'Name', 'N', '2022-05-24 09:44:09', 1265476890672672808, '2022-05-24 10:05:28', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528914716406108162, 1528914711771402242, 'remarks', 'remarks', 'varchar', '备注', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Remarks', 'N', '2022-05-24 09:44:09', 1265476890672672808, '2022-05-24 10:05:28', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528914717014282241, 1528914711771402242, 'update_time', 'updateTime', 'datetime', '更新时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-05-24 09:44:09', 1265476890672672808, '2022-05-24 10:05:28', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528914717630844930, 1528914711771402242, 'update_user', 'updateUser', 'bigint', '更新人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-05-24 09:44:09', 1265476890672672808, '2022-05-24 10:05:28', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528918378670989314, 1528918376926158850, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-05-24 09:58:42', 1265476890672672808, '2022-05-24 10:02:12', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528918379371438082, 1528918376926158850, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-05-24 09:58:42', 1265476890672672808, '2022-05-24 10:02:12', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528918380038332417, 1528918376926158850, 'id', 'id', 'bigint', 'id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-05-24 09:58:42', 1265476890672672808, '2022-05-24 10:02:12', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528918380709421058, 1528918376926158850, 'name', 'name', 'varchar', '产品单位名称', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'Name', 'N', '2022-05-24 09:58:43', 1265476890672672808, '2022-05-24 10:02:12', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528918381405675521, 1528918376926158850, 'remarks', 'remarks', 'varchar', '产品单位备注', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Remarks', 'N', '2022-05-24 09:58:43', 1265476890672672808, '2022-05-24 10:02:12', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528918382047404033, 1528918376926158850, 'update_time', 'updateTime', 'datetime', '修改时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-05-24 09:58:43', 1265476890672672808, '2022-05-24 10:02:12', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528918382747852801, 1528918376926158850, 'update_user', 'updateUser', 'bigint', '修改人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-05-24 09:58:43', 1265476890672672808, '2022-05-24 10:02:13', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528923116980441089, 1528923115369828353, 'code', 'code', 'varchar', '工序编号', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Code', 'N', '2022-05-24 10:17:32', 1265476890672672808, '2022-05-24 10:24:15', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528923117617975298, 1528923115369828353, 'id', 'id', 'int', 'id\r\n', 'Integer', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-05-24 10:17:32', 1265476890672672808, '2022-05-24 10:24:15', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528923118528139265, 1528923115369828353, 'mul_bad_item', 'mulBadItem', 'varchar', '不良品项列表（多选）', 'String', 'checkbox', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'MulBadItem', 'N', '2022-05-24 10:17:32', 1265476890672672808, '2022-05-24 10:24:15', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528923119199227905, 1528923115369828353, 'name', 'name', 'varchar', '工序名称', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'Name', 'N', '2022-05-24 10:17:32', 1265476890672672808, '2022-05-24 10:24:15', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528923119845150721, 1528923115369828353, 'remarks', 'remarks', 'varchar', '备注', 'String', 'textarea', NULL, 'Y', 'Y', 'N', 'N', 'N', 'eq', '', 'Remarks', 'N', '2022-05-24 10:17:32', 1265476890672672808, '2022-05-24 10:24:16', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1534372927078776834, 1534372926302830594, 'id', 'id', 'bigint', '报工表主键Id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-06-08 11:13:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534372927376572418, 1534372926302830594, 'work_order_id', 'workOrderId', 'bigint', '工单id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'WorkOrderId', 'N', '2022-06-08 11:13:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534372927527567362, 1534372926302830594, 'task_id', 'taskId', 'bigint', '工序id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'TaskId', 'N', '2022-06-08 11:13:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534372927917637633, 1534372926302830594, 'step_status', 'stepStatus', 'int', '工序状态', 'Integer', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'StepStatus', 'N', '2022-06-08 11:13:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534372928165101569, 1534372926302830594, 'production_name', 'productionName', 'varchar', '生产人员', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'ProductionName', 'N', '2022-06-08 11:13:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534372928462897154, 1534372926302830594, 'report_num', 'reportNum', 'bigint', '报工数', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'ReportNum', 'N', '2022-06-08 11:13:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534372930614575106, 1534372926302830594, 'good_num', 'goodNum', 'bigint', '良品数', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'GoodNum', 'N', '2022-06-08 11:13:09', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534372930857844737, 1534372926302830594, 'bad_num', 'badNum', 'bigint', '不良品数', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'BadNum', 'N', '2022-06-08 11:13:09', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534372931063365633, 1534372926302830594, 'start_time', 'startTime', 'datetime', '开始时间', 'Date', 'datepicker', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'StartTime', 'N', '2022-06-08 11:13:09', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534372931344384001, 1534372926302830594, 'end_time', 'endTime', 'datetime', '结束时间', 'Date', 'datepicker', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'EndTime', 'N', '2022-06-08 11:13:09', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534372931486990337, 1534372926302830594, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-06-08 11:13:09', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534372931759620097, 1534372926302830594, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-06-08 11:13:09', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534372931919003649, 1534372926302830594, 'update_time', 'updateTime', 'datetime', '修改时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-06-08 11:13:09', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534372932187439106, 1534372926302830594, 'update_user', 'updateUser', 'bigint', '修改人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-06-08 11:13:09', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534373506890862594, 1534373506437877761, 'id', 'id', 'bigint', 'id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-06-08 11:15:26', 1265476890672672808, '2022-06-08 11:16:08', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1534373506983137281, 1534373506437877761, 'code', 'code', 'varchar', '编码', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Code', 'N', '2022-06-08 11:15:26', 1265476890672672808, '2022-06-08 11:16:08', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1534373507293515778, 1534373506437877761, 'name', 'name', 'varchar', '工艺路线名称', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Name', 'N', '2022-06-08 11:15:26', 1265476890672672808, '2022-06-08 11:16:08', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1534373507616477185, 1534373506437877761, 'remarks', 'remarks', 'varchar', '备注', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'Remarks', 'N', '2022-06-08 11:15:26', 1265476890672672808, '2022-06-08 11:16:08', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1534373507683586050, 1534373506437877761, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-06-08 11:15:26', 1265476890672672808, '2022-06-08 11:16:08', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1534373507826192386, 1534373506437877761, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-06-08 11:15:26', 1265476890672672808, '2022-06-08 11:16:08', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1534373508207874049, 1534373506437877761, 'update_time', 'updateTime', 'datetime', '修改时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-06-08 11:15:26', 1265476890672672808, '2022-06-08 11:16:08', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1534373508337897474, 1534373506437877761, 'update_user', 'updateUser', 'bigint', '修改人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-06-08 11:15:26', 1265476890672672808, '2022-06-08 11:16:08', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1534376273143435265, 1534376271981613057, 'id', 'id', 'bigint', '工序路线关系表主键id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-06-08 11:26:26', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534376273642557442, 1534376271981613057, 'work_route_Id', 'workRouteId', 'bigint', '工艺路线id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'WorkRouteId', 'N', '2022-06-08 11:26:26', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534376273759997953, 1534376271981613057, 'source', 'source', 'bigint', '源节点', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Source', 'N', '2022-06-08 11:26:26', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534376274003267585, 1534376271981613057, 'target', 'target', 'bigint', '目标节点', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Target', 'N', '2022-06-08 11:26:26', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534376274187816962, 1534376271981613057, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-06-08 11:26:26', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534376274431086593, 1534376271981613057, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-06-08 11:26:26', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534376274590470145, 1534376271981613057, 'update_time', 'updateTime', 'datetime', '修改时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-06-08 11:26:26', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534376274938597378, 1534376271981613057, 'update_user', 'updateUser', 'bigint', '修改人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-06-08 11:26:26', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534421799176060929, 1534421798261702658, 'id', 'id', 'bigint', '主键id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-06-08 14:27:20', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534421799436107778, 1534421798261702658, 'inv_id', 'invId', 'varchar', '库单编号，外键', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', 'MUL', 'InvId', 'N', '2022-06-08 14:27:20', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534421799566131202, 1534421798261702658, 'pro_id', 'proId', 'varchar', '产品编号，外键', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'ProId', 'N', '2022-06-08 14:27:20', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534421799889092609, 1534421798261702658, 'num', 'num', 'bigint', '数量', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Num', 'N', '2022-06-08 14:27:20', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534421800153333762, 1534421798261702658, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-06-08 14:27:20', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534421801541648385, 1534421798261702658, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-06-08 14:27:20', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534421801889775618, 1534421798261702658, 'update_time', 'updateTime', 'datetime', '修改时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-06-08 14:27:20', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534421802485366786, 1534421798261702658, 'update_user', 'updateUser', 'bigint', '修改人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-06-08 14:27:21', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1536185610979614721, 1536185610023313410, 'id', 'id', 'bigint', '报工表主键Id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-06-13 11:16:05', 1265476890672672808, '2022-06-13 11:16:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1536185611801698306, 1536185610023313410, 'work_order_id', 'workOrderId', 'bigint', '工单id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'WorkOrderId', 'N', '2022-06-13 11:16:05', 1265476890672672808, '2022-06-13 11:16:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1536185612128854018, 1536185610023313410, 'task_id', 'taskId', 'bigint', '任务id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'TaskId', 'N', '2022-06-13 11:16:06', 1265476890672672808, '2022-06-13 11:16:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1536185612401483777, 1536185610023313410, 'step_status', 'stepStatus', 'int', '工序状态', 'Integer', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'StepStatus', 'N', '2022-06-13 11:16:06', 1265476890672672808, '2022-06-13 11:16:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1536185612716056577, 1536185610023313410, 'production_name', 'productionName', 'varchar', '生产人员', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'ProductionName', 'N', '2022-06-13 11:16:06', 1265476890672672808, '2022-06-13 11:16:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1536185614083399682, 1536185610023313410, 'report_num', 'reportNum', 'bigint', '报工数', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'ReportNum', 'N', '2022-06-13 11:16:06', 1265476890672672808, '2022-06-13 11:16:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1536185614393778177, 1536185610023313410, 'good_num', 'goodNum', 'bigint', '良品数', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'GoodNum', 'N', '2022-06-13 11:16:06', 1265476890672672808, '2022-06-13 11:16:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1536185614741905409, 1536185610023313410, 'bad_num', 'badNum', 'bigint', '不良品数', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'BadNum', 'N', '2022-06-13 11:16:06', 1265476890672672808, '2022-06-13 11:16:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1536185615052283905, 1536185610023313410, 'start_time', 'startTime', 'datetime', '开始时间', 'Date', 'datepicker', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'StartTime', 'N', '2022-06-13 11:16:06', 1265476890672672808, '2022-06-13 11:16:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1536185615333302273, 1536185610023313410, 'end_time', 'endTime', 'datetime', '结束时间', 'Date', 'datepicker', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'EndTime', 'N', '2022-06-13 11:16:06', 1265476890672672808, '2022-06-13 11:16:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1536185616419627009, 1536185610023313410, 'remarks', 'remarks', 'varchar', '备注', 'String', 'textarea', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Remarks', 'N', '2022-06-13 11:16:07', 1265476890672672808, '2022-06-13 11:16:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1536185620983029761, 1536185610023313410, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-06-13 11:16:08', 1265476890672672808, '2022-06-13 11:16:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1536185622300041217, 1536185610023313410, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-06-13 11:16:08', 1265476890672672808, '2022-06-13 11:16:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1536185623403143170, 1536185610023313410, 'update_time', 'updateTime', 'datetime', '修改时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-06-13 11:16:08', 1265476890672672808, '2022-06-13 11:16:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1536185624711766018, 1536185610023313410, 'update_user', 'updateUser', 'bigint', '修改人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-06-13 11:16:09', 1265476890672672808, '2022-06-13 11:16:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1539809981879037953, 1539809981665128449, 'id', 'id', 'bigint', '报工表主键Id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-06-23 11:18:03', 1265476890672672808, '2022-06-23 11:19:01', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1539809981971312641, 1539809981665128449, 'work_order_id', 'workOrderId', 'bigint', '工单id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'WorkOrderId', 'N', '2022-06-23 11:18:03', 1265476890672672808, '2022-06-23 11:19:01', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1539809982055198722, 1539809981665128449, 'task_id', 'taskId', 'bigint', '任务id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'TaskId', 'N', '2022-06-23 11:18:03', 1265476890672672808, '2022-06-23 11:19:01', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1539809982134890498, 1539809981665128449, 'production_user', 'productionUser', 'bigint', '生产人员', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'ProductionUser', 'N', '2022-06-23 11:18:03', 1265476890672672808, '2022-06-23 11:19:01', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1539809982218776577, 1539809981665128449, 'report_num', 'reportNum', 'int', '报工数', 'Integer', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'ReportNum', 'N', '2022-06-23 11:18:03', 1265476890672672808, '2022-06-23 11:19:01', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1539809982302662658, 1539809981665128449, 'good_num', 'goodNum', 'int', '良品数', 'Integer', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'GoodNum', 'N', '2022-06-23 11:18:03', 1265476890672672808, '2022-06-23 11:19:01', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1539809982361382914, 1539809981665128449, 'bad_num', 'badNum', 'int', '不良品数', 'Integer', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'BadNum', 'N', '2022-06-23 11:18:03', 1265476890672672808, '2022-06-23 11:19:01', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1539809982415908865, 1539809981665128449, 'start_time', 'startTime', 'datetime', '开始时间', 'Date', 'datepicker', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'StartTime', 'N', '2022-06-23 11:18:03', 1265476890672672808, '2022-06-23 11:19:01', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1539809982470434817, 1539809981665128449, 'end_time', 'endTime', 'datetime', '结束时间', 'Date', 'datepicker', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'EndTime', 'N', '2022-06-23 11:18:03', 1265476890672672808, '2022-06-23 11:19:01', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1539809982520766466, 1539809981665128449, 'work_time', 'workTime', 'int', '工作时长', 'Integer', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'WorkTime', 'N', '2022-06-23 11:18:03', 1265476890672672808, '2022-06-23 11:19:01', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1539809982579486721, 1539809981665128449, 'distinction', 'distinction', 'int', '区分该报工从哪里添加(1为从报工添加,0为从任务添加)', 'Integer', 'input', NULL, 'N', 'N', 'N', 'N', 'Y', 'eq', '', 'Distinction', 'N', '2022-06-23 11:18:03', 1265476890672672808, '2022-06-23 11:19:01', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1539809982646595585, 1539809981665128449, 'remarks', 'remarks', 'varchar', '备注', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Remarks', 'N', '2022-06-23 11:18:03', 1265476890672672808, '2022-06-23 11:19:01', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1539809982696927234, 1539809981665128449, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-06-23 11:18:03', 1265476890672672808, '2022-06-23 11:19:01', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1539809982755647490, 1539809981665128449, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-06-23 11:18:03', 1265476890672672808, '2022-06-23 11:19:01', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1539809982826950657, 1539809981665128449, 'update_time', 'updateTime', 'datetime', '修改时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-06-23 11:18:03', 1265476890672672808, '2022-06-23 11:19:01', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1539809982898253826, 1539809981665128449, 'update_user', 'updateUser', 'bigint', '修改人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-06-23 11:18:03', 1265476890672672808, '2022-06-23 11:19:01', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1539852516655755265, 1539852515879809026, 'id', 'id', 'bigint', 'id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852516781584385, 1539852515879809026, 'sort_num', 'sortNum', 'int', '序号', 'Integer', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'SortNum', 'N', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852517310066690, 1539852515879809026, 'code', 'code', 'varchar', '编码', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Code', 'N', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852517372981249, 1539852515879809026, 'work_order_id', 'workOrderId', 'bigint', '工单id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'WorkOrderId', 'N', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852517440090113, 1539852515879809026, 'pro_id', 'proId', 'bigint', '产品id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'ProId', 'N', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852517503004674, 1539852515879809026, 'pro_type_id', 'proTypeId', 'bigint', '产品类型id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'ProTypeId', 'N', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852517565919233, 1539852515879809026, 'work_step_id', 'workStepId', 'bigint', '工序id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'WorkStepId', 'N', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852517565919234, 1539852515879809026, 'good_num', 'goodNum', 'int', '良品数', 'Integer', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'GoodNum', 'N', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852517633028098, 1539852515879809026, 'bad_num', 'badNum', 'int', '不良品数', 'Integer', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'BadNum', 'N', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852517695942657, 1539852515879809026, 'pla_num', 'plaNum', 'int', '计划数', 'Integer', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'PlaNum', 'N', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852517695942658, 1539852515879809026, 'pla_start_time', 'plaStartTime', 'datetime', '计划开始时间', 'Date', 'datepicker', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'PlaStartTime', 'N', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852517763051522, 1539852515879809026, 'pla_end_time', 'plaEndTime', 'datetime', '计划结束时间', 'Date', 'datepicker', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'PlaEndTime', 'N', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852517825966082, 1539852515879809026, 'fact_sta_time', 'factStaTime', 'datetime', '实际开始时间', 'Date', 'datepicker', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'FactStaTime', 'N', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852517825966083, 1539852515879809026, 'fact_end_time', 'factEndTime', 'datetime', '实际结束时间', 'Date', 'datepicker', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'FactEndTime', 'N', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852517893074946, 1539852515879809026, 'status', 'status', 'int', '状态(未开始:-1;执行中:0;已结束:1;)', 'Integer', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Status', 'N', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852517955989505, 1539852515879809026, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852517955989506, 1539852515879809026, 'create_user', 'createUser', 'bigint', '', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852518018904066, 1539852515879809026, 'update_time', 'updateTime', 'datetime', '修改时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852518086012930, 1539852515879809026, 'update_user', 'updateUser', 'bigint', '修改人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852518148927490, 1539852515879809026, 'report_right', 'reportRight', 'bigint', '报工权限', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'ReportRight', 'N', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1541591753163268098, 1541591752827723778, 'encode', 'encode', 'varchar', '自定义编码', 'String', 'input', NULL, 'Y', 'Y', 'N', 'N', 'N', 'eq', '', 'Encode', 'N', '2022-06-28 09:18:10', 1265476890672672808, '2022-06-28 09:31:48', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1541591753242959874, 1541591752827723778, 'time_format', 'timeFormat', 'int', '时间选项', 'Integer', 'radio', 'time_type', 'Y', 'Y', 'N', 'Y', 'N', 'eq', '', 'TimeFormat', 'N', '2022-06-28 09:18:10', 1265476890672672808, '2022-06-28 09:31:48', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1541591753301680129, 1541591752827723778, 'serial_num', 'serialNum', 'int', '流水号位数', 'Integer', 'inputnumber', NULL, 'Y', 'Y', 'N', 'Y', 'N', 'eq', '', 'SerialNum', 'N', '2022-06-28 09:18:10', 1265476890672672808, '2022-06-28 09:31:48', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1541591753372983297, 1541591752827723778, 'table_name', 'tableName', 'varchar', '目标表单名称', 'String', 'select', 'cus_table_name', 'Y', 'Y', 'N', 'Y', 'N', 'eq', '', 'TableName', 'N', '2022-06-28 09:18:10', 1265476890672672808, '2022-06-28 09:31:48', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1541591753431703554, 1541591752827723778, 'id', 'id', 'bigint', '主键id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-06-28 09:18:10', 1265476890672672808, '2022-06-28 09:31:48', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1541591753494618113, 1541591752827723778, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-06-28 09:18:10', 1265476890672672808, '2022-06-28 09:31:48', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1541591753553338369, 1541591752827723778, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-06-28 09:18:10', 1265476890672672808, '2022-06-28 09:31:48', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1541591753616252929, 1541591752827723778, 'update_time', 'updateTime', 'datetime', '修改时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-06-28 09:18:10', 1265476890672672808, '2022-06-28 09:31:48', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1541591753674973186, 1541591752827723778, 'update_user', 'updateUser', 'bigint', '修改人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-06-28 09:18:10', 1265476890672672808, '2022-06-28 09:31:48', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549569610708279298, 1549569608074256386, 'id', 'id', 'bigint', '主键id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-07-20 09:39:19', 1265476890672672808, '2022-07-20 10:24:21', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549569610897022977, 1549569608074256386, 'pid', 'pid', 'bigint', '父类id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'N', 'N', 'eq', '', 'Pid', 'N', '2022-07-20 09:39:20', 1265476890672672808, '2022-07-20 10:24:21', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549569611056406530, 1549569608074256386, 'pids', 'pids', 'varchar', '父类id集合', 'String', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'Pids', 'N', '2022-07-20 09:39:20', 1265476890672672808, '2022-07-20 10:24:21', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549569611245150210, 1549569608074256386, 'sort_name', 'sortName', 'varchar', '客户分类名称', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'SortName', 'N', '2022-07-20 09:39:20', 1265476890672672808, '2022-07-20 10:24:21', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549569611442282497, 1549569608074256386, 'remark', 'remark', 'varchar', '备注信息', 'String', 'textarea', NULL, 'Y', 'Y', 'N', 'N', 'Y', 'like', '', 'Remark', 'N', '2022-07-20 09:39:20', 1265476890672672808, '2022-07-20 10:24:21', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549569611651997697, 1549569608074256386, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-07-20 09:39:20', 1265476890672672808, '2022-07-20 10:24:21', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549569611786215426, 1549569608074256386, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-07-20 09:39:20', 1265476890672672808, '2022-07-20 10:24:21', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549569611916238849, 1549569608074256386, 'update_time', 'updateTime', 'datetime', '修改时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-07-20 09:39:20', 1265476890672672808, '2022-07-20 10:24:21', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549569612058845186, 1549569608074256386, 'update_user', 'updateUser', 'bigint', '修改人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-07-20 09:39:20', 1265476890672672808, '2022-07-20 10:24:21', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549586211662794753, 1549586211230781441, 'id', 'id', 'bigint', '主键id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-07-20 10:45:17', 1265476890672672808, '2022-07-20 10:47:29', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549586211755069442, 1549586211230781441, 'cus_infor_name', 'cusInforName', 'varchar', '客户资料名称', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'CusInforName', 'N', '2022-07-20 10:45:17', 1265476890672672808, '2022-07-20 10:47:29', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549586214217125889, 1549586211230781441, 'code', 'code', 'varchar', '社会统一信用代码', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'Code', 'N', '2022-07-20 10:45:18', 1265476890672672808, '2022-07-20 10:47:29', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549586214397480962, 1549586211230781441, 'cus_sort_id', 'cusSortId', 'bigint', '客户分类id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'N', 'N', 'eq', '', 'CusSortId', 'N', '2022-07-20 10:45:18', 1265476890672672808, '2022-07-20 10:47:29', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549586216750485506, 1549586211230781441, 'remark', 'remark', 'varchar', '备注信息', 'String', 'textarea', NULL, 'Y', 'Y', 'N', 'N', 'Y', 'like', '', 'Remark', 'N', '2022-07-20 10:45:19', 1265476890672672808, '2022-07-20 10:47:29', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549586216851148802, 1549586211230781441, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-07-20 10:45:19', 1265476890672672808, '2022-07-20 10:47:29', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549586217035698177, 1549586211230781441, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-07-20 10:45:19', 1265476890672672808, '2022-07-20 10:47:29', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549586217203470338, 1549586211230781441, 'update_time', 'updateTime', 'datetime', '更新时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-07-20 10:45:19', 1265476890672672808, '2022-07-20 10:47:29', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549586217283162114, 1549586211230781441, 'update_user', 'updateUser', 'bigint', '更新人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-07-20 10:45:19', 1265476890672672808, '2022-07-20 10:47:29', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549605376654086146, 1549605375643258881, 'id', 'id', 'bigint', '主键id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-07-20 12:01:27', 1265476890672672808, '2022-07-20 12:02:39', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549605377371312129, 1549605375643258881, 'cus_infor_id', 'cusInforId', 'bigint', '客户资料id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'N', 'N', 'eq', '', 'CusInforId', 'N', '2022-07-20 12:01:27', 1265476890672672808, '2022-07-20 12:02:39', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549605378172424194, 1549605375643258881, 'cus_person', 'cusPerson', 'varchar', '联系人', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'CusPerson', 'N', '2022-07-20 12:01:27', 1265476890672672808, '2022-07-20 12:02:40', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549605378839318530, 1549605375643258881, 'cus_phone', 'cusPhone', 'varchar', '联系电话', 'String', 'input', NULL, 'Y', 'Y', 'N', 'N', 'Y', 'eq', '', 'CusPhone', 'N', '2022-07-20 12:01:27', 1265476890672672808, '2022-07-20 12:02:40', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549605379799814145, 1549605375643258881, 'maior_cus_person', 'maiorCusPerson', 'varchar', '主联系人', 'String', 'input', NULL, 'Y', 'Y', 'N', 'N', 'N', 'eq', '', 'MaiorCusPerson', 'N', '2022-07-20 12:01:27', 1265476890672672808, '2022-07-20 12:02:40', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549605380781281281, 1549605375643258881, 'remark', 'remark', 'varchar', '备注信息', 'String', 'textarea', NULL, 'Y', 'Y', 'N', 'N', 'Y', 'like', '', 'Remark', 'N', '2022-07-20 12:01:28', 1265476890672672808, '2022-07-20 12:02:40', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549605381037133826, 1549605375643258881, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-07-20 12:01:28', 1265476890672672808, '2022-07-20 12:02:40', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549605381162962945, 1549605375643258881, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-07-20 12:01:28', 1265476890672672808, '2022-07-20 12:02:40', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549605382148624385, 1549605375643258881, 'update_time', 'updateTime', 'datetime', '更新时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-07-20 12:01:28', 1265476890672672808, '2022-07-20 12:02:40', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549605383645990913, 1549605375643258881, 'update_user', 'updateUser', 'bigint', '更新人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-07-20 12:01:28', 1265476890672672808, '2022-07-20 12:02:41', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552192508581863426, 1552192507873026049, 'id', 'id', 'bigint', '主键', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-07-27 15:21:47', 1265476890672672808, '2022-07-27 15:23:14', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552192508837715970, 1552192507873026049, 'code', 'code', 'varchar', '采购订单编码', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'Code', 'N', '2022-07-27 15:21:47', 1265476890672672808, '2022-07-27 15:23:14', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552192508963545089, 1552192507873026049, 'arrival_time', 'arrivalTime', 'datetime', '到货时间', 'Date', 'datepicker', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'ArrivalTime', 'N', '2022-07-27 15:21:47', 1265476890672672808, '2022-07-27 15:23:14', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552192509219397633, 1552192507873026049, 'order_source', 'orderSource', 'bigint', '订单来源', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'OrderSource', 'N', '2022-07-27 15:21:47', 1265476890672672808, '2022-07-27 15:23:14', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552192509483638785, 1552192507873026049, 'remarks', 'remarks', 'varchar', '备注', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'Remarks', 'N', '2022-07-27 15:21:47', 1265476890672672808, '2022-07-27 15:23:14', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552192509613662209, 1552192507873026049, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-07-27 15:21:47', 1265476890672672808, '2022-07-27 15:23:14', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552192509814988801, 1552192507873026049, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-07-27 15:21:47', 1265476890672672808, '2022-07-27 15:23:14', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552192510003732481, 1552192507873026049, 'update_time', 'updateTime', 'datetime', '更新时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-07-27 15:21:47', 1265476890672672808, '2022-07-27 15:23:14', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552192510196670465, 1552192507873026049, 'update_user', 'updateUser', 'bigint', '更新人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-07-27 15:21:47', 1265476890672672808, '2022-07-27 15:23:14', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552194970470539266, 1552194969950445570, 'id', 'id', 'bigint', '主键', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-07-27 15:31:34', 1265476890672672808, '2022-07-27 15:57:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552194970793500674, 1552194969950445570, 'purchase_order_id', 'purchaseOrderId', 'bigint', '采购订单编号', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'PurchaseOrderId', 'N', '2022-07-27 15:31:34', 1265476890672672808, '2022-07-27 15:57:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552194970931912705, 1552194969950445570, 'code', 'code', 'varchar', '采购明细编号', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'Code', 'N', '2022-07-27 15:31:34', 1265476890672672808, '2022-07-27 15:57:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552194971208736769, 1552194969950445570, 'pro_id', 'proId', 'bigint', '产品名称', 'String', 'select', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'ProId', 'N', '2022-07-27 15:31:34', 1265476890672672808, '2022-07-27 15:57:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552194971401674753, 1552194969950445570, 'purchase_num', 'purchaseNum', 'int', '数量', 'Integer', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'N', 'eq', '', 'PurchaseNum', 'N', '2022-07-27 15:31:34', 1265476890672672808, '2022-07-27 15:57:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552194971670110209, 1552194969950445570, 'unit_price', 'unitPrice', 'double', '单价', 'Double', 'Double', NULL, 'Y', 'Y', 'N', 'Y', 'N', 'eq', '', 'UnitPrice', 'N', '2022-07-27 15:31:34', 1265476890672672808, '2022-07-27 15:57:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552194971863048194, 1552194969950445570, 'total_price', 'totalPrice', 'double', '总价', 'Double', 'Double', NULL, 'Y', 'Y', 'N', 'Y', 'N', 'eq', '', 'TotalPrice', 'N', '2022-07-27 15:31:34', 1265476890672672808, '2022-07-27 15:57:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552194972060180481, 1552194969950445570, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-07-27 15:31:34', 1265476890672672808, '2022-07-27 15:57:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552194972324421633, 1552194969950445570, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-07-27 15:31:34', 1265476890672672808, '2022-07-27 15:57:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552194972517359617, 1552194969950445570, 'update_time', 'updateTime', 'datetime', '更新时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-07-27 15:31:34', 1265476890672672808, '2022-07-27 15:57:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552194972710297602, 1552194969950445570, 'update_user', 'updateUser', 'bigint', '更新人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-07-27 15:31:35', 1265476890672672808, '2022-07-27 15:57:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552208701358616578, 1552208700809162754, 'id', 'id', 'bigint', '主键id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-07-27 16:26:08', 1265476890672672808, '2022-07-27 16:28:10', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552208701488640001, 1552208700809162754, 'code', 'code', 'varchar', '编号', 'String', 'input', NULL, 'Y', 'Y', 'N', 'N', 'Y', 'eq', '', 'Code', 'N', '2022-07-27 16:26:08', 1265476890672672808, '2022-07-27 16:28:10', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552208701643829249, 1552208700809162754, 'name', 'name', 'varchar', '名称', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'Name', 'N', '2022-07-27 16:26:08', 1265476890672672808, '2022-07-27 16:28:10', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552208701832572930, 1552208700809162754, 'remark', 'remark', 'varchar', '备注', 'String', 'textarea', NULL, 'Y', 'Y', 'N', 'N', 'Y', 'like', '', 'Remark', 'N', '2022-07-27 16:26:08', 1265476890672672808, '2022-07-27 16:28:10', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552208702038093825, 1552208700809162754, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-07-27 16:26:08', 1265476890672672808, '2022-07-27 16:28:10', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552208702247809026, 1552208700809162754, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-07-27 16:26:08', 1265476890672672808, '2022-07-27 16:28:10', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552208702407192578, 1552208700809162754, 'update_time', 'updateTime', 'datetime', '修改时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-07-27 16:26:08', 1265476890672672808, '2022-07-27 16:28:10', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552208702612713473, 1552208700809162754, 'update_user', 'updateUser', 'bigint', '修改人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-07-27 16:26:08', 1265476890672672808, '2022-07-27 16:28:10', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552474304292737026, 1552474302514352130, 'id', 'id', 'bigint', '主键', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-07-28 10:01:32', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1552474304687001601, 1552474302514352130, 'purchase_order_id', 'purchaseOrderId', 'bigint', '采购订单id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'PurchaseOrderId', 'N', '2022-07-28 10:01:32', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1552474304947048450, 1552474302514352130, 'code', 'code', 'varchar', '采购明细编号', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Code', 'N', '2022-07-28 10:01:33', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1552474305202900993, 1552474302514352130, 'pro_id', 'proId', 'bigint', '产品id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'ProId', 'N', '2022-07-28 10:01:33', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1552474305404227586, 1552474302514352130, 'purchase_num', 'purchaseNum', 'int', '数量', 'Integer', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'PurchaseNum', 'N', '2022-07-28 10:01:33', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1552474305664274434, 1552474302514352130, 'unit_price', 'unitPrice', 'double', '单价', 'Double', 'Double', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'UnitPrice', 'N', '2022-07-28 10:01:33', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1552474305861406721, 1552474302514352130, 'total_price', 'totalPrice', 'double', '总价', 'Double', 'Double', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'TotalPrice', 'N', '2022-07-28 10:01:33', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1552474306121453570, 1552474302514352130, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-07-28 10:01:33', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1552474306314391553, 1552474302514352130, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-07-28 10:01:33', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1552474306633158657, 1552474302514352130, 'update_time', 'updateTime', 'datetime', '更新时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-07-28 10:01:33', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1552474306901594114, 1552474302514352130, 'update_user', 'updateUser', 'bigint', '更新人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-07-28 10:01:33', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1552574101191671810, 1552574100327645186, 'id', 'id', 'bigint', '主键id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-07-28 16:38:06', 1265476890672672808, '2022-07-28 16:41:32', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552574101447524354, 1552574100327645186, 'pro_code', 'proCode', 'varchar', '产品编号', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'ProCode', 'N', '2022-07-28 16:38:06', 1265476890672672808, '2022-07-28 16:41:32', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552574101694988290, 1552574100327645186, 'pro_name', 'proName', 'varchar', '产品名称', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'ProName', 'N', '2022-07-28 16:38:06', 1265476890672672808, '2022-07-28 16:41:32', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552574101921480706, 1552574100327645186, 'sto_num', 'stoNum', 'int', '库存数量', 'Integer', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'N', 'eq', '', 'StoNum', 'N', '2022-07-28 16:38:06', 1265476890672672808, '2022-07-28 16:41:32', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552574102219276290, 1552574100327645186, 'war_hou_name', 'warHouName', 'varchar', '仓库名称', 'String', 'select', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'WarHouName', 'N', '2022-07-28 16:38:06', 1265476890672672808, '2022-07-28 16:41:32', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552574102588375041, 1552574100327645186, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-07-28 16:38:06', 1265476890672672808, '2022-07-28 16:41:32', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552574102852616194, 1552574100327645186, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-07-28 16:38:06', 1265476890672672808, '2022-07-28 16:41:32', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552574103104274433, 1552574100327645186, 'update_time', 'updateTime', 'datetime', '更新时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-07-28 16:38:06', 1265476890672672808, '2022-07-28 16:41:32', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552574103326572546, 1552574100327645186, 'update_user', 'updateUser', 'bigint', '更新人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-07-28 16:38:06', 1265476890672672808, '2022-07-28 16:41:32', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1554352652243931137, 1554352651656728578, 'id', 'id', 'bigint', '主键id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-08-02 14:25:25', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352652441063426, 1554352651656728578, 'pid', 'pid', 'bigint', '父类id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Pid', 'N', '2022-08-02 14:25:25', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352652634001410, 1554352651656728578, 'pids', 'pids', 'varchar', '父类id集合', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Pids', 'N', '2022-08-02 14:25:26', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352652826939394, 1554352651656728578, 'sort_name', 'sortName', 'varchar', '供应商分类名称', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'SortName', 'N', '2022-08-02 14:25:26', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352653019877377, 1554352651656728578, 'remark', 'remark', 'varchar', '备注', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Remark', 'N', '2022-08-02 14:25:26', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352653208621058, 1554352651656728578, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-08-02 14:25:26', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352653342838786, 1554352651656728578, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-08-02 14:25:26', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352653472862210, 1554352651656728578, 'update_time', 'updateTime', 'datetime', '更新时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-08-02 14:25:26', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352653674188802, 1554352651656728578, 'update_user', 'updateUser', 'bigint', '更新人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-08-02 14:25:26', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352777196441602, 1554352776881868802, 'id', 'id', 'bigint', '主键id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-08-02 14:25:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352777330659329, 1554352776881868802, 'supp_data_id', 'suppDataId', 'bigint', '供应商资料id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'SuppDataId', 'N', '2022-08-02 14:25:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352777464877057, 1554352776881868802, 'supp_person', 'suppPerson', 'varchar', '联系人', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'SuppPerson', 'N', '2022-08-02 14:25:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352777590706177, 1554352776881868802, 'supp_phone', 'suppPhone', 'varchar', '联系人电话', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'SuppPhone', 'N', '2022-08-02 14:25:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352777724923906, 1554352776881868802, 'maior_cus_person', 'maiorCusPerson', 'varchar', '主联系人', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'MaiorCusPerson', 'N', '2022-08-02 14:25:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352777787838465, 1554352776881868802, 'remark', 'remark', 'varchar', '备注', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Remark', 'N', '2022-08-02 14:25:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352777913667586, 1554352776881868802, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-08-02 14:25:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352778039496706, 1554352776881868802, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-08-02 14:25:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352778169520130, 1554352776881868802, 'update_time', 'updateTime', 'datetime', '更新时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-08-02 14:25:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352778303737857, 1554352776881868802, 'update_user', 'updateUser', 'bigint', '更新人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-08-02 14:25:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352829176451073, 1554352828849295362, 'id', 'id', 'bigint', '主键id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-08-02 14:26:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352829302280194, 1554352828849295362, 'supp_data_name', 'suppDataName', 'varchar', '供应商资料名称', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'SuppDataName', 'N', '2022-08-02 14:26:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352829432303618, 1554352828849295362, 'code', 'code', 'varchar', '社会统一信用代码', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Code', 'N', '2022-08-02 14:26:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352829562327042, 1554352828849295362, 'supp_sort_id', 'suppSortId', 'bigint', '客户分类id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'SuppSortId', 'N', '2022-08-02 14:26:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352829755265025, 1554352828849295362, 'remark', 'remark', 'varchar', '备注', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Remark', 'N', '2022-08-02 14:26:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352829948203009, 1554352828849295362, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-08-02 14:26:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352830011117570, 1554352828849295362, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-08-02 14:26:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352830208249857, 1554352828849295362, 'update_time', 'updateTime', 'datetime', '更新时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-08-02 14:26:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352830275358721, 1554352828849295362, 'update_user', 'updateUser', 'bigint', '更新人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-08-02 14:26:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1557621802560151554, 1557621802300104706, 'id', 'id', 'bigint', '主键id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-08-11 14:55:52', 1265476890672672808, '2022-08-11 14:58:37', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1557621802627260417, 1557621802300104706, 'name', 'name', 'varchar', '租户名称', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'Name', 'N', '2022-08-11 14:55:52', 1265476890672672808, '2022-08-11 14:58:37', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1557621802627260418, 1557621802300104706, 'code', 'code', 'varchar', '租户的编码', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Code', 'N', '2022-08-11 14:55:52', 1265476890672672808, '2022-08-11 14:58:37', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1557621802690174978, 1557621802300104706, 'db_name', 'dbName', 'varchar', '关联的数据库名称', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'DbName', 'N', '2022-08-11 14:55:52', 1265476890672672808, '2022-08-11 14:58:37', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1557621802757283842, 1557621802300104706, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-08-11 14:55:52', 1265476890672672808, '2022-08-11 14:58:37', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1557621802824392705, 1557621802300104706, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-08-11 14:55:52', 1265476890672672808, '2022-08-11 14:58:37', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1557621802891501570, 1557621802300104706, 'update_time', 'updateTime', 'datetime', '更新时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-08-11 14:55:52', 1265476890672672808, '2022-08-11 14:58:37', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1557621803025719297, 1557621802300104706, 'update_user', 'updateUser', 'bigint', '更新人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-08-11 14:55:52', 1265476890672672808, '2022-08-11 14:58:37', 1265476890672672808);

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
                               `id` bigint(20) NOT NULL COMMENT '主键',
                               `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
                               `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '编码',
                               `value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '值',
                               `sys_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '是否是系统参数（Y-是，N-否）',
                               `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
                               `status` tinyint(4) NOT NULL COMMENT '状态（字典 0正常 1停用 2删除）',
                               `group_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'DEFAULT' COMMENT '常量所属分类的编码，来自于“常量的分类”字典',
                               `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                               `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                               `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                               `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                               PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统参数配置表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1265117443880853506, 'jwt密钥', 'SNOWY_JWT_SECRET', 'snowy', 'Y', '（重要）jwt密钥，默认为空，自行设置', 0, 'DEFAULT', '2020-05-26 06:35:19', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1265117443880853507, '默认密码', 'SNOWY_DEFAULT_PASSWORD', '123456', 'Y', '默认密码', 0, 'DEFAULT', '2020-05-26 06:37:56', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1265117443880853508, 'token过期时间', 'SNOWY_TOKEN_EXPIRE', '86400', 'Y', 'token过期时间（单位：秒）', 0, 'DEFAULT', '2020-05-27 11:54:49', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1265117443880853509, 'session会话过期时间', 'SNOWY_SESSION_EXPIRE', '7200', 'Y', 'session会话过期时间（单位：秒）', 0, 'DEFAULT', '2020-05-27 11:54:49', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1265117443880853519, '阿里云短信keyId', 'SNOWY_ALIYUN_SMS_ACCESSKEY_ID', '你的keyId', 'Y', '阿里云短信keyId', 0, 'ALIYUN_SMS', '2020-06-07 16:27:11', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1269547042242371585, '阿里云短信secret', 'SNOWY_ALIYUN_SMS_ACCESSKEY_SECRET', '你的secret', 'Y', '阿里云短信secret', 0, 'ALIYUN_SMS', '2020-06-07 16:29:37', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1269547130041737217, '阿里云短信签名', 'SNOWY_ALIYUN_SMS_SIGN_NAME', 'Snowy快速开发平台', 'Y', '阿里云短信签名', 0, 'ALIYUN_SMS', '2020-06-07 16:29:58', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1269547279530926081, '阿里云短信-登录模板号', 'SNOWY_ALIYUN_SMS_LOGIN_TEMPLATE_CODE', 'SMS_1877123456', 'Y', '阿里云短信-登录模板号', 0, 'ALIYUN_SMS', '2020-06-07 16:30:33', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1269547410879750145, '阿里云短信默认失效时间', 'SNOWY_ALIYUN_SMS_INVALIDATE_MINUTES', '5', 'Y', '阿里云短信默认失效时间（单位：分钟）', 0, 'ALIYUN_SMS', '2020-06-07 16:31:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1269575927357071361, '腾讯云短信secretId', 'SNOWY_TENCENT_SMS_SECRET_ID', '你的secretId', 'Y', '腾讯云短信secretId', 0, 'TENCENT_SMS', '2020-06-07 18:24:23', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1269575991693500418, '腾讯云短信secretKey', 'SNOWY_TENCENT_SMS_SECRET_KEY', '你的secretkey', 'Y', '腾讯云短信secretKey', 0, 'TENCENT_SMS', '2020-06-07 18:24:39', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1269576044084551682, '腾讯云短信sdkAppId', 'SNOWY_TENCENT_SMS_SDK_APP_ID', '1400375123', 'Y', '腾讯云短信sdkAppId', 0, 'TENCENT_SMS', '2020-06-07 18:24:51', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1269576089294954497, '腾讯云短信签名', 'SNOWY_TENCENT_SMS_SIGN', 'Snowy快速开发平台', 'Y', '腾讯云短信签名', 0, 'TENCENT_SMS', '2020-06-07 18:25:02', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1270378172860403713, '邮箱host', 'SNOWY_EMAIL_HOST', 'smtp.126.com', 'Y', '邮箱host', 0, 'EMAIL', '2020-06-09 23:32:14', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1270378295543795714, '邮箱用户名', 'SNOWY_EMAIL_USERNAME', 'test@126.com', 'Y', '邮箱用户名', 0, 'EMAIL', '2020-06-09 23:32:43', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1270378340510928897, '邮箱密码', 'SNOWY_EMAIL_PASSWORD', '你的邮箱密码', 'Y', '邮箱密码', 0, 'EMAIL', '2020-06-09 23:32:54', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1270378527358783489, '邮箱端口', 'SNOWY_EMAIL_PORT', '465', 'Y', '邮箱端口', 0, 'EMAIL', '2020-06-09 23:33:38', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1270378790035460097, '邮箱是否开启ssl', 'SNOWY_EMAIL_SSL', 'true', 'Y', '邮箱是否开启ssl', 0, 'EMAIL', '2020-06-09 23:34:41', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1270380786649972737, '邮箱发件人', 'SNOWY_EMAIL_FROM', 'test@126.com', 'Y', '邮箱发件人', 0, 'EMAIL', '2020-06-09 23:42:37', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1270380786649972738, 'win本地上传文件路径', 'SNOWY_FILE_UPLOAD_PATH_FOR_WINDOWS', 'd:/tmp', 'Y', 'win本地上传文件路径', 0, 'FILE_PATH', '2020-06-09 23:42:37', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1270380786649972739, 'linux/mac本地上传文件路径', 'SNOWY_FILE_UPLOAD_PATH_FOR_LINUX', '/tmp', 'Y', 'linux/mac本地上传文件路径', 0, 'FILE_PATH', '2020-06-09 23:42:37', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1270380786649982740, 'Snowy演示环境', 'SNOWY_DEMO_ENV_FLAG', 'false', 'Y', 'Snowy演示环境的开关，true-打开，false-关闭，如果演示环境开启，则只能读数据不能写数据', 0, 'DEFAULT', '2020-06-09 23:42:37', 1265476890672672808, '2020-09-03 14:38:17', 1265476890672672808);
INSERT INTO `sys_config` VALUES (1270380786649982741, 'Snowy放开XSS过滤的接口', 'SNOWY_UN_XSS_FILTER_URL', '/demo/xssfilter,/demo/unxss', 'Y', '多个url可以用英文逗号隔开', 0, 'DEFAULT', '2020-06-09 23:42:37', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1270380786649982742, '单用户登陆的开关', 'SNOWY_ENABLE_SINGLE_LOGIN', 'false', 'Y', '单用户登陆的开关，true-打开，false-关闭，如果一个人登录两次，就会将上一次登陆挤下去', 0, 'DEFAULT', '2020-06-09 23:42:37', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1270380786649982743, '登录验证码的开关', 'SNOWY_CAPTCHA_OPEN', 'true', 'Y', '登录验证码的开关，true-打开，false-关闭', 0, 'DEFAULT', '2020-06-09 23:42:37', 1265476890672672808, '2021-12-16 19:43:29', 1265476890672672808);
INSERT INTO `sys_config` VALUES (1280694281648070659, '阿里云定位api接口地址', 'SNOWY_IP_GEO_API', 'http://api01.aliyun.venuscn.com/ip?ip=%s', 'Y', '阿里云定位api接口地址', 0, 'DEFAULT', '2020-07-20 10:44:46', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1280694281648070660, '阿里云定位appCode', 'SNOWY_IP_GEO_APP_CODE', '461535aabeae4f34861884d392f5d452', 'Y', '阿里云定位appCode', 0, 'DEFAULT', '2020-07-20 10:44:46', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1288309751255412737, 'Oauth用户登录的开关', 'SNOWY_ENABLE_OAUTH_LOGIN', 'true', 'Y', 'Oauth用户登录的开关', 0, 'OAUTH', '2020-07-29 11:05:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1288310043346743297, 'Oauth码云登录ClientId', 'SNOWY_OAUTH_GITEE_CLIENT_ID', '你的clientId', 'Y', 'Oauth码云登录ClientId', 0, 'OAUTH', '2020-07-29 11:07:05', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1288310157876408321, 'Oauth码云登录ClientSecret', 'SNOWY_OAUTH_GITEE_CLIENT_SECRET', '你的clientSecret', 'Y', 'Oauth码云登录ClientSecret', 0, 'OAUTH', '2020-07-29 11:07:32', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1288310280056483841, 'Oauth码云登录回调地址', 'SNOWY_OAUTH_GITEE_REDIRECT_URI', 'http://localhost:83/oauth/callback/gitee', 'Y', 'Oauth码云登录回调地址', 0, 'OAUTH', '2020-07-29 11:08:01', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1410861340761313282, '在线文档地址', 'SNOWY_ONLY_OFFICE_SERVICE_URL', 'https://xiaonuo.vip/', 'N', 'beizhu', 0, 'DEFAULT', '2021-07-02 15:22:11', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1471107941140008961, 'token是否加解密', 'SNOWY_TOKEN_ENCRYPTION_OPEN', 'true', 'Y', 'token是否加解密', 0, 'CRYPTOGRAM', '2021-12-15 21:20:40', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1471108086246150145, '操作日志是否加密', 'SNOWY_VISLOG_ENCRYPTION_OPEN', 'true', 'Y', '操作日志是否加密，默认开启', 0, 'CRYPTOGRAM', '2021-12-15 21:21:14', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1471108334985154562, '登录登出日志是否加密', 'SNOWY_OPLOG_ENCRYPTION_OPEN', 'true', 'Y', '登录登出日志是否加密', 0, 'CRYPTOGRAM', '2021-12-15 21:22:14', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1471108457198784514, '铭感字段值是否加解密', 'SNOWY_FIELD_ENC_DEC_OPEN', 'true', 'Y', '铭感字段值是否加解密，默认开启', 0, 'CRYPTOGRAM', '2021-12-15 21:22:43', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1471109091222482946, '是否开启租户功能', 'SNOWY_TENANT_OPEN', 'false', 'Y', '是否开启租户功能，默认关闭', 0, 'DEFAULT', '2021-12-15 21:25:14', 1265476890672672808, '2022-08-22 09:22:38', 1265476890672672808);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
                                  `id` bigint(20) NOT NULL COMMENT '主键',
                                  `type_id` bigint(20) NOT NULL COMMENT '字典类型id',
                                  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '值',
                                  `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '编码',
                                  `sort` int(11) NOT NULL COMMENT '排序',
                                  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
                                  `status` tinyint(4) NOT NULL COMMENT '状态（字典 0正常 1停用 2删除）',
                                  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                  `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                  `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统字典值表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1265216536659087357, 1265216211667636234, '男', '1', 100, '男性', 0, '2020-04-01 10:23:29', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265216536659087358, 1265216211667636234, '女', '2', 100, '女性', 0, '2020-04-01 10:23:49', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265216536659087359, 1265216211667636234, '未知', '3', 100, '未知性别', 0, '2020-04-01 10:24:01', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265216536659087361, 1265216211667636235, '默认常量', 'DEFAULT', 100, '默认常量，都以SNOWY_开头的', 0, '2020-04-14 23:25:45', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265216536659087363, 1265216211667636235, '阿里云短信', 'ALIYUN_SMS', 100, '阿里云短信配置', 0, '2020-04-14 23:25:45', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265216536659087364, 1265216211667636235, '腾讯云短信', 'TENCENT_SMS', 100, '腾讯云短信', 0, '2020-04-14 23:25:45', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265216536659087365, 1265216211667636235, '邮件配置', 'EMAIL', 100, '邮箱配置', 0, '2020-04-14 23:25:45', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265216536659087366, 1265216211667636235, '文件上传路径', 'FILE_PATH', 100, '文件上传路径', 0, '2020-04-14 23:25:45', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265216536659087367, 1265216211667636235, 'Oauth配置', 'OAUTH', 100, 'Oauth配置', 0, '2020-04-14 23:25:45', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265216617500102656, 1265216211667636226, '正常', '0', 100, '正常', 0, '2020-05-26 17:41:44', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265216617500102657, 1265216211667636226, '停用', '1', 100, '停用', 0, '2020-05-26 17:42:03', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265216938389524482, 1265216211667636226, '删除', '2', 100, '删除', 0, '2020-05-26 17:43:19', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265217669028892673, 1265217074079453185, '否', 'N', 100, '否', 0, '2020-05-26 17:46:14', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265217706584690689, 1265217074079453185, '是', 'Y', 100, '是', 0, '2020-05-26 17:46:23', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265220776437731330, 1265217846770913282, '登录', '1', 100, '登录', 0, '2020-05-26 17:58:34', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265220806070489090, 1265217846770913282, '登出', '2', 100, '登出', 0, '2020-05-26 17:58:41', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265221129564573697, 1265221049302372354, '目录', '0', 100, '目录', 0, '2020-05-26 17:59:59', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265221163119005697, 1265221049302372354, '菜单', '1', 100, '菜单', 0, '2020-05-26 18:00:07', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265221188091891713, 1265221049302372354, '按钮', '2', 100, '按钮', 0, '2020-05-26 18:00:13', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265466389204967426, 1265466149622128641, '未发送', '0', 100, '未发送', 0, '2020-05-27 10:14:33', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265466432670539778, 1265466149622128641, '发送成功', '1', 100, '发送成功', 0, '2020-05-27 10:14:43', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265466486097584130, 1265466149622128641, '发送失败', '2', 100, '发送失败', 0, '2020-05-27 10:14:56', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265466530477514754, 1265466149622128641, '失效', '3', 100, '失效', 0, '2020-05-27 10:15:07', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265466835009150978, 1265466752209395713, '无', '0', 100, '无', 0, '2020-05-27 10:16:19', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265466874758569986, 1265466752209395713, '组件', '1', 100, '组件', 0, '2020-05-27 10:16:29', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265466925476093953, 1265466752209395713, '内链', '2', 100, '内链', 0, '2020-05-27 10:16:41', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265466962209808385, 1265466752209395713, '外链', '3', 100, '外链', 0, '2020-05-27 10:16:50', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265467428423475202, 1265467337566461954, '系统权重', '1', 100, '系统权重', 0, '2020-05-27 10:18:41', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265467503090475009, 1265467337566461954, '业务权重', '2', 100, '业务权重', 0, '2020-05-27 10:18:59', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265468138431062018, 1265468028632571905, '全部数据', '1', 100, '全部数据', 0, '2020-05-27 10:21:30', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265468194928336897, 1265468028632571905, '本部门及以下数据', '2', 100, '本部门及以下数据', 0, '2020-05-27 10:21:44', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265468241992622082, 1265468028632571905, '本部门数据', '3', 100, '本部门数据', 0, '2020-05-27 10:21:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265468273634451457, 1265468028632571905, '仅本人数据', '4', 100, '仅本人数据', 0, '2020-05-27 10:22:02', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265468302046666753, 1265468028632571905, '自定义数据', '5', 100, '自定义数据', 0, '2020-05-27 10:22:09', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265468508100239362, 1265468437904367618, 'app', '1', 100, 'app', 0, '2020-05-27 10:22:58', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265468543433056258, 1265468437904367618, 'pc', '2', 100, 'pc', 0, '2020-05-27 10:23:07', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265468576874242050, 1265468437904367618, '其他', '3', 100, '其他', 0, '2020-05-27 10:23:15', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1275617233011335170, 1275617093517172738, '其它', '0', 100, '其它', 0, '2020-06-24 10:30:23', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1275617295355469826, 1275617093517172738, '增加', '1', 100, '增加', 0, '2020-06-24 10:30:38', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1275617348610547714, 1275617093517172738, '删除', '2', 100, '删除', 0, '2020-06-24 10:30:50', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1275617395515449346, 1275617093517172738, '编辑', '3', 100, '编辑', 0, '2020-06-24 10:31:02', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1275617433612312577, 1275617093517172738, '更新', '4', 100, '更新', 0, '2020-06-24 10:31:11', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1275617472707420161, 1275617093517172738, '查询', '5', 100, '查询', 0, '2020-06-24 10:31:20', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1275617502973517826, 1275617093517172738, '详情', '6', 100, '详情', 0, '2020-06-24 10:31:27', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1275617536959963137, 1275617093517172738, '树', '7', 100, '树', 0, '2020-06-24 10:31:35', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1275617619524837377, 1275617093517172738, '导入', '8', 100, '导入', 0, '2020-06-24 10:31:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1275617651816783873, 1275617093517172738, '导出', '9', 100, '导出', 0, '2020-06-24 10:32:03', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1275617683475390465, 1275617093517172738, '授权', '10', 100, '授权', 0, '2020-06-24 10:32:10', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1275617709928865793, 1275617093517172738, '强退', '11', 100, '强退', 0, '2020-06-24 10:32:17', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1275617739091861505, 1275617093517172738, '清空', '12', 100, '清空', 0, '2020-06-24 10:32:23', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1275617788601425921, 1275617093517172738, '修改状态', '13', 100, '修改状态', 0, '2020-06-24 10:32:35', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1277774590944317441, 1277774529430654977, '阿里云', '1', 100, '阿里云', 0, '2020-06-30 09:22:57', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1277774666055913474, 1277774529430654977, '腾讯云', '2', 100, '腾讯云', 0, '2020-06-30 09:23:15', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1277774695168577538, 1277774529430654977, 'minio', '3', 100, 'minio', 0, '2020-06-30 09:23:22', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1277774726835572737, 1277774529430654977, '本地', '4', 100, '本地', 0, '2020-06-30 09:23:29', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1278607123583868929, 1278606951432855553, '运行', '1', 100, '运行', 0, '2020-07-02 16:31:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1278607162943217666, 1278606951432855553, '停止', '2', 100, '停止', 0, '2020-07-02 16:31:18', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1278939265862004738, 1278911800547147777, '通知', '1', 100, '通知', 0, '2020-07-03 14:30:57', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1278939319922388994, 1278911800547147777, '公告', '2', 100, '公告', 0, '2020-07-03 14:31:10', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1278939399001796609, 1278911952657776642, '草稿', '0', 100, '草稿', 0, '2020-07-03 14:31:29', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1278939432686252034, 1278911952657776642, '发布', '1', 100, '发布', 0, '2020-07-03 14:31:37', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1278939458804183041, 1278911952657776642, '撤回', '2', 100, '撤回', 0, '2020-07-03 14:31:43', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1278939485878415362, 1278911952657776642, '删除', '3', 100, '删除', 0, '2020-07-03 14:31:50', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1291390260160299009, 1291390159941599233, '是', 'true', 100, '是', 2, '2020-08-06 23:06:46', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1291390315437031426, 1291390159941599233, '否', 'false', 100, '否', 2, '2020-08-06 23:06:59', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1342446007168466945, 1342445962104864770, '下载压缩包', '1', 100, '下载压缩包', 0, '2020-12-25 20:24:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1342446035433881601, 1342445962104864770, '生成到本项目', '2', 100, '生成到本项目', 0, '2020-12-25 20:24:11', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1358094655567454210, 1358094419419750401, '输入框', 'input', 100, '输入框', 0, '2021-02-07 00:46:13', 1265476890672672808, '2021-02-08 01:01:28', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1358094740510498817, 1358094419419750401, '时间选择', 'datepicker', 100, '时间选择', 0, '2021-02-07 00:46:33', 1265476890672672808, '2021-02-08 01:04:07', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1358094793149014017, 1358094419419750401, '下拉框', 'select', 100, '下拉框', 0, '2021-02-07 00:46:46', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1358095496009506817, 1358094419419750401, '单选框', 'radio', 100, '单选框', 0, '2021-02-07 00:49:33', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1358095673084633090, 1358094419419750401, '开关', 'switch', 100, '开关', 2, '2021-02-07 00:50:15', 1265476890672672808, '2021-02-11 19:07:18', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1358458689433190402, 1358457818733428737, '等于', 'eq', 1, '等于', 0, '2021-02-08 00:52:45', 1265476890672672808, '2021-02-13 23:35:36', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1358458785168179202, 1358457818733428737, '模糊', 'like', 2, '模糊', 0, '2021-02-08 00:53:08', 1265476890672672808, '2021-02-13 23:35:46', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1358460475682406401, 1358094419419750401, '多选框', 'checkbox', 100, '多选框', 0, '2021-02-08 00:59:51', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1358460819019743233, 1358094419419750401, '数字输入框', 'inputnumber', 100, '数字输入框', 0, '2021-02-08 01:01:13', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1358470210267725826, 1358470065111252994, 'Long', 'Long', 100, 'Long', 0, '2021-02-08 01:38:32', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1358470239351029762, 1358470065111252994, 'String', 'String', 100, 'String', 0, '2021-02-08 01:38:39', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1358470265640927233, 1358470065111252994, 'Date', 'Date', 100, 'Date', 0, '2021-02-08 01:38:45', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1358470300168437761, 1358470065111252994, 'Integer', 'Integer', 100, 'Integer', 0, '2021-02-08 01:38:53', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1358470697377415169, 1358470065111252994, 'boolean', 'boolean', 100, 'boolean', 0, '2021-02-08 01:40:28', 1265476890672672808, '2021-02-08 01:40:47', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1358471133434036226, 1358470065111252994, 'int', 'int', 100, 'int', 0, '2021-02-08 01:42:12', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1358471188291338241, 1358470065111252994, 'double', 'double', 100, 'double', 0, '2021-02-08 01:42:25', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1358756511688761346, 1358457818733428737, '大于', 'gt', 3, '大于', 0, '2021-02-08 20:36:12', 1265476890672672808, '2021-02-13 23:45:24', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1358756547159990274, 1358457818733428737, '小于', 'lt', 4, '大于', 0, '2021-02-08 20:36:20', 1265476890672672808, '2021-02-13 23:45:29', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1358756609990664193, 1358457818733428737, '不等于', 'ne', 7, '不等于', 0, '2021-02-08 20:36:35', 1265476890672672808, '2021-02-13 23:45:46', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1358756685030957057, 1358457818733428737, '大于等于', 'ge', 5, '大于等于', 0, '2021-02-08 20:36:53', 1265476890672672808, '2021-02-13 23:45:35', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1358756800525312001, 1358457818733428737, '小于等于', 'le', 6, '小于等于', 0, '2021-02-08 20:37:20', 1265476890672672808, '2021-02-13 23:45:40', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1360529773814083586, 1358094419419750401, '文本域', 'textarea', 100, '文本域', 0, '2021-02-13 18:02:30', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1360606105914732545, 1358457818733428737, '不为空', 'isNotNull', 8, '不为空', 0, '2021-02-13 23:05:49', 1265476890672672808, '2021-02-13 23:45:50', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1471107569499508737, 1265216211667636235, '加密配置', 'CRYPTOGRAM', 100, '加密配置，默认全开', 0, '2021-12-15 21:19:11', 1265476890672672808, '2021-12-15 21:19:24', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1534370624561029121, 1534370298088988673, '其他出库', 'other_out', 100, '其他出库', 0, '2022-06-08 11:03:59', 1265476890672672808, '2022-06-08 11:07:00', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1534370774654197762, 1534370298088988673, '采购退货出库', 'pur_out', 100, '采购退货出库', 0, '2022-06-08 11:04:35', 1265476890672672808, '2022-06-08 11:08:38', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1534371162073669634, 1534370298088988673, '普通出库', 'ord_out', 100, '普通出库', 0, '2022-06-08 11:06:07', 1265476890672672808, '2022-06-08 11:07:18', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1534371664297603073, 1534371575181225985, '字符串', 'string', 100, NULL, 0, '2022-06-08 11:08:07', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1534371775545139202, 1534371402801537025, '模具缺失', 'Diemissing', 100, '模具缺失', 0, '2022-06-08 11:08:33', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1534371910538829825, 1534370205881409537, '普通入库', 'ord_in', 100, '普通入库', 0, '2022-06-08 11:09:05', 1265476890672672808, '2022-06-08 11:10:17', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1534371910769500162, 1534371402801537025, '其它', 'other', 100, '其他', 0, '2022-06-08 11:09:05', 1265476890672672808, '2022-08-16 11:24:52', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1534371989345607681, 1534370205881409537, '其他入库', 'other_in', 100, '其他入库', 0, '2022-06-08 11:09:24', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1534372101077671938, 1534370205881409537, '采购入库', 'pur_in', 100, '采购入库', 0, '2022-06-08 11:09:51', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1534372255730032641, 1534371402801537025, '破损', 'damaged', 100, '破损', 0, '2022-06-08 11:10:28', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1534372356875673601, 1534371402801537025, '测试数据', 'ceshi1', 100, '11', 0, '2022-06-08 11:10:52', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1534375984302686209, 1534375649047773185, '百', 'hundred', 100, '百', 2, '2022-06-08 11:25:17', 1265476890672672808, '2022-06-22 11:02:03', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1534376071464517633, 1534375649047773185, '千', 'thousand', 100, '千', 2, '2022-06-08 11:25:37', 1265476890672672808, '2022-06-22 11:02:05', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1534376187206336514, 1534375649047773185, '万', 'ten_thousand', 100, '万', 2, '2022-06-08 11:26:05', 1265476890672672808, '2022-06-22 11:02:07', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1539443353173667842, 1534375649047773185, '个', 'indivual', 100, NULL, 0, '2022-06-22 11:01:12', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1539443446530486274, 1534375649047773185, '件', 'piece', 100, NULL, 0, '2022-06-22 11:01:34', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1539444094273630209, 1534375649047773185, '套', 'sleeve', 100, NULL, 0, '2022-06-22 11:04:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1539444301958787073, 1534375649047773185, '只', 'single', 100, NULL, 0, '2022-06-22 11:04:58', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1539444436134572034, 1534375649047773185, '台', 'desk', 100, NULL, 0, '2022-06-22 11:05:30', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1539444541944279042, 1534375649047773185, '米', 'meter', 100, NULL, 0, '2022-06-22 11:05:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1539444621967405058, 1534375649047773185, '条', 'strip', 100, NULL, 0, '2022-06-22 11:06:14', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1539444768386363394, 1534375649047773185, '根', 'base', 100, NULL, 0, '2022-06-22 11:06:49', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1539444884954460162, 1534375649047773185, '张', 'leaf', 100, NULL, 0, '2022-06-22 11:07:17', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1539444960623898625, 1534375649047773185, '车', 'car', 100, NULL, 0, '2022-06-22 11:07:35', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1539445033567039490, 1534375649047773185, '箱', 'box', 100, NULL, 0, '2022-06-22 11:07:52', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1539445116857528321, 1534375649047773185, '立方', 'cube', 100, NULL, 0, '2022-06-22 11:08:12', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1539445206221369346, 1534375649047773185, '吨', 'ton', 100, NULL, 0, '2022-06-22 11:08:33', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1539445256955670530, 1534375649047773185, 'KG', 'kg', 100, NULL, 0, '2022-06-22 11:08:45', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1539445340317462529, 1534375649047773185, '副', 'vice', 100, NULL, 0, '2022-06-22 11:09:05', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1539445432814448641, 1534375649047773185, '平方', 'square', 100, NULL, 0, '2022-06-22 11:09:27', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1539445527370838018, 1534375649047773185, '卷', 'roll', 100, NULL, 0, '2022-06-22 11:09:50', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1541592159251587074, 1541591973620080641, '年', '0', 100, NULL, 0, '2022-06-28 09:19:47', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1541592211361619970, 1541591973620080641, '年月', '1', 100, NULL, 0, '2022-06-28 09:19:59', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1541592242990866434, 1541591973620080641, '年月日', '2', 100, NULL, 0, '2022-06-28 09:20:07', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1541592783582126081, 1541592393646071809, '入库单', 'dw_inv', 100, NULL, 0, '2022-06-28 09:22:16', 1265476890672672808, '2022-06-28 09:26:38', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1541593787136466945, 1541592393646071809, '出库单', ' dw_inv ', 100, NULL, 0, '2022-06-28 09:26:15', 1265476890672672808, '2022-06-28 09:26:30', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1541593988588888066, 1541592393646071809, '产品管理', 'dw_pro', 100, NULL, 0, '2022-06-28 09:27:03', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1541594070629474305, 1541592393646071809, '产品类型', 'dw_type', 100, NULL, 0, '2022-06-28 09:27:23', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1541594136652013570, 1541592393646071809, '任务', 'dw_task', 100, NULL, 0, '2022-06-28 09:27:38', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1541594233825648641, 1541592393646071809, '工单', 'dw_work_order', 100, NULL, 0, '2022-06-28 09:28:01', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1541594693559115777, 1541592393646071809, '工艺路线', 'dw_work_route', 100, NULL, 0, '2022-06-28 09:29:51', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1541594900241833985, 1541592393646071809, '工序', 'dw_work_step', 100, NULL, 0, '2022-06-28 09:30:40', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1541700530479316994, 1534371402801537025, '出现划痕', '20220628', 100, NULL, 0, '2022-06-28 16:30:25', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1541700792732368897, 1534371402801537025, '起皱凹陷', '65399', 100, NULL, 0, '2022-06-28 16:31:27', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1541700927822606337, 1534375649047773185, '个', 'indal', 100, NULL, 2, '2022-06-28 16:31:59', 1265476890672672808, '2022-06-28 16:32:11', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1541704912394629121, 1534375649047773185, '片', 'slice', 100, NULL, 0, '2022-06-28 16:47:49', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1561634089585934337, 1534371575181225985, '数字', 'number', 102, NULL, 0, '2022-08-22 16:39:15', 1265476890672672808, '2022-08-26 09:53:09', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1561634156946456578, 1534371575181225985, '时间', 'date', 104, NULL, 0, '2022-08-22 16:39:31', 1265476890672672808, '2022-08-22 16:39:53', 1265476890672672808);

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
                                  `id` bigint(20) NOT NULL COMMENT '主键',
                                  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
                                  `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '编码',
                                  `sort` int(11) NOT NULL COMMENT '排序',
                                  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
                                  `status` tinyint(4) NOT NULL COMMENT '状态（字典 0正常 1停用 2删除）',
                                  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                  `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                  `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统字典类型表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1265216211667636226, '通用状态', 'common_status', 100, '通用状态', 0, '2020-05-26 17:40:26', 1265476890672672808, '2020-06-08 11:31:47', 1265476890672672808);
INSERT INTO `sys_dict_type` VALUES (1265216211667636234, '性别', 'sex', 100, '性别字典', 0, '2020-04-01 10:12:30', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1265216211667636235, '常量的分类', 'consts_type', 100, '常量的分类，用于区别一组配置', 0, '2020-04-14 23:24:13', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1265217074079453185, '是否', 'yes_or_no', 100, '是否', 0, '2020-05-26 17:43:52', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1265217846770913282, '访问类型', 'vis_type', 100, '访问类型', 0, '2020-05-26 17:46:56', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1265221049302372354, '菜单类型', 'menu_type', 100, '菜单类型', 0, '2020-05-26 17:59:39', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1265466149622128641, '发送类型', 'send_type', 100, '发送类型', 0, '2020-05-27 10:13:36', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1265466752209395713, '打开方式', 'open_type', 100, '打开方式', 0, '2020-05-27 10:16:00', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1265467337566461954, '菜单权重', 'menu_weight', 100, '菜单权重', 0, '2020-05-27 10:18:19', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1265468028632571905, '数据范围类型', 'data_scope_type', 100, '数据范围类型', 0, '2020-05-27 10:21:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1265468437904367618, '短信发送来源', 'sms_send_source', 100, '短信发送来源', 0, '2020-05-27 10:22:42', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1275617093517172738, '操作类型', 'op_type', 100, '操作类型', 0, '2020-06-24 10:29:50', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1277774529430654977, '文件存储位置', 'file_storage_location', 100, '文件存储位置', 0, '2020-06-30 09:22:42', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1278606951432855553, '运行状态', 'run_status', 100, '定时任务运行状态', 0, '2020-07-02 16:30:27', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1278911800547147777, '通知公告类型', 'notice_type', 100, '通知公告类型', 0, '2020-07-03 12:41:49', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1278911952657776642, '通知公告状态', 'notice_status', 100, '通知公告状态', 0, '2020-07-03 12:42:25', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1291390159941599233, '是否boolean', 'yes_true_false', 100, '是否boolean', 2, '2020-08-06 23:06:22', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1342445962104864770, '代码生成方式', 'code_gen_create_type', 100, '代码生成方式', 0, '2020-12-25 20:23:53', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1358094419419750401, '代码生成作用类型', 'code_gen_effect_type', 100, '代码生成作用类型', 0, '2021-02-07 00:45:16', 1265476890672672808, '2021-02-08 00:47:48', 1265476890672672808);
INSERT INTO `sys_dict_type` VALUES (1358457818733428737, '代码生成查询类型', 'code_gen_query_type', 100, '代码生成查询类型', 0, '2021-02-08 00:49:18', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1358470065111252994, '代码生成java类型', 'code_gen_java_type', 100, '代码生成java类型', 0, '2021-02-08 01:37:57', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1534370205881409537, '入库类型', 'in_type', 100, '入库类型', 0, '2022-06-08 11:02:19', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1534370298088988673, '出库类型', 'out_type', 100, '出库类型', 0, '2022-06-08 11:02:41', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1534371402801537025, '不良品项', 'bad_item', 100, '不良品项', 0, '2022-06-08 11:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1534371575181225985, '字段类型', 'field_type', 100, NULL, 0, '2022-06-08 11:07:45', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1534375649047773185, '库存单位', 'unit', 100, '库存单位', 0, '2022-06-08 11:23:57', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1541591973620080641, '时间类型单选', 'time_type', 100, NULL, 0, '2022-06-28 09:19:03', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1541592393646071809, '自定义表名', 'cus_table_name', 100, NULL, 0, '2022-06-28 09:20:43', 1265476890672672808, NULL, NULL);

-- ----------------------------
-- Table structure for sys_emp
-- ----------------------------
DROP TABLE IF EXISTS `sys_emp`;
CREATE TABLE `sys_emp`  (
                            `id` bigint(20) NOT NULL COMMENT '主键',
                            `job_num` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '工号',
                            `org_id` bigint(20) NOT NULL COMMENT '所属机构id',
                            `org_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '所属机构名称',
                            PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '员工表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_emp
-- ----------------------------
INSERT INTO `sys_emp` VALUES (1275735541155614721, '001', 1265476890672672769, '华夏集团新疆分公司');
INSERT INTO `sys_emp` VALUES (1280700700074041345, '110', 1265476890672672771, '研发部');
INSERT INTO `sys_emp` VALUES (1280709549107552257, NULL, 1265476890672672770, '华夏集团成都分公司');
INSERT INTO `sys_emp` VALUES (1471457941179072513, NULL, 1265476890672672770, '华夏集团成都分公司');
INSERT INTO `sys_emp` VALUES (1551457178297200641, NULL, 1265476890651701250, '华夏集团');
INSERT INTO `sys_emp` VALUES (1551462379850723329, NULL, 1265476890651701250, '华夏集团');
INSERT INTO `sys_emp` VALUES (1560148932795891714, NULL, 1265476890651701250, '华夏集团');
INSERT INTO `sys_emp` VALUES (1560149290020569089, NULL, 1265476890651701250, '华夏集团');
INSERT INTO `sys_emp` VALUES (1560149788173860866, NULL, 1265476890651701250, '华夏集团');
INSERT INTO `sys_emp` VALUES (1560149962497523714, NULL, 1265476890651701250, '华夏集团');
INSERT INTO `sys_emp` VALUES (1574927397500977154, NULL, 1265476890651701250, '华夏集团');

-- ----------------------------
-- Table structure for sys_emp_ext_org_pos
-- ----------------------------
DROP TABLE IF EXISTS `sys_emp_ext_org_pos`;
CREATE TABLE `sys_emp_ext_org_pos`  (
                                        `id` bigint(20) NOT NULL COMMENT '主键',
                                        `emp_id` bigint(20) NOT NULL COMMENT '员工id',
                                        `org_id` bigint(20) NOT NULL COMMENT '机构id',
                                        `pos_id` bigint(20) NOT NULL COMMENT '岗位id',
                                        PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '员工附属机构岗位表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_emp_ext_org_pos
-- ----------------------------

-- ----------------------------
-- Table structure for sys_emp_pos
-- ----------------------------
DROP TABLE IF EXISTS `sys_emp_pos`;
CREATE TABLE `sys_emp_pos`  (
                                `id` bigint(20) NOT NULL COMMENT '主键',
                                `emp_id` bigint(20) NOT NULL COMMENT '员工id',
                                `pos_id` bigint(20) NOT NULL COMMENT '职位id',
                                PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '员工职位关联表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_emp_pos
-- ----------------------------
INSERT INTO `sys_emp_pos` VALUES (1280710828479324161, 1280700700074041345, 1265476890672672790);
INSERT INTO `sys_emp_pos` VALUES (1471457757112041474, 1280709549107552257, 1265476890672672788);
INSERT INTO `sys_emp_pos` VALUES (1471457941179072514, 1471457941179072513, 1265476890672672788);
INSERT INTO `sys_emp_pos` VALUES (1528561867151138818, 1275735541155614721, 1265476890672672787);
INSERT INTO `sys_emp_pos` VALUES (1551457178603384834, 1551457178297200641, 1265476890672672787);
INSERT INTO `sys_emp_pos` VALUES (1551462380186267650, 1551462379850723329, 1265476890672672787);
INSERT INTO `sys_emp_pos` VALUES (1560148934280675330, 1560148932795891714, 1265476890672672790);
INSERT INTO `sys_emp_pos` VALUES (1560149291710873601, 1560149290020569089, 1265476890672672787);
INSERT INTO `sys_emp_pos` VALUES (1560149790174543874, 1560149788173860866, 1265476890672672790);
INSERT INTO `sys_emp_pos` VALUES (1560149962925342722, 1560149962497523714, 1265476890672672790);
INSERT INTO `sys_emp_pos` VALUES (1574927397639389185, 1574927397500977154, 1265476890672672787);

-- ----------------------------
-- Table structure for sys_file_info
-- ----------------------------
DROP TABLE IF EXISTS `sys_file_info`;
CREATE TABLE `sys_file_info`  (
                                  `id` bigint(20) NOT NULL COMMENT '主键id',
                                  `file_location` tinyint(4) NOT NULL COMMENT '文件存储位置（1:阿里云，2:腾讯云，3:minio，4:本地）',
                                  `file_bucket` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件仓库',
                                  `file_origin_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文件名称（上传时候的文件名）',
                                  `file_suffix` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件后缀',
                                  `file_size_kb` bigint(20) NULL DEFAULT NULL COMMENT '文件大小kb',
                                  `file_size_info` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件大小信息，计算后的',
                                  `file_object_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '存储到bucket的名称（文件唯一标识id）',
                                  `file_path` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '存储路径',
                                  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                  `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建用户',
                                  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                                  `update_user` bigint(20) NULL DEFAULT NULL COMMENT '修改用户',
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '文件信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_file_info
-- ----------------------------

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
                             `id` bigint(20) NOT NULL COMMENT '主键',
                             `pid` bigint(20) NOT NULL COMMENT '父id',
                             `pids` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '父ids',
                             `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
                             `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '编码',
                             `type` tinyint(4) NOT NULL DEFAULT 1 COMMENT '菜单类型（字典 0目录 1菜单 2按钮）',
                             `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图标',
                             `router` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '路由地址',
                             `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '组件地址',
                             `permission` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限标识',
                             `application` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '应用分类（应用编码）',
                             `open_type` tinyint(4) NOT NULL COMMENT '打开方式（字典 0无 1组件 2内链 3外链）',
                             `visible` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '是否可见（Y-是，N-否）',
                             `link` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '链接地址',
                             `redirect` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '重定向地址',
                             `weight` tinyint(4) NULL DEFAULT NULL COMMENT '权重（字典 1系统权重 2业务权重）',
                             `sort` int(11) NOT NULL COMMENT '排序',
                             `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
                             `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '状态（字典 0正常 1停用 2删除）',
                             `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                             `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                             `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                             `update_user` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
                             PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统菜单表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, 1560145190851772417, '[0],', '用户查询（自）', 'sys_user_mgr_page_g', 2, NULL, NULL, NULL, 'sysUser:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 16:36:49', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (2, 1560145190851772417, '[0],', '用户编辑（自）', 'sys_user_mgr_edit_g', 2, NULL, NULL, NULL, 'sysUser:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 12:20:23', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (3, 1560145190851772417, '[0],', '用户增加（自）', 'sys_user_mgr_add_g', 2, NULL, NULL, NULL, 'sysUser:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 16:37:35', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (4, 1560145190851772417, '[0],', '用户删除（自）', 'sys_user_mgr_delete_g', 2, NULL, NULL, NULL, 'sysUser:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 16:37:58', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5, 1560145190851772417, '[0],', '用户详情（自）', 'sys_user_mgr_detail_g', 2, NULL, NULL, NULL, 'sysUser:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 16:38:25', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6, 1560145190851772417, '[0],', '用户导出（自）', 'sys_user_mgr_export_g', 2, NULL, NULL, NULL, 'sysUser:export', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 12:21:59', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7, 1560145190851772417, '[0],', '用户选择器（自）', 'sys_user_mgr_selector_g', 2, NULL, NULL, NULL, 'sysUser:selector', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-03 13:30:14', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8, 1560145190851772417, '[0],', '用户授权角色（自）', 'sys_user_mgr_grant_role_g', 2, NULL, NULL, NULL, 'sysUser:grantRole', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 09:22:01', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (9, 1560145190851772417, '[0],', '用户拥有角色（自）', 'sys_user_mgr_own_role_g', 2, NULL, NULL, NULL, 'sysUser:ownRole', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-29 14:27:22', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (10, 1560145190851772417, '[0],', '用户授权数据（自）', 'sys_user_mgr_grant_data_g', 2, NULL, NULL, NULL, 'sysUser:grantData', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 09:22:13', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (11, 1560145190851772417, '[0],', '用户拥有数据（自）', 'sys_user_mgr_own_data_g', 2, NULL, NULL, NULL, 'sysUser:ownData', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-29 14:27:41', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (12, 1560145190851772417, '[0],', '用户更新信息（自）', 'sys_user_mgr_update_info_g', 2, NULL, NULL, NULL, 'sysUser:updateInfo', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 16:19:32', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (13, 1560145190851772417, '[0],', '用户修改密码（自）', 'sys_user_mgr_update_pwd_g', 2, NULL, NULL, NULL, 'sysUser:updatePwd', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 16:20:25', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (14, 1560145190851772417, '[0],', '用户修改状态（自）', 'sys_user_mgr_change_status_g', 2, NULL, NULL, NULL, 'sysUser:changeStatus', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-23 11:13:14', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (15, 1560145190851772417, '[0],', '用户修改头像（自）', 'sys_user_mgr_update_avatar_g', 2, NULL, NULL, NULL, 'sysUser:updateAvatar', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 12:21:42', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (16, 1560145190851772417, '[0],', '用户重置密码（自）', 'sys_user_mgr_reset_pwd_g', 2, NULL, NULL, NULL, 'sysUser:resetPwd', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-29 15:01:51', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (17, 1560145190851772417, '[0],', '机构查询（自）', 'sys_org_mgr_page_g', 2, NULL, NULL, NULL, 'sysOrg:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 17:17:37', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (18, 1560145190851772417, '[0],', '机构列表（自）', 'sys_org_mgr_list_g', 2, NULL, NULL, NULL, 'sysOrg:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 11:54:26', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (19, 1560145190851772417, '[0],', '机构增加（自）', 'sys_org_mgr_add_g', 2, NULL, NULL, NULL, 'sysOrg:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 17:19:53', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (20, 1560145190851772417, '[0],', '机构编辑（自）', 'sys_org_mgr_edit_g', 2, NULL, NULL, NULL, 'sysOrg:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 11:54:37', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (21, 1560145190851772417, '[0],', '机构删除（自）', 'sys_org_mgr_delete_g', 2, NULL, NULL, NULL, 'sysOrg:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 17:20:48', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (22, 1560145190851772417, '[0],', '机构详情（自）', 'sys_org_mgr_detail_g', 2, NULL, NULL, NULL, 'sysOrg:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 17:21:15', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (23, 1560145190851772417, '[0],', '机构树（自）', 'sys_org_mgr_tree_g', 2, NULL, NULL, NULL, 'sysOrg:tree', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 17:21:58', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (24, 1560145190851772417, '[0],', '职位查询（自）', 'sys_pos_mgr_page_g', 2, NULL, NULL, NULL, 'sysPos:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 18:41:48', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (25, 1560145190851772417, '[0],', '职位列表（自）', 'sys_pos_mgr_list_g', 2, NULL, NULL, NULL, 'sysPos:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 11:55:57', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (26, 1560145190851772417, '[0],', '职位增加（自）', 'sys_pos_mgr_add_g', 2, NULL, NULL, NULL, 'sysPos:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 18:42:20', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (27, 1560145190851772417, '[0],', '职位编辑（自）', 'sys_pos_mgr_edit_g', 2, NULL, NULL, NULL, 'sysPos:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 11:56:08', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (28, 1560145190851772417, '[0],', '职位删除（自）', 'sys_pos_mgr_delete_g', 2, NULL, NULL, NULL, 'sysPos:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 18:42:39', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (29, 1560145190851772417, '[0],', '职位详情（自）', 'sys_pos_mgr_detail_g', 2, NULL, NULL, NULL, 'sysPos:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 18:43:00', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (30, 1560145190851772417, '[0],', '应用查询（自）', 'sys_app_mgr_page_g', 2, NULL, NULL, NULL, 'sysApp:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 16:41:58', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (31, 1560145190851772417, '[0],', '应用列表（自）', 'sys_app_mgr_list_g', 2, NULL, NULL, NULL, 'sysApp:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 10:04:59', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (32, 1560145190851772417, '[0],', '应用增加（自）', 'sys_app_mgr_add_g', 2, NULL, NULL, NULL, 'sysApp:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 16:44:10', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (33, 1560145190851772417, '[0],', '应用编辑（自）', 'sys_app_mgr_edit_g', 2, NULL, NULL, NULL, 'sysApp:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 10:04:34', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (34, 1560145190851772417, '[0],', '应用删除（自）', 'sys_app_mgr_delete_g', 2, NULL, NULL, NULL, 'sysApp:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 17:14:29', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (35, 1560145190851772417, '[0],', '应用详情（自）', 'sys_app_mgr_detail_g', 2, NULL, NULL, NULL, 'sysApp:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 17:14:56', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (36, 1560145190851772417, '[0],', '设为默认应用（自）', 'sys_app_mgr_set_as_default_g', 2, NULL, NULL, NULL, 'sysApp:setAsDefault', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 17:14:56', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (37, 1560145190851772417, '[0],', '菜单列表（自）', 'sys_menu_mgr_list_g', 2, NULL, NULL, NULL, 'sysMenu:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 18:45:20', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (38, 1560145190851772417, '[0],', '菜单增加（自）', 'sys_menu_mgr_add_g', 2, NULL, NULL, NULL, 'sysMenu:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 18:45:37', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (39, 1560145190851772417, '[0],', '菜单编辑（自）', 'sys_menu_mgr_edit_g', 2, NULL, NULL, NULL, 'sysMenu:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 11:52:00', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (40, 1560145190851772417, '[0],', '菜单删除（自）', 'sys_menu_mgr_delete_g', 2, NULL, NULL, NULL, 'sysMenu:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 18:46:01', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (41, 1560145190851772417, '[0],', '菜单详情（自）', 'sys_menu_mgr_detail_g', 2, NULL, NULL, NULL, 'sysMenu:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 18:46:22', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (42, 1560145190851772417, '[0],', '菜单授权树（自）', 'sys_menu_mgr_grant_tree_g', 2, NULL, NULL, NULL, 'sysMenu:treeForGrant', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-03 09:50:31', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (43, 1560145190851772417, '[0],', '菜单树（自）', 'sys_menu_mgr_tree_g', 2, NULL, NULL, NULL, 'sysMenu:tree', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 18:47:50', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (44, 1560145190851772417, '[0],', '菜单切换（自）', 'sys_menu_mgr_change_g', 2, NULL, NULL, NULL, 'sysMenu:change', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-03 09:51:43', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (45, 1560145190851772417, '[0],', '角色查询（自）', 'sys_role_mgr_page_g', 2, NULL, NULL, NULL, 'sysRole:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-28 16:02:09', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (46, 1560145190851772417, '[0],', '角色增加（自）', 'sys_role_mgr_add_g', 2, NULL, NULL, NULL, 'sysRole:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-28 16:02:27', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (47, 1560145190851772417, '[0],', '角色编辑（自）', 'sys_role_mgr_edit_g', 2, NULL, NULL, NULL, 'sysRole:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 11:57:27', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (48, 1560145190851772417, '[0],', '角色删除（自）', 'sys_role_mgr_delete_g', 2, NULL, NULL, NULL, 'sysRole:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-28 16:02:46', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (49, 1560145190851772417, '[0],', '角色详情（自）', 'sys_role_mgr_detail_g', 2, NULL, NULL, NULL, 'sysRole:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-28 16:03:01', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (50, 1560145190851772417, '[0],', '角色下拉（自）', 'sys_role_mgr_drop_down_g', 2, NULL, NULL, NULL, 'sysRole:dropDown', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-29 15:45:39', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (51, 1560145190851772417, '[0],', '角色授权菜单（自）', 'sys_role_mgr_grant_menu_g', 2, NULL, NULL, NULL, 'sysRole:grantMenu', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 09:16:27', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (52, 1560145190851772417, '[0],', '角色拥有菜单（自）', 'sys_role_mgr_own_menu_g', 2, NULL, NULL, NULL, 'sysRole:ownMenu', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-29 14:21:54', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (53, 1560145190851772417, '[0],', '角色授权数据（自）', 'sys_role_mgr_grant_data_g', 2, NULL, NULL, NULL, 'sysRole:grantData', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 09:16:56', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (54, 1560145190851772417, '[0],', '角色拥有数据（自）', 'sys_role_mgr_own_data_g', 2, NULL, NULL, NULL, 'sysRole:ownData', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-29 14:23:08', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (55, 1560145190851772417, '[0],', '配置查询（自）', 'system_tools_config_page_g', 2, NULL, NULL, NULL, 'sysConfig:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-27 17:02:22', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (56, 1560145190851772417, '[0],', '配置列表（自）', 'system_tools_config_list_g', 2, NULL, NULL, NULL, 'sysConfig:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-27 17:02:42', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (57, 1560145190851772417, '[0],', '配置增加（自）', 'system_tools_config_add_g', 2, NULL, NULL, NULL, 'sysConfig:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-27 17:03:31', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (58, 1560145190851772417, '[0],', '配置编辑（自）', 'system_tools_config_edit_g', 2, NULL, NULL, NULL, 'sysConfig:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-27 17:03:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (59, 1560145190851772417, '[0],', '配置删除（自）', 'system_tools_config_delete_g', 2, NULL, NULL, NULL, 'sysConfig:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-27 17:03:44', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (60, 1560145190851772417, '[0],', '配置详情（自）', 'system_tools_config_detail_g', 2, NULL, NULL, NULL, 'sysConfig:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-27 17:02:59', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (61, 1560145190851772417, '[0],', '发送文本邮件（自）', 'sys_email_mgr_send_email_g', 2, NULL, NULL, NULL, 'email:sendEmail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 11:45:39', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (62, 1560145190851772417, '[0],', '发送html邮件（自）', 'sys_email_mgr_send_email_html_g', 2, NULL, NULL, NULL, 'email:sendEmailHtml', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 11:45:57', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (63, 1560145190851772417, '[0],', '短信发送查询（自）', 'sys_sms_mgr_page_g', 2, NULL, NULL, NULL, 'sms:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 12:16:56', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (64, 1560145190851772417, '[0],', '发送验证码短信（自）', 'sys_sms_mgr_send_login_message_g', 2, NULL, NULL, NULL, 'sms:sendLoginMessage', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 12:02:31', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (65, 1560145190851772417, '[0],', '验证短信验证码（自）', 'sys_sms_mgr_validate_message_g', 2, NULL, NULL, NULL, 'sms:validateMessage', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 12:02:50', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (66, 1560145190851772417, '[0],', '字典类型查询（自）', 'sys_dict_mgr_dict_type_page_g', 2, NULL, NULL, NULL, 'sysDictType:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 11:20:22', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (67, 1560145190851772417, '[0],', '字典类型列表（自）', 'sys_dict_mgr_dict_type_list_g', 2, NULL, NULL, NULL, 'sysDictType:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-29 15:12:35', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (68, 1560145190851772417, '[0],', '字典类型增加（自）', 'sys_dict_mgr_dict_type_add_g', 2, NULL, NULL, NULL, 'sysDictType:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 11:19:58', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (69, 1560145190851772417, '[0],', '字典类型删除（自）', 'sys_dict_mgr_dict_type_delete_g', 2, NULL, NULL, NULL, 'sysDictType:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 11:21:30', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (70, 1560145190851772417, '[0],', '字典类型编辑（自）', 'sys_dict_mgr_dict_type_edit_g', 2, NULL, NULL, NULL, 'sysDictType:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 11:21:42', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (71, 1560145190851772417, '[0],', '字典类型详情（自）', 'sys_dict_mgr_dict_type_detail_g', 2, NULL, NULL, NULL, 'sysDictType:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 11:22:06', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (72, 1560145190851772417, '[0],', '字典类型下拉（自）', 'sys_dict_mgr_dict_type_drop_down_g', 2, NULL, NULL, NULL, 'sysDictType:dropDown', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 11:22:23', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (73, 1560145190851772417, '[0],', '字典类型修改状态（自）', 'sys_dict_mgr_dict_type_change_status_g', 2, NULL, NULL, NULL, 'sysDictType:changeStatus', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-23 11:15:50', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (74, 1560145190851772417, '[0],', '字典值查询（自）', 'sys_dict_mgr_dict_page_g', 2, NULL, NULL, NULL, 'sysDictData:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 11:23:11', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (75, 1560145190851772417, '[0],', '字典值列表（自）', 'sys_dict_mgr_dict_list_g', 2, NULL, NULL, NULL, 'sysDictData:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 11:24:58', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (76, 1560145190851772417, '[0],', '字典值增加（自）', 'sys_dict_mgr_dict_add_g', 2, NULL, NULL, NULL, 'sysDictData:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 11:22:51', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (77, 1560145190851772417, '[0],', '字典值删除（自）', 'sys_dict_mgr_dict_delete_g', 2, NULL, NULL, NULL, 'sysDictData:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 11:23:26', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (78, 1560145190851772417, '[0],', '字典值编辑（自）', 'sys_dict_mgr_dict_edit_g', 2, NULL, NULL, NULL, 'sysDictData:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 11:24:21', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (79, 1560145190851772417, '[0],', '字典值详情（自）', 'sys_dict_mgr_dict_detail_g', 2, NULL, NULL, NULL, 'sysDictData:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 11:24:42', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (80, 1560145190851772417, '[0],', '字典值修改状态（自）', 'sys_dict_mgr_dict_change_status_g', 2, NULL, NULL, NULL, 'sysDictData:changeStatus', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-23 11:17:53', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (81, 1560145190851772417, '[0],', '访问日志查询（自）', 'sys_log_mgr_vis_log_page_g', 2, NULL, NULL, NULL, 'sysVisLog:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 09:55:51', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (82, 1560145190851772417, '[0],', '访问日志清空（自）', 'sys_log_mgr_vis_log_delete_g', 2, NULL, NULL, NULL, 'sysVisLog:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 09:56:57', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (83, 1560145190851772417, '[0],', '操作日志查询（自）', 'sys_log_mgr_op_log_page_g', 2, NULL, NULL, NULL, 'sysOpLog:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 09:57:39', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (84, 1560145190851772417, '[0],', '操作日志清空（自）', 'sys_log_mgr_op_log_delete_g', 2, NULL, NULL, NULL, 'sysOpLog:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 09:58:13', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (85, 1560145190851772417, '[0],', '服务监控查询（自）', 'sys_monitor_mgr_machine_monitor_query_g', 2, NULL, NULL, NULL, 'sysMachine:query', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-05 16:05:33', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (86, 1560145190851772417, '[0],', '在线用户列表（自）', 'sys_monitor_mgr_online_user_list_g', 2, NULL, NULL, NULL, 'sysOnlineUser:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-05 16:03:46', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (87, 1560145190851772417, '[0],', '在线用户强退（自）', 'sys_monitor_mgr_online_user_force_exist_g', 2, NULL, NULL, NULL, 'sysOnlineUser:forceExist', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-05 16:04:16', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (88, 1560145190851772417, '[0],', '公告查询（自）', 'sys_notice_mgr_page_g', 2, NULL, NULL, NULL, 'sysNotice:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-29 15:45:30', 1265476890672672808, '2022-07-18 21:17:05', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (89, 1560145190851772417, '[0],', '公告增加（自）', 'sys_notice_mgr_add_g', 2, NULL, NULL, NULL, 'sysNotice:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-29 15:45:57', 1265476890672672808, '2022-07-18 21:17:05', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (90, 1560145190851772417, '[0],', '公告编辑（自）', 'sys_notice_mgr_edit_g', 2, NULL, NULL, NULL, 'sysNotice:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-29 15:46:22', 1265476890672672808, '2022-07-18 21:17:05', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (91, 1560145190851772417, '[0],', '公告删除（自）', 'sys_notice_mgr_delete_g', 2, NULL, NULL, NULL, 'sysNotice:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-29 15:46:11', 1265476890672672808, '2022-07-18 21:17:05', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (92, 1560145190851772417, '[0],', '公告查看（自）', 'sys_notice_mgr_detail_g', 2, NULL, NULL, NULL, 'sysNotice:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-29 15:46:33', 1265476890672672808, '2022-07-18 21:17:05', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (93, 1560145190851772417, '[0],', '公告修改状态（自）', 'sys_notice_mgr_changeStatus_g', 2, NULL, NULL, NULL, 'sysNotice:changeStatus', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-29 15:46:50', 1265476890672672808, '2022-07-18 21:17:05', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (94, 1560145190851772417, '[0],', '已收公告查询（自）', 'sys_notice_mgr_received_page_g', 2, NULL, NULL, NULL, 'sysNotice:received', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-29 16:33:43', 1265476890672672808, '2022-07-18 21:17:05', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (95, 1560145190851772417, '[0],', '文件查询（自）', 'sys_file_mgr_sys_file_page_g', 2, NULL, NULL, NULL, 'sysFileInfo:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-24 17:35:38', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (96, 1560145190851772417, '[0],', '文件列表（自）', 'sys_file_mgr_sys_file_list_g', 2, NULL, NULL, NULL, 'sysFileInfo:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-24 17:35:49', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (97, 1560145190851772417, '[0],', '文件删除（自）', 'sys_file_mgr_sys_file_delete_g', 2, NULL, NULL, NULL, 'sysFileInfo:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-24 17:36:11', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (98, 1560145190851772417, '[0],', '文件详情（自）', 'sys_file_mgr_sys_file_detail_g', 2, NULL, NULL, NULL, 'sysFileInfo:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-24 17:36:01', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (99, 1560145190851772417, '[0],', '文件上传（自）', 'sys_file_mgr_sys_file_upload_g', 2, NULL, NULL, NULL, 'sysFileInfo:upload', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-24 17:34:29', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (100, 1560145190851772417, '[0],', '文件下载（自）', 'sys_file_mgr_sys_file_download_g', 2, NULL, NULL, NULL, 'sysFileInfo:download', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-24 17:34:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (101, 1560145190851772417, '[0],', '图片预览（自）', 'sys_file_mgr_sys_file_preview_g', 2, NULL, NULL, NULL, 'sysFileInfo:preview', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-24 17:35:19', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (102, 1560145190851772417, '[0],', '定时任务查询（自）', 'sys_timers_mgr_page_g', 2, NULL, NULL, NULL, 'sysTimers:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-01 17:19:43', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (103, 1560145190851772417, '[0],', '定时任务列表（自）', 'sys_timers_mgr_list_g', 2, NULL, NULL, NULL, 'sysTimers:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-01 17:19:56', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (104, 1560145190851772417, '[0],', '定时任务详情（自）', 'sys_timers_mgr_detail_g', 2, NULL, NULL, NULL, 'sysTimers:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-01 17:20:10', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (105, 1560145190851772417, '[0],', '定时任务增加（自）', 'sys_timers_mgr_add_g', 2, NULL, NULL, NULL, 'sysTimers:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-01 17:20:23', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (106, 1560145190851772417, '[0],', '定时任务删除（自）', 'sys_timers_mgr_delete_g', 2, NULL, NULL, NULL, 'sysTimers:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-01 17:20:33', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (107, 1560145190851772417, '[0],', '定时任务编辑（自）', 'sys_timers_mgr_edit_g', 2, NULL, NULL, NULL, 'sysTimers:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-01 17:20:43', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (108, 1560145190851772417, '[0],', '定时任务可执行列表（自）', 'sys_timers_mgr_get_action_classes_g', 2, NULL, NULL, NULL, 'sysTimers:getActionClasses', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-01 17:22:16', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (109, 1560145190851772417, '[0],', '定时任务启动（自）', 'sys_timers_mgr_start_g', 2, NULL, NULL, NULL, 'sysTimers:start', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-01 17:22:32', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (110, 1560145190851772417, '[0],', '定时任务关闭（自）', 'sys_timers_mgr_stop_g', 2, NULL, NULL, NULL, 'sysTimers:stop', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-01 17:22:43', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (111, 1560145190851772417, '[0],', '系统区域列表（自）', 'sys_area_mgr_list_g', 2, NULL, NULL, NULL, 'sysArea:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2021-05-19 14:01:39', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (112, 1560145190851772417, '[0],', '报工审批（自）', 'work_report_approval_g', 2, '', '', '', 'workReport:approval', 'system', 0, 'Y', '', '', 1, 100, NULL, 0, '2022-06-30 15:29:04', 1265476890672672808, '2022-06-30 15:29:19', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (113, 1560145190851772417, '[0],', '产品类型树（自）', 'protype_index_tree_g', 2, '', '', '', 'proType:tree', 'system', 0, 'Y', '', '', 2, 100, NULL, 0, '2022-07-25 16:13:04', 1265476890672672808, '2022-07-25 16:13:37', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (114, 1560145190851772417, '[0],', '字段配置新增（自）', 'fieldconfig_index_add_g', 2, NULL, NULL, NULL, 'fieldConfig:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (115, 1560145190851772417, '[0],', '客户分类查询（自）', 'cussort_index_page_g', 2, NULL, NULL, NULL, 'cusSort:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (116, 1560145190851772417, '[0],', '供应商分类新增（自）', 'suppsort_index_add_g', 2, NULL, NULL, NULL, 'suppSort:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:25', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (117, 1560145190851772417, '[0],', '采购细明导出（自）', 'puordetail_index_export_g', 2, NULL, NULL, NULL, 'puorDetail:export', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (118, 1560145190851772417, '[0],', '物料清单编辑（自）', 'bom_index_edit_g', 2, NULL, NULL, NULL, 'bom:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:26:55', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (119, 1560145190851772417, '[0],', '生产计划编辑（自）', 'proplan_index_edit_g', 2, NULL, NULL, NULL, 'proPlan:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-21 16:58:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (120, 1560145190851772417, '[0],', '自定义编号规则查询（自）', 'customcode_index_page_g', 2, NULL, NULL, NULL, 'customCode:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (121, 1560145190851772417, '[0],', '工单表新增（自）', 'workorder_index_add_g', 2, NULL, NULL, NULL, 'workOrder:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:18', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (122, 1560145190851772417, '[0],', '产品表列表（自）', 'pro_index_list_g', 2, NULL, NULL, NULL, 'pro:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:36:37', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (123, 1560145190851772417, '[0],', '采购细明删除（自）', 'puordetail_index_delete_g', 2, NULL, NULL, NULL, 'puorDetail:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (124, 1560145190851772417, '[0],', '物料清单列表（自）', 'bom_index_list_g', 2, NULL, NULL, NULL, 'bom:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:26:55', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (125, 1560145190851772417, '[0],', '客户分类导出（自）', 'cussort_index_export_g', 2, NULL, NULL, NULL, 'cusSort:export', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (126, 1560145190851772417, '[0],', '出库单列表（自）', 'invOut_index_list_g', 2, NULL, NULL, NULL, 'invOut:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:48:27', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (127, 1560145190851772417, '[0],', '出入库明细新增（自）', 'invdetail_index_add_g', 2, NULL, NULL, NULL, 'invDetail:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-10 16:05:47', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (128, 1560145190851772417, '[0],', '任务列表（自）', 'task_index_list_g', 2, NULL, NULL, NULL, 'task:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:30', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (129, 1560145190851772417, '[0],', '生产计划导出（自）', 'proplan_index_export_g', 2, NULL, NULL, NULL, 'proPlan:export', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-21 16:58:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (130, 1560145190851772417, '[0],', '供应商联系人查询（自）', 'suppperson_index_page_g', 2, NULL, NULL, NULL, 'suppPerson:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:27:59', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (131, 1560145190851772417, '[0],', '工艺路线列表（自）', 'workroute_index_list_g', 2, NULL, NULL, NULL, 'workRoute:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:34', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (132, 1560145190851772417, '[0],', 'SaaS租户删除（自）', 'tenantinfo_index_delete_g', 2, NULL, NULL, NULL, 'tenantInfo:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (133, 1560145190851772417, '[0],', '产品类型表列表（自）', 'protype_index_list_g', 2, NULL, NULL, NULL, 'proType:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 12:00:45', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (134, 1560145190851772417, '[0],', '采购细明查询（自）', 'puordetail_index_page_g', 2, NULL, NULL, NULL, 'puorDetail:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (135, 1560145190851772417, '[0],', ' 供应商资料查询（自）', 'suppdata_index_page_g', 2, NULL, NULL, NULL, 'suppData:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:14', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (136, 1560145190851772417, '[0],', '供应商联系人新增（自）', 'suppperson_index_add_g', 2, NULL, NULL, NULL, 'suppPerson:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:27:59', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (137, 1560145190851772417, '[0],', '生产计划新增（自）', 'proplan_index_add_g', 2, NULL, NULL, NULL, 'proPlan:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-21 16:58:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (138, 1560145190851772417, '[0],', '工艺路线查询（自）', 'workroute_index_page_g', 2, NULL, NULL, NULL, 'workRoute:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:34', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (139, 1560145190851772417, '[0],', '采购订单删除（自）', 'puororder_index_delete_g', 2, NULL, NULL, NULL, 'puorOrder:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (140, 1560145190851772417, '[0],', '客户资料新增（自）', 'cusinfor_index_add_g', 2, NULL, NULL, NULL, 'cusInfor:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:34', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (141, 1560145190851772417, '[0],', '产品表编辑（自）', 'pro_index_edit_g', 2, NULL, NULL, NULL, 'pro:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:36:37', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (142, 1560145190851772417, '[0],', '工单任务关系表删除（自）', 'workordertask_index_delete_g', 2, NULL, NULL, NULL, 'workOrderTask:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:44:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (143, 1560145190851772417, '[0],', '客户联系人查看（自）', 'cusperson_index_detail_g', 2, NULL, NULL, NULL, 'cusPerson:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:42', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (144, 1560145190851772417, '[0],', '报工编辑（自）', 'workreport_index_edit_g', 2, NULL, NULL, NULL, 'workReport:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:49', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (145, 1560145190851772417, '[0],', '采购订单编辑（自）', 'puororder_index_edit_g', 2, NULL, NULL, NULL, 'puorOrder:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (146, 1560145190851772417, '[0],', '客户资料删除（自）', 'cusinfor_index_delete_g', 2, NULL, NULL, NULL, 'cusInfor:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:34', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (147, 1560145190851772417, '[0],', '销售订单删除（自）', 'saleorder_index_delete_g', 2, NULL, NULL, NULL, 'saleOrder:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 11:17:42', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (148, 1560145190851772417, '[0],', '库存余额导出（自）', 'stockbalance_index_export_g', 2, NULL, NULL, NULL, 'stockBalance:export', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-28 16:50:46', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (149, 1560145190851772417, '[0],', '供应商联系人查看（自）', 'suppperson_index_detail_g', 2, NULL, NULL, NULL, 'suppPerson:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:27:59', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (150, 1560145190851772417, '[0],', '库存余额查询（自）', 'stockbalance_index_page_g', 2, NULL, NULL, NULL, 'stockBalance:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-28 16:50:46', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (151, 1560145190851772417, '[0],', '自定义编号规则新增（自）', 'customcode_index_add_g', 2, NULL, NULL, NULL, 'customCode:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (152, 1560145190851772417, '[0],', '生产计划查询（自）', 'proplan_index_page_g', 2, NULL, NULL, NULL, 'proPlan:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-21 16:58:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (153, 1560145190851772417, '[0],', '产品类型表导出（自）', 'protype_index_export_g', 2, NULL, NULL, NULL, 'proType:export', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 12:00:45', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (154, 1560145190851772417, '[0],', '仓库管理查询（自）', 'warehouse_index_page_g', 2, NULL, NULL, NULL, 'wareHouse:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 16:37:01', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (155, 1560145190851772417, '[0],', '客户联系人删除（自）', 'cusperson_index_delete_g', 2, NULL, NULL, NULL, 'cusPerson:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:42', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (156, 1560145190851772417, '[0],', '入库单导出（自）', 'invIn_index_export_g', 2, NULL, NULL, NULL, 'invIn:export', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:49:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (157, 1560145190851772417, '[0],', '任务编辑（自）', 'task_index_edit_g', 2, NULL, NULL, NULL, 'task:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:30', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (158, 1560145190851772417, '[0],', '自定义编号规则删除（自）', 'customcode_index_delete_g', 2, NULL, NULL, NULL, 'customCode:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (159, 1560145190851772417, '[0],', '工单任务关系表查询（自）', 'workordertask_index_page_g', 2, NULL, NULL, NULL, 'workOrderTask:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:44:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (160, 1560145190851772417, '[0],', '工序新增（自）', 'workstep_index_add_g', 2, NULL, NULL, NULL, 'workStep:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:13', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (161, 1560145190851772417, '[0],', '仓库管理导出（自）', 'warehouse_index_export_g', 2, NULL, NULL, NULL, 'wareHouse:export', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 16:37:01', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (162, 1560145190851772417, '[0],', '报工列表（自）', 'workreport_index_list_g', 2, NULL, NULL, NULL, 'workReport:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:49', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (163, 1560145190851772417, '[0],', '工艺路线编辑（自）', 'workroute_index_edit_g', 2, NULL, NULL, NULL, 'workRoute:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:34', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (164, 1560145190851772417, '[0],', '仓库管理新增（自）', 'warehouse_index_add_g', 2, NULL, NULL, NULL, 'wareHouse:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 16:37:01', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (165, 1560145190851772417, '[0],', '出入库明细查询（自）', 'invdetail_index_page_g', 2, NULL, NULL, NULL, 'invDetail:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-10 16:05:47', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (166, 1560145190851772417, '[0],', '工序路线关系表查询（自）', 'worksteproute_index_page_g', 2, NULL, NULL, NULL, 'workStepRoute:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:39:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (167, 1560145190851772417, '[0],', '字段配置导出（自）', 'fieldconfig_index_export_g', 2, NULL, NULL, NULL, 'fieldConfig:export', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (168, 1560145190851772417, '[0],', '客户资料列表（自）', 'cusinfor_index_list_g', 2, NULL, NULL, NULL, 'cusInfor:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:34', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (169, 1560145190851772417, '[0],', '工单任务关系表编辑（自）', 'workordertask_index_edit_g', 2, NULL, NULL, NULL, 'workOrderTask:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:44:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (170, 1560145190851772417, '[0],', '字段配置编辑（自）', 'fieldconfig_index_edit_g', 2, NULL, NULL, NULL, 'fieldConfig:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (171, 1560145190851772417, '[0],', '出库单删除（自）', 'invOut_index_delete_g', 2, NULL, NULL, NULL, 'invOut:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:48:27', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (172, 1560145190851772417, '[0],', '仓库管理列表（自）', 'warehouse_index_list_g', 2, NULL, NULL, NULL, 'wareHouse:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 16:37:01', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (173, 1560145190851772417, '[0],', '采购订单列表（自）', 'puororder_index_list_g', 2, NULL, NULL, NULL, 'puorOrder:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (174, 1560145190851772417, '[0],', '物料清单导出（自）', 'bom_index_export_g', 2, NULL, NULL, NULL, 'bom:export', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:26:55', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (175, 1560145190851772417, '[0],', '工单任务关系表导出（自）', 'workordertask_index_export_g', 2, NULL, NULL, NULL, 'workOrderTask:export', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:44:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (176, 1560145190851772417, '[0],', 'SaaS租户导出（自）', 'tenantinfo_index_export_g', 2, NULL, NULL, NULL, 'tenantInfo:export', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (177, 1560145190851772417, '[0],', '销售订单新增（自）', 'saleorder_index_add_g', 2, NULL, NULL, NULL, 'saleOrder:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 11:17:42', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (178, 1560145190851772417, '[0],', '采购订单新增（自）', 'puororder_index_add_g', 2, NULL, NULL, NULL, 'puorOrder:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (179, 1560145190851772417, '[0],', '工序路线关系表列表（自）', 'worksteproute_index_list_g', 2, NULL, NULL, NULL, 'workStepRoute:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:39:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (180, 1560145190851772417, '[0],', '工序路线关系表删除（自）', 'worksteproute_index_delete_g', 2, NULL, NULL, NULL, 'workStepRoute:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:39:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (181, 1560145190851772417, '[0],', '工单表编辑（自）', 'workorder_index_edit_g', 2, NULL, NULL, NULL, 'workOrder:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:18', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (182, 1560145190851772417, '[0],', ' 供应商资料列表（自）', 'suppdata_index_list_g', 2, NULL, NULL, NULL, 'suppData:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:14', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (183, 1560145190851772417, '[0],', '工艺路线删除（自）', 'workroute_index_delete_g', 2, NULL, NULL, NULL, 'workRoute:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:34', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (184, 1560145190851772417, '[0],', '物料清单新增（自）', 'bom_index_add_g', 2, NULL, NULL, NULL, 'bom:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:26:55', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (185, 1560145190851772417, '[0],', '客户联系人编辑（自）', 'cusperson_index_edit_g', 2, NULL, NULL, NULL, 'cusPerson:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:42', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (186, 1560145190851772417, '[0],', '工序删除（自）', 'workstep_index_delete_g', 2, NULL, NULL, NULL, 'workStep:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:13', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (187, 1560145190851772417, '[0],', '销售订单编辑（自）', 'saleorder_index_edit_g', 2, NULL, NULL, NULL, 'saleOrder:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 11:17:42', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (188, 1560145190851772417, '[0],', '入库单列表（自）', 'invIn_index_list_g', 2, NULL, NULL, NULL, 'invIn:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:49:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (189, 1560145190851772417, '[0],', '出入库明细导出（自）', 'invdetail_index_export_g', 2, NULL, NULL, NULL, 'invDetail:export', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-10 16:05:47', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (190, 1560145190851772417, '[0],', '任务查看（自）', 'task_index_detail_g', 2, NULL, NULL, NULL, 'task:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:30', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (191, 1560145190851772417, '[0],', '出库单新增（自）', 'invOut_index_add_g', 2, NULL, NULL, NULL, 'invOut:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:48:27', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (192, 1560145190851772417, '[0],', '工单任务关系表列表（自）', 'workordertask_index_list_g', 2, NULL, NULL, NULL, 'workOrderTask:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:44:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (193, 1560145190851772417, '[0],', '出入库明细查看（自）', 'invdetail_index_detail_g', 2, NULL, NULL, NULL, 'invDetail:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-10 16:05:47', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (194, 1560145190851772417, '[0],', '销售订单查看（自）', 'saleorder_index_detail_g', 2, NULL, NULL, NULL, 'saleOrder:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 11:17:42', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (195, 1560145190851772417, '[0],', '工序路线关系表导出（自）', 'worksteproute_index_export_g', 2, NULL, NULL, NULL, 'workStepRoute:export', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:39:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (196, 1560145190851772417, '[0],', '采购订单查询（自）', 'puororder_index_page_g', 2, NULL, NULL, NULL, 'puorOrder:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (197, 1560145190851772417, '[0],', '客户分类删除（自）', 'cussort_index_delete_g', 2, NULL, NULL, NULL, 'cusSort:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (198, 1560145190851772417, '[0],', '工序导出（自）', 'workstep_index_export_g', 2, NULL, NULL, NULL, 'workStep:export', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:13', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (199, 1560145190851772417, '[0],', '供应商联系人删除（自）', 'suppperson_index_delete_g', 2, NULL, NULL, NULL, 'suppPerson:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:27:59', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (200, 1560145190851772417, '[0],', '供应商联系人列表（自）', 'suppperson_index_list_g', 2, NULL, NULL, NULL, 'suppPerson:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:27:59', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (201, 1560145190851772417, '[0],', '产品类型表查看（自）', 'protype_index_detail_g', 2, NULL, NULL, NULL, 'proType:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 12:00:45', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (202, 1560145190851772417, '[0],', '产品类型表新增（自）', 'protype_index_add_g', 2, NULL, NULL, NULL, 'proType:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 12:00:45', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (203, 1560145190851772417, '[0],', '物料清单查询（自）', 'bom_index_page_g', 2, NULL, NULL, NULL, 'bom:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:26:55', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (204, 1560145190851772417, '[0],', '客户资料导出（自）', 'cusinfor_index_export_g', 2, NULL, NULL, NULL, 'cusInfor:export', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:34', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (205, 1560145190851772417, '[0],', '供应商联系人编辑（自）', 'suppperson_index_edit_g', 2, NULL, NULL, NULL, 'suppPerson:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:27:59', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (206, 1560145190851772417, '[0],', '客户资料查询（自）', 'cusinfor_index_page_g', 2, NULL, NULL, NULL, 'cusInfor:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:34', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (207, 1560145190851772417, '[0],', '工序查看（自）', 'workstep_index_detail_g', 2, NULL, NULL, NULL, 'workStep:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:13', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (208, 1560145190851772417, '[0],', '仓库管理查看（自）', 'warehouse_index_detail_g', 2, NULL, NULL, NULL, 'wareHouse:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 16:37:01', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (209, 1560145190851772417, '[0],', '采购订单导出（自）', 'puororder_index_export_g', 2, NULL, NULL, NULL, 'puorOrder:export', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (210, 1560145190851772417, '[0],', '产品表查看（自）', 'pro_index_detail_g', 2, NULL, NULL, NULL, 'pro:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:36:37', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (211, 1560145190851772417, '[0],', '工序路线关系表查看（自）', 'worksteproute_index_detail_g', 2, NULL, NULL, NULL, 'workStepRoute:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:39:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (212, 1560145190851772417, '[0],', 'SaaS租户查询（自）', 'tenantinfo_index_page_g', 2, NULL, NULL, NULL, 'tenantInfo:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (213, 1560145190851772417, '[0],', '出库单查询（自）', 'invOut_index_page_g', 2, NULL, NULL, NULL, 'invOut:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:48:27', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (214, 1560145190851772417, '[0],', '工序查询（自）', 'workstep_index_page_g', 2, NULL, NULL, NULL, 'workStep:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:13', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (215, 1560145190851772417, '[0],', '供应商分类删除（自）', 'suppsort_index_delete_g', 2, NULL, NULL, NULL, 'suppSort:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:25', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (216, 1560145190851772417, '[0],', '供应商分类列表（自）', 'suppsort_index_list_g', 2, NULL, NULL, NULL, 'suppSort:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:25', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (217, 1560145190851772417, '[0],', 'SaaS租户查看（自）', 'tenantinfo_index_detail_g', 2, NULL, NULL, NULL, 'tenantInfo:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (218, 1560145190851772417, '[0],', '仓库管理编辑（自）', 'warehouse_index_edit_g', 2, NULL, NULL, NULL, 'wareHouse:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 16:37:01', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (219, 1560145190851772417, '[0],', '物料清单查看（自）', 'bom_index_detail_g', 2, NULL, NULL, NULL, 'bom:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:26:55', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (220, 1560145190851772417, '[0],', '供应商分类导出（自）', 'suppsort_index_export_g', 2, NULL, NULL, NULL, 'suppSort:export', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:25', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (221, 1560145190851772417, '[0],', '自定义编号规则列表（自）', 'customcode_index_list_g', 2, NULL, NULL, NULL, 'customCode:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (222, 1560145190851772417, '[0],', ' 供应商资料编辑（自）', 'suppdata_index_edit_g', 2, NULL, NULL, NULL, 'suppData:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:14', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (223, 1560145190851772417, '[0],', '自定义编号规则导出（自）', 'customcode_index_export_g', 2, NULL, NULL, NULL, 'customCode:export', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (224, 1560145190851772417, '[0],', '入库单删除（自）', 'invIn_index_delete_g', 2, NULL, NULL, NULL, 'invIn:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:49:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (225, 1560145190851772417, '[0],', '自定义编号规则查看（自）', 'customcode_index_detail_g', 2, NULL, NULL, NULL, 'customCode:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (226, 1560145190851772417, '[0],', '报工新增（自）', 'workreport_index_add_g', 2, NULL, NULL, NULL, 'workReport:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:49', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (227, 1560145190851772417, '[0],', '供应商分类查看（自）', 'suppsort_index_detail_g', 2, NULL, NULL, NULL, 'suppSort:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:25', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (228, 1560145190851772417, '[0],', '入库单编辑（自）', 'invIn_index_edit_g', 2, NULL, NULL, NULL, 'invIn:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:49:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (229, 1560145190851772417, '[0],', '出入库明细删除（自）', 'invdetail_index_delete_g', 2, NULL, NULL, NULL, 'invDetail:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-10 16:05:47', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (230, 1560145190851772417, '[0],', ' 供应商资料新增（自）', 'suppdata_index_add_g', 2, NULL, NULL, NULL, 'suppData:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:14', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (231, 1560145190851772417, '[0],', '客户联系人新增（自）', 'cusperson_index_add_g', 2, NULL, NULL, NULL, 'cusPerson:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:42', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (232, 1560145190851772417, '[0],', '工序列表（自）', 'workstep_index_list_g', 2, NULL, NULL, NULL, 'workStep:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:13', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (233, 1560145190851772417, '[0],', '出入库明细编辑（自）', 'invdetail_index_edit_g', 2, NULL, NULL, NULL, 'invDetail:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-10 16:05:47', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (234, 1560145190851772417, '[0],', '字段配置删除（自）', 'fieldconfig_index_delete_g', 2, NULL, NULL, NULL, 'fieldConfig:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (235, 1560145190851772417, '[0],', '生产计划删除（自）', 'proplan_index_delete_g', 2, NULL, NULL, NULL, 'proPlan:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-21 16:58:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (236, 1560145190851772417, '[0],', ' 供应商资料查看（自）', 'suppdata_index_detail_g', 2, NULL, NULL, NULL, 'suppData:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:14', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (237, 1560145190851772417, '[0],', '销售订单查询（自）', 'saleorder_index_page_g', 2, NULL, NULL, NULL, 'saleOrder:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 11:17:42', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (238, 1560145190851772417, '[0],', '物料清单删除（自）', 'bom_index_delete_g', 2, NULL, NULL, NULL, 'bom:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:26:55', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (239, 1560145190851772417, '[0],', '客户分类新增（自）', 'cussort_index_add_g', 2, NULL, NULL, NULL, 'cusSort:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (240, 1560145190851772417, '[0],', '产品类型表查询（自）', 'protype_index_page_g', 2, NULL, NULL, NULL, 'proType:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 12:00:45', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (241, 1560145190851772417, '[0],', '出库单查看（自）', 'invOut_index_detail_g', 2, NULL, NULL, NULL, 'invOut:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:48:27', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (242, 1560145190851772417, '[0],', '客户分类编辑（自）', 'cussort_index_edit_g', 2, NULL, NULL, NULL, 'cusSort:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (243, 1560145190851772417, '[0],', '工单表导出（自）', 'workorder_index_export_g', 2, NULL, NULL, NULL, 'workOrder:export', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:18', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (244, 1560145190851772417, '[0],', '任务开始\r\n（自）', 'task_index_add_g', 2, NULL, NULL, NULL, 'task:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:30', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (245, 1560145190851772417, '[0],', '字段配置查看（自）', 'fieldconfig_index_detail_g', 2, NULL, NULL, NULL, 'fieldConfig:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (246, 1560145190851772417, '[0],', '出入库明细列表（自）', 'invdetail_index_list_g', 2, NULL, NULL, NULL, 'invDetail:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-10 16:05:47', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (247, 1560145190851772417, '[0],', '客户联系人查询（自）', 'cusperson_index_page_g', 2, NULL, NULL, NULL, 'cusPerson:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:42', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (248, 1560145190851772417, '[0],', '库存余额查看（自）', 'stockbalance_index_detail_g', 2, NULL, NULL, NULL, 'stockBalance:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-28 16:50:46', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (249, 1560145190851772417, '[0],', '库存余额删除（自）', 'stockbalance_index_delete_g', 2, NULL, NULL, NULL, 'stockBalance:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-28 16:50:46', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (250, 1560145190851772417, '[0],', '入库单新增（自）', 'invIn_index_add_g', 2, NULL, NULL, NULL, 'invIn:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:49:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (251, 1560145190851772417, '[0],', '采购细明编辑（自）', 'puordetail_index_edit_g', 2, NULL, NULL, NULL, 'puorDetail:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (252, 1560145190851772417, '[0],', '任务查询（自）', 'task_index_page_g', 2, NULL, NULL, NULL, 'task:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:30', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (253, 1560145190851772417, '[0],', '客户资料编辑（自）', 'cusinfor_index_edit_g', 2, NULL, NULL, NULL, 'cusInfor:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:34', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (254, 1560145190851772417, '[0],', '采购细明查看（自）', 'puordetail_index_detail_g', 2, NULL, NULL, NULL, 'puorDetail:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (255, 1560145190851772417, '[0],', '工艺路线导出（自）', 'workroute_index_export_g', 2, NULL, NULL, NULL, 'workRoute:export', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:34', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (256, 1560145190851772417, '[0],', '工艺路线新增（自）', 'workroute_index_add_g', 2, NULL, NULL, NULL, 'workRoute:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:34', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (257, 1560145190851772417, '[0],', '产品表新增（自）', 'pro_index_add_g', 2, NULL, NULL, NULL, 'pro:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:36:37', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (258, 1560145190851772417, '[0],', '产品表删除（自）', 'pro_index_delete_g', 2, NULL, NULL, NULL, 'pro:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:36:37', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (259, 1560145190851772417, '[0],', '工单表删除（自）', 'workorder_index_delete_g', 2, NULL, NULL, NULL, 'workOrder:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:18', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (260, 1560145190851772417, '[0],', '供应商联系人导出（自）', 'suppperson_index_export_g', 2, NULL, NULL, NULL, 'suppPerson:export', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:27:59', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (261, 1560145190851772417, '[0],', '任务结束（自）', 'task_index_export_g', 2, NULL, NULL, NULL, 'task:export', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:30', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (262, 1560145190851772417, '[0],', '字段配置查询（自）', 'fieldconfig_index_page_g', 2, NULL, NULL, NULL, 'fieldConfig:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (263, 1560145190851772417, '[0],', '客户分类列表（自）', 'cussort_index_list_g', 2, NULL, NULL, NULL, 'cusSort:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (264, 1560145190851772417, '[0],', '供应商分类查询（自）', 'suppsort_index_page_g', 2, NULL, NULL, NULL, 'suppSort:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:25', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (265, 1560145190851772417, '[0],', '客户联系人列表（自）', 'cusperson_index_list_g', 2, NULL, NULL, NULL, 'cusPerson:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:42', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (266, 1560145190851772417, '[0],', '工序路线关系表编辑（自）', 'worksteproute_index_edit_g', 2, NULL, NULL, NULL, 'workStepRoute:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:39:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (267, 1560145190851772417, '[0],', '产品类型表编辑（自）', 'protype_index_edit_g', 2, NULL, NULL, NULL, 'proType:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 12:00:45', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (268, 1560145190851772417, '[0],', '库存余额列表（自）', 'stockbalance_index_list_g', 2, NULL, NULL, NULL, 'stockBalance:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-28 16:50:46', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (269, 1560145190851772417, '[0],', '客户分类查看（自）', 'cussort_index_detail_g', 2, NULL, NULL, NULL, 'cusSort:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (270, 1560145190851772417, '[0],', '库存余额编辑（自）', 'stockbalance_index_edit_g', 2, NULL, NULL, NULL, 'stockBalance:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-28 16:50:46', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (271, 1560145190851772417, '[0],', '工序编辑（自）', 'workstep_index_edit_g', 2, NULL, NULL, NULL, 'workStep:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:13', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (272, 1560145190851772417, '[0],', '生产计划查看（自）', 'proplan_index_detail_g', 2, NULL, NULL, NULL, 'proPlan:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-21 16:58:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (273, 1560145190851772417, '[0],', '工单表列表（自）', 'workorder_index_list_g', 2, NULL, NULL, NULL, 'workOrder:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:18', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (274, 1560145190851772417, '[0],', '产品表导出（自）', 'pro_index_export_g', 2, NULL, NULL, NULL, 'pro:export', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:36:37', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (275, 1560145190851772417, '[0],', '仓库管理删除（自）', 'warehouse_index_delete_g', 2, NULL, NULL, NULL, 'wareHouse:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 16:37:01', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (276, 1560145190851772417, '[0],', 'SaaS租户列表（自）', 'tenantinfo_index_list_g', 2, NULL, NULL, NULL, 'tenantInfo:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (277, 1560145190851772417, '[0],', 'SaaS租户新增（自）', 'tenantinfo_index_add_g', 2, NULL, NULL, NULL, 'tenantInfo:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (278, 1560145190851772417, '[0],', ' 供应商资料删除（自）', 'suppdata_index_delete_g', 2, NULL, NULL, NULL, 'suppData:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:14', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (279, 1560145190851772417, '[0],', 'SaaS租户编辑（自）', 'tenantinfo_index_edit_g', 2, NULL, NULL, NULL, 'tenantInfo:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (280, 1560145190851772417, '[0],', '生产计划列表（自）', 'proplan_index_list_g', 2, NULL, NULL, NULL, 'proPlan:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-21 16:58:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (281, 1560145190851772417, '[0],', '供应商分类编辑（自）', 'suppsort_index_edit_g', 2, NULL, NULL, NULL, 'suppSort:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:25', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (282, 1560145190851772417, '[0],', '库存余额新增（自）', 'stockbalance_index_add_g', 2, NULL, NULL, NULL, 'stockBalance:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-28 16:50:46', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (283, 1560145190851772417, '[0],', '工艺路线查看（自）', 'workroute_index_detail_g', 2, NULL, NULL, NULL, 'workRoute:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:34', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (284, 1560145190851772417, '[0],', '工单表查询（自）', 'workorder_index_page_g', 2, NULL, NULL, NULL, 'workOrder:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:18', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (285, 1560145190851772417, '[0],', '销售订单列表（自）', 'saleorder_index_list_g', 2, NULL, NULL, NULL, 'saleOrder:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 11:17:42', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (286, 1560145190851772417, '[0],', '客户联系人导出（自）', 'cusperson_index_export_g', 2, NULL, NULL, NULL, 'cusPerson:export', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:42', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (287, 1560145190851772417, '[0],', '产品表查询（自）', 'pro_index_page_g', 2, NULL, NULL, NULL, 'pro:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:36:37', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (288, 1560145190851772417, '[0],', '工序路线关系表新增（自）', 'worksteproute_index_add_g', 2, NULL, NULL, NULL, 'workStepRoute:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:39:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (289, 1560145190851772417, '[0],', ' 供应商资料导出（自）', 'suppdata_index_export_g', 2, NULL, NULL, NULL, 'suppData:export', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:14', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (290, 1560145190851772417, '[0],', '入库单查询（自）', 'invIn_index_page_g', 2, NULL, NULL, NULL, 'invIn:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:49:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (291, 1560145190851772417, '[0],', '出库单编辑（自）', 'invOut_index_edit_g', 2, NULL, NULL, NULL, 'invOut:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:48:27', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (292, 1560145190851772417, '[0],', '任务删除（自）', 'task_index_delete_g', 2, NULL, NULL, NULL, 'task:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:30', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (293, 1560145190851772417, '[0],', '出库单导出（自）', 'invOut_index_export_g', 2, NULL, NULL, NULL, 'invOut:export', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:48:27', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (294, 1560145190851772417, '[0],', '采购订单查看（自）', 'puororder_index_detail_g', 2, NULL, NULL, NULL, 'puorOrder:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (295, 1560145190851772417, '[0],', '工单表查看（自）', 'workorder_index_detail_g', 2, NULL, NULL, NULL, 'workOrder:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:18', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (296, 1560145190851772417, '[0],', '采购细明新增（自）', 'puordetail_index_add_g', 2, NULL, NULL, NULL, 'puorDetail:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (297, 1560145190851772417, '[0],', '工单任务关系表查看（自）', 'workordertask_index_detail_g', 2, NULL, NULL, NULL, 'workOrderTask:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:44:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (298, 1560145190851772417, '[0],', '报工导出（自）', 'workreport_index_export_g', 2, NULL, NULL, NULL, 'workReport:export', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:49', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (299, 1560145190851772417, '[0],', '报工查看（自）', 'workreport_index_detail_g', 2, NULL, NULL, NULL, 'workReport:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:49', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (300, 1560145190851772417, '[0],', '采购细明列表（自）', 'puordetail_index_list_g', 2, NULL, NULL, NULL, 'puorDetail:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (301, 1560145190851772417, '[0],', '销售订单导出（自）', 'saleorder_index_export_g', 2, NULL, NULL, NULL, 'saleOrder:export', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 11:17:42', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (302, 1560145190851772417, '[0],', '客户资料查看（自）', 'cusinfor_index_detail_g', 2, NULL, NULL, NULL, 'cusInfor:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:34', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (303, 1560145190851772417, '[0],', '自定义编号规则编辑（自）', 'customcode_index_edit_g', 2, NULL, NULL, NULL, 'customCode:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (304, 1560145190851772417, '[0],', '入库单查看（自）', 'invIn_index_detail_g', 2, NULL, NULL, NULL, 'invIn:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:49:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (305, 1560145190851772417, '[0],', '字段配置列表（自）', 'fieldconfig_index_list_g', 2, NULL, NULL, NULL, 'fieldConfig:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (306, 1560145190851772417, '[0],', '报工删除（自）', 'workreport_index_delete_g', 2, NULL, NULL, NULL, 'workReport:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:49', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (307, 1560145190851772417, '[0],', '工单任务关系表新增（自）', 'workordertask_index_add_g', 2, NULL, NULL, NULL, 'workOrderTask:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:44:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (308, 1560145190851772417, '[0],', '报工查询（自）', 'workreport_index_page_g', 2, NULL, NULL, NULL, 'workReport:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:49', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (309, 1560145190851772417, '[0],', '产品类型表删除（自）', 'protype_index_delete_g', 2, NULL, NULL, NULL, 'proType:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 12:00:45', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255311, 0, '[0],', '主控面板', 'system_index', 0, 'home', '/', 'RouteView', NULL, 'system', 0, 'Y', NULL, '/analysis', 1, 1, NULL, 0, '2020-05-25 02:19:24', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642255321, 1264622039642255311, '[0],[1264622039642255311],', '分析页', 'system_index_dashboard', 1, NULL, 'analysis', 'system/dashboard/Analysis', NULL, 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-25 02:21:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642255331, 1264622039642255311, '[0],[1264622039642255311],', '工作台', 'system_index_workplace', 1, NULL, 'workplace', 'system/dashboard/Workplace', NULL, 'system', 0, 'Y', NULL, NULL, 1, 2, NULL, 0, '2020-05-25 02:23:48', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642255341, 0, '[0],', '组织架构', 'sys_mgr', 0, 'team', '/sys', 'PageView', NULL, 'hr', 0, 'Y', NULL, NULL, 1, 2, NULL, 0, '2020-03-27 15:58:16', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255351, 1264622039642255341, '[0],[1264622039642255341],', '用户管理', 'sys_user_mgr', 1, NULL, '/mgr_user', 'system/user/index', NULL, 'hr', 1, 'Y', NULL, NULL, 1, 3, NULL, 0, '2020-03-27 16:10:21', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255361, 1264622039642255351, '[0],[1264622039642255341],[1264622039642255351],', '用户查询', 'sys_user_mgr_page', 2, NULL, NULL, NULL, 'sysUser:page', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 16:36:49', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255371, 1264622039642255351, '[0],[1264622039642255341],[1264622039642255351],', '用户编辑', 'sys_user_mgr_edit', 2, NULL, NULL, NULL, 'sysUser:edit', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 12:20:23', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255381, 1264622039642255351, '[0],[1264622039642255341],[1264622039642255351],', '用户增加', 'sys_user_mgr_add', 2, NULL, NULL, NULL, 'sysUser:add', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 16:37:35', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255391, 1264622039642255351, '[0],[1264622039642255341],[1264622039642255351],', '用户删除', 'sys_user_mgr_delete', 2, NULL, NULL, NULL, 'sysUser:delete', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 16:37:58', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255401, 1264622039642255351, '[0],[1264622039642255341],[1264622039642255351],', '用户详情', 'sys_user_mgr_detail', 2, NULL, NULL, NULL, 'sysUser:detail', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 16:38:25', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255411, 1264622039642255351, '[0],[1264622039642255341],[1264622039642255351],', '用户导出', 'sys_user_mgr_export', 2, NULL, NULL, NULL, 'sysUser:export', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 12:21:59', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255421, 1264622039642255351, '[0],[1264622039642255341],[1264622039642255351],', '用户选择器', 'sys_user_mgr_selector', 2, NULL, NULL, NULL, 'sysUser:selector', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-03 13:30:14', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255431, 1264622039642255351, '[0],[1264622039642255341],[1264622039642255351],', '用户授权角色', 'sys_user_mgr_grant_role', 2, NULL, NULL, NULL, 'sysUser:grantRole', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 09:22:01', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255441, 1264622039642255351, '[0],[1264622039642255341],[1264622039642255351],', '用户拥有角色', 'sys_user_mgr_own_role', 2, NULL, NULL, NULL, 'sysUser:ownRole', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-29 14:27:22', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255451, 1264622039642255351, '[0],[1264622039642255341],[1264622039642255351],', '用户授权数据', 'sys_user_mgr_grant_data', 2, NULL, NULL, NULL, 'sysUser:grantData', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 09:22:13', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255461, 1264622039642255351, '[0],[1264622039642255341],[1264622039642255351],', '用户拥有数据', 'sys_user_mgr_own_data', 2, NULL, NULL, NULL, 'sysUser:ownData', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-29 14:27:41', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255471, 1264622039642255351, '[0],[1264622039642255341],[1264622039642255351],', '用户更新信息', 'sys_user_mgr_update_info', 2, NULL, NULL, NULL, 'sysUser:updateInfo', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 16:19:32', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255481, 1264622039642255351, '[0],[1264622039642255341],[1264622039642255351],', '用户修改密码', 'sys_user_mgr_update_pwd', 2, NULL, NULL, NULL, 'sysUser:updatePwd', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 16:20:25', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255491, 1264622039642255351, '[0],[1264622039642255341],[1264622039642255351],', '用户修改状态', 'sys_user_mgr_change_status', 2, NULL, NULL, NULL, 'sysUser:changeStatus', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-23 11:13:14', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255501, 1264622039642255351, '[0],[1264622039642255341],[1264622039642255351],', '用户修改头像', 'sys_user_mgr_update_avatar', 2, NULL, NULL, NULL, 'sysUser:updateAvatar', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 12:21:42', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255511, 1264622039642255351, '[0],[1264622039642255341],[1264622039642255351],', '用户重置密码', 'sys_user_mgr_reset_pwd', 2, NULL, NULL, NULL, 'sysUser:resetPwd', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-29 15:01:51', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255521, 1264622039642255341, '[0],[1264622039642255341],', '机构管理', 'sys_org_mgr', 1, NULL, '/org', 'system/org/index', NULL, 'hr', 1, 'Y', NULL, NULL, 1, 4, NULL, 0, '2020-03-27 17:15:39', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255531, 1264622039642255521, '[0],[1264622039642255341],[1264622039642255521]', '机构查询', 'sys_org_mgr_page', 2, NULL, NULL, NULL, 'sysOrg:page', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 17:17:37', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255541, 1264622039642255521, '[0],[1264622039642255341],[1264622039642255521]', '机构列表', 'sys_org_mgr_list', 2, NULL, NULL, NULL, 'sysOrg:list', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 11:54:26', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255551, 1264622039642255521, '[0],[1264622039642255341],[1264622039642255521]', '机构增加', 'sys_org_mgr_add', 2, NULL, NULL, NULL, 'sysOrg:add', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 17:19:53', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255561, 1264622039642255521, '[0],[1264622039642255341],[1264622039642255521]', '机构编辑', 'sys_org_mgr_edit', 2, NULL, NULL, NULL, 'sysOrg:edit', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 11:54:37', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255571, 1264622039642255521, '[0],[1264622039642255341],[1264622039642255521]', '机构删除', 'sys_org_mgr_delete', 2, NULL, NULL, NULL, 'sysOrg:delete', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 17:20:48', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255581, 1264622039642255521, '[0],[1264622039642255341],[1264622039642255521]', '机构详情', 'sys_org_mgr_detail', 2, NULL, NULL, NULL, 'sysOrg:detail', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 17:21:15', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255591, 1264622039642255521, '[0],[1264622039642255341],[1264622039642255521]', '机构树', 'sys_org_mgr_tree', 2, NULL, NULL, NULL, 'sysOrg:tree', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 17:21:58', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255601, 1264622039642255341, '[0],[1264622039642255341],', '职位管理', 'sys_pos_mgr', 1, NULL, '/pos', 'system/pos/index', NULL, 'hr', 1, 'Y', NULL, NULL, 1, 5, NULL, 0, '2020-03-27 18:38:31', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255611, 1264622039642255601, '[0],[1264622039642255341],[1264622039642255601],', '职位查询', 'sys_pos_mgr_page', 2, NULL, NULL, NULL, 'sysPos:page', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 18:41:48', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255621, 1264622039642255601, '[0],[1264622039642255341],[1264622039642255601],', '职位列表', 'sys_pos_mgr_list', 2, NULL, NULL, NULL, 'sysPos:list', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 11:55:57', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255631, 1264622039642255601, '[0],[1264622039642255341],[1264622039642255601],', '职位增加', 'sys_pos_mgr_add', 2, NULL, NULL, NULL, 'sysPos:add', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 18:42:20', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255641, 1264622039642255601, '[0],[1264622039642255341],[1264622039642255601],', '职位编辑', 'sys_pos_mgr_edit', 2, NULL, NULL, NULL, 'sysPos:edit', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 11:56:08', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255651, 1264622039642255601, '[0],[1264622039642255341],[1264622039642255601],', '职位删除', 'sys_pos_mgr_delete', 2, NULL, NULL, NULL, 'sysPos:delete', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 18:42:39', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255661, 1264622039642255601, '[0],[1264622039642255341],[1264622039642255601],', '职位详情', 'sys_pos_mgr_detail', 2, NULL, NULL, NULL, 'sysPos:detail', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 18:43:00', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255671, 0, '[0],', '权限管理', 'auth_manager', 0, 'safety-certificate', '/auth', 'PageView', NULL, 'hr', 0, 'Y', NULL, NULL, 1, 3, NULL, 0, '2020-07-15 15:51:57', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255681, 1264622039642255671, '[0],[1264622039642255671],', '应用管理', 'sys_app_mgr', 1, NULL, '/app', 'system/app/index', NULL, 'hr', 1, 'Y', NULL, NULL, 1, 6, NULL, 0, '2020-03-27 16:40:21', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255691, 1264622039642255681, '[0],[1264622039642255671],[1264622039642255681],', '应用查询', 'sys_app_mgr_page', 2, NULL, NULL, NULL, 'sysApp:page', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 16:41:58', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255701, 1264622039642255681, '[0],[1264622039642255671],[1264622039642255681],', '应用列表', 'sys_app_mgr_list', 2, NULL, NULL, NULL, 'sysApp:list', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 10:04:59', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255711, 1264622039642255681, '[0],[1264622039642255671],[1264622039642255681],', '应用增加', 'sys_app_mgr_add', 2, NULL, NULL, NULL, 'sysApp:add', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 16:44:10', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255721, 1264622039642255681, '[0],[1264622039642255671],[1264622039642255681],', '应用编辑', 'sys_app_mgr_edit', 2, NULL, NULL, NULL, 'sysApp:edit', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 10:04:34', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255731, 1264622039642255681, '[0],[1264622039642255671],[1264622039642255681],', '应用删除', 'sys_app_mgr_delete', 2, NULL, NULL, NULL, 'sysApp:delete', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 17:14:29', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255741, 1264622039642255681, '[0],[1264622039642255671],[1264622039642255681],', '应用详情', 'sys_app_mgr_detail', 2, NULL, NULL, NULL, 'sysApp:detail', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 17:14:56', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255751, 1264622039642255681, '[0],[1264622039642255671],[1264622039642255681],', '设为默认应用', 'sys_app_mgr_set_as_default', 2, NULL, NULL, NULL, 'sysApp:setAsDefault', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 17:14:56', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255761, 1264622039642255671, '[0],[1264622039642255671],', '菜单管理', 'sys_menu_mgr', 1, NULL, '/menu', 'system/menu/index', NULL, 'hr', 1, 'Y', NULL, NULL, 1, 7, NULL, 0, '2020-03-27 18:44:35', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255771, 1264622039642255761, '[0],[1264622039642255671],[1264622039642255761],', '菜单列表', 'sys_menu_mgr_list', 2, NULL, NULL, NULL, 'sysMenu:list', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 18:45:20', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255781, 1264622039642255761, '[0],[1264622039642255671],[1264622039642255761],', '菜单增加', 'sys_menu_mgr_add', 2, NULL, NULL, NULL, 'sysMenu:add', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 18:45:37', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255791, 1264622039642255761, '[0],[1264622039642255671],[1264622039642255761],', '菜单编辑', 'sys_menu_mgr_edit', 2, NULL, NULL, NULL, 'sysMenu:edit', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 11:52:00', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255801, 1264622039642255761, '[0],[1264622039642255671],[1264622039642255761],', '菜单删除', 'sys_menu_mgr_delete', 2, NULL, NULL, NULL, 'sysMenu:delete', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 18:46:01', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255811, 1264622039642255761, '[0],[1264622039642255671],[1264622039642255761],', '菜单详情', 'sys_menu_mgr_detail', 2, NULL, NULL, NULL, 'sysMenu:detail', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 18:46:22', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255821, 1264622039642255761, '[0],[1264622039642255671],[1264622039642255761],', '菜单授权树', 'sys_menu_mgr_grant_tree', 2, NULL, NULL, NULL, 'sysMenu:treeForGrant', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-03 09:50:31', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255831, 1264622039642255761, '[0],[1264622039642255671],[1264622039642255761],', '菜单树', 'sys_menu_mgr_tree', 2, NULL, NULL, NULL, 'sysMenu:tree', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 18:47:50', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255841, 1264622039642255761, '[0],[1264622039642255671],[1264622039642255761],', '菜单切换', 'sys_menu_mgr_change', 2, NULL, NULL, NULL, 'sysMenu:change', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-03 09:51:43', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255851, 1264622039642255671, '[0],[1264622039642255671],', '角色管理', 'sys_role_mgr', 1, NULL, '/role', 'system/role/index', NULL, 'hr', 1, 'Y', NULL, NULL, 1, 8, NULL, 0, '2020-03-28 16:01:09', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255861, 1264622039642255851, '[0],[1264622039642255671],[1264622039642255851],', '角色查询', 'sys_role_mgr_page', 2, NULL, NULL, NULL, 'sysRole:page', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-28 16:02:09', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255871, 1264622039642255851, '[0],[1264622039642255671],[1264622039642255851],', '角色增加', 'sys_role_mgr_add', 2, NULL, NULL, NULL, 'sysRole:add', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-28 16:02:27', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255881, 1264622039642255851, '[0],[1264622039642255671],[1264622039642255851],', '角色编辑', 'sys_role_mgr_edit', 2, NULL, NULL, NULL, 'sysRole:edit', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 11:57:27', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255891, 1264622039642255851, '[0],[1264622039642255671],[1264622039642255851],', '角色删除', 'sys_role_mgr_delete', 2, NULL, NULL, NULL, 'sysRole:delete', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-28 16:02:46', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255901, 1264622039642255851, '[0],[1264622039642255671],[1264622039642255851],', '角色详情', 'sys_role_mgr_detail', 2, NULL, NULL, NULL, 'sysRole:detail', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-28 16:03:01', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255911, 1264622039642255851, '[0],[1264622039642255671],[1264622039642255851],', '角色下拉', 'sys_role_mgr_drop_down', 2, NULL, NULL, NULL, 'sysRole:dropDown', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-29 15:45:39', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255921, 1264622039642255851, '[0],[1264622039642255671],[1264622039642255851],', '角色授权菜单', 'sys_role_mgr_grant_menu', 2, NULL, NULL, NULL, 'sysRole:grantMenu', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 09:16:27', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255931, 1264622039642255851, '[0],[1264622039642255671],[1264622039642255851],', '角色拥有菜单', 'sys_role_mgr_own_menu', 2, NULL, NULL, NULL, 'sysRole:ownMenu', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-29 14:21:54', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255941, 1264622039642255851, '[0],[1264622039642255671],[1264622039642255851],', '角色授权数据', 'sys_role_mgr_grant_data', 2, NULL, NULL, NULL, 'sysRole:grantData', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 09:16:56', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255951, 1264622039642255851, '[0],[1264622039642255671],[1264622039642255851],', '角色拥有数据', 'sys_role_mgr_own_data', 2, NULL, NULL, NULL, 'sysRole:ownData', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-29 14:23:08', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255961, 0, '[0],', '开发管理', 'system_tools', 0, 'euro', '/tools', 'PageView', NULL, 'system', 1, 'Y', NULL, NULL, 1, 4, NULL, 0, '2020-05-25 02:10:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642255971, 1264622039642255961, '[0],[1264622039642255961],', '系统配置', 'system_tools_config', 1, NULL, '/config', 'system/config/index', NULL, 'system', 1, 'Y', NULL, NULL, 1, 9, NULL, 0, '2020-05-25 02:12:56', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642255981, 1264622039642255971, '[0],[1264622039642255961],[1264622039642255971],', '配置查询', 'system_tools_config_page', 2, NULL, NULL, NULL, 'sysConfig:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-27 17:02:22', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642255991, 1264622039642255971, '[0],[1264622039642255961],[1264622039642255971],', '配置列表', 'system_tools_config_list', 2, NULL, NULL, NULL, 'sysConfig:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-27 17:02:42', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256001, 1264622039642255971, '[0],[1264622039642255961],[1264622039642255971],', '配置增加', 'system_tools_config_add', 2, NULL, NULL, NULL, 'sysConfig:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-27 17:03:31', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256011, 1264622039642255971, '[0],[1264622039642255961],[1264622039642255971],', '配置编辑', 'system_tools_config_edit', 2, NULL, NULL, NULL, 'sysConfig:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-27 17:03:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256021, 1264622039642255971, '[0],[1264622039642255961],[1264622039642255971],', '配置删除', 'system_tools_config_delete', 2, NULL, NULL, NULL, 'sysConfig:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-27 17:03:44', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256031, 1264622039642255971, '[0],[1264622039642255961],[1264622039642255971],', '配置详情', 'system_tools_config_detail', 2, NULL, NULL, NULL, 'sysConfig:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-27 17:02:59', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256041, 1264622039642255961, '[0],[1264622039642255961],', '邮件发送', 'sys_email_mgr', 1, NULL, '/email', 'system/email/index', NULL, 'system', 1, 'Y', NULL, NULL, 1, 10, NULL, 0, '2020-07-02 11:44:21', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256051, 1264622039642256041, '[0],[1264622039642255961],[1264622039642256041],', '发送文本邮件', 'sys_email_mgr_send_email', 2, NULL, NULL, NULL, 'email:sendEmail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 11:45:39', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256061, 1264622039642256041, '[0],[1264622039642255961],[1264622039642256041],', '发送html邮件', 'sys_email_mgr_send_email_html', 2, NULL, NULL, NULL, 'email:sendEmailHtml', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 11:45:57', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256071, 1264622039642255961, '[0],[1264622039642255961],', '短信管理', 'sys_sms_mgr', 1, NULL, '/sms', 'system/sms/index', NULL, 'system', 1, 'Y', NULL, NULL, 1, 11, NULL, 0, '2020-07-02 12:00:12', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256081, 1264622039642256071, '[0],[1264622039642255961],[1264622039642256071],', '短信发送查询', 'sys_sms_mgr_page', 2, NULL, NULL, NULL, 'sms:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 12:16:56', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256091, 1264622039642256071, '[0],[1264622039642255961],[1264622039642256071],', '发送验证码短信', 'sys_sms_mgr_send_login_message', 2, NULL, NULL, NULL, 'sms:sendLoginMessage', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 12:02:31', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256101, 1264622039642256071, '[0],[1264622039642255961],[1264622039642256071],', '验证短信验证码', 'sys_sms_mgr_validate_message', 2, NULL, NULL, NULL, 'sms:validateMessage', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 12:02:50', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256111, 1264622039642255961, '[0],[1264622039642255961],', '字典管理', 'sys_dict_mgr', 1, NULL, '/dict', 'system/dict/index', NULL, 'system', 1, 'Y', NULL, NULL, 1, 12, NULL, 0, '2020-04-01 11:17:26', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256121, 1264622039642256111, '[0],[1264622039642255961],[1264622039642256111],', '字典类型查询', 'sys_dict_mgr_dict_type_page', 2, NULL, NULL, NULL, 'sysDictType:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 11:20:22', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256131, 1264622039642256111, '[0],[1264622039642255961],[1264622039642256111],', '字典类型列表', 'sys_dict_mgr_dict_type_list', 2, NULL, NULL, NULL, 'sysDictType:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-29 15:12:35', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256141, 1264622039642256111, '[0],[1264622039642255961],[1264622039642256111],', '字典类型增加', 'sys_dict_mgr_dict_type_add', 2, NULL, NULL, NULL, 'sysDictType:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 11:19:58', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256151, 1264622039642256111, '[0],[1264622039642255961],[1264622039642256111],', '字典类型删除', 'sys_dict_mgr_dict_type_delete', 2, NULL, NULL, NULL, 'sysDictType:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 11:21:30', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256161, 1264622039642256111, '[0],[1264622039642255961],[1264622039642256111],', '字典类型编辑', 'sys_dict_mgr_dict_type_edit', 2, NULL, NULL, NULL, 'sysDictType:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 11:21:42', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256171, 1264622039642256111, '[0],[1264622039642255961],[1264622039642256111],', '字典类型详情', 'sys_dict_mgr_dict_type_detail', 2, NULL, NULL, NULL, 'sysDictType:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 11:22:06', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256181, 1264622039642256111, '[0],[1264622039642255961],[1264622039642256111],', '字典类型下拉', 'sys_dict_mgr_dict_type_drop_down', 2, NULL, NULL, NULL, 'sysDictType:dropDown', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 11:22:23', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256191, 1264622039642256111, '[0],[1264622039642255961],[1264622039642256111],', '字典类型修改状态', 'sys_dict_mgr_dict_type_change_status', 2, NULL, NULL, NULL, 'sysDictType:changeStatus', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-23 11:15:50', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256201, 1264622039642256111, '[0],[1264622039642255961],[1264622039642256111],', '字典值查询', 'sys_dict_mgr_dict_page', 2, NULL, NULL, NULL, 'sysDictData:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 11:23:11', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256211, 1264622039642256111, '[0],[1264622039642255961],[1264622039642256111],', '字典值列表', 'sys_dict_mgr_dict_list', 2, NULL, NULL, NULL, 'sysDictData:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 11:24:58', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256221, 1264622039642256111, '[0],[1264622039642255961],[1264622039642256111],', '字典值增加', 'sys_dict_mgr_dict_add', 2, NULL, NULL, NULL, 'sysDictData:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 11:22:51', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256231, 1264622039642256111, '[0],[1264622039642255961],[1264622039642256111],', '字典值删除', 'sys_dict_mgr_dict_delete', 2, NULL, NULL, NULL, 'sysDictData:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 11:23:26', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256241, 1264622039642256111, '[0],[1264622039642255961],[1264622039642256111],', '字典值编辑', 'sys_dict_mgr_dict_edit', 2, NULL, NULL, NULL, 'sysDictData:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 11:24:21', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256251, 1264622039642256111, '[0],[1264622039642255961],[1264622039642256111],', '字典值详情', 'sys_dict_mgr_dict_detail', 2, NULL, NULL, NULL, 'sysDictData:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 11:24:42', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256261, 1264622039642256111, '[0],[1264622039642255961],[1264622039642256111],', '字典值修改状态', 'sys_dict_mgr_dict_change_status', 2, NULL, NULL, NULL, 'sysDictData:changeStatus', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-23 11:17:53', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256271, 1264622039642255961, '[0],[1264622039642255961],', '接口文档', 'sys_swagger_mgr', 1, NULL, '/swagger', 'Iframe', NULL, 'system', 2, 'Y', 'http://localhost:82/doc.html', NULL, 1, 13, NULL, 0, '2020-07-02 12:16:56', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256281, 0, '[0],', '日志管理', 'sys_log_mgr', 0, 'read', '/log', 'PageView', NULL, 'system', 1, 'Y', NULL, NULL, 1, 5, NULL, 0, '2020-04-01 09:25:01', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256291, 1264622039642256281, '[0],[1264622039642256281],', '访问日志', 'sys_log_mgr_vis_log', 1, NULL, '/vislog', 'system/log/vislog/index', NULL, 'system', 0, 'Y', NULL, NULL, 1, 14, NULL, 0, '2020-04-01 09:26:40', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256301, 1264622039642256291, '[0],[1264622039642256281],[1264622039642256291],', '访问日志查询', 'sys_log_mgr_vis_log_page', 2, NULL, NULL, NULL, 'sysVisLog:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 09:55:51', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256311, 1264622039642256291, '[0],[1264622039642256281],[1264622039642256291],', '访问日志清空', 'sys_log_mgr_vis_log_delete', 2, NULL, NULL, NULL, 'sysVisLog:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 09:56:57', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256321, 1264622039642256281, '[0],[1264622039642256281],', '操作日志', 'sys_log_mgr_op_log', 1, NULL, '/oplog', 'system/log/oplog/index', NULL, 'system', 0, 'Y', NULL, NULL, 1, 15, NULL, 0, '2020-04-01 09:26:59', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256331, 1264622039642256321, '[0],[1264622039642256281],[1264622039642256321],', '操作日志查询', 'sys_log_mgr_op_log_page', 2, NULL, NULL, NULL, 'sysOpLog:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 09:57:39', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256341, 1264622039642256321, '[0],[1264622039642256281],[1264622039642256321],', '操作日志清空', 'sys_log_mgr_op_log_delete', 2, NULL, NULL, NULL, 'sysOpLog:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 09:58:13', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256351, 0, '[0],', '系统监控', 'sys_monitor_mgr', 0, 'deployment-unit', '/monitor', 'PageView', NULL, 'system', 1, 'Y', NULL, NULL, 1, 6, NULL, 0, '2020-06-05 16:00:50', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256361, 1264622039642256351, '[0],[1264622039642256351],', '服务监控', 'sys_monitor_mgr_machine_monitor', 1, NULL, '/machine', 'system/machine/index', NULL, 'system', 1, 'Y', NULL, NULL, 1, 16, NULL, 0, '2020-06-05 16:02:38', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256371, 1264622039642256361, '[0],[1264622039642256351],[1264622039642256361],', '服务监控查询', 'sys_monitor_mgr_machine_monitor_query', 2, NULL, NULL, NULL, 'sysMachine:query', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-05 16:05:33', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256381, 1264622039642256351, '[0],[1264622039642256351],', '在线用户', 'sys_monitor_mgr_online_user', 1, NULL, '/onlineUser', 'system/onlineUser/index', NULL, 'system', 1, 'Y', NULL, NULL, 1, 17, NULL, 0, '2020-06-05 16:01:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256391, 1264622039642256381, '[0],[1264622039642256351],[1264622039642256381],', '在线用户列表', 'sys_monitor_mgr_online_user_list', 2, NULL, NULL, NULL, 'sysOnlineUser:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-05 16:03:46', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256401, 1264622039642256381, '[0],[1264622039642256351],[1264622039642256381],', '在线用户强退', 'sys_monitor_mgr_online_user_force_exist', 2, NULL, NULL, NULL, 'sysOnlineUser:forceExist', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-05 16:04:16', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256411, 1264622039642256351, '[0],[1264622039642256351],', '数据监控', 'sys_monitor_mgr_druid', 1, NULL, '/druid', 'Iframe', NULL, 'system', 2, 'Y', 'http://localhost:82/druid', NULL, 1, 18, NULL, 0, '2020-06-28 16:15:07', 1265476890672672808, '2020-09-13 09:39:10', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642256421, 0, '[0],', '通知公告', 'sys_notice', 0, 'sound', '/notice', 'PageView', NULL, 'teamwork', 1, 'Y', NULL, NULL, 1, 7, NULL, 0, '2020-06-29 15:41:53', 1265476890672672808, '2022-07-18 21:17:05', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642256431, 1264622039642256421, '[0],[1264622039642256421],', '公告管理', 'sys_notice_mgr', 1, NULL, '/notice', 'system/notice/index', NULL, 'teamwork', 1, 'Y', NULL, NULL, 1, 19, NULL, 0, '2020-06-29 15:44:24', 1265476890672672808, '2022-07-18 21:17:05', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642256441, 1264622039642256431, '[0],[1264622039642256421],[1264622039642256431],', '公告查询', 'sys_notice_mgr_page', 2, NULL, NULL, NULL, 'sysNotice:page', 'teamwork', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-29 15:45:30', 1265476890672672808, '2022-07-18 21:17:05', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642256451, 1264622039642256431, '[0],[1264622039642256421],[1264622039642256431],', '公告增加', 'sys_notice_mgr_add', 2, NULL, NULL, NULL, 'sysNotice:add', 'teamwork', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-29 15:45:57', 1265476890672672808, '2022-07-18 21:17:05', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642256461, 1264622039642256431, '[0],[1264622039642256421],[1264622039642256431],', '公告编辑', 'sys_notice_mgr_edit', 2, NULL, NULL, NULL, 'sysNotice:edit', 'teamwork', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-29 15:46:22', 1265476890672672808, '2022-07-18 21:17:05', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642256471, 1264622039642256431, '[0],[1264622039642256421],[1264622039642256431],', '公告删除', 'sys_notice_mgr_delete', 2, NULL, NULL, NULL, 'sysNotice:delete', 'teamwork', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-29 15:46:11', 1265476890672672808, '2022-07-18 21:17:05', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642256481, 1264622039642256431, '[0],[1264622039642256421],[1264622039642256431],', '公告查看', 'sys_notice_mgr_detail', 2, NULL, NULL, NULL, 'sysNotice:detail', 'teamwork', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-29 15:46:33', 1265476890672672808, '2022-07-18 21:17:05', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642256491, 1264622039642256431, '[0],[1264622039642256421],[1264622039642256431],', '公告修改状态', 'sys_notice_mgr_changeStatus', 2, NULL, NULL, NULL, 'sysNotice:changeStatus', 'teamwork', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-29 15:46:50', 1265476890672672808, '2022-07-18 21:17:05', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642256501, 1264622039642256421, '[0],[1264622039642256421],', '已收公告', 'sys_notice_mgr_received', 1, NULL, '/noticeReceived', 'system/noticeReceived/index', NULL, 'teamwork', 1, 'Y', NULL, NULL, 1, 20, NULL, 0, '2020-06-29 16:32:53', 1265476890672672808, '2022-07-18 21:17:05', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642256511, 1264622039642256501, '[0],[1264622039642256421],[1264622039642256501],', '已收公告查询', 'sys_notice_mgr_received_page', 2, NULL, NULL, NULL, 'sysNotice:received', 'teamwork', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-29 16:33:43', 1265476890672672808, '2022-07-18 21:17:05', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642256521, 0, '[0],', '文件管理', 'sys_file_mgr', 0, 'file', '/file', 'PageView', NULL, 'system', 1, 'Y', NULL, NULL, 1, 8, NULL, 0, '2020-06-24 17:31:10', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256531, 1264622039642256521, '[0],[1264622039642256521],', '系统文件', 'sys_file_mgr_sys_file', 1, NULL, '/file', 'system/file/index', NULL, 'system', 1, 'Y', NULL, NULL, 1, 21, NULL, 0, '2020-06-24 17:32:57', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256541, 1264622039642256531, '[0],[1264622039642256521],[1264622039642256531],', '文件查询', 'sys_file_mgr_sys_file_page', 2, NULL, NULL, NULL, 'sysFileInfo:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-24 17:35:38', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256551, 1264622039642256531, '[0],[1264622039642256521],[1264622039642256531],', '文件列表', 'sys_file_mgr_sys_file_list', 2, NULL, NULL, NULL, 'sysFileInfo:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-24 17:35:49', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256561, 1264622039642256531, '[0],[1264622039642256521],[1264622039642256531],', '文件删除', 'sys_file_mgr_sys_file_delete', 2, NULL, NULL, NULL, 'sysFileInfo:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-24 17:36:11', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256571, 1264622039642256531, '[0],[1264622039642256521],[1264622039642256531],', '文件详情', 'sys_file_mgr_sys_file_detail', 2, NULL, NULL, NULL, 'sysFileInfo:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-24 17:36:01', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256581, 1264622039642256531, '[0],[1264622039642256521],[1264622039642256531],', '文件上传', 'sys_file_mgr_sys_file_upload', 2, NULL, NULL, NULL, 'sysFileInfo:upload', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-24 17:34:29', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256591, 1264622039642256531, '[0],[1264622039642256521],[1264622039642256531],', '文件下载', 'sys_file_mgr_sys_file_download', 2, NULL, NULL, NULL, 'sysFileInfo:download', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-24 17:34:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256601, 1264622039642256531, '[0],[1264622039642256521],[1264622039642256531],', '图片预览', 'sys_file_mgr_sys_file_preview', 2, NULL, NULL, NULL, 'sysFileInfo:preview', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-24 17:35:19', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256611, 0, '[0],', '定时任务', 'sys_timers', 0, 'dashboard', '/timers', 'PageView', NULL, 'system', 1, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-01 17:17:20', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256621, 1264622039642256611, '[0],[1264622039642256611],', '任务管理', 'sys_timers_mgr', 1, NULL, '/timers', 'system/timers/index', NULL, 'system', 1, 'Y', NULL, NULL, 1, 22, NULL, 0, '2020-07-01 17:18:53', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256631, 1264622039642256621, '[0],[1264622039642256611],[1264622039642256621],', '定时任务查询', 'sys_timers_mgr_page', 2, NULL, NULL, NULL, 'sysTimers:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-01 17:19:43', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256641, 1264622039642256621, '[0],[1264622039642256611],[1264622039642256621],', '定时任务列表', 'sys_timers_mgr_list', 2, NULL, NULL, NULL, 'sysTimers:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-01 17:19:56', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256651, 1264622039642256621, '[0],[1264622039642256611],[1264622039642256621],', '定时任务详情', 'sys_timers_mgr_detail', 2, NULL, NULL, NULL, 'sysTimers:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-01 17:20:10', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256661, 1264622039642256621, '[0],[1264622039642256611],[1264622039642256621],', '定时任务增加', 'sys_timers_mgr_add', 2, NULL, NULL, NULL, 'sysTimers:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-01 17:20:23', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256671, 1264622039642256621, '[0],[1264622039642256611],[1264622039642256621],', '定时任务删除', 'sys_timers_mgr_delete', 2, NULL, NULL, NULL, 'sysTimers:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-01 17:20:33', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256681, 1264622039642256621, '[0],[1264622039642256611],[1264622039642256621],', '定时任务编辑', 'sys_timers_mgr_edit', 2, NULL, NULL, NULL, 'sysTimers:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-01 17:20:43', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256691, 1264622039642256621, '[0],[1264622039642256611],[1264622039642256621],', '定时任务可执行列表', 'sys_timers_mgr_get_action_classes', 2, NULL, NULL, NULL, 'sysTimers:getActionClasses', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-01 17:22:16', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256701, 1264622039642256621, '[0],[1264622039642256611],[1264622039642256621],', '定时任务启动', 'sys_timers_mgr_start', 2, NULL, NULL, NULL, 'sysTimers:start', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-01 17:22:32', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256711, 1264622039642256621, '[0],[1264622039642256611],[1264622039642256621],', '定时任务关闭', 'sys_timers_mgr_stop', 2, NULL, NULL, NULL, 'sysTimers:stop', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-01 17:22:43', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256721, 0, '[0],', '区域管理', 'sys_area', 0, 'environment', '/area', 'PageView', NULL, 'system', 1, 'Y', NULL, NULL, 1, 100, NULL, 0, '2021-05-19 13:55:40', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256731, 1264622039642256721, '[0],[1264622039642256721],', '系统区域', 'sys_area_mgr', 1, NULL, '/area', 'system/area/index', NULL, 'system', 1, 'Y', NULL, NULL, 1, 100, NULL, 0, '2021-05-19 13:57:42', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256741, 1264622039642256731, '[0],[1264622039642256721],[1264622039642256731],', '系统区域列表', 'sys_area_mgr_list', 2, NULL, NULL, NULL, 'sysArea:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2021-05-19 14:01:39', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1342445437296771074, 0, '[0],', '代码生成', 'code_gen', 1, 'thunderbolt', '/codeGenerate/index', 'gen/codeGenerate/index', NULL, 'system_tool', 1, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-12-25 20:21:48', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1410859007809736705, 1264622039642256521, '[0],[1264622039642256521],', '在线文档', 'file_oline', 1, '', '/file_oline', 'system/fileOnline/index', '', 'system', 1, 'Y', NULL, '', 1, 100, NULL, 0, '2021-07-02 15:12:55', 1265476890672672808, '2021-08-25 20:02:46', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1534384317233524738, 0, '[0],', '基础数据', 'basic_data', 0, 'setting', '/basicData', 'PageView', '', 'manufacturing', 1, 'Y', NULL, '', 1, 11, NULL, 0, '2022-06-08 11:58:23', 1265476890672672808, '2022-06-08 13:42:49', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1534410297847214081, 0, '[0],', '生产管理', 'prod_manage', 0, 'hdd', '/prodManage', 'PageView', '', 'manufacturing', 1, 'Y', NULL, '', 1, 12, NULL, 0, '2022-06-08 13:41:38', 1265476890672672808, '2022-06-08 13:42:54', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1534411830716350466, 0, '[0],', '库存管理', 'inv_mgt', 0, 'desktop', '/inv/mgt', 'PageView', '', 'manufacturing', 0, 'Y', NULL, '', 1, 100, NULL, 0, '2022-06-08 13:47:43', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1538776469361139714, 0, '[0],', '大屏展示', 'bigScreen', 1, 'radar-chart', '/bigScreen/index', 'main/bigScreen/index', '', 'manufacturing', 1, 'Y', NULL, '', 1, 1, NULL, 0, '2022-06-20 14:51:14', 1265476890672672808, '2022-06-20 14:52:41', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1542409869531873282, 7346283523793183379, '[0],[1534410297847214081],[7346283523793183379],', '报工审批', 'work_report_approval', 2, '', '', '', 'workReport:approval', 'manufacturing', 0, 'Y', '', '', 1, 100, NULL, 0, '2022-06-30 15:29:04', 1265476890672672808, '2022-06-30 15:29:19', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1551480636561321985, 6527167616560933135, '[0],[1534384317233524738],[6527167616560933135],', '产品类型树', 'protype_index_tree', 2, '', '', '', 'proType:tree', 'manufacturing', 0, 'Y', '', '', 2, 100, NULL, 0, '2022-07-25 16:13:04', 1265476890672672808, '2022-07-25 16:13:37', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1552209067446579201, 0, '[0],', '采购管理', 'purc_manage', 0, 'schedule', '/purcManage', 'PageView', '', 'manufacturing', 1, 'Y', NULL, '', 1, 100, NULL, 0, '2022-07-27 16:27:35', 1265476890672672808, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1558006778241753089, 0, '[0],', '供应商管理', 'supplier', 0, 'rest', '/supplier', 'PageView', '', 'crm', 1, 'Y', NULL, '', 1, 100, NULL, 0, '2022-08-12 16:25:37', 1265476890672672808, '2022-08-12 16:27:47', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1558006914472747010, 0, '[0],', '客户管理', 'customer', 0, 'usergroup-add', '/customer', 'PageView', '', 'crm', 1, 'Y', NULL, '', 1, 100, NULL, 0, '2022-08-12 16:26:09', 1265476890672672808, '2022-08-12 16:27:11', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1560145190851772417, 0, '[0],', '接口管理', 'api', 1, '', '/api', 'api', '', 'manufacturing', 1, 'N', '', '', 1, 9, NULL, 0, '2022-08-18 14:02:54', 1265476890672672808, '2022-08-18 14:33:22', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1560158090005905410, 9202857208449608897, '[0],[1534410297847214081],[9202857208449608897],', '任务开始', 'task_index_start', 2, '', '', '', 'task:start', 'manufacturing', 0, 'Y', '', '', 1, 100, NULL, 0, '2022-08-18 14:54:10', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1560158326283632641, 9202857208449608897, '[0],[1534410297847214081],[9202857208449608897],', '任务结束', 'task_index_end', 2, '', 'task:end', '', 'task:end', 'manufacturing', 0, 'Y', '', '', 1, 100, NULL, 0, '2022-08-18 14:55:06', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1562329827773108225, 5000566847705099773, '[0],[1534411830716350466],[5000566847705099773],', '仓库树', 'warehouse_index_tree', 2, '', '', '', 'wareHouse:tree', 'manufacturing', 0, 'Y', '', '', 2, 100, NULL, 0, '2022-08-24 14:43:52', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1574929774689878018, 4794640277308881312, '[0],[1558006778241753089],[4794640277308881312],', '供应商分类树', 'suppSort_index_tree', 2, '', '', '', 'suppSort:tree', 'crm', 0, 'Y', '', '', 1, 100, NULL, 0, '2022-09-28 09:11:34', 1574927397500977154, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1574930219948802050, 7072750871619746982, '[0],[1558006914472747010],[7072750871619746982],', '客户分类树', 'cusSort_index_tree', 2, '', '', '', 'cusSort:tree', 'crm', 0, 'Y', '', '', 1, 100, NULL, 0, '2022-09-28 09:13:20', 1574927397500977154, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1574930637298827265, 8957317986837088422, '[0],[8957317986837088422],', '字段配置信息列表', 'customCode_index_InformationList', 2, '', '', '', 'customCode:InformationList', 'manufacturing', 0, 'Y', '', '', 1, 100, NULL, 0, '2022-09-28 09:14:59', 1574927397500977154, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1574930880073531393, 5474507168841280952, '[0],[5474507168841280952],', '编号规则信息列表', 'codeGenerate_index_InformationList', 2, '', '/codeGenerate/InformationList', '', 'codeGenerate:InformationList', 'manufacturing', 0, 'Y', '', '', 1, 100, NULL, 0, '2022-09-28 09:15:57', 1574927397500977154, NULL, NULL);
INSERT INTO `sys_menu` VALUES (4621333945127042674, 8957317986837088422, '[0],[8957317986837088422],', '字段配置新增', 'fieldconfig_index_add', 2, NULL, NULL, NULL, 'fieldConfig:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (4674816360932725215, 7072750871619746982, '[0],[1558006914472747010],[7072750871619746982],', '客户分类查询', 'cussort_index_page', 2, NULL, NULL, NULL, 'cusSort:page', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (4678695510847297372, 4794640277308881312, '[0],[1558006778241753089],[4794640277308881312],', '供应商分类新增', 'suppsort_index_add', 2, NULL, NULL, NULL, 'suppSort:add', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:25', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (4700141466883779130, 8287951873357908072, '[0],[1552209067446579201],[8287951873357908072],', '采购细明导出', 'puordetail_index_export', 2, NULL, NULL, NULL, 'puorDetail:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (4719172401435180793, 7128085356547940830, '[0],[1534384317233524738],[7128085356547940830],', '物料清单编辑', 'bom_index_edit', 2, NULL, NULL, NULL, 'bom:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:26:55', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (4757443910107701590, 8477499608364957015, '[0],[1534410297847214081],[8477499608364957015],', '生产计划编辑', 'proplan_index_edit', 2, NULL, NULL, NULL, 'proPlan:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-21 16:58:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (4770899845682577376, 5474507168841280952, '[0],[5474507168841280952],', '自定义编号规则查询', 'customcode_index_page', 2, NULL, NULL, NULL, 'customCode:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (4794640277308881312, 1558006778241753089, '[0],[1558006778241753089],', '供应商分类', 'suppsort_index', 1, 'ordered-list', '/suppSort', 'main/suppsort/index', NULL, 'crm', 1, 'Y', NULL, NULL, 1, 50, NULL, 0, NULL, NULL, '2022-08-12 16:28:25', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (4830986669641223230, 6507903030541782366, '[0],[1534410297847214081],[6507903030541782366],', '工单表新增', 'workorder_index_add', 2, NULL, NULL, NULL, 'workOrder:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:18', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (4832107133629024589, 6436044181834026359, '[0],[1534384317233524738],[6436044181834026359],', '产品表列表', 'pro_index_list', 2, NULL, NULL, NULL, 'pro:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:36:37', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (4834264129878073409, 8287951873357908072, '[0],[1552209067446579201],[8287951873357908072],', '采购细明删除', 'puordetail_index_delete', 2, NULL, NULL, NULL, 'puorDetail:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (4835110481928644952, 7128085356547940830, '[0],[1534384317233524738],[7128085356547940830],', '物料清单列表', 'bom_index_list', 2, NULL, NULL, NULL, 'bom:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:26:55', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (4890328064515383248, 0, '[0],', 'SaaS租户', 'tenantinfo_index', 1, NULL, '/tenantInfo', 'main/tenantinfo/index', NULL, 'ht', 1, 'Y', NULL, NULL, 1, 100, NULL, 2, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (4903707486851837034, 7072750871619746982, '[0],[1558006914472747010],[7072750871619746982],', '客户分类导出', 'cussort_index_export', 2, NULL, NULL, NULL, 'cusSort:export', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (4967796364303223216, 8633823152794346306, '[0],[1534411830716350466],[8633823152794346306],', '出库单列表', 'invOut_index_list', 2, NULL, NULL, NULL, 'invOut:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:48:27', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (4998978684524443692, 1534384317233524738, '[0],[1534384317233524738],', '工序', 'workstep_index', 1, 'deployment-unit', '/workStep', 'main/workstep/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2022-08-12 16:29:40', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5000566847705099773, 1534411830716350466, '[0],[1534411830716350466],', '仓库管理', 'warehouse_index', 1, 'hdd', '/wareHouse', 'main/warehouse/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 16:49:13', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5002055413507195458, 8016744771368544956, '[0],[1534411830716350466],[8016744771368544956],', '出入库明细新增', 'invdetail_index_add', 2, NULL, NULL, NULL, 'invDetail:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-10 16:05:47', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5012839659883217263, 1534411830716350466, '[0],[1534411830716350466],', '入库单', 'invIn_index', 1, 'login', '/invIn', 'main/invIn/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2022-06-08 13:49:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5014700545593967407, 9202857208449608897, '[0],[1534410297847214081],[9202857208449608897],', '任务列表', 'task_index_list', 2, NULL, NULL, NULL, 'task:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:30', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5016248121790626902, 8477499608364957015, '[0],[1534410297847214081],[8477499608364957015],', '生产计划导出', 'proplan_index_export', 2, NULL, NULL, NULL, 'proPlan:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-21 16:58:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5027224879967170377, 6414357916883420801, '[0],[1558006778241753089],[6414357916883420801],', '供应商联系人查询', 'suppperson_index_page', 2, NULL, NULL, NULL, 'suppPerson:page', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:27:59', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5079108238558434584, 7702400310012060990, '[0],[1534384317233524738],[7702400310012060990],', '工艺路线列表', 'workroute_index_list', 2, NULL, NULL, NULL, 'workRoute:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:34', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5091294346539048091, 4890328064515383248, '[0],[4890328064515383248],', 'SaaS租户删除', 'tenantinfo_index_delete', 2, NULL, NULL, NULL, 'tenantInfo:delete', 'ht', 0, 'Y', NULL, NULL, 1, 100, NULL, 2, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (5101364812197334235, 6700801806460342017, '[0],[6700801806460342017],', '供应商分类新增', 'suppsort_index_add', 2, NULL, NULL, NULL, 'suppSort:add', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 2, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (5142735407151148234, 6527167616560933135, '[0],[1534384317233524738],[6527167616560933135],', '产品类型表列表', 'protype_index_list', 2, NULL, NULL, NULL, 'proType:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 12:00:45', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5162865219757254273, 8287951873357908072, '[0],[1552209067446579201],[8287951873357908072],', '采购细明查询', 'puordetail_index_page', 2, NULL, NULL, NULL, 'puorDetail:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5196743176498588333, 6134354751483903106, '[0],[1558006778241753089],[6134354751483903106],', ' 供应商资料查询', 'suppdata_index_page', 2, NULL, NULL, NULL, 'suppData:page', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:14', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5197311839117522988, 6414357916883420801, '[0],[1558006778241753089],[6414357916883420801],', '供应商联系人新增', 'suppperson_index_add', 2, NULL, NULL, NULL, 'suppPerson:add', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:27:59', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5206868747918117433, 8477499608364957015, '[0],[1534410297847214081],[8477499608364957015],', '生产计划新增', 'proplan_index_add', 2, NULL, NULL, NULL, 'proPlan:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-21 16:58:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5228961670953770885, 5445032970491536505, '[0],[1534410297847214081],[5445032970491536505],', '装配工单新增', 'ass_index_add', 2, NULL, NULL, NULL, 'ass:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-09-06 13:55:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5234105579337808561, 5445032970491536505, '[0],[1534410297847214081],[5445032970491536505],', '装配工单删除', 'ass_index_delete', 2, NULL, NULL, NULL, 'ass:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-09-06 13:55:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5243613315131080710, 7702400310012060990, '[0],[1534384317233524738],[7702400310012060990],', '工艺路线查询', 'workroute_index_page', 2, NULL, NULL, NULL, 'workRoute:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:34', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5274675770695528925, 8422209827465537355, '[0],[1552209067446579201],[8422209827465537355],', '采购订单删除', 'puororder_index_delete', 2, NULL, NULL, NULL, 'puorOrder:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5276828216703973340, 8436831550169549160, '[0],[1558006914472747010],[8436831550169549160],', '客户资料新增', 'cusinfor_index_add', 2, NULL, NULL, NULL, 'cusInfor:add', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:34', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5279583233581044419, 6436044181834026359, '[0],[1534384317233524738],[6436044181834026359],', '产品表编辑', 'pro_index_edit', 2, NULL, NULL, NULL, 'pro:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:36:37', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5327083612717101630, 6667327679590719676, '[0],[1534410297847214081],[6667327679590719676],', '工单任务关系表删除', 'workordertask_index_delete', 2, NULL, NULL, NULL, 'workOrderTask:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:44:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5346549595871021092, 8700710314151240148, '[0],[1558006914472747010],[8700710314151240148],', '客户联系人查看', 'cusperson_index_detail', 2, NULL, NULL, NULL, 'cusPerson:detail', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:42', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5378908852735764919, 7346283523793183379, '[0],[1534410297847214081],[7346283523793183379],', '报工编辑', 'workreport_index_edit', 2, NULL, NULL, NULL, 'workReport:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:49', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5382528335658667107, 8422209827465537355, '[0],[1552209067446579201],[8422209827465537355],', '采购订单编辑', 'puororder_index_edit', 2, NULL, NULL, NULL, 'puorOrder:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5445032970491536505, 1534410297847214081, '[0],[1534410297847214081],', '装配工单', 'ass_index', 1, 'bg-colors', '/ass', 'main/ass/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 2, NULL, 0, NULL, NULL, '2022-09-06 13:56:29', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5448185299220002627, 8436831550169549160, '[0],[1558006914472747010],[8436831550169549160],', '客户资料删除', 'cusinfor_index_delete', 2, NULL, NULL, NULL, 'cusInfor:delete', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:34', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5457126757845470309, 5934688735467584877, '[0],[1534410297847214081],[5934688735467584877],', '销售订单删除', 'saleorder_index_delete', 2, NULL, NULL, NULL, 'saleOrder:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 11:17:42', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5459011313540319790, 5721174223820977430, '[0],[1534411830716350466],[5721174223820977430],', '库存余额导出', 'stockbalance_index_export', 2, NULL, NULL, NULL, 'stockBalance:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-28 16:50:46', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5465134665687105949, 6414357916883420801, '[0],[1558006778241753089],[6414357916883420801],', '供应商联系人查看', 'suppperson_index_detail', 2, NULL, NULL, NULL, 'suppPerson:detail', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:27:59', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5469043739505193743, 5721174223820977430, '[0],[1534411830716350466],[5721174223820977430],', '库存余额查询', 'stockbalance_index_page', 2, NULL, NULL, NULL, 'stockBalance:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-28 16:50:46', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5474507168841280952, 0, '[0],', '编号规则', 'customcode_index', 1, 'setting', '/customCode', 'main/customcode/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (5480200211927228567, 5474507168841280952, '[0],[5474507168841280952],', '自定义编号规则新增', 'customcode_index_add', 2, NULL, NULL, NULL, 'customCode:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (5523506981641034218, 8477499608364957015, '[0],[1534410297847214081],[8477499608364957015],', '生产计划查询', 'proplan_index_page', 2, NULL, NULL, NULL, 'proPlan:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-21 16:58:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5553078347948343545, 6527167616560933135, '[0],[1534384317233524738],[6527167616560933135],', '产品类型表导出', 'protype_index_export', 2, NULL, NULL, NULL, 'proType:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 12:00:45', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5555524248707858432, 5000566847705099773, '[0],[1534411830716350466],[5000566847705099773],', '仓库管理查询', 'warehouse_index_page', 2, NULL, NULL, NULL, 'wareHouse:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 16:37:01', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5587279777867961498, 8700710314151240148, '[0],[1558006914472747010],[8700710314151240148],', '客户联系人删除', 'cusperson_index_delete', 2, NULL, NULL, NULL, 'cusPerson:delete', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:42', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5608609939719113803, 6700801806460342017, '[0],[6700801806460342017],', '供应商分类查看', 'suppsort_index_detail', 2, NULL, NULL, NULL, 'suppSort:detail', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 2, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (5624458298951752771, 5012839659883217263, '[0],[1534411830716350466],[5012839659883217263],', '入库单导出', 'invIn_index_export', 2, NULL, NULL, NULL, 'invIn:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:49:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5642803668063215805, 9202857208449608897, '[0],[1534410297847214081],[9202857208449608897],', '任务编辑', 'task_index_edit', 2, NULL, NULL, NULL, 'task:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:30', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5653092812632926489, 5474507168841280952, '[0],[5474507168841280952],', '自定义编号规则删除', 'customcode_index_delete', 2, NULL, NULL, NULL, 'customCode:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (5668705457330168859, 6667327679590719676, '[0],[1534410297847214081],[6667327679590719676],', '工单任务关系表查询', 'workordertask_index_page', 2, NULL, NULL, NULL, 'workOrderTask:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:44:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5707993274387362828, 4998978684524443692, '[0],[1534384317233524738],[4998978684524443692],', '工序新增', 'workstep_index_add', 2, NULL, NULL, NULL, 'workStep:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:13', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5714641428045333341, 5000566847705099773, '[0],[1534411830716350466],[5000566847705099773],', '仓库管理导出', 'warehouse_index_export', 2, NULL, NULL, NULL, 'wareHouse:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 16:37:01', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5721174223820977430, 1534411830716350466, '[0],[1534411830716350466],', '库存余额', 'stockbalance_index', 1, 'gold', '/stockBalance', 'main/stockbalance/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-28 16:50:46', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5753930915061776016, 7346283523793183379, '[0],[1534410297847214081],[7346283523793183379],', '报工列表', 'workreport_index_list', 2, NULL, NULL, NULL, 'workReport:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:49', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5788080164048630938, 7702400310012060990, '[0],[1534384317233524738],[7702400310012060990],', '工艺路线编辑', 'workroute_index_edit', 2, NULL, NULL, NULL, 'workRoute:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:34', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5801879553212456867, 5000566847705099773, '[0],[1534411830716350466],[5000566847705099773],', '仓库管理新增', 'warehouse_index_add', 2, NULL, NULL, NULL, 'wareHouse:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 16:37:01', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5803272132980049305, 8016744771368544956, '[0],[1534411830716350466],[8016744771368544956],', '出入库明细查询', 'invdetail_index_page', 2, NULL, NULL, NULL, 'invDetail:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-10 16:05:47', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5820472386138045917, 8426505795261326687, '[0],[1534384317233524738],[8426505795261326687],', '工序路线关系表查询', 'worksteproute_index_page', 2, NULL, NULL, NULL, 'workStepRoute:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:39:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5846194751422568093, 8957317986837088422, '[0],[8957317986837088422],', '字段配置导出', 'fieldconfig_index_export', 2, NULL, NULL, NULL, 'fieldConfig:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (5871841993475141582, 8436831550169549160, '[0],[1558006914472747010],[8436831550169549160],', '客户资料列表', 'cusinfor_index_list', 2, NULL, NULL, NULL, 'cusInfor:list', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:34', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5886355131876064420, 6667327679590719676, '[0],[1534410297847214081],[6667327679590719676],', '工单任务关系表编辑', 'workordertask_index_edit', 2, NULL, NULL, NULL, 'workOrderTask:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:44:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5886900280768283007, 8957317986837088422, '[0],[8957317986837088422],', '字段配置编辑', 'fieldconfig_index_edit', 2, NULL, NULL, NULL, 'fieldConfig:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (5893520734687245059, 8633823152794346306, '[0],[1534411830716350466],[8633823152794346306],', '出库单删除', 'invOut_index_delete', 2, NULL, NULL, NULL, 'invOut:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:48:27', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5901981891345615661, 5000566847705099773, '[0],[1534411830716350466],[5000566847705099773],', '仓库管理列表', 'warehouse_index_list', 2, NULL, NULL, NULL, 'wareHouse:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 16:37:01', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5934688735467584877, 1534410297847214081, '[0],[1534410297847214081],', '销售订单', 'saleorder_index', 1, 'file-ppt', '/saleOrder', 'main/saleorder/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 2, NULL, 0, NULL, NULL, '2022-08-12 16:29:22', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5999927780812383399, 8422209827465537355, '[0],[1552209067446579201],[8422209827465537355],', '采购订单列表', 'puororder_index_list', 2, NULL, NULL, NULL, 'puorOrder:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6015462925549607928, 7128085356547940830, '[0],[1534384317233524738],[7128085356547940830],', '物料清单导出', 'bom_index_export', 2, NULL, NULL, NULL, 'bom:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:26:55', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6020421073902938241, 6667327679590719676, '[0],[1534410297847214081],[6667327679590719676],', '工单任务关系表导出', 'workordertask_index_export', 2, NULL, NULL, NULL, 'workOrderTask:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:44:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6037995103127006961, 4890328064515383248, '[0],[4890328064515383248],', 'SaaS租户导出', 'tenantinfo_index_export', 2, NULL, NULL, NULL, 'tenantInfo:export', 'ht', 0, 'Y', NULL, NULL, 1, 100, NULL, 2, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (6045763991836034103, 5934688735467584877, '[0],[1534410297847214081],[5934688735467584877],', '销售订单新增', 'saleorder_index_add', 2, NULL, NULL, NULL, 'saleOrder:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 11:17:42', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6046397975825687693, 8422209827465537355, '[0],[1552209067446579201],[8422209827465537355],', '采购订单新增', 'puororder_index_add', 2, NULL, NULL, NULL, 'puorOrder:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6103813152661113624, 8426505795261326687, '[0],[1534384317233524738],[8426505795261326687],', '工序路线关系表列表', 'worksteproute_index_list', 2, NULL, NULL, NULL, 'workStepRoute:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:39:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6116805160244737678, 8426505795261326687, '[0],[1534384317233524738],[8426505795261326687],', '工序路线关系表删除', 'worksteproute_index_delete', 2, NULL, NULL, NULL, 'workStepRoute:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:39:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6122595225588020183, 6507903030541782366, '[0],[1534410297847214081],[6507903030541782366],', '工单表编辑', 'workorder_index_edit', 2, NULL, NULL, NULL, 'workOrder:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:18', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6122993178045887049, 6134354751483903106, '[0],[1558006778241753089],[6134354751483903106],', ' 供应商资料列表', 'suppdata_index_list', 2, NULL, NULL, NULL, 'suppData:list', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:14', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6134354751483903106, 1558006778241753089, '[0],[1558006778241753089],', '供应商资料', 'suppdata_index', 1, 'solution', '/suppData', 'main/suppdata/index', NULL, 'crm', 1, 'Y', NULL, NULL, 1, 50, NULL, 0, NULL, NULL, '2022-08-12 16:28:14', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6194217070047486945, 7702400310012060990, '[0],[1534384317233524738],[7702400310012060990],', '工艺路线删除', 'workroute_index_delete', 2, NULL, NULL, NULL, 'workRoute:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:34', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6201934794874381431, 6700801806460342017, '[0],[6700801806460342017],', '供应商分类删除', 'suppsort_index_delete', 2, NULL, NULL, NULL, 'suppSort:delete', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 2, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (6204862670753579307, 7128085356547940830, '[0],[1534384317233524738],[7128085356547940830],', '物料清单新增', 'bom_index_add', 2, NULL, NULL, NULL, 'bom:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:26:55', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6238776219082572290, 8700710314151240148, '[0],[1558006914472747010],[8700710314151240148],', '客户联系人编辑', 'cusperson_index_edit', 2, NULL, NULL, NULL, 'cusPerson:edit', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:42', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6249597959063106635, 4998978684524443692, '[0],[1534384317233524738],[4998978684524443692],', '工序删除', 'workstep_index_delete', 2, NULL, NULL, NULL, 'workStep:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:13', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6257208246505535137, 5934688735467584877, '[0],[1534410297847214081],[5934688735467584877],', '销售订单编辑', 'saleorder_index_edit', 2, NULL, NULL, NULL, 'saleOrder:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 11:17:42', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6280770118494855200, 5012839659883217263, '[0],[1534411830716350466],[5012839659883217263],', '入库单列表', 'invIn_index_list', 2, NULL, NULL, NULL, 'invIn:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:49:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6290232610677682017, 8016744771368544956, '[0],[1534411830716350466],[8016744771368544956],', '出入库明细导出', 'invdetail_index_export', 2, NULL, NULL, NULL, 'invDetail:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-10 16:05:47', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6299920358694835309, 9202857208449608897, '[0],[1534410297847214081],[9202857208449608897],', '任务查看', 'task_index_detail', 2, NULL, NULL, NULL, 'task:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:30', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6326961474985796183, 8633823152794346306, '[0],[1534411830716350466],[8633823152794346306],', '出库单新增', 'invOut_index_add', 2, NULL, NULL, NULL, 'invOut:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:48:27', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6329171771009724483, 6667327679590719676, '[0],[1534410297847214081],[6667327679590719676],', '工单任务关系表列表', 'workordertask_index_list', 2, NULL, NULL, NULL, 'workOrderTask:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:44:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6347609996552577764, 8016744771368544956, '[0],[1534411830716350466],[8016744771368544956],', '出入库明细查看', 'invdetail_index_detail', 2, NULL, NULL, NULL, 'invDetail:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-10 16:05:47', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6354039257511829492, 5934688735467584877, '[0],[1534410297847214081],[5934688735467584877],', '销售订单查看', 'saleorder_index_detail', 2, NULL, NULL, NULL, 'saleOrder:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 11:17:42', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6374585520258378360, 8426505795261326687, '[0],[1534384317233524738],[8426505795261326687],', '工序路线关系表导出', 'worksteproute_index_export', 2, NULL, NULL, NULL, 'workStepRoute:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:39:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6385901691378408995, 6700801806460342017, '[0],[6700801806460342017],', '供应商分类导出', 'suppsort_index_export', 2, NULL, NULL, NULL, 'suppSort:export', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 2, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (6405406552095053594, 8422209827465537355, '[0],[1552209067446579201],[8422209827465537355],', '采购订单查询', 'puororder_index_page', 2, NULL, NULL, NULL, 'puorOrder:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6414357916883420801, 1558006778241753089, '[0],[1558006778241753089],', '供应商联系人', 'suppperson_index', 1, 'team', '/suppPerson', 'main/suppperson/index', NULL, 'crm', 1, 'Y', NULL, NULL, 1, 50, NULL, 0, NULL, NULL, '2022-08-12 16:27:59', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6429021782911745716, 7072750871619746982, '[0],[1558006914472747010],[7072750871619746982],', '客户分类删除', 'cussort_index_delete', 2, NULL, NULL, NULL, 'cusSort:delete', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6436044181834026359, 1534384317233524738, '[0],[1534384317233524738],', '产品管理', 'pro_index', 1, 'barcode', '/pro', 'main/pro/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 4, NULL, 0, NULL, NULL, '2022-08-12 16:29:52', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6449912099524079616, 4998978684524443692, '[0],[1534384317233524738],[4998978684524443692],', '工序导出', 'workstep_index_export', 2, NULL, NULL, NULL, 'workStep:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:13', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6457085339840740289, 5445032970491536505, '[0],[1534410297847214081],[5445032970491536505],', '装配工单导出', 'ass_index_export', 2, NULL, NULL, NULL, 'ass:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-09-06 13:55:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6494634442879742520, 6414357916883420801, '[0],[1558006778241753089],[6414357916883420801],', '供应商联系人删除', 'suppperson_index_delete', 2, NULL, NULL, NULL, 'suppPerson:delete', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:27:59', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6507903030541782366, 1534410297847214081, '[0],[1534410297847214081],', '工单', 'workorder_index', 1, 'cloud-download', '/workOrder', 'main/workorder/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 3, NULL, 0, NULL, NULL, '2022-08-12 16:29:28', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6527167616560933135, 1534384317233524738, '[0],[1534384317233524738],', '产品类型', 'protype_index', 1, 'cluster', '/proType', 'main/protype/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 3, NULL, 0, NULL, NULL, '2022-08-12 16:29:28', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6534238167742390015, 6700801806460342017, '[0],[6700801806460342017],', '供应商分类查询', 'suppsort_index_page', 2, NULL, NULL, NULL, 'suppSort:page', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 2, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (6535734057645557495, 6414357916883420801, '[0],[1558006778241753089],[6414357916883420801],', '供应商联系人列表', 'suppperson_index_list', 2, NULL, NULL, NULL, 'suppPerson:list', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:27:59', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6542835870761280820, 5445032970491536505, '[0],[1534410297847214081],[5445032970491536505],', '装配工单查询', 'ass_index_page', 2, NULL, NULL, NULL, 'ass:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-09-06 13:55:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6547301851685353031, 6527167616560933135, '[0],[1534384317233524738],[6527167616560933135],', '产品类型表查看', 'protype_index_detail', 2, NULL, NULL, NULL, 'proType:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 12:00:45', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6570253563256899359, 6527167616560933135, '[0],[1534384317233524738],[6527167616560933135],', '产品类型表新增', 'protype_index_add', 2, NULL, NULL, NULL, 'proType:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 12:00:45', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6591367648784009715, 7128085356547940830, '[0],[1534384317233524738],[7128085356547940830],', '物料清单查询', 'bom_index_page', 2, NULL, NULL, NULL, 'bom:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:26:55', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6655166482691941995, 8436831550169549160, '[0],[1558006914472747010],[8436831550169549160],', '客户资料导出', 'cusinfor_index_export', 2, NULL, NULL, NULL, 'cusInfor:export', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:34', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6659298240010835782, 6414357916883420801, '[0],[1558006778241753089],[6414357916883420801],', '供应商联系人编辑', 'suppperson_index_edit', 2, NULL, NULL, NULL, 'suppPerson:edit', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:27:59', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6667327679590719676, 1534410297847214081, '[0],[1534410297847214081],', '工单任务关系', 'workordertask_index', 1, 'box-plot', '/workOrderTask', 'main/workordertask/index', NULL, 'manufacturing', 1, 'N', NULL, NULL, 1, 3, NULL, 0, NULL, NULL, '2022-06-28 14:11:59', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6696300230304937737, 8436831550169549160, '[0],[1558006914472747010],[8436831550169549160],', '客户资料查询', 'cusinfor_index_page', 2, NULL, NULL, NULL, 'cusInfor:page', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:34', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6700801806460342017, 0, '[0],', '供应商分类', 'suppsort_index', 1, '', '/suppSort', 'main/suppsort/index', NULL, 'crm', 1, 'Y', NULL, NULL, 1, 100, NULL, 2, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (6704297887824939633, 6700801806460342017, '[0],[6700801806460342017],', '供应商分类列表', 'suppsort_index_list', 2, NULL, NULL, NULL, 'suppSort:list', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 2, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (6709768906758836391, 4998978684524443692, '[0],[1534384317233524738],[4998978684524443692],', '工序查看', 'workstep_index_detail', 2, NULL, NULL, NULL, 'workStep:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:13', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6739113753546148532, 5000566847705099773, '[0],[1534411830716350466],[5000566847705099773],', '仓库管理查看', 'warehouse_index_detail', 2, NULL, NULL, NULL, 'wareHouse:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 16:37:01', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6813707923637624035, 8422209827465537355, '[0],[1552209067446579201],[8422209827465537355],', '采购订单导出', 'puororder_index_export', 2, NULL, NULL, NULL, 'puorOrder:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6845666717193712873, 6436044181834026359, '[0],[1534384317233524738],[6436044181834026359],', '产品表查看', 'pro_index_detail', 2, NULL, NULL, NULL, 'pro:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:36:37', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6860312372874164125, 8426505795261326687, '[0],[1534384317233524738],[8426505795261326687],', '工序路线关系表查看', 'worksteproute_index_detail', 2, NULL, NULL, NULL, 'workStepRoute:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:39:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6879316079334546932, 4890328064515383248, '[0],[4890328064515383248],', 'SaaS租户查询', 'tenantinfo_index_page', 2, NULL, NULL, NULL, 'tenantInfo:page', 'ht', 0, 'Y', NULL, NULL, 1, 100, NULL, 2, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (6900932255841430981, 8633823152794346306, '[0],[1534411830716350466],[8633823152794346306],', '出库单查询', 'invOut_index_page', 2, NULL, NULL, NULL, 'invOut:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:48:27', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6905244625723471705, 4998978684524443692, '[0],[1534384317233524738],[4998978684524443692],', '工序查询', 'workstep_index_page', 2, NULL, NULL, NULL, 'workStep:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:13', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6947194724669964805, 4794640277308881312, '[0],[1558006778241753089],[4794640277308881312],', '供应商分类删除', 'suppsort_index_delete', 2, NULL, NULL, NULL, 'suppSort:delete', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:25', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6996269839693777638, 4794640277308881312, '[0],[1558006778241753089],[4794640277308881312],', '供应商分类列表', 'suppsort_index_list', 2, NULL, NULL, NULL, 'suppSort:list', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:25', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7030392685370585623, 4890328064515383248, '[0],[4890328064515383248],', 'SaaS租户查看', 'tenantinfo_index_detail', 2, NULL, NULL, NULL, 'tenantInfo:detail', 'ht', 0, 'Y', NULL, NULL, 1, 100, NULL, 2, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (7044559182298687818, 5000566847705099773, '[0],[1534411830716350466],[5000566847705099773],', '仓库管理编辑', 'warehouse_index_edit', 2, NULL, NULL, NULL, 'wareHouse:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 16:37:01', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7047324015553475476, 7128085356547940830, '[0],[1534384317233524738],[7128085356547940830],', '物料清单查看', 'bom_index_detail', 2, NULL, NULL, NULL, 'bom:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:26:55', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7072750871619746982, 1558006914472747010, '[0],[1558006914472747010],', '客户分类', 'cussort_index', 1, 'folder-open', '/cusSort', 'main/cussort/index', NULL, 'crm', 1, 'Y', NULL, NULL, 1, 50, NULL, 0, NULL, NULL, '2022-08-12 16:28:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7119232719243003213, 4794640277308881312, '[0],[1558006778241753089],[4794640277308881312],', '供应商分类导出', 'suppsort_index_export', 2, NULL, NULL, NULL, 'suppSort:export', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:25', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7128085356547940830, 1534384317233524738, '[0],[1534384317233524738],', '物料清单', 'bom_index', 1, 'bg-colors', '/bom', 'main/bom/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:26:55', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7200474546305867050, 5474507168841280952, '[0],[5474507168841280952],', '自定义编号规则列表', 'customcode_index_list', 2, NULL, NULL, NULL, 'customCode:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (7209147716433162075, 6134354751483903106, '[0],[1558006778241753089],[6134354751483903106],', ' 供应商资料编辑', 'suppdata_index_edit', 2, NULL, NULL, NULL, 'suppData:edit', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:14', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7242870236365548592, 5474507168841280952, '[0],[5474507168841280952],', '自定义编号规则导出', 'customcode_index_export', 2, NULL, NULL, NULL, 'customCode:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (7254023492833901715, 5012839659883217263, '[0],[1534411830716350466],[5012839659883217263],', '入库单删除', 'invIn_index_delete', 2, NULL, NULL, NULL, 'invIn:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:49:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7287765970469918079, 5474507168841280952, '[0],[5474507168841280952],', '自定义编号规则查看', 'customcode_index_detail', 2, NULL, NULL, NULL, 'customCode:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (7288710125399936914, 7346283523793183379, '[0],[1534410297847214081],[7346283523793183379],', '报工新增', 'workreport_index_add', 2, NULL, NULL, NULL, 'workReport:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:49', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7294415501551357401, 4794640277308881312, '[0],[1558006778241753089],[4794640277308881312],', '供应商分类查看', 'suppsort_index_detail', 2, NULL, NULL, NULL, 'suppSort:detail', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:25', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7318591877639154878, 5012839659883217263, '[0],[1534411830716350466],[5012839659883217263],', '入库单编辑', 'invIn_index_edit', 2, NULL, NULL, NULL, 'invIn:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:49:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7323225129555267277, 8016744771368544956, '[0],[1534411830716350466],[8016744771368544956],', '出入库明细删除', 'invdetail_index_delete', 2, NULL, NULL, NULL, 'invDetail:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-10 16:05:47', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7337865129337018225, 6134354751483903106, '[0],[1558006778241753089],[6134354751483903106],', ' 供应商资料新增', 'suppdata_index_add', 2, NULL, NULL, NULL, 'suppData:add', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:14', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7346283523793183379, 1534410297847214081, '[0],[1534410297847214081],', '报工', 'workreport_index', 1, 'file', '/workReport', 'main/workreport/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 5, NULL, 0, NULL, NULL, '2022-08-12 16:29:42', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7357393608825227850, 5445032970491536505, '[0],[1534410297847214081],[5445032970491536505],', '装配工单编辑', 'ass_index_edit', 2, NULL, NULL, NULL, 'ass:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-09-06 13:55:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7380411400614516902, 8700710314151240148, '[0],[1558006914472747010],[8700710314151240148],', '客户联系人新增', 'cusperson_index_add', 2, NULL, NULL, NULL, 'cusPerson:add', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:42', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7388361180917918171, 4998978684524443692, '[0],[1534384317233524738],[4998978684524443692],', '工序列表', 'workstep_index_list', 2, NULL, NULL, NULL, 'workStep:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:13', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7395287038762629813, 8016744771368544956, '[0],[1534411830716350466],[8016744771368544956],', '出入库明细编辑', 'invdetail_index_edit', 2, NULL, NULL, NULL, 'invDetail:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-10 16:05:47', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7447629070175932292, 5445032970491536505, '[0],[1534410297847214081],[5445032970491536505],', '装配工单列表', 'ass_index_list', 2, NULL, NULL, NULL, 'ass:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-09-06 13:55:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7455112058491820064, 8957317986837088422, '[0],[8957317986837088422],', '字段配置删除', 'fieldconfig_index_delete', 2, NULL, NULL, NULL, 'fieldConfig:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (7516608184768822750, 8477499608364957015, '[0],[1534410297847214081],[8477499608364957015],', '生产计划删除', 'proplan_index_delete', 2, NULL, NULL, NULL, 'proPlan:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-21 16:58:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7529678467290942551, 6134354751483903106, '[0],[1558006778241753089],[6134354751483903106],', ' 供应商资料查看', 'suppdata_index_detail', 2, NULL, NULL, NULL, 'suppData:detail', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:14', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7561922286979539034, 5934688735467584877, '[0],[1534410297847214081],[5934688735467584877],', '销售订单查询', 'saleorder_index_page', 2, NULL, NULL, NULL, 'saleOrder:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 11:17:42', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7581916369308597479, 7128085356547940830, '[0],[1534384317233524738],[7128085356547940830],', '物料清单删除', 'bom_index_delete', 2, NULL, NULL, NULL, 'bom:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:26:55', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7596329328691560085, 7072750871619746982, '[0],[1558006914472747010],[7072750871619746982],', '客户分类新增', 'cussort_index_add', 2, NULL, NULL, NULL, 'cusSort:add', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7621956982866257638, 6527167616560933135, '[0],[1534384317233524738],[6527167616560933135],', '产品类型表查询', 'protype_index_page', 2, NULL, NULL, NULL, 'proType:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 12:00:45', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7629147488562302699, 8633823152794346306, '[0],[1534411830716350466],[8633823152794346306],', '出库单查看', 'invOut_index_detail', 2, NULL, NULL, NULL, 'invOut:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:48:27', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7635960329677377171, 7072750871619746982, '[0],[1558006914472747010],[7072750871619746982],', '客户分类编辑', 'cussort_index_edit', 2, NULL, NULL, NULL, 'cusSort:edit', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7639361031662168624, 6507903030541782366, '[0],[1534410297847214081],[6507903030541782366],', '工单表导出', 'workorder_index_export', 2, NULL, NULL, NULL, 'workOrder:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:18', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7656571819576826411, 9202857208449608897, '[0],[1534410297847214081],[9202857208449608897],', '任务添加', 'task_index_add', 2, NULL, NULL, NULL, 'task:add', 'manufacturing', 0, 'N', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-18 14:56:10', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7678659259268274624, 8957317986837088422, '[0],[8957317986837088422],', '字段配置查看', 'fieldconfig_index_detail', 2, NULL, NULL, NULL, 'fieldConfig:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (7682643462888492777, 8016744771368544956, '[0],[1534411830716350466],[8016744771368544956],', '出入库明细列表', 'invdetail_index_list', 2, NULL, NULL, NULL, 'invDetail:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-10 16:05:47', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7702400310012060990, 1534384317233524738, '[0],[1534384317233524738],', '工艺路线', 'workroute_index', 1, 'gateway', '/workRoute', 'main/workroute/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 2, NULL, 0, NULL, NULL, '2022-08-12 16:30:04', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7721498005843290304, 8700710314151240148, '[0],[1558006914472747010],[8700710314151240148],', '客户联系人查询', 'cusperson_index_page', 2, NULL, NULL, NULL, 'cusPerson:page', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:42', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7723724790018395451, 5721174223820977430, '[0],[1534411830716350466],[5721174223820977430],', '库存余额查看', 'stockbalance_index_detail', 2, NULL, NULL, NULL, 'stockBalance:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-28 16:50:46', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7764860386896651377, 5721174223820977430, '[0],[1534411830716350466],[5721174223820977430],', '库存余额删除', 'stockbalance_index_delete', 2, NULL, NULL, NULL, 'stockBalance:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-28 16:50:46', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7812048842906278623, 5012839659883217263, '[0],[1534411830716350466],[5012839659883217263],', '入库单新增', 'invIn_index_add', 2, NULL, NULL, NULL, 'invIn:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:49:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7845222658049694795, 8287951873357908072, '[0],[1552209067446579201],[8287951873357908072],', '采购细明编辑', 'puordetail_index_edit', 2, NULL, NULL, NULL, 'puorDetail:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7882898168203081196, 9202857208449608897, '[0],[1534410297847214081],[9202857208449608897],', '任务查询', 'task_index_page', 2, NULL, NULL, NULL, 'task:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:30', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7898061431233899837, 8436831550169549160, '[0],[1558006914472747010],[8436831550169549160],', '客户资料编辑', 'cusinfor_index_edit', 2, NULL, NULL, NULL, 'cusInfor:edit', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:34', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7909153361025684710, 8287951873357908072, '[0],[1552209067446579201],[8287951873357908072],', '采购细明查看', 'puordetail_index_detail', 2, NULL, NULL, NULL, 'puorDetail:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7936411315868128942, 7702400310012060990, '[0],[1534384317233524738],[7702400310012060990],', '工艺路线导出', 'workroute_index_export', 2, NULL, NULL, NULL, 'workRoute:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:34', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7940657177277675482, 7702400310012060990, '[0],[1534384317233524738],[7702400310012060990],', '工艺路线新增', 'workroute_index_add', 2, NULL, NULL, NULL, 'workRoute:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:34', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7946795106595000695, 6436044181834026359, '[0],[1534384317233524738],[6436044181834026359],', '产品表新增', 'pro_index_add', 2, NULL, NULL, NULL, 'pro:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:36:37', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7950819887425681478, 6436044181834026359, '[0],[1534384317233524738],[6436044181834026359],', '产品表删除', 'pro_index_delete', 2, NULL, NULL, NULL, 'pro:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:36:37', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7974141988632466544, 6507903030541782366, '[0],[1534410297847214081],[6507903030541782366],', '工单表删除', 'workorder_index_delete', 2, NULL, NULL, NULL, 'workOrder:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:18', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8000986046974593578, 6414357916883420801, '[0],[1558006778241753089],[6414357916883420801],', '供应商联系人导出', 'suppperson_index_export', 2, NULL, NULL, NULL, 'suppPerson:export', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:27:59', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8016744771368544956, 1534411830716350466, '[0],[1534411830716350466],', '出入库明细', 'invdetail_index', 1, 'bar-chart', '/invDetail', 'main/invdetail/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 3, NULL, 0, NULL, NULL, '2022-06-10 16:05:47', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8088901678692869692, 9202857208449608897, '[0],[1534410297847214081],[9202857208449608897],', '任务导出', 'task_index_export', 2, NULL, NULL, NULL, 'task:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-18 14:52:19', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8099644559700436461, 8957317986837088422, '[0],[8957317986837088422],', '字段配置查询', 'fieldconfig_index_page', 2, NULL, NULL, NULL, 'fieldConfig:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (8107628912599125262, 7072750871619746982, '[0],[1558006914472747010],[7072750871619746982],', '客户分类列表', 'cussort_index_list', 2, NULL, NULL, NULL, 'cusSort:list', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8175022271486866402, 4794640277308881312, '[0],[1558006778241753089],[4794640277308881312],', '供应商分类查询', 'suppsort_index_page', 2, NULL, NULL, NULL, 'suppSort:page', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:25', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8181816557328888214, 8700710314151240148, '[0],[1558006914472747010],[8700710314151240148],', '客户联系人列表', 'cusperson_index_list', 2, NULL, NULL, NULL, 'cusPerson:list', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:42', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8181822590852115190, 8426505795261326687, '[0],[1534384317233524738],[8426505795261326687],', '工序路线关系表编辑', 'worksteproute_index_edit', 2, NULL, NULL, NULL, 'workStepRoute:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:39:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8191527329183937863, 5445032970491536505, '[0],[1534410297847214081],[5445032970491536505],', '装配工单查看', 'ass_index_detail', 2, NULL, NULL, NULL, 'ass:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-09-06 13:55:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8197158197771689059, 6527167616560933135, '[0],[1534384317233524738],[6527167616560933135],', '产品类型表编辑', 'protype_index_edit', 2, NULL, NULL, NULL, 'proType:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 12:00:45', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8254320371052864398, 5721174223820977430, '[0],[1534411830716350466],[5721174223820977430],', '库存余额列表', 'stockbalance_index_list', 2, NULL, NULL, NULL, 'stockBalance:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-28 16:50:46', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8259333771823873238, 7072750871619746982, '[0],[1558006914472747010],[7072750871619746982],', '客户分类查看', 'cussort_index_detail', 2, NULL, NULL, NULL, 'cusSort:detail', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8273410905135864011, 5721174223820977430, '[0],[1534411830716350466],[5721174223820977430],', '库存余额编辑', 'stockbalance_index_edit', 2, NULL, NULL, NULL, 'stockBalance:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-28 16:50:46', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8275137972166495794, 4998978684524443692, '[0],[1534384317233524738],[4998978684524443692],', '工序编辑', 'workstep_index_edit', 2, NULL, NULL, NULL, 'workStep:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:13', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8287951873357908072, 1552209067446579201, '[0],[1552209067446579201],', '采购明细', 'puordetail_index', 1, NULL, '/puorDetail', 'main/puordetail/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-29 14:45:24', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8293588485966810596, 8477499608364957015, '[0],[1534410297847214081],[8477499608364957015],', '生产计划查看', 'proplan_index_detail', 2, NULL, NULL, NULL, 'proPlan:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-21 16:58:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8313819116019873398, 6507903030541782366, '[0],[1534410297847214081],[6507903030541782366],', '工单表列表', 'workorder_index_list', 2, NULL, NULL, NULL, 'workOrder:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:18', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8315301036989682205, 6436044181834026359, '[0],[1534384317233524738],[6436044181834026359],', '产品表导出', 'pro_index_export', 2, NULL, NULL, NULL, 'pro:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:36:37', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8342821565086833982, 5000566847705099773, '[0],[1534411830716350466],[5000566847705099773],', '仓库管理删除', 'warehouse_index_delete', 2, NULL, NULL, NULL, 'wareHouse:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 16:37:01', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8381831167975323900, 4890328064515383248, '[0],[4890328064515383248],', 'SaaS租户列表', 'tenantinfo_index_list', 2, NULL, NULL, NULL, 'tenantInfo:list', 'ht', 0, 'Y', NULL, NULL, 1, 100, NULL, 2, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (8404132105857398918, 4890328064515383248, '[0],[4890328064515383248],', 'SaaS租户新增', 'tenantinfo_index_add', 2, NULL, NULL, NULL, 'tenantInfo:add', 'ht', 0, 'Y', NULL, NULL, 1, 100, NULL, 2, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (8422209827465537355, 1552209067446579201, '[0],[1552209067446579201],', '采购订单', 'puororder_index', 1, NULL, '/puorOrder', 'main/puororder/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8424802293696759825, 6134354751483903106, '[0],[1558006778241753089],[6134354751483903106],', ' 供应商资料删除', 'suppdata_index_delete', 2, NULL, NULL, NULL, 'suppData:delete', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:14', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8426505795261326687, 1534384317233524738, '[0],[1534384317233524738],', '工序路线关系', 'worksteproute_index', 1, 'link', '/workStepRoute', 'main/worksteproute/index', NULL, 'manufacturing', 1, 'N', NULL, NULL, 1, 6, NULL, 0, NULL, NULL, '2022-06-28 14:12:11', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8436218677725644487, 4890328064515383248, '[0],[4890328064515383248],', 'SaaS租户编辑', 'tenantinfo_index_edit', 2, NULL, NULL, NULL, 'tenantInfo:edit', 'ht', 0, 'Y', NULL, NULL, 1, 100, NULL, 2, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (8436831550169549160, 1558006914472747010, '[0],[1558006914472747010],', '客户资料', 'cusinfor_index', 1, 'solution', '/cusInfor', 'main/cusinfor/index', NULL, 'crm', 1, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:34', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8471984269988902242, 6700801806460342017, '[0],[6700801806460342017],', '供应商分类编辑', 'suppsort_index_edit', 2, NULL, NULL, NULL, 'suppSort:edit', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 2, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (8477499608364957015, 1534410297847214081, '[0],[1534410297847214081],', '生产计划', 'proplan_index', 1, 'tool', '/proPlan', 'main/proplan/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2022-08-12 16:29:05', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8494981115107347954, 8477499608364957015, '[0],[1534410297847214081],[8477499608364957015],', '生产计划列表', 'proplan_index_list', 2, NULL, NULL, NULL, 'proPlan:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-21 16:58:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8502207026072268708, 4794640277308881312, '[0],[1558006778241753089],[4794640277308881312],', '供应商分类编辑', 'suppsort_index_edit', 2, NULL, NULL, NULL, 'suppSort:edit', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:25', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8515665128226371022, 5721174223820977430, '[0],[1534411830716350466],[5721174223820977430],', '库存余额新增', 'stockbalance_index_add', 2, NULL, NULL, NULL, 'stockBalance:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-28 16:50:46', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8562547749529140265, 7702400310012060990, '[0],[1534384317233524738],[7702400310012060990],', '工艺路线查看', 'workroute_index_detail', 2, NULL, NULL, NULL, 'workRoute:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:34', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8562653372099681846, 6507903030541782366, '[0],[1534410297847214081],[6507903030541782366],', '工单表查询', 'workorder_index_page', 2, NULL, NULL, NULL, 'workOrder:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:18', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8590002636006568603, 5934688735467584877, '[0],[1534410297847214081],[5934688735467584877],', '销售订单列表', 'saleorder_index_list', 2, NULL, NULL, NULL, 'saleOrder:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 11:17:42', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8613714595480779524, 8700710314151240148, '[0],[1558006914472747010],[8700710314151240148],', '客户联系人导出', 'cusperson_index_export', 2, NULL, NULL, NULL, 'cusPerson:export', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:42', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8618504262472492350, 6436044181834026359, '[0],[1534384317233524738],[6436044181834026359],', '产品表查询', 'pro_index_page', 2, NULL, NULL, NULL, 'pro:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:36:37', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8621765388717143178, 8426505795261326687, '[0],[1534384317233524738],[8426505795261326687],', '工序路线关系表新增', 'worksteproute_index_add', 2, NULL, NULL, NULL, 'workStepRoute:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:39:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8633823152794346306, 1534411830716350466, '[0],[1534411830716350466],', '出库单', 'invOut_index', 1, 'logout', '/invOut', 'main/invOut/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 2, NULL, 0, NULL, NULL, '2022-06-08 13:48:27', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8655981106868069232, 6134354751483903106, '[0],[1558006778241753089],[6134354751483903106],', ' 供应商资料导出', 'suppdata_index_export', 2, NULL, NULL, NULL, 'suppData:export', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:14', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8700710314151240148, 1558006914472747010, '[0],[1558006914472747010],', '联系人', 'cusperson_index', 1, 'team', '/cusPerson', 'main/cusperson/index', NULL, 'crm', 1, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:42', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8775511535879974972, 5012839659883217263, '[0],[1534411830716350466],[5012839659883217263],', '入库单查询', 'invIn_index_page', 2, NULL, NULL, NULL, 'invIn:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:49:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8833258242949762178, 8633823152794346306, '[0],[1534411830716350466],[8633823152794346306],', '出库单编辑', 'invOut_index_edit', 2, NULL, NULL, NULL, 'invOut:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:48:27', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8834719374925492199, 9202857208449608897, '[0],[1534410297847214081],[9202857208449608897],', '任务删除', 'task_index_delete', 2, NULL, NULL, NULL, 'task:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:30', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8868921547741003555, 8633823152794346306, '[0],[1534411830716350466],[8633823152794346306],', '出库单导出', 'invOut_index_export', 2, NULL, NULL, NULL, 'invOut:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:48:27', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8903136733033804117, 8422209827465537355, '[0],[1552209067446579201],[8422209827465537355],', '采购订单查看', 'puororder_index_detail', 2, NULL, NULL, NULL, 'puorOrder:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8931538668430408569, 6507903030541782366, '[0],[1534410297847214081],[6507903030541782366],', '工单表查看', 'workorder_index_detail', 2, NULL, NULL, NULL, 'workOrder:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:18', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8946168826263694974, 8287951873357908072, '[0],[1552209067446579201],[8287951873357908072],', '采购细明新增', 'puordetail_index_add', 2, NULL, NULL, NULL, 'puorDetail:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8957317986837088422, 0, '[0],', '字段配置', 'fieldconfig_index', 1, 'setting', '/fieldConfig', 'main/fieldconfig/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:46:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8959792232643188225, 6667327679590719676, '[0],[1534410297847214081],[6667327679590719676],', '工单任务关系表查看', 'workordertask_index_detail', 2, NULL, NULL, NULL, 'workOrderTask:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:44:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8973112451110891359, 7346283523793183379, '[0],[1534410297847214081],[7346283523793183379],', '报工导出', 'workreport_index_export', 2, NULL, NULL, NULL, 'workReport:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:49', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (9022874385071881655, 7346283523793183379, '[0],[1534410297847214081],[7346283523793183379],', '报工查看', 'workreport_index_detail', 2, NULL, NULL, NULL, 'workReport:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:49', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (9090782745729954273, 8287951873357908072, '[0],[1552209067446579201],[8287951873357908072],', '采购细明列表', 'puordetail_index_list', 2, NULL, NULL, NULL, 'puorDetail:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (9118159149629415380, 5934688735467584877, '[0],[1534410297847214081],[5934688735467584877],', '销售订单导出', 'saleorder_index_export', 2, NULL, NULL, NULL, 'saleOrder:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 11:17:42', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (9135927743117002076, 8436831550169549160, '[0],[1558006914472747010],[8436831550169549160],', '客户资料查看', 'cusinfor_index_detail', 2, NULL, NULL, NULL, 'cusInfor:detail', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 16:28:34', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (9150731911657480775, 5474507168841280952, '[0],[5474507168841280952],', '自定义编号规则编辑', 'customcode_index_edit', 2, NULL, NULL, NULL, 'customCode:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (9152465645130034198, 5012839659883217263, '[0],[1534411830716350466],[5012839659883217263],', '入库单查看', 'invIn_index_detail', 2, NULL, NULL, NULL, 'invIn:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:49:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (9156093698218665129, 8957317986837088422, '[0],[8957317986837088422],', '字段配置列表', 'fieldconfig_index_list', 2, NULL, NULL, NULL, 'fieldConfig:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (9173957282399717676, 7346283523793183379, '[0],[1534410297847214081],[7346283523793183379],', '报工删除', 'workreport_index_delete', 2, NULL, NULL, NULL, 'workReport:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:49', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (9180800170765745954, 6667327679590719676, '[0],[1534410297847214081],[6667327679590719676],', '工单任务关系表新增', 'workordertask_index_add', 2, NULL, NULL, NULL, 'workOrderTask:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:44:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (9202857208449608897, 1534410297847214081, '[0],[1534410297847214081],', '任务', 'task_index', 1, 'share-alt', '/task', 'main/task/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 4, NULL, 0, NULL, NULL, '2022-08-12 16:29:36', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (9208275275751949837, 7346283523793183379, '[0],[1534410297847214081],[7346283523793183379],', '报工查询', 'workreport_index_page', 2, NULL, NULL, NULL, 'workReport:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:49', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (9222389351831596218, 6527167616560933135, '[0],[1534384317233524738],[6527167616560933135],', '产品类型表删除', 'protype_index_delete', 2, NULL, NULL, NULL, 'proType:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 12:00:45', 1265476890672672808);

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
                               `id` bigint(20) NOT NULL COMMENT '主键',
                               `title` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
                               `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '内容',
                               `type` tinyint(4) NOT NULL COMMENT '类型（字典 1通知 2公告）',
                               `public_user_id` bigint(20) NOT NULL COMMENT '发布人id',
                               `public_user_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '发布人姓名',
                               `public_org_id` bigint(20) NULL DEFAULT NULL COMMENT '发布机构id',
                               `public_org_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '发布机构名称',
                               `public_time` datetime(0) NULL DEFAULT NULL COMMENT '发布时间',
                               `cancel_time` datetime(0) NULL DEFAULT NULL COMMENT '撤回时间',
                               `status` tinyint(4) NOT NULL COMMENT '状态（字典 0草稿 1发布 2撤回 3删除）',
                               `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                               `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                               `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                               `update_user` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
                               PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '通知表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES (1569878997300224001, '测试', '从<span style=\"font-style: italic;\">洒下阿萨的</span>但是<span style=\"text-decoration-line: line-through;\">心爱的</span><p></p>', 1, 1265476890672672808, '超级管理员', NULL, NULL, '2022-09-14 10:41:35', '2022-09-14 10:44:21', 3, '2022-09-14 10:41:35', 1265476890672672808, '2022-09-14 10:44:24', 1265476890672672808);
INSERT INTO `sys_notice` VALUES (1569879825209696258, '测试', '<span style=\"font-family: Verdana; font-size: x-large; text-decoration-line: underline; font-style: italic;\">我的武器</span><span style=\"text-decoration-line: line-through;\">大全取得完全的青蛙四点五七三七</span><p></p>', 2, 1265476890672672808, '超级管理员', NULL, NULL, '2022-09-14 10:44:52', NULL, 1, '2022-09-14 10:44:52', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_notice` VALUES (1569879984257703937, '测试数据', '分为五的范围<p></p>', 1, 1265476890672672808, '超级管理员', NULL, NULL, '2022-09-14 10:45:30', NULL, 1, '2022-09-14 10:45:30', 1265476890672672808, NULL, NULL);

-- ----------------------------
-- Table structure for sys_notice_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice_user`;
CREATE TABLE `sys_notice_user`  (
                                    `id` bigint(20) NOT NULL COMMENT '主键',
                                    `notice_id` bigint(20) NOT NULL COMMENT '通知公告id',
                                    `user_id` bigint(20) NOT NULL COMMENT '用户id',
                                    `status` tinyint(4) NOT NULL COMMENT '状态（字典 0未读 1已读）',
                                    `read_time` datetime(0) NULL DEFAULT NULL COMMENT '阅读时间',
                                    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统用户数据范围表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_notice_user
-- ----------------------------
INSERT INTO `sys_notice_user` VALUES (1569878997363138561, 1569878997300224001, 1275735541155614721, 0, NULL);
INSERT INTO `sys_notice_user` VALUES (1569878997363138562, 1569878997300224001, 1280709549107552257, 0, NULL);
INSERT INTO `sys_notice_user` VALUES (1569878997426053121, 1569878997300224001, 1471457941179072513, 0, NULL);
INSERT INTO `sys_notice_user` VALUES (1569878997426053122, 1569878997300224001, 1551457178297200641, 0, NULL);
INSERT INTO `sys_notice_user` VALUES (1569879826061139970, 1569879825209696258, 1551462379850723329, 0, NULL);
INSERT INTO `sys_notice_user` VALUES (1569879985176256514, 1569879984257703937, 1551457178297200641, 0, NULL);
INSERT INTO `sys_notice_user` VALUES (1569879986149335041, 1569879984257703937, 1551462379850723329, 0, NULL);

-- ----------------------------
-- Table structure for sys_oauth_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_oauth_user`;
CREATE TABLE `sys_oauth_user`  (
                                   `id` bigint(20) NOT NULL COMMENT '主键',
                                   `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '第三方平台的用户唯一id',
                                   `access_token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户授权的token',
                                   `nick_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '昵称',
                                   `avatar` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像',
                                   `blog` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户网址',
                                   `company` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所在公司',
                                   `location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '位置',
                                   `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
                                   `gender` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '性别',
                                   `source` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户来源',
                                   `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户备注（各平台中的用户个人介绍）',
                                   `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                   `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建用户',
                                   `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                   `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新用户',
                                   PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '第三方认证用户信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_oauth_user
-- ----------------------------

-- ----------------------------
-- Table structure for sys_op_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_op_log`;
CREATE TABLE `sys_op_log`  (
                               `id` bigint(20) NOT NULL COMMENT '主键',
                               `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
                               `op_type` tinyint(4) NULL DEFAULT NULL COMMENT '操作类型',
                               `success` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否执行成功（Y-是，N-否）',
                               `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '具体消息',
                               `ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'ip',
                               `location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '地址',
                               `browser` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '浏览器',
                               `os` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作系统',
                               `url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求地址',
                               `class_name` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类名称',
                               `method_name` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '方法名称',
                               `req_method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求方式（GET POST PUT DELETE)',
                               `param` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '请求参数',
                               `result` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '返回结果',
                               `op_time` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
                               `account` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作账号',
                               `sign_value` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '签名数据（除ID外）',
                               PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统操作日志表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_op_log
-- ----------------------------
INSERT INTO `sys_op_log` VALUES (1585189074364493825, '操作日志_清空', 12, 'Y', '成功', '127.0.0.1', '-', 'Chrome', 'Windows 10 or Windows Server 2016', '/sysOpLog/delete', 'vip.xiaonuo.sys.modular.log.controller.SysLogController', 'opLogDelete', 'POST', '', '{\"code\":200,\"message\":\"请求成功\",\"success\":true}', '2022-10-26 16:38:21', 'nhszcy', '75d07feb6986da64253b5af07a77cf71d3784c2b784884e9ecb9a17445ffd8f5');
INSERT INTO `sys_op_log` VALUES (1585193074711306242, '系统字典类型_树', 5, 'Y', '成功', '127.0.0.1', '-', 'Chrome', 'Windows 10 or Windows Server 2016', '/sysDictType/tree', 'vip.xiaonuo.sys.modular.dict.controller.SysDictTypeController', 'tree', 'GET', '', '{\"code\":200,\"data\":[{\"children\":[{\"children\":[],\"code\":\"0\",\"id\":1265216617500102656,\"name\":\"正常\",\"pid\":1265216211667636226},{\"children\":[],\"code\":\"1\",\"id\":1265216617500102657,\"name\":\"停用\",\"pid\":1265216211667636226},{\"children\":[],\"code\":\"2\",\"id\":1265216938389524482,\"name\":\"删除\",\"pid\":1265216211667636226}],\"code\":\"common_status\",\"id\":1265216211667636226,\"name\":\"通用状态\",\"pid\":0},{\"children\":[{\"children\":[],\"code\":\"1\",\"id\":1265216536659087357,\"name\":\"男\",\"pid\":1265216211667636234},{\"children\":[],\"code\":\"2\",\"id\":1265216536659087358,\"name\":\"女\",\"pid\":1265216211667636234},{\"children\":[],\"code\":\"3\",\"id\":1265216536659087359,\"name\":\"未知\",\"pid\":1265216211667636234}],\"code\":\"sex\",\"id\":1265216211667636234,\"name\":\"性别\",\"pid\":0},{\"children\":[{\"children\":[],\"code\":\"DEFAULT\",\"id\":1265216536659087361,\"name\":\"默认常量\",\"pid\":1265216211667636235},{\"children\":[],\"code\":\"ALIYUN_SMS\",\"id\":1265216536659087363,\"name\":\"阿里云短信\",\"pid\":1265216211667636235},{\"children\":[],\"code\":\"TENCENT_SMS\",\"id\":1265216536659087364,\"name\":\"腾讯云短信\",\"pid\":1265216211667636235},{\"children\":[],\"code\":\"EMAIL\",\"id\":1265216536659087365,\"name\":\"邮件配置\",\"pid\":1265216211667636235},{\"children\":[],\"code\":\"FILE_PATH\",\"id\":1265216536659087366,\"name\":\"文件上传路径\",\"pid\":1265216211667636235},{\"children\":[],\"code\":\"OAUTH\",\"id\":1265216536659087367,\"name\":\"Oauth配置\",\"pid\":1265216211667636235},{\"children\":[],\"code\":\"CRYPTOGRAM\",\"id\":1471107569499508737,\"name\":\"加密配置\",\"pid\":1265216211667636235}],\"code\":\"consts_type\",\"id\":1265216211667636235,\"name\":\"常量的分类\",\"pid\":0},{\"children\":[{\"children\":[],\"code\":\"N\",\"id\":1265217669028892673,\"name\":\"否\",\"pid\":1265217074079453185},{\"children\":[],\"code\":\"Y\",\"id\":1265217706584690689,\"name\":\"是\",\"pid\":1265217074079453185}],\"code\":\"yes_or_no\",\"id\":1265217074079453185,\"name\":\"是否\",\"pid\":0},{\"children\":[{\"children\":[],\"code\":\"1\",\"id\":1265220776437731330,\"name\":\"登录\",\"pid\":1265217846770913282},{\"children\":[],\"code\":\"2\",\"id\":1265220806070489090,\"name\":\"登出\",\"pid\":1265217846770913282}],\"code\":\"vis_type\",\"id\":1265217846770913282,\"name\":\"访问类型\",\"pid\":0},{\"children\":[{\"children\":[],\"code\":\"0\",\"id\":1265221129564573697,\"name\":\"目录\",\"pid\":1265221049302372354},{\"children\":[],\"code\":\"1\",\"id\":1265221163119005697,\"name\":\"菜单\",\"pid\":1265221049302372354},{\"children\":[],\"code\":\"2\",\"id\":1265221188091891713,\"name\":\"按钮\",\"pid\":1265221049302372354}],\"code\":\"menu_type\",\"id\":1265221049302372354,\"name\":\"菜单类型\",\"pid\":0},{\"children\":[{\"children\":[],\"code\":\"0\",\"id\":1265466389204967426,\"name\":\"未发送\",\"pid\":1265466149622128641},{\"children\":[],\"code\":\"1\",\"id\":1265466432670539778,\"name\":\"发送成功\",\"pid\":1265466149622128641},{\"children\":[],\"code\":\"2\",\"id\":1265466486097584130,\"name\":\"发送失败\",\"pid\":1265466149622128641},{\"children\":[],\"code\":\"3\",\"id\":1265466530477514754,\"name\":\"失效\",\"pid\":1265466149622128641}],\"code\":\"send_type\",\"id\":1265466149622128641,\"name\":\"发送类型\",\"pid\":0},{\"children\":[{\"children\":[],\"code\":\"0\",\"id\":1265466835009150978,\"name\":\"无\",\"pid\":1265466752209395713},{\"children\":[],\"code\":\"1\",\"id\":1265466874758569986,\"name\":\"组件\",\"pid\":1265466752209395713},{\"children\":[],\"code\":\"2\",\"id\":1265466925476093953,\"name\":\"内链\",\"pid\":1265466752209395713},{\"children\":[],\"code\":\"3\",\"id\":1265466962209808385,\"name\":\"外链\",\"pid\":1265466752209395713}],\"code\":\"open_type\",\"id\":1265466752209395713,\"name\":\"打开方式\",\"pid\":0},{\"children\":[{\"children\":[],\"code\":\"1\",\"id\":1265467428423475202,\"name\":\"系统权重\",\"pid\":1265467337566461954},{\"children\":[],\"code\":\"2\",\"id\":1265467503090475009,\"name\":\"业务权重\",\"pid\":1265467337566461954}],\"code\":\"menu_weight\",\"id\":1265467337566461954,\"name\":\"菜单权重\",\"pid\":0},{\"children\":[{\"children\":[],\"code\":\"1\",\"id\":1265468138431062018,\"name\":\"全部数据\",\"pid\":1265468028632571905},{\"children\":[],\"code\":\"2\",\"id\":1265468194928336897,\"name\":\"本部门及以下数据\",\"pid\":1265468028632571905},{\"children\":[],\"code\":\"3\",\"id\":1265468241992622082,\"name\":\"本部门数据\",\"pid\":1265468028632571905},{\"children\":[],\"code\":\"4\",\"id\":1265468273634451457,\"name\":\"仅本人数据\",\"pid\":1265468028632571905},{\"children\":[],\"code\":\"5\",\"id\":1265468302046666753,\"name\":\"自定义数据\",\"pid\":1265468028632571905}],\"code\":\"data_scope_type\",\"id\":1265468028632571905,\"name\":\"数据范围类型\",\"pid\":0},{\"children\":[{\"children\":[],\"code\":\"1\",\"id\":1265468508100239362,\"name\":\"app\",\"pid\":1265468437904367618},{\"children\":[],\"code\":\"2\",\"id\":1265468543433056258,\"name\":\"pc\",\"pid\":1265468437904367618},{\"children\":[],\"code\":\"3\",\"id\":1265468576874242050,\"name\":\"其他\",\"pid\":1265468437904367618}],\"code\":\"sms_send_source\",\"id\":1265468437904367618,\"name\":\"短信发送来源\",\"pid\":0},{\"children\":[{\"children\":[],\"code\":\"0\",\"id\":1275617233011335170,\"name\":\"其它\",\"pid\":1275617093517172738},{\"children\":[],\"code\":\"1\",\"id\":1275617295355469826,\"name\":\"增加\",\"pid\":1275617093517172738},{\"children\":[],\"code\":\"2\",\"id\":1275617348610547714,\"name\":\"删除\",\"pid\":1275617093517172738},{\"children\":[],\"code\":\"3\",\"id\":1275617395515449346,\"name\":\"编辑\",\"pid\":1275617093517172738},{\"children\":[],\"code\":\"4\",\"id\":1275617433612312577,\"name\":\"更新\",\"pid\":1275617093517172738},{\"children\":[],\"code\":\"5\",\"id\":1275617472707420161,\"name\":\"查询\",\"pid\":1275617093517172738},{\"children\":[],\"code\":\"6\",\"id\":1275617502973517826,\"name\":\"详情\",\"pid\":1275617093517172738},{\"children\":[],\"code\":\"7\",\"id\":1275617536959963137,\"name\":\"树\",\"pid\":1275617093517172738},{\"children\":[],\"code\":\"8\",\"id\":1275617619524837377,\"name\":\"导入\",\"pid\":1275617093517172738},{\"children\":[],\"code\":\"9\",\"id\":1275617651816783873,\"name\":\"导出\",\"pid\":1275617093517172738},{\"children\":[],\"code\":\"10\",\"id\":1275617683475390465,\"name\":\"授权\",\"pid\":1275617093517172738},{\"children\":[],\"code\":\"11\",\"id\":1275617709928865793,\"name\":\"强退\",\"pid\":1275617093517172738},{\"children\":[],\"code\":\"12\",\"id\":1275617739091861505,\"name\":\"清空\",\"pid\":1275617093517172738},{\"children\":[],\"code\":\"13\",\"id\":1275617788601425921,\"name\":\"修改状态\",\"pid\":1275617093517172738}],\"code\":\"op_type\",\"id\":1275617093517172738,\"name\":\"操作类型\",\"pid\":0},{\"children\":[{\"children\":[],\"code\":\"1\",\"id\":1277774590944317441,\"name\":\"阿里云\",\"pid\":1277774529430654977},{\"children\":[],\"code\":\"2\",\"id\":1277774666055913474,\"name\":\"腾讯云\",\"pid\":1277774529430654977},{\"children\":[],\"code\":\"3\",\"id\":1277774695168577538,\"name\":\"minio\",\"pid\":1277774529430654977},{\"children\":[],\"code\":\"4\",\"id\":1277774726835572737,\"name\":\"本地\",\"pid\":1277774529430654977}],\"code\":\"file_storage_location\",\"id\":1277774529430654977,\"name\":\"文件存储位置\",\"pid\":0},{\"children\":[{\"children\":[],\"code\":\"1\",\"id\":1278607123583868929,\"name\":\"运行\",\"pid\":1278606951432855553},{\"children\":[],\"code\":\"2\",\"id\":1278607162943217666,\"name\":\"停止\",\"pid\":1278606951432855553}],\"code\":\"run_status\",\"id\":1278606951432855553,\"name\":\"运行状态\",\"pid\":0},{\"children\":[{\"children\":[],\"code\":\"1\",\"id\":1278939265862004738,\"name\":\"通知\",\"pid\":1278911800547147777},{\"children\":[],\"code\":\"2\",\"id\":1278939319922388994,\"name\":\"公告\",\"pid\":1278911800547147777}],\"code\":\"notice_type\",\"id\":1278911800547147777,\"name\":\"通知公告类型\",\"pid\":0},{\"children\":[{\"children\":[],\"code\":\"0\",\"id\":1278939399001796609,\"name\":\"草稿\",\"pid\":1278911952657776642},{\"children\":[],\"code\":\"1\",\"id\":1278939432686252034,\"name\":\"发布\",\"pid\":1278911952657776642},{\"children\":[],\"code\":\"2\",\"id\":1278939458804183041,\"name\":\"撤回\",\"pid\":1278911952657776642},{\"children\":[],\"code\":\"3\",\"id\":1278939485878415362,\"name\":\"删除\",\"pid\":1278911952657776642}],\"code\":\"notice_status\",\"id\":1278911952657776642,\"name\":\"通知公告状态\",\"pid\":0},{\"children\":[{\"children\":[],\"code\":\"1\",\"id\":1342446007168466945,\"name\":\"下载压缩包\",\"pid\":1342445962104864770},{\"children\":[],\"code\":\"2\",\"id\":1342446035433881601,\"name\":\"生成到本项目\",\"pid\":1342445962104864770}],\"code\":\"code_gen_create_type\",\"id\":1342445962104864770,\"name\":\"代码生成方式\",\"pid\":0},{\"children\":[{\"children\":[],\"code\":\"input\",\"id\":1358094655567454210,\"name\":\"输入框\",\"pid\":1358094419419750401},{\"children\":[],\"code\":\"datepicker\",\"id\":1358094740510498817,\"name\":\"时间选择\",\"pid\":1358094419419750401},{\"children\":[],\"code\":\"select\",\"id\":1358094793149014017,\"name\":\"下拉框\",\"pid\":1358094419419750401},{\"children\":[],\"code\":\"radio\",\"id\":1358095496009506817,\"name\":\"单选框\",\"pid\":1358094419419750401},{\"children\":[],\"code\":\"checkbox\",\"id\":1358460475682406401,\"name\":\"多选框\",\"pid\":1358094419419750401},{\"children\":[],\"code\":\"inputnumber\",\"id\":1358460819019743233,\"name\":\"数字输入框\",\"pid\":1358094419419750401},{\"children\":[],\"code\":\"textarea\",\"id\":1360529773814083586,\"name\":\"文本域\",\"pid\":1358094419419750401}],\"code\":\"code_gen_effect_type\",\"id\":1358094419419750401,\"name\":\"代码生成作用类型\",\"pid\":0},{\"children\":[{\"children\":[],\"code\":\"eq\",\"id\":1358458689433190402,\"name\":\"等于\",\"pid\":1358457818733428737},{\"children\":[],\"code\":\"like\",\"id\":1358458785168179202,\"name\":\"模糊\",\"pid\":1358457818733428737},{\"children\":[],\"code\":\"gt\",\"id\":1358756511688761346,\"name\":\"大于\",\"pid\":1358457818733428737},{\"children\":[],\"code\":\"lt\",\"id\":1358756547159990274,\"name\":\"小于\",\"pid\":1358457818733428737},{\"children\":[],\"code\":\"ne\",\"id\":1358756609990664193,\"name\":\"不等于\",\"pid\":1358457818733428737},{\"children\":[],\"code\":\"ge\",\"id\":1358756685030957057,\"name\":\"大于等于\",\"pid\":1358457818733428737},{\"children\":[],\"code\":\"le\",\"id\":1358756800525312001,\"name\":\"小于等于\",\"pid\":1358457818733428737},{\"children\":[],\"code\":\"isNotNull\",\"id\":1360606105914732545,\"name\":\"不为空\",\"pid\":1358457818733428737}],\"code\":\"code_gen_query_type\",\"id\":1358457818733428737,\"name\":\"代码生成查询类型\",\"pid\":0},{\"children\":[{\"children\":[],\"code\":\"Long\",\"id\":1358470210267725826,\"name\":\"Long\",\"pid\":1358470065111252994},{\"children\":[],\"code\":\"String\",\"id\":1358470239351029762,\"name\":\"String\",\"pid\":1358470065111252994},{\"children\":[],\"code\":\"Date\",\"id\":1358470265640927233,\"name\":\"Date\",\"pid\":1358470065111252994},{\"children\":[],\"code\":\"Integer\",\"id\":1358470300168437761,\"name\":\"Integer\",\"pid\":1358470065111252994},{\"children\":[],\"code\":\"boolean\",\"id\":1358470697377415169,\"name\":\"boolean\",\"pid\":1358470065111252994},{\"children\":[],\"code\":\"int\",\"id\":1358471133434036226,\"name\":\"int\",\"pid\":1358470065111252994},{\"children\":[],\"code\":\"double\",\"id\":1358471188291338241,\"name\":\"double\",\"pid\":1358470065111252994}],\"code\":\"code_gen_java_type\",\"id\":1358470065111252994,\"name\":\"代码生成java类型\",\"pid\":0},{\"children\":[{\"children\":[],\"code\":\"ord_in\",\"id\":1534371910538829825,\"name\":\"普通入库\",\"pid\":1534370205881409537},{\"children\":[],\"code\":\"other_in\",\"id\":1534371989345607681,\"name\":\"其他入库\",\"pid\":1534370205881409537},{\"children\":[],\"code\":\"pur_in\",\"id\":1534372101077671938,\"name\":\"采购入库\",\"pid\":1534370205881409537}],\"code\":\"in_type\",\"id\":1534370205881409537,\"name\":\"入库类型\",\"pid\":0},{\"children\":[{\"children\":[],\"code\":\"other_out\",\"id\":1534370624561029121,\"name\":\"其他出库\",\"pid\":1534370298088988673},{\"children\":[],\"code\":\"pur_out\",\"id\":1534370774654197762,\"name\":\"采购退货出库\",\"pid\":1534370298088988673},{\"children\":[],\"code\":\"ord_out\",\"id\":1534371162073669634,\"name\":\"普通出库\",\"pid\":1534370298088988673}],\"code\":\"out_type\",\"id\":1534370298088988673,\"name\":\"出库类型\",\"pid\":0},{\"children\":[{\"children\":[],\"code\":\"Diemissing\",\"id\":1534371775545139202,\"name\":\"模具缺失\",\"pid\":1534371402801537025},{\"children\":[],\"code\":\"other\",\"id\":1534371910769500162,\"name\":\"其它\",\"pid\":1534371402801537025},{\"children\":[],\"code\":\"damaged\",\"id\":1534372255730032641,\"name\":\"破损\",\"pid\":1534371402801537025},{\"children\":[],\"code\":\"ceshi1\",\"id\":1534372356875673601,\"name\":\"测试数据\",\"pid\":1534371402801537025},{\"children\":[],\"code\":\"20220628\",\"id\":1541700530479316994,\"name\":\"出现划痕\",\"pid\":1534371402801537025},{\"children\":[],\"code\":\"65399\",\"id\":1541700792732368897,\"name\":\"起皱凹陷\",\"pid\":1534371402801537025}],\"code\":\"bad_item\",\"id\":1534371402801537025,\"name\":\"不良品项\",\"pid\":0},{\"children\":[{\"children\":[],\"code\":\"string\",\"id\":1534371664297603073,\"name\":\"字符串\",\"pid\":1534371575181225985},{\"children\":[],\"code\":\"number\",\"id\":1561634089585934337,\"name\":\"数字\",\"pid\":1534371575181225985},{\"children\":[],\"code\":\"date\",\"id\":1561634156946456578,\"name\":\"时间\",\"pid\":1534371575181225985}],\"code\":\"field_type\",\"id\":1534371575181225985,\"name\":\"字段类型\",\"pid\":0},{\"children\":[{\"children\":[],\"code\":\"indivual\",\"id\":1539443353173667842,\"name\":\"个\",\"pid\":1534375649047773185},{\"children\":[],\"code\":\"piece\",\"id\":1539443446530486274,\"name\":\"件\",\"pid\":1534375649047773185},{\"children\":[],\"code\":\"sleeve\",\"id\":1539444094273630209,\"name\":\"套\",\"pid\":1534375649047773185},{\"children\":[],\"code\":\"single\",\"id\":1539444301958787073,\"name\":\"只\",\"pid\":1534375649047773185},{\"children\":[],\"code\":\"desk\",\"id\":1539444436134572034,\"name\":\"台\",\"pid\":1534375649047773185},{\"children\":[],\"code\":\"meter\",\"id\":1539444541944279042,\"name\":\"米\",\"pid\":1534375649047773185},{\"children\":[],\"code\":\"strip\",\"id\":1539444621967405058,\"name\":\"条\",\"pid\":1534375649047773185},{\"children\":[],\"code\":\"base\",\"id\":1539444768386363394,\"name\":\"根\",\"pid\":1534375649047773185},{\"children\":[],\"code\":\"leaf\",\"id\":1539444884954460162,\"name\":\"张\",\"pid\":1534375649047773185},{\"children\":[],\"code\":\"car\",\"id\":1539444960623898625,\"name\":\"车\",\"pid\":1534375649047773185},{\"children\":[],\"code\":\"box\",\"id\":1539445033567039490,\"name\":\"箱\",\"pid\":1534375649047773185},{\"children\":[],\"code\":\"cube\",\"id\":1539445116857528321,\"name\":\"立方\",\"pid\":1534375649047773185},{\"children\":[],\"code\":\"ton\",\"id\":1539445206221369346,\"name\":\"吨\",\"pid\":1534375649047773185},{\"children\":[],\"code\":\"kg\",\"id\":1539445256955670530,\"name\":\"KG\",\"pid\":1534375649047773185},{\"children\":[],\"code\":\"vice\",\"id\":1539445340317462529,\"name\":\"副\",\"pid\":1534375649047773185},{\"children\":[],\"code\":\"square\",\"id\":1539445432814448641,\"name\":\"平方\",\"pid\":1534375649047773185},{\"children\":[],\"code\":\"roll\",\"id\":1539445527370838018,\"name\":\"卷\",\"pid\":1534375649047773185},{\"children\":[],\"code\":\"slice\",\"id\":1541704912394629121,\"name\":\"片\",\"pid\":1534375649047773185}],\"code\":\"unit\",\"id\":1534375649047773185,\"name\":\"库存单位\",\"pid\":0},{\"children\":[{\"children\":[],\"code\":\"0\",\"id\":1541592159251587074,\"name\":\"年\",\"pid\":1541591973620080641},{\"children\":[],\"code\":\"1\",\"id\":1541592211361619970,\"name\":\"年月\",\"pid\":1541591973620080641},{\"children\":[],\"code\":\"2\",\"id\":1541592242990866434,\"name\":\"年月日\",\"pid\":1541591973620080641}],\"code\":\"time_type\",\"id\":1541591973620080641,\"name\":\"时间类型单选\",\"pid\":0},{\"children\":[{\"children\":[],\"code\":\"dw_inv\",\"id\":1541592783582126081,\"name\":\"入库单\",\"pid\":1541592393646071809},{\"children\":[],\"code\":\" dw_inv \",\"id\":1541593787136466945,\"name\":\"出库单\",\"pid\":1541592393646071809},{\"children\":[],\"code\":\"dw_pro\",\"id\":1541593988588888066,\"name\":\"产品管理\",\"pid\":1541592393646071809},{\"children\":[],\"code\":\"dw_type\",\"id\":1541594070629474305,\"name\":\"产品类型\",\"pid\":1541592393646071809},{\"children\":[],\"code\":\"dw_task\",\"id\":1541594136652013570,\"name\":\"任务\",\"pid\":1541592393646071809},{\"children\":[],\"code\":\"dw_work_order\",\"id\":1541594233825648641,\"name\":\"工单\",\"pid\":1541592393646071809},{\"children\":[],\"code\":\"dw_work_route\",\"id\":1541594693559115777,\"name\":\"工艺路线\",\"pid\":1541592393646071809},{\"children\":[],\"code\":\"dw_work_step\",\"id\":1541594900241833985,\"name\":\"工序\",\"pid\":1541592393646071809}],\"code\":\"cus_table_name\",\"id\":1541592393646071809,\"name\":\"自定义表名\",\"pid\":0}],\"message\":\"请求成功\",\"success\":true}', '2022-10-26 16:54:15', 'superAdmin', 'aa206f9d89252746f07d23cefef984e58983580ab5805da926bb400adbb53f72');
INSERT INTO `sys_op_log` VALUES (1585193082248470529, '系统菜单_切换', 7, 'Y', '成功', '127.0.0.1', '-', 'Chrome', 'Windows 10 or Windows Server 2016', '/sysMenu/change', 'vip.xiaonuo.sys.modular.menu.controller.SysMenuController', 'change', 'POST', '{\"application\":\"system\"}', '{\"code\":200,\"data\":[{\"component\":\"RouteView\",\"hidden\":false,\"id\":1264622039642255311,\"meta\":{\"icon\":\"home\",\"show\":true,\"title\":\"主控面板\"},\"name\":\"system_index\",\"path\":\"/\",\"pid\":0,\"redirect\":\"/analysis\"},{\"component\":\"system/dashboard/Workplace\",\"hidden\":false,\"id\":1264622039642255331,\"meta\":{\"show\":true,\"title\":\"工作台\"},\"name\":\"system_index_workplace\",\"path\":\"workplace\",\"pid\":1264622039642255311},{\"component\":\"PageView\",\"hidden\":false,\"id\":1264622039642255961,\"meta\":{\"icon\":\"euro\",\"show\":true,\"title\":\"开发管理\"},\"name\":\"system_tools\",\"path\":\"/tools\",\"pid\":0},{\"component\":\"PageView\",\"hidden\":false,\"id\":1264622039642256281,\"meta\":{\"icon\":\"read\",\"show\":true,\"title\":\"日志管理\"},\"name\":\"sys_log_mgr\",\"path\":\"/log\",\"pid\":0},{\"component\":\"PageView\",\"hidden\":false,\"id\":1264622039642256351,\"meta\":{\"icon\":\"deployment-unit\",\"show\":true,\"title\":\"系统监控\"},\"name\":\"sys_monitor_mgr\",\"path\":\"/monitor\",\"pid\":0},{\"component\":\"PageView\",\"hidden\":false,\"id\":1264622039642256521,\"meta\":{\"icon\":\"file\",\"show\":true,\"title\":\"文件管理\"},\"name\":\"sys_file_mgr\",\"path\":\"/file\",\"pid\":0},{\"component\":\"system/config/index\",\"hidden\":false,\"id\":1264622039642255971,\"meta\":{\"show\":true,\"title\":\"系统配置\"},\"name\":\"system_tools_config\",\"path\":\"/config\",\"pid\":1264622039642255961},{\"component\":\"system/email/index\",\"hidden\":false,\"id\":1264622039642256041,\"meta\":{\"show\":true,\"title\":\"邮件发送\"},\"name\":\"sys_email_mgr\",\"path\":\"/email\",\"pid\":1264622039642255961},{\"component\":\"system/sms/index\",\"hidden\":false,\"id\":1264622039642256071,\"meta\":{\"show\":true,\"title\":\"短信管理\"},\"name\":\"sys_sms_mgr\",\"path\":\"/sms\",\"pid\":1264622039642255961},{\"component\":\"system/dict/index\",\"hidden\":false,\"id\":1264622039642256111,\"meta\":{\"show\":true,\"title\":\"字典管理\"},\"name\":\"sys_dict_mgr\",\"path\":\"/dict\",\"pid\":1264622039642255961},{\"component\":\"Iframe\",\"hidden\":false,\"id\":1264622039642256271,\"meta\":{\"link\":\"http://localhost:82/doc.html\",\"show\":true,\"title\":\"接口文档\"},\"name\":\"sys_swagger_mgr\",\"path\":\"/swagger\",\"pid\":1264622039642255961},{\"component\":\"system/log/vislog/index\",\"hidden\":false,\"id\":1264622039642256291,\"meta\":{\"show\":true,\"title\":\"访问日志\"},\"name\":\"sys_log_mgr_vis_log\",\"path\":\"/vislog\",\"pid\":1264622039642256281},{\"component\":\"system/log/oplog/index\",\"hidden\":false,\"id\":1264622039642256321,\"meta\":{\"show\":true,\"title\":\"操作日志\"},\"name\":\"sys_log_mgr_op_log\",\"path\":\"/oplog\",\"pid\":1264622039642256281},{\"component\":\"system/machine/index\",\"hidden\":false,\"id\":1264622039642256361,\"meta\":{\"show\":true,\"title\":\"服务监控\"},\"name\":\"sys_monitor_mgr_machine_monitor\",\"path\":\"/machine\",\"pid\":1264622039642256351},{\"component\":\"system/onlineUser/index\",\"hidden\":false,\"id\":1264622039642256381,\"meta\":{\"show\":true,\"title\":\"在线用户\"},\"name\":\"sys_monitor_mgr_online_user\",\"path\":\"/onlineUser\",\"pid\":1264622039642256351},{\"component\":\"Iframe\",\"hidden\":false,\"id\":1264622039642256411,\"meta\":{\"link\":\"http://localhost:82/druid\",\"show\":true,\"title\":\"数据监控\"},\"name\":\"sys_monitor_mgr_druid\",\"path\":\"/druid\",\"pid\":1264622039642256351},{\"component\":\"system/file/index\",\"hidden\":false,\"id\":1264622039642256531,\"meta\":{\"show\":true,\"title\":\"系统文件\"},\"name\":\"sys_file_mgr_sys_file\",\"path\":\"/file\",\"pid\":1264622039642256521},{\"component\":\"system/timers/index\",\"hidden\":false,\"id\":1264622039642256621,\"meta\":{\"show\":true,\"title\":\"任务管理\"},\"name\":\"sys_timers_mgr\",\"path\":\"/timers\",\"pid\":1264622039642256611},{\"component\":\"system/fileOnline/index\",\"hidden\":false,\"id\":1410859007809736705,\"meta\":{\"icon\":\"\",\"show\":true,\"title\":\"在线文档\"},\"name\":\"file_oline\",\"path\":\"/file_oline\",\"pid\":1264622039642256521,\"redirect\":\"\"},{\"component\":\"PageView\",\"hidden\":false,\"id\":1264622039642256721,\"meta\":{\"icon\":\"environment\",\"show\":true,\"title\":\"区域管理\"},\"name\":\"sys_area\",\"path\":\"/area\",\"pid\":0},{\"component\":\"system/area/index\",\"hidden\":false,\"id\":1264622039642256731,\"meta\":{\"show\":true,\"title\":\"系统区域\"},\"name\":\"sys_area_mgr\",\"path\":\"/area\",\"pid\":1264622039642256721},{\"component\":\"system/dashboard/Analysis\",\"hidden\":false,\"id\":1264622039642255321,\"meta\":{\"show\":true,\"title\":\"分析页\"},\"name\":\"system_index_dashboard\",\"path\":\"analysis\",\"pid\":1264622039642255311},{\"component\":\"PageView\",\"hidden\":false,\"id\":1264622039642256611,\"meta\":{\"icon\":\"dashboard\",\"show\":true,\"title\":\"定时任务\"},\"name\":\"sys_timers\",\"path\":\"/timers\",\"pid\":0}],\"message\":\"请求成功\",\"success\":true}', '2022-10-26 16:54:17', 'superAdmin', '984f322a7c3921f56223456d5c7353fd6022b11af0b26ba0de5d5a064aaf8194');
INSERT INTO `sys_op_log` VALUES (1585193084848939010, '系统菜单_切换', 7, 'Y', '成功', '127.0.0.1', '-', 'Chrome', 'Windows 10 or Windows Server 2016', '/sysMenu/change', 'vip.xiaonuo.sys.modular.menu.controller.SysMenuController', 'change', 'POST', '{\"application\":\"system\"}', '{\"code\":200,\"data\":[{\"component\":\"RouteView\",\"hidden\":false,\"id\":1264622039642255311,\"meta\":{\"icon\":\"home\",\"show\":true,\"title\":\"主控面板\"},\"name\":\"system_index\",\"path\":\"/\",\"pid\":0,\"redirect\":\"/analysis\"},{\"component\":\"system/dashboard/Workplace\",\"hidden\":false,\"id\":1264622039642255331,\"meta\":{\"show\":true,\"title\":\"工作台\"},\"name\":\"system_index_workplace\",\"path\":\"workplace\",\"pid\":1264622039642255311},{\"component\":\"PageView\",\"hidden\":false,\"id\":1264622039642255961,\"meta\":{\"icon\":\"euro\",\"show\":true,\"title\":\"开发管理\"},\"name\":\"system_tools\",\"path\":\"/tools\",\"pid\":0},{\"component\":\"PageView\",\"hidden\":false,\"id\":1264622039642256281,\"meta\":{\"icon\":\"read\",\"show\":true,\"title\":\"日志管理\"},\"name\":\"sys_log_mgr\",\"path\":\"/log\",\"pid\":0},{\"component\":\"PageView\",\"hidden\":false,\"id\":1264622039642256351,\"meta\":{\"icon\":\"deployment-unit\",\"show\":true,\"title\":\"系统监控\"},\"name\":\"sys_monitor_mgr\",\"path\":\"/monitor\",\"pid\":0},{\"component\":\"PageView\",\"hidden\":false,\"id\":1264622039642256521,\"meta\":{\"icon\":\"file\",\"show\":true,\"title\":\"文件管理\"},\"name\":\"sys_file_mgr\",\"path\":\"/file\",\"pid\":0},{\"component\":\"system/config/index\",\"hidden\":false,\"id\":1264622039642255971,\"meta\":{\"show\":true,\"title\":\"系统配置\"},\"name\":\"system_tools_config\",\"path\":\"/config\",\"pid\":1264622039642255961},{\"component\":\"system/email/index\",\"hidden\":false,\"id\":1264622039642256041,\"meta\":{\"show\":true,\"title\":\"邮件发送\"},\"name\":\"sys_email_mgr\",\"path\":\"/email\",\"pid\":1264622039642255961},{\"component\":\"system/sms/index\",\"hidden\":false,\"id\":1264622039642256071,\"meta\":{\"show\":true,\"title\":\"短信管理\"},\"name\":\"sys_sms_mgr\",\"path\":\"/sms\",\"pid\":1264622039642255961},{\"component\":\"system/dict/index\",\"hidden\":false,\"id\":1264622039642256111,\"meta\":{\"show\":true,\"title\":\"字典管理\"},\"name\":\"sys_dict_mgr\",\"path\":\"/dict\",\"pid\":1264622039642255961},{\"component\":\"Iframe\",\"hidden\":false,\"id\":1264622039642256271,\"meta\":{\"link\":\"http://localhost:82/doc.html\",\"show\":true,\"title\":\"接口文档\"},\"name\":\"sys_swagger_mgr\",\"path\":\"/swagger\",\"pid\":1264622039642255961},{\"component\":\"system/log/vislog/index\",\"hidden\":false,\"id\":1264622039642256291,\"meta\":{\"show\":true,\"title\":\"访问日志\"},\"name\":\"sys_log_mgr_vis_log\",\"path\":\"/vislog\",\"pid\":1264622039642256281},{\"component\":\"system/log/oplog/index\",\"hidden\":false,\"id\":1264622039642256321,\"meta\":{\"show\":true,\"title\":\"操作日志\"},\"name\":\"sys_log_mgr_op_log\",\"path\":\"/oplog\",\"pid\":1264622039642256281},{\"component\":\"system/machine/index\",\"hidden\":false,\"id\":1264622039642256361,\"meta\":{\"show\":true,\"title\":\"服务监控\"},\"name\":\"sys_monitor_mgr_machine_monitor\",\"path\":\"/machine\",\"pid\":1264622039642256351},{\"component\":\"system/onlineUser/index\",\"hidden\":false,\"id\":1264622039642256381,\"meta\":{\"show\":true,\"title\":\"在线用户\"},\"name\":\"sys_monitor_mgr_online_user\",\"path\":\"/onlineUser\",\"pid\":1264622039642256351},{\"component\":\"Iframe\",\"hidden\":false,\"id\":1264622039642256411,\"meta\":{\"link\":\"http://localhost:82/druid\",\"show\":true,\"title\":\"数据监控\"},\"name\":\"sys_monitor_mgr_druid\",\"path\":\"/druid\",\"pid\":1264622039642256351},{\"component\":\"system/file/index\",\"hidden\":false,\"id\":1264622039642256531,\"meta\":{\"show\":true,\"title\":\"系统文件\"},\"name\":\"sys_file_mgr_sys_file\",\"path\":\"/file\",\"pid\":1264622039642256521},{\"component\":\"system/timers/index\",\"hidden\":false,\"id\":1264622039642256621,\"meta\":{\"show\":true,\"title\":\"任务管理\"},\"name\":\"sys_timers_mgr\",\"path\":\"/timers\",\"pid\":1264622039642256611},{\"component\":\"system/fileOnline/index\",\"hidden\":false,\"id\":1410859007809736705,\"meta\":{\"icon\":\"\",\"show\":true,\"title\":\"在线文档\"},\"name\":\"file_oline\",\"path\":\"/file_oline\",\"pid\":1264622039642256521,\"redirect\":\"\"},{\"component\":\"PageView\",\"hidden\":false,\"id\":1264622039642256721,\"meta\":{\"icon\":\"environment\",\"show\":true,\"title\":\"区域管理\"},\"name\":\"sys_area\",\"path\":\"/area\",\"pid\":0},{\"component\":\"system/area/index\",\"hidden\":false,\"id\":1264622039642256731,\"meta\":{\"show\":true,\"title\":\"系统区域\"},\"name\":\"sys_area_mgr\",\"path\":\"/area\",\"pid\":1264622039642256721},{\"component\":\"system/dashboard/Analysis\",\"hidden\":false,\"id\":1264622039642255321,\"meta\":{\"show\":true,\"title\":\"分析页\"},\"name\":\"system_index_dashboard\",\"path\":\"analysis\",\"pid\":1264622039642255311},{\"component\":\"PageView\",\"hidden\":false,\"id\":1264622039642256611,\"meta\":{\"icon\":\"dashboard\",\"show\":true,\"title\":\"定时任务\"},\"name\":\"sys_timers\",\"path\":\"/timers\",\"pid\":0}],\"message\":\"请求成功\",\"success\":true}', '2022-10-26 16:54:17', 'superAdmin', 'ec8d08c530c5feac6888d11178aafbc6d3326214a223850f983aa612bd2c659e');
INSERT INTO `sys_op_log` VALUES (1585193091421413378, '系统菜单_切换', 7, 'Y', '成功', '127.0.0.1', '-', 'Chrome', 'Windows 10 or Windows Server 2016', '/sysMenu/change', 'vip.xiaonuo.sys.modular.menu.controller.SysMenuController', 'change', 'POST', '{\"application\":\"manufacturing\"}', '{\"code\":200,\"data\":[{\"component\":\"main/bigScreen/index\",\"hidden\":false,\"id\":1538776469361139714,\"meta\":{\"icon\":\"radar-chart\",\"show\":true,\"title\":\"大屏展示\"},\"name\":\"bigScreen\",\"path\":\"/bigScreen/index\",\"pid\":0,\"redirect\":\"\"},{\"component\":\"main/workstep/index\",\"hidden\":false,\"id\":4998978684524443692,\"meta\":{\"icon\":\"deployment-unit\",\"show\":true,\"title\":\"工序\"},\"name\":\"workstep_index\",\"path\":\"/workStep\",\"pid\":1534384317233524738},{\"component\":\"main/proplan/index\",\"hidden\":false,\"id\":8477499608364957015,\"meta\":{\"icon\":\"tool\",\"show\":true,\"title\":\"生产计划\"},\"name\":\"proplan_index\",\"path\":\"/proPlan\",\"pid\":1534410297847214081},{\"component\":\"main/invIn/index\",\"hidden\":false,\"id\":5012839659883217263,\"meta\":{\"icon\":\"login\",\"show\":true,\"title\":\"入库单\"},\"name\":\"invIn_index\",\"path\":\"/invIn\",\"pid\":1534411830716350466},{\"component\":\"main/workroute/index\",\"hidden\":false,\"id\":7702400310012060990,\"meta\":{\"icon\":\"gateway\",\"show\":true,\"title\":\"工艺路线\"},\"name\":\"workroute_index\",\"path\":\"/workRoute\",\"pid\":1534384317233524738},{\"component\":\"main/invOut/index\",\"hidden\":false,\"id\":8633823152794346306,\"meta\":{\"icon\":\"logout\",\"show\":true,\"title\":\"出库单\"},\"name\":\"invOut_index\",\"path\":\"/invOut\",\"pid\":1534411830716350466},{\"component\":\"main/saleorder/index\",\"hidden\":false,\"id\":5934688735467584877,\"meta\":{\"icon\":\"file-ppt\",\"show\":true,\"title\":\"销售订单\"},\"name\":\"saleorder_index\",\"path\":\"/saleOrder\",\"pid\":1534410297847214081},{\"component\":\"main/ass/index\",\"hidden\":false,\"id\":5445032970491536505,\"meta\":{\"icon\":\"bg-colors\",\"show\":true,\"title\":\"装配工单\"},\"name\":\"ass_index\",\"path\":\"/ass\",\"pid\":1534410297847214081},{\"component\":\"main/protype/index\",\"hidden\":false,\"id\":6527167616560933135,\"meta\":{\"icon\":\"cluster\",\"show\":true,\"title\":\"产品类型\"},\"name\":\"protype_index\",\"path\":\"/proType\",\"pid\":1534384317233524738},{\"component\":\"main/workorder/index\",\"hidden\":false,\"id\":6507903030541782366,\"meta\":{\"icon\":\"cloud-download\",\"show\":true,\"title\":\"工单\"},\"name\":\"workorder_index\",\"path\":\"/workOrder\",\"pid\":1534410297847214081},{\"component\":\"main/workordertask/index\",\"hidden\":false,\"id\":6667327679590719676,\"meta\":{\"icon\":\"box-plot\",\"show\":false,\"title\":\"工单任务关系\"},\"name\":\"workordertask_index\",\"path\":\"/workOrderTask\",\"pid\":1534410297847214081},{\"component\":\"main/invdetail/index\",\"hidden\":false,\"id\":8016744771368544956,\"meta\":{\"icon\":\"bar-chart\",\"show\":true,\"title\":\"出入库明细\"},\"name\":\"invdetail_index\",\"path\":\"/invDetail\",\"pid\":1534411830716350466},{\"component\":\"main/pro/index\",\"hidden\":false,\"id\":6436044181834026359,\"meta\":{\"icon\":\"barcode\",\"show\":true,\"title\":\"产品管理\"},\"name\":\"pro_index\",\"path\":\"/pro\",\"pid\":1534384317233524738},{\"component\":\"main/task/index\",\"hidden\":false,\"id\":9202857208449608897,\"meta\":{\"icon\":\"share-alt\",\"show\":true,\"title\":\"任务\"},\"name\":\"task_index\",\"path\":\"/task\",\"pid\":1534410297847214081},{\"component\":\"main/workreport/index\",\"hidden\":false,\"id\":7346283523793183379,\"meta\":{\"icon\":\"file\",\"show\":true,\"title\":\"报工\"},\"name\":\"workreport_index\",\"path\":\"/workReport\",\"pid\":1534410297847214081},{\"component\":\"main/worksteproute/index\",\"hidden\":false,\"id\":8426505795261326687,\"meta\":{\"icon\":\"link\",\"show\":false,\"title\":\"工序路线关系\"},\"name\":\"worksteproute_index\",\"path\":\"/workStepRoute\",\"pid\":1534384317233524738},{\"component\":\"api\",\"hidden\":false,\"id\":1560145190851772417,\"meta\":{\"icon\":\"\",\"link\":\"\",\"show\":false,\"title\":\"接口管理\"},\"name\":\"api\",\"path\":\"/api\",\"pid\":0,\"redirect\":\"\"},{\"component\":\"PageView\",\"hidden\":false,\"id\":1534384317233524738,\"meta\":{\"icon\":\"setting\",\"show\":true,\"title\":\"基础数据\"},\"name\":\"basic_data\",\"path\":\"/basicData\",\"pid\":0,\"redirect\":\"\"},{\"component\":\"PageView\",\"hidden\":false,\"id\":1534410297847214081,\"meta\":{\"icon\":\"hdd\",\"show\":true,\"title\":\"生产管理\"},\"name\":\"prod_manage\",\"path\":\"/prodManage\",\"pid\":0,\"redirect\":\"\"},{\"component\":\"PageView\",\"hidden\":false,\"id\":1552209067446579201,\"meta\":{\"icon\":\"schedule\",\"show\":true,\"title\":\"采购管理\"},\"name\":\"purc_manage\",\"path\":\"/purcManage\",\"pid\":0,\"redirect\":\"\"},{\"component\":\"PageView\",\"hidden\":false,\"id\":1534411830716350466,\"meta\":{\"icon\":\"desktop\",\"show\":true,\"title\":\"库存管理\"},\"name\":\"inv_mgt\",\"path\":\"/inv/mgt\",\"pid\":0,\"redirect\":\"\"},{\"component\":\"main/stockbalance/index\",\"hidden\":false,\"id\":5721174223820977430,\"meta\":{\"icon\":\"gold\",\"show\":true,\"title\":\"库存余额\"},\"name\":\"stockbalance_index\",\"path\":\"/stockBalance\",\"pid\":1534411830716350466},{\"component\":\"main/puororder/index\",\"hidden\":false,\"id\":8422209827465537355,\"meta\":{\"show\":true,\"title\":\"采购订单\"},\"name\":\"puororder_index\",\"path\":\"/puorOrder\",\"pid\":1552209067446579201},{\"component\":\"main/puordetail/index\",\"hidden\":false,\"id\":8287951873357908072,\"meta\":{\"show\":true,\"title\":\"采购明细\"},\"name\":\"puordetail_index\",\"path\":\"/puorDetail\",\"pid\":1552209067446579201},{\"component\":\"main/fieldconfig/index\",\"hidden\":false,\"id\":8957317986837088422,\"meta\":{\"icon\":\"setting\",\"show\":true,\"title\":\"字段配置\"},\"name\":\"fieldconfig_index\",\"path\":\"/fieldConfig\",\"pid\":0},{\"component\":\"main/customcode/index\",\"hidden\":false,\"id\":5474507168841280952,\"meta\":{\"icon\":\"setting\",\"show\":true,\"title\":\"编号规则\"},\"name\":\"customcode_index\",\"path\":\"/customCode\",\"pid\":0},{\"component\":\"main/bom/index\",\"hidden\":false,\"id\":7128085356547940830,\"meta\":{\"icon\":\"bg-colors\",\"show\":true,\"title\":\"物料清单\"},\"name\":\"bom_index\",\"path\":\"/bom\",\"pid\":1534384317233524738},{\"component\":\"main/warehouse/index\",\"hidden\":false,\"id\":5000566847705099773,\"meta\":{\"icon\":\"hdd\",\"show\":true,\"title\":\"仓库管理\"},\"name\":\"warehouse_index\",\"path\":\"/wareHouse\",\"pid\":1534411830716350466}],\"message\":\"请求成功\",\"success\":true}', '2022-10-26 16:54:19', 'superAdmin', '81cd4768d1f6389ac0a1360ca226953764cde0bd26eb6cfdef631fcee7dd9488');
INSERT INTO `sys_op_log` VALUES (1585193093363376129, '系统菜单_切换', 7, 'Y', '成功', '127.0.0.1', '-', 'Chrome', 'Windows 10 or Windows Server 2016', '/sysMenu/change', 'vip.xiaonuo.sys.modular.menu.controller.SysMenuController', 'change', 'POST', '{\"application\":\"hr\"}', '{\"code\":200,\"data\":[{\"component\":\"PageView\",\"hidden\":false,\"id\":1264622039642255341,\"meta\":{\"icon\":\"team\",\"show\":true,\"title\":\"组织架构\"},\"name\":\"sys_mgr\",\"path\":\"/sys\",\"pid\":0},{\"component\":\"PageView\",\"hidden\":false,\"id\":1264622039642255671,\"meta\":{\"icon\":\"safety-certificate\",\"show\":true,\"title\":\"权限管理\"},\"name\":\"auth_manager\",\"path\":\"/auth\",\"pid\":0},{\"component\":\"system/user/index\",\"hidden\":false,\"id\":1264622039642255351,\"meta\":{\"show\":true,\"title\":\"用户管理\"},\"name\":\"sys_user_mgr\",\"path\":\"/mgr_user\",\"pid\":1264622039642255341},{\"component\":\"system/org/index\",\"hidden\":false,\"id\":1264622039642255521,\"meta\":{\"show\":true,\"title\":\"机构管理\"},\"name\":\"sys_org_mgr\",\"path\":\"/org\",\"pid\":1264622039642255341},{\"component\":\"system/pos/index\",\"hidden\":false,\"id\":1264622039642255601,\"meta\":{\"show\":true,\"title\":\"职位管理\"},\"name\":\"sys_pos_mgr\",\"path\":\"/pos\",\"pid\":1264622039642255341},{\"component\":\"system/app/index\",\"hidden\":false,\"id\":1264622039642255681,\"meta\":{\"show\":true,\"title\":\"应用管理\"},\"name\":\"sys_app_mgr\",\"path\":\"/app\",\"pid\":1264622039642255671},{\"component\":\"system/menu/index\",\"hidden\":false,\"id\":1264622039642255761,\"meta\":{\"show\":true,\"title\":\"菜单管理\"},\"name\":\"sys_menu_mgr\",\"path\":\"/menu\",\"pid\":1264622039642255671},{\"component\":\"system/role/index\",\"hidden\":false,\"id\":1264622039642255851,\"meta\":{\"show\":true,\"title\":\"角色管理\"},\"name\":\"sys_role_mgr\",\"path\":\"/role\",\"pid\":1264622039642255671}],\"message\":\"请求成功\",\"success\":true}', '2022-10-26 16:54:20', 'superAdmin', 'fe6dbc27e5cd79ef982e3712c9ced909f322b1ead232aa0a9ba2c347e7a02a5c');
INSERT INTO `sys_op_log` VALUES (1585193100300754945, '系统组织机构_树', 7, 'Y', '成功', '127.0.0.1', '-', 'Chrome', 'Windows 10 or Windows Server 2016', '/sysOrg/tree', 'vip.xiaonuo.sys.modular.org.controller.SysOrgController', 'tree', 'GET', '{}', '{\"code\":200,\"data\":[{\"children\":[{\"children\":[{\"children\":[],\"id\":1265476890672672771,\"parentId\":1265476890672672769,\"pid\":1265476890672672769,\"title\":\"研发部\",\"value\":\"1265476890672672771\",\"weight\":100},{\"children\":[],\"id\":1265476890672672772,\"parentId\":1265476890672672769,\"pid\":1265476890672672769,\"title\":\"企划部\",\"value\":\"1265476890672672772\",\"weight\":100}],\"id\":1265476890672672769,\"parentId\":1265476890651701250,\"pid\":1265476890651701250,\"title\":\"华夏集团乌鲁木齐分公司\",\"value\":\"1265476890672672769\",\"weight\":100},{\"children\":[{\"children\":[{\"children\":[],\"id\":1265476890672672775,\"parentId\":1265476890672672773,\"pid\":1265476890672672773,\"title\":\"市场部二部\",\"value\":\"1265476890672672775\",\"weight\":100}],\"id\":1265476890672672773,\"parentId\":1265476890672672770,\"pid\":1265476890672672770,\"title\":\"市场部\",\"value\":\"1265476890672672773\",\"weight\":100},{\"children\":[],\"id\":1265476890672672774,\"parentId\":1265476890672672770,\"pid\":1265476890672672770,\"title\":\"财务部\",\"value\":\"1265476890672672774\",\"weight\":100}],\"id\":1265476890672672770,\"parentId\":1265476890651701250,\"pid\":1265476890651701250,\"title\":\"华夏集团成都分公司\",\"value\":\"1265476890672672770\",\"weight\":100}],\"id\":1265476890651701250,\"parentId\":0,\"pid\":0,\"title\":\"华夏集团\",\"value\":\"1265476890651701250\",\"weight\":100}],\"message\":\"请求成功\",\"success\":true}', '2022-10-26 16:54:21', 'superAdmin', '22a98bee7c6ff3b1624a2eaa4348271a288c7616fbec53bb5996cc6221fbf297');
INSERT INTO `sys_op_log` VALUES (1585193101525491714, '系统字典类型_下拉', 5, 'Y', '成功', '127.0.0.1', '-', 'Chrome', 'Windows 10 or Windows Server 2016', '/sysDictType/dropDown', 'vip.xiaonuo.sys.modular.dict.controller.SysDictTypeController', 'dropDown', 'GET', '{\"code\":\"common_status\"}', '{\"code\":200,\"data\":[{\"code\":\"0\",\"value\":\"正常\"},{\"code\":\"1\",\"value\":\"停用\"},{\"code\":\"2\",\"value\":\"删除\"}],\"message\":\"请求成功\",\"success\":true}', '2022-10-26 16:54:22', 'superAdmin', '69217f7838ec974d429082020152f81f7a495d44ae445359a3cd373e93ef4876');
INSERT INTO `sys_op_log` VALUES (1585193101525491715, '系统字典类型_下拉', 5, 'Y', '成功', '127.0.0.1', '-', 'Chrome', 'Windows 10 or Windows Server 2016', '/sysDictType/dropDown', 'vip.xiaonuo.sys.modular.dict.controller.SysDictTypeController', 'dropDown', 'GET', '{\"code\":\"sex\"}', '{\"code\":200,\"data\":[{\"code\":\"1\",\"value\":\"男\"},{\"code\":\"2\",\"value\":\"女\"},{\"code\":\"3\",\"value\":\"未知\"}],\"message\":\"请求成功\",\"success\":true}', '2022-10-26 16:54:22', 'superAdmin', 'a0598ecf48b9d88c009551f7551e5cee9a0d3c5e87543ae0058a7ea54af2e25d');
INSERT INTO `sys_op_log` VALUES (1585193101986865154, '系统用户_查询', 5, 'Y', '成功', '127.0.0.1', '-', 'Chrome', 'Windows 10 or Windows Server 2016', '/sysUser/page', 'vip.xiaonuo.sys.modular.user.controller.SysUserController', 'page', 'GET', '{}', '{\"code\":200,\"data\":{\"pageNo\":1,\"pageSize\":10,\"rainbow\":[1],\"rows\":[{\"account\":\"yubaoshan\",\"birthday\":718041600000,\"email\":\"await183@qq.com\",\"id\":1275735541155614721,\"name\":\"俞宝山\",\"nickName\":\"俞宝山\",\"phone\":\"1f6e48e5610ba731115d118365dede4a\",\"sex\":1,\"status\":0,\"sysEmpInfo\":{\"jobNum\":\"001\",\"orgId\":1265476890672672769,\"orgName\":\"华夏集团新疆分公司\"},\"tel\":\"\"},{\"account\":\"xuyuxiang\",\"birthday\":1593532800000,\"id\":1280709549107552257,\"name\":\"徐玉祥\",\"nickName\":\"就是那个锅\",\"phone\":\"d5437be867777b7e27893bac3a3605a2\",\"sex\":1,\"status\":0,\"sysEmpInfo\":{\"orgId\":1265476890672672770,\"orgName\":\"华夏集团成都分公司\"}},{\"account\":\"dongxiayu\",\"birthday\":1639584000000,\"id\":1471457941179072513,\"name\":\"董夏雨\",\"nickName\":\"阿董\",\"phone\":\"24c5ce408af1c56078c4d58098ebc218\",\"sex\":1,\"status\":0,\"sysEmpInfo\":{\"orgId\":1265476890672672770,\"orgName\":\"华夏集团成都分公司\"}},{\"account\":\"Admin\",\"id\":1551457178297200641,\"name\":\"测试用户\",\"phone\":\"66664efc7eecc63d93f7c1ab56c690be\",\"sex\":1,\"status\":0,\"sysEmpInfo\":{\"orgId\":1265476890651701250,\"orgName\":\"华夏集团\"}},{\"account\":\"Admin1\",\"id\":1551462379850723329,\"name\":\"Admin\",\"phone\":\"4884cc97e8656d2e7842ead0fb22fa1b\",\"sex\":1,\"status\":0,\"sysEmpInfo\":{\"orgId\":1265476890651701250,\"orgName\":\"华夏集团\"}},{\"account\":\" admin\",\"id\":1560148932795891714,\"name\":\"管理员\",\"nickName\":\"管理员\",\"phone\":\"dc9456435c3f298f662811b8f9b57d5a\",\"sex\":1,\"status\":0,\"sysEmpInfo\":{\"orgId\":1265476890651701250,\"orgName\":\"华夏集团\"}},{\"account\":\"manager\",\"id\":1560149290020569089,\"name\":\"经理\",\"nickName\":\"经理\",\"phone\":\"dc9456435c3f298f662811b8f9b57d5a\",\"sex\":1,\"status\":0,\"sysEmpInfo\":{\"orgId\":1265476890651701250,\"orgName\":\"华夏集团\"}},{\"account\":\"gleader\",\"id\":1560149788173860866,\"name\":\"小组长 \",\"phone\":\"dc9456435c3f298f662811b8f9b57d5a\",\"sex\":1,\"status\":0,\"sysEmpInfo\":{\"orgId\":1265476890651701250,\"orgName\":\"华夏集团\"}},{\"account\":\"wperson\",\"id\":1560149962497523714,\"name\":\"生产人员\",\"phone\":\"dc9456435c3f298f662811b8f9b57d5a\",\"sex\":1,\"status\":0,\"sysEmpInfo\":{\"orgId\":1265476890651701250,\"orgName\":\"华夏集团\"}},{\"account\":\"nhszcy\",\"birthday\":1664294400000,\"id\":1574927397500977154,\"name\":\"南海数字产业\",\"nickName\":\"南海数字产业\",\"phone\":\"fb0e6dadcedfb3eaca597709a5d2304c\",\"sex\":1,\"status\":0,\"sysEmpInfo\":{\"orgId\":1265476890651701250,\"orgName\":\"华夏集团\"}}],\"totalPage\":1,\"totalRows\":10},\"message\":\"请求成功\",\"success\":true}', '2022-10-26 16:54:22', 'superAdmin', 'e968cc01fd86e2128b3e16ac862f9ba65af58792af5ce95e8a924a223234aaef');
INSERT INTO `sys_op_log` VALUES (1585193131162443777, '系统角色_查询', 5, 'Y', '成功', '127.0.0.1', '-', 'Chrome', 'Windows 10 or Windows Server 2016', '/sysRole/page', 'vip.xiaonuo.sys.modular.role.controller.SysRoleController', 'page', 'GET', '{}', '{\"code\":200,\"data\":{\"pageNo\":1,\"pageSize\":10,\"rainbow\":[1],\"rows\":[{\"code\":\"admin\",\"createTime\":1660803289000,\"createUser\":1265476890672672808,\"dataScopeType\":1,\"id\":1560148188390817794,\"name\":\"管理员\",\"sort\":1,\"status\":0},{\"code\":\"manager\",\"createTime\":1660803311000,\"createUser\":1265476890672672808,\"dataScopeType\":1,\"id\":1560148282678771714,\"name\":\"经理\",\"sort\":2,\"status\":0,\"updateTime\":1660803349000,\"updateUser\":1265476890672672808},{\"code\":\"gleader\",\"createTime\":1660803326000,\"createUser\":1265476890672672808,\"dataScopeType\":1,\"id\":1560148341927510018,\"name\":\"小组长\",\"sort\":3,\"status\":0,\"updateTime\":1660803397000,\"updateUser\":1265476890672672808},{\"code\":\"wperson\",\"createTime\":1660803342000,\"createUser\":1265476890672672808,\"dataScopeType\":1,\"id\":1560148409690685442,\"name\":\"生产人员\",\"sort\":4,\"status\":0,\"updateTime\":1660803380000,\"updateUser\":1265476890672672808},{\"code\":\"demo\",\"createTime\":1664326782000,\"createUser\":1265476890672672808,\"dataScopeType\":1,\"id\":1574926790555828225,\"name\":\"测试\",\"sort\":100,\"status\":0},{\"code\":\"test_user\",\"createTime\":1658731403000,\"createUser\":1265476890672672808,\"dataScopeType\":1,\"id\":1551458068714385410,\"name\":\"测试用户\",\"remark\":\"测试用户\",\"sort\":100,\"status\":0},{\"code\":\"ent_manager_role\",\"createTime\":1585826846000,\"createUser\":1265476890672672808,\"dataScopeType\":1,\"id\":1265476890672672817,\"name\":\"组织架构管理员\",\"remark\":\"组织架构管理员\",\"sort\":100,\"status\":0,\"updateTime\":1656573363000,\"updateUser\":1265476890672672808},{\"code\":\"auth_role\",\"createTime\":1585826920000,\"createUser\":1265476890672672808,\"dataScopeType\":1,\"id\":1265476890672672818,\"name\":\"权限管理员\",\"remark\":\"权限管理员\",\"sort\":101,\"status\":0,\"updateTime\":1656573386000,\"updateUser\":1265476890672672808}],\"totalPage\":1,\"totalRows\":8},\"message\":\"请求成功\",\"success\":true}', '2022-10-26 16:54:29', 'superAdmin', '41d91448f87fce5e3d5bd52557ee3cb8de7a8b022176cbd6a7bd39ac8e44d3a5');
INSERT INTO `sys_op_log` VALUES (1585193154608603138, '系统角色_拥有菜单', 6, 'Y', '成功', '127.0.0.1', '-', 'Chrome', 'Windows 10 or Windows Server 2016', '/sysRole/ownMenu', 'vip.xiaonuo.sys.modular.role.controller.SysRoleController', 'ownMenu', 'GET', '{\"id\":1551458068714385410}', '{\"code\":200,\"data\":[1538776469361139714,1264622039642256441,1264622039642256431,1264622039642256421,1264622039642256451,1264622039642256461,1264622039642256471,1264622039642256481,1264622039642256491,1264622039642256501,1264622039642256511,1264622039642256541,1264622039642256531,1264622039642256521,1264622039642256551,1264622039642256561,1264622039642256571,1264622039642256581,1264622039642256591,1264622039642256601,1410859007809736705,6570253563256899359,6527167616560933135,1534384317233524738,1551480636561321985,6547301851685353031,8197158197771689059,9222389351831596218,5142735407151148234,7621956982866257638,5553078347948343545,6436044181834026359,8315301036989682205,8618504262472492350,7950819887425681478,4832107133629024589,7946795106595000695,5279583233581044419,6845666717193712873,4998978684524443692,6449912099524079616,5707993274387362828,8275137972166495794,6249597959063106635,6905244625723471705,6709768906758836391,7388361180917918171,7702400310012060990,5243613315131080710,5079108238558434584,8562547749529140265,5788080164048630938,7936411315868128942,7940657177277675482,6194217070047486945,8426505795261326687,6103813152661113624,6374585520258378360,8621765388717143178,6116805160244737678,6860312372874164125,5820472386138045917,8181822590852115190,7639361031662168624,6507903030541782366,1534410297847214081,8562653372099681846,4830986669641223230,7974141988632466544,8313819116019873398,8931538668430408569,6122595225588020183,9202857208449608897,7656571819576826411,5014700545593967407,8088901678692869692,6299920358694835309,5642803668063215805,8834719374925492199,7882898168203081196,6667327679590719676,8959792232643188225,5668705457330168859,9180800170765745954,5327083612717101630,6329171771009724483,6020421073902938241,5886355131876064420,7346283523793183379,1542409869531873282,9208275275751949837,9173957282399717676,8973112451110891359,5753930915061776016,7288710125399936914,9022874385071881655,5378908852735764919,8477499608364957015,5206868747918117433,4757443910107701590,5016248121790626902,7516608184768822750,8293588485966810596,5523506981641034218,8494981115107347954,9152465645130034198,5012839659883217263,1534411830716350466,6280770118494855200,8775511535879974972,5624458298951752771,7254023492833901715,7318591877639154878,7812048842906278623,8633823152794346306,5893520734687245059,8868921547741003555,6326961474985796183,8833258242949762178,4967796364303223216,6900932255841430981,7629147488562302699,8016744771368544956,5002055413507195458,6290232610677682017,5803272132980049305,7395287038762629813,7323225129555267277,6347609996552577764,7682643462888492777,6696300230304937737,8436831550169549160,7898061431233899837,5448185299220002627,9135927743117002076,6655166482691941995,5871841993475141582,5276828216703973340,7455112058491820064,8957317986837088422,4621333945127042674,5886900280768283007,5846194751422568093,9156093698218665129,7678659259268274624,8099644559700436461,8107628912599125262,7072750871619746982,4903707486851837034,7635960329677377171,7596329328691560085,6429021782911745716,8259333771823873238,4674816360932725215,5653092812632926489,5474507168841280952,7200474546305867050,7242870236365548592,9150731911657480775,7287765970469918079,5480200211927228567,4770899845682577376,6238776219082572290,8700710314151240148,8613714595480779524,5346549595871021092,8181816557328888214,5587279777867961498,7380411400614516902,7721498005843290304,1264622039642256641,1264622039642256621,1264622039642256611,1264622039642256651,1264622039642256661,1264622039642256671,1264622039642256681,1264622039642256691,1264622039642256701,1264622039642256711,1264622039642256631,1264622039642255361,1264622039642255591,1264622039642255351,1264622039642255341,1264622039642255521],\"message\":\"请求成功\",\"success\":true}', '2022-10-26 16:54:34', 'superAdmin', '61ba0daaca3ce3081701ca99861b46a5969f006507b13f521a4a98005a96074d');
INSERT INTO `sys_op_log` VALUES (1585193166382014466, '系统菜单_授权树', 7, 'Y', '成功', '127.0.0.1', '-', 'Chrome', 'Windows 10 or Windows Server 2016', '/sysMenu/treeForGrant', 'vip.xiaonuo.sys.modular.menu.controller.SysMenuController', 'treeForGrant', 'GET', '{}', '{\"code\":200,\"data\":[{\"children\":[],\"id\":1538776469361139714,\"parentId\":0,\"pid\":0,\"title\":\"大屏展示\",\"value\":\"1538776469361139714\",\"weight\":1},{\"children\":[{\"children\":[],\"id\":1264622039642255331,\"parentId\":1264622039642255311,\"pid\":1264622039642255311,\"title\":\"工作台\",\"value\":\"1264622039642255331\",\"weight\":2},{\"children\":[],\"id\":1264622039642255321,\"parentId\":1264622039642255311,\"pid\":1264622039642255311,\"title\":\"分析页\",\"value\":\"1264622039642255321\",\"weight\":100}],\"id\":1264622039642255311,\"parentId\":0,\"pid\":0,\"title\":\"主控面板\",\"value\":\"1264622039642255311\",\"weight\":1},{\"children\":[{\"children\":[{\"children\":[],\"id\":1264622039642255361,\"parentId\":1264622039642255351,\"pid\":1264622039642255351,\"title\":\"用户查询\",\"value\":\"1264622039642255361\",\"weight\":100},{\"children\":[],\"id\":1264622039642255371,\"parentId\":1264622039642255351,\"pid\":1264622039642255351,\"title\":\"用户编辑\",\"value\":\"1264622039642255371\",\"weight\":100},{\"children\":[],\"id\":1264622039642255381,\"parentId\":1264622039642255351,\"pid\":1264622039642255351,\"title\":\"用户增加\",\"value\":\"1264622039642255381\",\"weight\":100},{\"children\":[],\"id\":1264622039642255391,\"parentId\":1264622039642255351,\"pid\":1264622039642255351,\"title\":\"用户删除\",\"value\":\"1264622039642255391\",\"weight\":100},{\"children\":[],\"id\":1264622039642255401,\"parentId\":1264622039642255351,\"pid\":1264622039642255351,\"title\":\"用户详情\",\"value\":\"1264622039642255401\",\"weight\":100},{\"children\":[],\"id\":1264622039642255411,\"parentId\":1264622039642255351,\"pid\":1264622039642255351,\"title\":\"用户导出\",\"value\":\"1264622039642255411\",\"weight\":100},{\"children\":[],\"id\":1264622039642255421,\"parentId\":1264622039642255351,\"pid\":1264622039642255351,\"title\":\"用户选择器\",\"value\":\"1264622039642255421\",\"weight\":100},{\"children\":[],\"id\":1264622039642255431,\"parentId\":1264622039642255351,\"pid\":1264622039642255351,\"title\":\"用户授权角色\",\"value\":\"1264622039642255431\",\"weight\":100},{\"children\":[],\"id\":1264622039642255441,\"parentId\":1264622039642255351,\"pid\":1264622039642255351,\"title\":\"用户拥有角色\",\"value\":\"1264622039642255441\",\"weight\":100},{\"children\":[],\"id\":1264622039642255451,\"parentId\":1264622039642255351,\"pid\":1264622039642255351,\"title\":\"用户授权数据\",\"value\":\"1264622039642255451\",\"weight\":100},{\"children\":[],\"id\":1264622039642255461,\"parentId\":1264622039642255351,\"pid\":1264622039642255351,\"title\":\"用户拥有数据\",\"value\":\"1264622039642255461\",\"weight\":100},{\"children\":[],\"id\":1264622039642255471,\"parentId\":1264622039642255351,\"pid\":1264622039642255351,\"title\":\"用户更新信息\",\"value\":\"1264622039642255471\",\"weight\":100},{\"children\":[],\"id\":1264622039642255481,\"parentId\":1264622039642255351,\"pid\":1264622039642255351,\"title\":\"用户修改密码\",\"value\":\"1264622039642255481\",\"weight\":100},{\"children\":[],\"id\":1264622039642255491,\"parentId\":1264622039642255351,\"pid\":1264622039642255351,\"title\":\"用户修改状态\",\"value\":\"1264622039642255491\",\"weight\":100},{\"children\":[],\"id\":1264622039642255501,\"parentId\":1264622039642255351,\"pid\":1264622039642255351,\"title\":\"用户修改头像\",\"value\":\"1264622039642255501\",\"weight\":100},{\"children\":[],\"id\":1264622039642255511,\"parentId\":1264622039642255351,\"pid\":1264622039642255351,\"title\":\"用户重置密码\",\"value\":\"1264622039642255511\",\"weight\":100}],\"id\":1264622039642255351,\"parentId\":1264622039642255341,\"pid\":1264622039642255341,\"title\":\"用户管理\",\"value\":\"1264622039642255351\",\"weight\":3},{\"children\":[{\"children\":[],\"id\":1264622039642255531,\"parentId\":1264622039642255521,\"pid\":1264622039642255521,\"title\":\"机构查询\",\"value\":\"1264622039642255531\",\"weight\":100},{\"children\":[],\"id\":1264622039642255541,\"parentId\":1264622039642255521,\"pid\":1264622039642255521,\"title\":\"机构列表\",\"value\":\"1264622039642255541\",\"weight\":100},{\"children\":[],\"id\":1264622039642255551,\"parentId\":1264622039642255521,\"pid\":1264622039642255521,\"title\":\"机构增加\",\"value\":\"1264622039642255551\",\"weight\":100},{\"children\":[],\"id\":1264622039642255561,\"parentId\":1264622039642255521,\"pid\":1264622039642255521,\"title\":\"机构编辑\",\"value\":\"1264622039642255561\",\"weight\":100},{\"children\":[],\"id\":1264622039642255571,\"parentId\":1264622039642255521,\"pid\":1264622039642255521,\"title\":\"机构删除\",\"value\":\"1264622039642255571\",\"weight\":100},{\"children\":[],\"id\":1264622039642255581,\"parentId\":1264622039642255521,\"pid\":1264622039642255521,\"title\":\"机构详情\",\"value\":\"1264622039642255581\",\"weight\":100},{\"children\":[],\"id\":1264622039642255591,\"parentId\":1264622039642255521,\"pid\":1264622039642255521,\"title\":\"机构树\",\"value\":\"1264622039642255591\",\"weight\":100}],\"id\":1264622039642255521,\"parentId\":1264622039642255341,\"pid\":1264622039642255341,\"title\":\"机构管理\",\"value\":\"1264622039642255521\",\"weight\":4},{\"children\":[{\"children\":[],\"id\":1264622039642255621,\"parentId\":1264622039642255601,\"pid\":1264622039642255601,\"title\":\"职位列表\",\"value\":\"1264622039642255621\",\"weight\":100},{\"children\":[],\"id\":1264622039642255631,\"parentId\":1264622039642255601,\"pid\":1264622039642255601,\"title\":\"职位增加\",\"value\":\"1264622039642255631\",\"weight\":100},{\"children\":[],\"id\":1264622039642255641,\"parentId\":1264622039642255601,\"pid\":1264622039642255601,\"title\":\"职位编辑\",\"value\":\"1264622039642255641\",\"weight\":100},{\"children\":[],\"id\":1264622039642255651,\"parentId\":1264622039642255601,\"pid\":1264622039642255601,\"title\":\"职位删除\",\"value\":\"1264622039642255651\",\"weight\":100},{\"children\":[],\"id\":1264622039642255661,\"parentId\":1264622039642255601,\"pid\":1264622039642255601,\"title\":\"职位详情\",\"value\":\"1264622039642255661\",\"weight\":100},{\"children\":[],\"id\":1264622039642255611,\"parentId\":1264622039642255601,\"pid\":1264622039642255601,\"title\":\"职位查询\",\"value\":\"1264622039642255611\",\"weight\":100}],\"id\":1264622039642255601,\"parentId\":1264622039642255341,\"pid\":1264622039642255341,\"title\":\"职位管理\",\"value\":\"1264622039642255601\",\"weight\":5}],\"id\":1264622039642255341,\"parentId\":0,\"pid\":0,\"title\":\"组织架构\",\"value\":\"1264622039642255341\",\"weight\":2},{\"children\":[{\"children\":[{\"children\":[],\"id\":1264622039642255691,\"parentId\":1264622039642255681,\"pid\":1264622039642255681,\"title\":\"应用查询\",\"value\":\"1264622039642255691\",\"weight\":100},{\"children\":[],\"id\":1264622039642255701,\"parentId\":1264622039642255681,\"pid\":1264622039642255681,\"title\":\"应用列表\",\"value\":\"1264622039642255701\",\"weight\":100},{\"children\":[],\"id\":1264622039642255711,\"parentId\":1264622039642255681,\"pid\":1264622039642255681,\"title\":\"应用增加\",\"value\":\"1264622039642255711\",\"weight\":100},{\"children\":[],\"id\":1264622039642255721,\"parentId\":1264622039642255681,\"pid\":1264622039642255681,\"title\":\"应用编辑\",\"value\":\"1264622039642255721\",\"weight\":100},{\"children\":[],\"id\":1264622039642255731,\"parentId\":1264622039642255681,\"pid\":1264622039642255681,\"title\":\"应用删除\",\"value\":\"1264622039642255731\",\"weight\":100},{\"children\":[],\"id\":1264622039642255741,\"parentId\":1264622039642255681,\"pid\":1264622039642255681,\"title\":\"应用详情\",\"value\":\"1264622039642255741\",\"weight\":100},{\"children\":[],\"id\":1264622039642255751,\"parentId\":1264622039642255681,\"pid\":1264622039642255681,\"title\":\"设为默认应用\",\"value\":\"1264622039642255751\",\"weight\":100}],\"id\":1264622039642255681,\"parentId\":1264622039642255671,\"pid\":1264622039642255671,\"title\":\"应用管理\",\"value\":\"1264622039642255681\",\"weight\":6},{\"children\":[{\"children\":[],\"id\":1264622039642255771,\"parentId\":1264622039642255761,\"pid\":1264622039642255761,\"title\":\"菜单列表\",\"value\":\"1264622039642255771\",\"weight\":100},{\"children\":[],\"id\":1264622039642255781,\"parentId\":1264622039642255761,\"pid\":1264622039642255761,\"title\":\"菜单增加\",\"value\":\"1264622039642255781\",\"weight\":100},{\"children\":[],\"id\":1264622039642255791,\"parentId\":1264622039642255761,\"pid\":1264622039642255761,\"title\":\"菜单编辑\",\"value\":\"1264622039642255791\",\"weight\":100},{\"children\":[],\"id\":1264622039642255801,\"parentId\":1264622039642255761,\"pid\":1264622039642255761,\"title\":\"菜单删除\",\"value\":\"1264622039642255801\",\"weight\":100},{\"children\":[],\"id\":1264622039642255811,\"parentId\":1264622039642255761,\"pid\":1264622039642255761,\"title\":\"菜单详情\",\"value\":\"1264622039642255811\",\"weight\":100},{\"children\":[],\"id\":1264622039642255821,\"parentId\":1264622039642255761,\"pid\":1264622039642255761,\"title\":\"菜单授权树\",\"value\":\"1264622039642255821\",\"weight\":100},{\"children\":[],\"id\":1264622039642255831,\"parentId\":1264622039642255761,\"pid\":1264622039642255761,\"title\":\"菜单树\",\"value\":\"1264622039642255831\",\"weight\":100},{\"children\":[],\"id\":1264622039642255841,\"parentId\":1264622039642255761,\"pid\":1264622039642255761,\"title\":\"菜单切换\",\"value\":\"1264622039642255841\",\"weight\":100}],\"id\":1264622039642255761,\"parentId\":1264622039642255671,\"pid\":1264622039642255671,\"title\":\"菜单管理\",\"value\":\"1264622039642255761\",\"weight\":7},{\"children\":[{\"children\":[],\"id\":1264622039642255881,\"parentId\":1264622039642255851,\"pid\":1264622039642255851,\"title\":\"角色编辑\",\"value\":\"1264622039642255881\",\"weight\":100},{\"children\":[],\"id\":1264622039642255891,\"parentId\":1264622039642255851,\"pid\":1264622039642255851,\"title\":\"角色删除\",\"value\":\"1264622039642255891\",\"weight\":100},{\"children\":[],\"id\":1264622039642255901,\"parentId\":1264622039642255851,\"pid\":1264622039642255851,\"title\":\"角色详情\",\"value\":\"1264622039642255901\",\"weight\":100},{\"children\":[],\"id\":1264622039642255911,\"parentId\":1264622039642255851,\"pid\":1264622039642255851,\"title\":\"角色下拉\",\"value\":\"1264622039642255911\",\"weight\":100},{\"children\":[],\"id\":1264622039642255921,\"parentId\":1264622039642255851,\"pid\":1264622039642255851,\"title\":\"角色授权菜单\",\"value\":\"1264622039642255921\",\"weight\":100},{\"children\":[],\"id\":1264622039642255931,\"parentId\":1264622039642255851,\"pid\":1264622039642255851,\"title\":\"角色拥有菜单\",\"value\":\"1264622039642255931\",\"weight\":100},{\"children\":[],\"id\":1264622039642255941,\"parentId\":1264622039642255851,\"pid\":1264622039642255851,\"title\":\"角色授权数据\",\"value\":\"1264622039642255941\",\"weight\":100},{\"children\":[],\"id\":1264622039642255951,\"parentId\":1264622039642255851,\"pid\":1264622039642255851,\"title\":\"角色拥有数据\",\"value\":\"1264622039642255951\",\"weight\":100},{\"children\":[],\"id\":1264622039642255861,\"parentId\":1264622039642255851,\"pid\":1264622039642255851,\"title\":\"角色查询\",\"value\":\"1264622039642255861\",\"weight\":100},{\"children\":[],\"id\":1264622039642255871,\"parentId\":1264622039642255851,\"pid\":1264622039642255851,\"title\":\"角色增加\",\"value\":\"1264622039642255871\",\"weight\":100}],\"id\":1264622039642255851,\"parentId\":1264622039642255671,\"pid\":1264622039642255671,\"title\":\"角色管理\",\"value\":\"1264622039642255851\",\"weight\":8}],\"id\":1264622039642255671,\"parentId\":0,\"pid\":0,\"title\":\"权限管理\",\"value\":\"1264622039642255671\",\"weight\":3},{\"children\":[{\"children\":[{\"children\":[],\"id\":1264622039642255981,\"parentId\":1264622039642255971,\"pid\":1264622039642255971,\"title\":\"配置查询\",\"value\":\"1264622039642255981\",\"weight\":100},{\"children\":[],\"id\":1264622039642255991,\"parentId\":1264622039642255971,\"pid\":1264622039642255971,\"title\":\"配置列表\",\"value\":\"1264622039642255991\",\"weight\":100},{\"children\":[],\"id\":1264622039642256001,\"parentId\":1264622039642255971,\"pid\":1264622039642255971,\"title\":\"配置增加\",\"value\":\"1264622039642256001\",\"weight\":100},{\"children\":[],\"id\":1264622039642256011,\"parentId\":1264622039642255971,\"pid\":1264622039642255971,\"title\":\"配置编辑\",\"value\":\"1264622039642256011\",\"weight\":100},{\"children\":[],\"id\":1264622039642256021,\"parentId\":1264622039642255971,\"pid\":1264622039642255971,\"title\":\"配置删除\",\"value\":\"1264622039642256021\",\"weight\":100},{\"children\":[],\"id\":1264622039642256031,\"parentId\":1264622039642255971,\"pid\":1264622039642255971,\"title\":\"配置详情\",\"value\":\"1264622039642256031\",\"weight\":100}],\"id\":1264622039642255971,\"parentId\":1264622039642255961,\"pid\":1264622039642255961,\"title\":\"系统配置\",\"value\":\"1264622039642255971\",\"weight\":9},{\"children\":[{\"children\":[],\"id\":1264622039642256051,\"parentId\":1264622039642256041,\"pid\":1264622039642256041,\"title\":\"发送文本邮件\",\"value\":\"1264622039642256051\",\"weight\":100},{\"children\":[],\"id\":1264622039642256061,\"parentId\":1264622039642256041,\"pid\":1264622039642256041,\"title\":\"发送html邮件\",\"value\":\"1264622039642256061\",\"weight\":100}],\"id\":1264622039642256041,\"parentId\":1264622039642255961,\"pid\":1264622039642255961,\"title\":\"邮件发送\",\"value\":\"1264622039642256041\",\"weight\":10},{\"children\":[{\"children\":[],\"id\":1264622039642256081,\"parentId\":1264622039642256071,\"pid\":1264622039642256071,\"title\":\"短信发送查询\",\"value\":\"1264622039642256081\",\"weight\":100},{\"children\":[],\"id\":1264622039642256091,\"parentId\":1264622039642256071,\"pid\":1264622039642256071,\"title\":\"发送验证码短信\",\"value\":\"1264622039642256091\",\"weight\":100},{\"children\":[],\"id\":1264622039642256101,\"parentId\":1264622039642256071,\"pid\":1264622039642256071,\"title\":\"验证短信验证码\",\"value\":\"1264622039642256101\",\"weight\":100}],\"id\":1264622039642256071,\"parentId\":1264622039642255961,\"pid\":1264622039642255961,\"title\":\"短信管理\",\"value\":\"1264622039642256071\",\"weight\":11},{\"children\":[{\"children\":[],\"id\":1264622039642256131,\"parentId\":1264622039642256111,\"pid\":1264622039642256111,\"title\":\"字典类型列表\",\"value\":\"1264622039642256131\",\"weight\":100},{\"children\":[],\"id\":1264622039642256141,\"parentId\":1264622039642256111,\"pid\":1264622039642256111,\"title\":\"字典类型增加\",\"value\":\"1264622039642256141\",\"weight\":100},{\"children\":[],\"id\":1264622039642256151,\"parentId\":1264622039642256111,\"pid\":1264622039642256111,\"title\":\"字典类型删除\",\"value\":\"1264622039642256151\",\"weight\":100},{\"children\":[],\"id\":1264622039642256161,\"parentId\":1264622039642256111,\"pid\":1264622039642256111,\"title\":\"字典类型编辑\",\"value\":\"1264622039642256161\",\"weight\":100},{\"children\":[],\"id\":1264622039642256171,\"parentId\":1264622039642256111,\"pid\":1264622039642256111,\"title\":\"字典类型详情\",\"value\":\"1264622039642256171\",\"weight\":100},{\"children\":[],\"id\":1264622039642256181,\"parentId\":1264622039642256111,\"pid\":1264622039642256111,\"title\":\"字典类型下拉\",\"value\":\"1264622039642256181\",\"weight\":100},{\"children\":[],\"id\":1264622039642256191,\"parentId\":1264622039642256111,\"pid\":1264622039642256111,\"title\":\"字典类型修改状态\",\"value\":\"1264622039642256191\",\"weight\":100},{\"children\":[],\"id\":1264622039642256201,\"parentId\":1264622039642256111,\"pid\":1264622039642256111,\"title\":\"字典值查询\",\"value\":\"1264622039642256201\",\"weight\":100},{\"children\":[],\"id\":1264622039642256211,\"parentId\":1264622039642256111,\"pid\":1264622039642256111,\"title\":\"字典值列表\",\"value\":\"1264622039642256211\",\"weight\":100},{\"children\":[],\"id\":1264622039642256221,\"parentId\":1264622039642256111,\"pid\":1264622039642256111,\"title\":\"字典值增加\",\"value\":\"1264622039642256221\",\"weight\":100},{\"children\":[],\"id\":1264622039642256231,\"parentId\":1264622039642256111,\"pid\":1264622039642256111,\"title\":\"字典值删除\",\"value\":\"1264622039642256231\",\"weight\":100},{\"children\":[],\"id\":1264622039642256241,\"parentId\":1264622039642256111,\"pid\":1264622039642256111,\"title\":\"字典值编辑\",\"value\":\"1264622039642256241\",\"weight\":100},{\"children\":[],\"id\":1264622039642256251,\"parentId\":1264622039642256111,\"pid\":1264622039642256111,\"title\":\"字典值详情\",\"value\":\"1264622039642256251\",\"weight\":100},{\"children\":[],\"id\":1264622039642256261,\"parentId\":1264622039642256111,\"pid\":1264622039642256111,\"title\":\"字典值修改状态\",\"value\":\"1264622039642256261\",\"weight\":100},{\"children\":[],\"id\":1264622039642256121,\"parentId\":1264622039642256111,\"pid\":1264622039642256111,\"title\":\"字典类型查询\",\"value\":\"1264622039642256121\",\"weight\":100}],\"id\":1264622039642256111,\"parentId\":1264622039642255961,\"pid\":1264622039642255961,\"title\":\"字典管理\",\"value\":\"1264622039642256111\",\"weight\":12},{\"children\":[],\"id\":1264622039642256271,\"parentId\":1264622039642255961,\"pid\":1264622039642255961,\"title\":\"接口文档\",\"value\":\"1264622039642256271\",\"weight\":13}],\"id\":1264622039642255961,\"parentId\":0,\"pid\":0,\"title\":\"开发管理\",\"value\":\"1264622039642255961\",\"weight\":4},{\"children\":[{\"children\":[{\"children\":[],\"id\":1264622039642256301,\"parentId\":1264622039642256291,\"pid\":1264622039642256291,\"title\":\"访问日志查询\",\"value\":\"1264622039642256301\",\"weight\":100},{\"children\":[],\"id\":1264622039642256311,\"parentId\":1264622039642256291,\"pid\":1264622039642256291,\"title\":\"访问日志清空\",\"value\":\"1264622039642256311\",\"weight\":100}],\"id\":1264622039642256291,\"parentId\":1264622039642256281,\"pid\":1264622039642256281,\"title\":\"访问日志\",\"value\":\"1264622039642256291\",\"weight\":14},{\"children\":[{\"children\":[],\"id\":1264622039642256331,\"parentId\":1264622039642256321,\"pid\":1264622039642256321,\"title\":\"操作日志查询\",\"value\":\"1264622039642256331\",\"weight\":100},{\"children\":[],\"id\":1264622039642256341,\"parentId\":1264622039642256321,\"pid\":1264622039642256321,\"title\":\"操作日志清空\",\"value\":\"1264622039642256341\",\"weight\":100}],\"id\":1264622039642256321,\"parentId\":1264622039642256281,\"pid\":1264622039642256281,\"title\":\"操作日志\",\"value\":\"1264622039642256321\",\"weight\":15}],\"id\":1264622039642256281,\"parentId\":0,\"pid\":0,\"title\":\"日志管理\",\"value\":\"1264622039642256281\",\"weight\":5},{\"children\":[{\"children\":[{\"children\":[],\"id\":1264622039642256371,\"parentId\":1264622039642256361,\"pid\":1264622039642256361,\"title\":\"服务监控查询\",\"value\":\"1264622039642256371\",\"weight\":100}],\"id\":1264622039642256361,\"parentId\":1264622039642256351,\"pid\":1264622039642256351,\"title\":\"服务监控\",\"value\":\"1264622039642256361\",\"weight\":16},{\"children\":[{\"children\":[],\"id\":1264622039642256391,\"parentId\":1264622039642256381,\"pid\":1264622039642256381,\"title\":\"在线用户列表\",\"value\":\"1264622039642256391\",\"weight\":100},{\"children\":[],\"id\":1264622039642256401,\"parentId\":1264622039642256381,\"pid\":1264622039642256381,\"title\":\"在线用户强退\",\"value\":\"1264622039642256401\",\"weight\":100}],\"id\":1264622039642256381,\"parentId\":1264622039642256351,\"pid\":1264622039642256351,\"title\":\"在线用户\",\"value\":\"1264622039642256381\",\"weight\":17},{\"children\":[],\"id\":1264622039642256411,\"parentId\":1264622039642256351,\"pid\":1264622039642256351,\"title\":\"数据监控\",\"value\":\"1264622039642256411\",\"weight\":18}],\"id\":1264622039642256351,\"parentId\":0,\"pid\":0,\"title\":\"系统监控\",\"value\":\"1264622039642256351\",\"weight\":6},{\"children\":[{\"children\":[{\"children\":[],\"id\":1264622039642256441,\"parentId\":1264622039642256431,\"pid\":1264622039642256431,\"title\":\"公告查询\",\"value\":\"1264622039642256441\",\"weight\":100},{\"children\":[],\"id\":1264622039642256451,\"parentId\":1264622039642256431,\"pid\":1264622039642256431,\"title\":\"公告增加\",\"value\":\"1264622039642256451\",\"weight\":100},{\"children\":[],\"id\":1264622039642256461,\"parentId\":1264622039642256431,\"pid\":1264622039642256431,\"title\":\"公告编辑\",\"value\":\"1264622039642256461\",\"weight\":100},{\"children\":[],\"id\":1264622039642256471,\"parentId\":1264622039642256431,\"pid\":1264622039642256431,\"title\":\"公告删除\",\"value\":\"1264622039642256471\",\"weight\":100},{\"children\":[],\"id\":1264622039642256481,\"parentId\":1264622039642256431,\"pid\":1264622039642256431,\"title\":\"公告查看\",\"value\":\"1264622039642256481\",\"weight\":100},{\"children\":[],\"id\":1264622039642256491,\"parentId\":1264622039642256431,\"pid\":1264622039642256431,\"title\":\"公告修改状态\",\"value\":\"1264622039642256491\",\"weight\":100}],\"id\":1264622039642256431,\"parentId\":1264622039642256421,\"pid\":1264622039642256421,\"title\":\"公告管理\",\"value\":\"1264622039642256431\",\"weight\":19},{\"children\":[{\"children\":[],\"id\":1264622039642256511,\"parentId\":1264622039642256501,\"pid\":1264622039642256501,\"title\":\"已收公告查询\",\"value\":\"1264622039642256511\",\"weight\":100}],\"id\":1264622039642256501,\"parentId\":1264622039642256421,\"pid\":1264622039642256421,\"title\":\"已收公告\",\"value\":\"1264622039642256501\",\"weight\":20}],\"id\":1264622039642256421,\"parentId\":0,\"pid\":0,\"title\":\"通知公告\",\"value\":\"1264622039642256421\",\"weight\":7},{\"children\":[{\"children\":[{\"children\":[],\"id\":1264622039642256541,\"parentId\":1264622039642256531,\"pid\":1264622039642256531,\"title\":\"文件查询\",\"value\":\"1264622039642256541\",\"weight\":100},{\"children\":[],\"id\":1264622039642256551,\"parentId\":1264622039642256531,\"pid\":1264622039642256531,\"title\":\"文件列表\",\"value\":\"1264622039642256551\",\"weight\":100},{\"children\":[],\"id\":1264622039642256561,\"parentId\":1264622039642256531,\"pid\":1264622039642256531,\"title\":\"文件删除\",\"value\":\"1264622039642256561\",\"weight\":100},{\"children\":[],\"id\":1264622039642256571,\"parentId\":1264622039642256531,\"pid\":1264622039642256531,\"title\":\"文件详情\",\"value\":\"1264622039642256571\",\"weight\":100},{\"children\":[],\"id\":1264622039642256581,\"parentId\":1264622039642256531,\"pid\":1264622039642256531,\"title\":\"文件上传\",\"value\":\"1264622039642256581\",\"weight\":100},{\"children\":[],\"id\":1264622039642256591,\"parentId\":1264622039642256531,\"pid\":1264622039642256531,\"title\":\"文件下载\",\"value\":\"1264622039642256591\",\"weight\":100},{\"children\":[],\"id\":1264622039642256601,\"parentId\":1264622039642256531,\"pid\":1264622039642256531,\"title\":\"图片预览\",\"value\":\"1264622039642256601\",\"weight\":100}],\"id\":1264622039642256531,\"parentId\":1264622039642256521,\"pid\":1264622039642256521,\"title\":\"系统文件\",\"value\":\"1264622039642256531\",\"weight\":21},{\"children\":[],\"id\":1410859007809736705,\"parentId\":1264622039642256521,\"pid\":1264622039642256521,\"title\":\"在线文档\",\"value\":\"1410859007809736705\",\"weight\":100}],\"id\":1264622039642256521,\"parentId\":0,\"pid\":0,\"title\":\"文件管理\",\"value\":\"1264622039642256521\",\"weight\":8},{\"children\":[{\"children\":[],\"id\":256,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"工艺路线新增（自）\",\"value\":\"256\",\"weight\":100},{\"children\":[],\"id\":1,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"用户查询（自）\",\"value\":\"1\",\"weight\":100},{\"children\":[],\"id\":257,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"产品表新增（自）\",\"value\":\"257\",\"weight\":100},{\"children\":[],\"id\":2,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"用户编辑（自）\",\"value\":\"2\",\"weight\":100},{\"children\":[],\"id\":258,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"产品表删除（自）\",\"value\":\"258\",\"weight\":100},{\"children\":[],\"id\":3,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"用户增加（自）\",\"value\":\"3\",\"weight\":100},{\"children\":[],\"id\":259,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"工单表删除（自）\",\"value\":\"259\",\"weight\":100},{\"children\":[],\"id\":4,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"用户删除（自）\",\"value\":\"4\",\"weight\":100},{\"children\":[],\"id\":260,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"供应商联系人导出（自）\",\"value\":\"260\",\"weight\":100},{\"children\":[],\"id\":5,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"用户详情（自）\",\"value\":\"5\",\"weight\":100},{\"children\":[],\"id\":261,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"任务结束（自）\",\"value\":\"261\",\"weight\":100},{\"children\":[],\"id\":6,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"用户导出（自）\",\"value\":\"6\",\"weight\":100},{\"children\":[],\"id\":262,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"字段配置查询（自）\",\"value\":\"262\",\"weight\":100},{\"children\":[],\"id\":7,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"用户选择器（自）\",\"value\":\"7\",\"weight\":100},{\"children\":[],\"id\":263,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"客户分类列表（自）\",\"value\":\"263\",\"weight\":100},{\"children\":[],\"id\":8,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"用户授权角色（自）\",\"value\":\"8\",\"weight\":100},{\"children\":[],\"id\":264,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"供应商分类查询（自）\",\"value\":\"264\",\"weight\":100},{\"children\":[],\"id\":9,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"用户拥有角色（自）\",\"value\":\"9\",\"weight\":100},{\"children\":[],\"id\":265,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"客户联系人列表（自）\",\"value\":\"265\",\"weight\":100},{\"children\":[],\"id\":10,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"用户授权数据（自）\",\"value\":\"10\",\"weight\":100},{\"children\":[],\"id\":266,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"工序路线关系表编辑（自）\",\"value\":\"266\",\"weight\":100},{\"children\":[],\"id\":11,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"用户拥有数据（自）\",\"value\":\"11\",\"weight\":100},{\"children\":[],\"id\":267,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"产品类型表编辑（自）\",\"value\":\"267\",\"weight\":100},{\"children\":[],\"id\":12,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"用户更新信息（自）\",\"value\":\"12\",\"weight\":100},{\"children\":[],\"id\":268,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"库存余额列表（自）\",\"value\":\"268\",\"weight\":100},{\"children\":[],\"id\":13,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"用户修改密码（自）\",\"value\":\"13\",\"weight\":100},{\"children\":[],\"id\":269,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"客户分类查看（自）\",\"value\":\"269\",\"weight\":100},{\"children\":[],\"id\":14,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"用户修改状态（自）\",\"value\":\"14\",\"weight\":100},{\"children\":[],\"id\":270,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"库存余额编辑（自）\",\"value\":\"270\",\"weight\":100},{\"children\":[],\"id\":15,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"用户修改头像（自）\",\"value\":\"15\",\"weight\":100},{\"children\":[],\"id\":271,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"工序编辑（自）\",\"value\":\"271\",\"weight\":100},{\"children\":[],\"id\":16,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"用户重置密码（自）\",\"value\":\"16\",\"weight\":100},{\"children\":[],\"id\":272,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"生产计划查看（自）\",\"value\":\"272\",\"weight\":100},{\"children\":[],\"id\":17,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"机构查询（自）\",\"value\":\"17\",\"weight\":100},{\"children\":[],\"id\":273,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"工单表列表（自）\",\"value\":\"273\",\"weight\":100},{\"children\":[],\"id\":18,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"机构列表（自）\",\"value\":\"18\",\"weight\":100},{\"children\":[],\"id\":274,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"产品表导出（自）\",\"value\":\"274\",\"weight\":100},{\"children\":[],\"id\":19,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"机构增加（自）\",\"value\":\"19\",\"weight\":100},{\"children\":[],\"id\":275,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"仓库管理删除（自）\",\"value\":\"275\",\"weight\":100},{\"children\":[],\"id\":20,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"机构编辑（自）\",\"value\":\"20\",\"weight\":100},{\"children\":[],\"id\":276,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"SaaS租户列表（自）\",\"value\":\"276\",\"weight\":100},{\"children\":[],\"id\":21,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"机构删除（自）\",\"value\":\"21\",\"weight\":100},{\"children\":[],\"id\":277,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"SaaS租户新增（自）\",\"value\":\"277\",\"weight\":100},{\"children\":[],\"id\":22,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"机构详情（自）\",\"value\":\"22\",\"weight\":100},{\"children\":[],\"id\":278,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\" 供应商资料删除（自）\",\"value\":\"278\",\"weight\":100},{\"children\":[],\"id\":23,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"机构树（自）\",\"value\":\"23\",\"weight\":100},{\"children\":[],\"id\":279,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"SaaS租户编辑（自）\",\"value\":\"279\",\"weight\":100},{\"children\":[],\"id\":24,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"职位查询（自）\",\"value\":\"24\",\"weight\":100},{\"children\":[],\"id\":280,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"生产计划列表（自）\",\"value\":\"280\",\"weight\":100},{\"children\":[],\"id\":25,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"职位列表（自）\",\"value\":\"25\",\"weight\":100},{\"children\":[],\"id\":281,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"供应商分类编辑（自）\",\"value\":\"281\",\"weight\":100},{\"children\":[],\"id\":26,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"职位增加（自）\",\"value\":\"26\",\"weight\":100},{\"children\":[],\"id\":282,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"库存余额新增（自）\",\"value\":\"282\",\"weight\":100},{\"children\":[],\"id\":27,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"职位编辑（自）\",\"value\":\"27\",\"weight\":100},{\"children\":[],\"id\":283,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"工艺路线查看（自）\",\"value\":\"283\",\"weight\":100},{\"children\":[],\"id\":28,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"职位删除（自）\",\"value\":\"28\",\"weight\":100},{\"children\":[],\"id\":284,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"工单表查询（自）\",\"value\":\"284\",\"weight\":100},{\"children\":[],\"id\":29,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"职位详情（自）\",\"value\":\"29\",\"weight\":100},{\"children\":[],\"id\":285,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"销售订单列表（自）\",\"value\":\"285\",\"weight\":100},{\"children\":[],\"id\":30,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"应用查询（自）\",\"value\":\"30\",\"weight\":100},{\"children\":[],\"id\":286,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"客户联系人导出（自）\",\"value\":\"286\",\"weight\":100},{\"children\":[],\"id\":31,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"应用列表（自）\",\"value\":\"31\",\"weight\":100},{\"children\":[],\"id\":287,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"产品表查询（自）\",\"value\":\"287\",\"weight\":100},{\"children\":[],\"id\":32,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"应用增加（自）\",\"value\":\"32\",\"weight\":100},{\"children\":[],\"id\":288,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"工序路线关系表新增（自）\",\"value\":\"288\",\"weight\":100},{\"children\":[],\"id\":33,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"应用编辑（自）\",\"value\":\"33\",\"weight\":100},{\"children\":[],\"id\":289,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\" 供应商资料导出（自）\",\"value\":\"289\",\"weight\":100},{\"children\":[],\"id\":34,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"应用删除（自）\",\"value\":\"34\",\"weight\":100},{\"children\":[],\"id\":290,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"入库单查询（自）\",\"value\":\"290\",\"weight\":100},{\"children\":[],\"id\":35,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"应用详情（自）\",\"value\":\"35\",\"weight\":100},{\"children\":[],\"id\":291,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"出库单编辑（自）\",\"value\":\"291\",\"weight\":100},{\"children\":[],\"id\":36,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"设为默认应用（自）\",\"value\":\"36\",\"weight\":100},{\"children\":[],\"id\":292,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"任务删除（自）\",\"value\":\"292\",\"weight\":100},{\"children\":[],\"id\":37,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"菜单列表（自）\",\"value\":\"37\",\"weight\":100},{\"children\":[],\"id\":293,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"出库单导出（自）\",\"value\":\"293\",\"weight\":100},{\"children\":[],\"id\":38,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"菜单增加（自）\",\"value\":\"38\",\"weight\":100},{\"children\":[],\"id\":294,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"采购订单查看（自）\",\"value\":\"294\",\"weight\":100},{\"children\":[],\"id\":39,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"菜单编辑（自）\",\"value\":\"39\",\"weight\":100},{\"children\":[],\"id\":295,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"工单表查看（自）\",\"value\":\"295\",\"weight\":100},{\"children\":[],\"id\":40,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"菜单删除（自）\",\"value\":\"40\",\"weight\":100},{\"children\":[],\"id\":296,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"采购细明新增（自）\",\"value\":\"296\",\"weight\":100},{\"children\":[],\"id\":41,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"菜单详情（自）\",\"value\":\"41\",\"weight\":100},{\"children\":[],\"id\":297,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"工单任务关系表查看（自）\",\"value\":\"297\",\"weight\":100},{\"children\":[],\"id\":42,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"菜单授权树（自）\",\"value\":\"42\",\"weight\":100},{\"children\":[],\"id\":298,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"报工导出（自）\",\"value\":\"298\",\"weight\":100},{\"children\":[],\"id\":43,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"菜单树（自）\",\"value\":\"43\",\"weight\":100},{\"children\":[],\"id\":299,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"报工查看（自）\",\"value\":\"299\",\"weight\":100},{\"children\":[],\"id\":44,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"菜单切换（自）\",\"value\":\"44\",\"weight\":100},{\"children\":[],\"id\":300,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"采购细明列表（自）\",\"value\":\"300\",\"weight\":100},{\"children\":[],\"id\":45,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"角色查询（自）\",\"value\":\"45\",\"weight\":100},{\"children\":[],\"id\":301,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"销售订单导出（自）\",\"value\":\"301\",\"weight\":100},{\"children\":[],\"id\":46,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"角色增加（自）\",\"value\":\"46\",\"weight\":100},{\"children\":[],\"id\":302,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"客户资料查看（自）\",\"value\":\"302\",\"weight\":100},{\"children\":[],\"id\":47,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"角色编辑（自）\",\"value\":\"47\",\"weight\":100},{\"children\":[],\"id\":303,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"自定义编号规则编辑（自）\",\"value\":\"303\",\"weight\":100},{\"children\":[],\"id\":48,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"角色删除（自）\",\"value\":\"48\",\"weight\":100},{\"children\":[],\"id\":304,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"入库单查看（自）\",\"value\":\"304\",\"weight\":100},{\"children\":[],\"id\":49,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"角色详情（自）\",\"value\":\"49\",\"weight\":100},{\"children\":[],\"id\":305,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"字段配置列表（自）\",\"value\":\"305\",\"weight\":100},{\"children\":[],\"id\":50,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"角色下拉（自）\",\"value\":\"50\",\"weight\":100},{\"children\":[],\"id\":306,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"报工删除（自）\",\"value\":\"306\",\"weight\":100},{\"children\":[],\"id\":51,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"角色授权菜单（自）\",\"value\":\"51\",\"weight\":100},{\"children\":[],\"id\":307,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"工单任务关系表新增（自）\",\"value\":\"307\",\"weight\":100},{\"children\":[],\"id\":52,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"角色拥有菜单（自）\",\"value\":\"52\",\"weight\":100},{\"children\":[],\"id\":308,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"报工查询（自）\",\"value\":\"308\",\"weight\":100},{\"children\":[],\"id\":53,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"角色授权数据（自）\",\"value\":\"53\",\"weight\":100},{\"children\":[],\"id\":309,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"产品类型表删除（自）\",\"value\":\"309\",\"weight\":100},{\"children\":[],\"id\":54,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"角色拥有数据（自）\",\"value\":\"54\",\"weight\":100},{\"children\":[],\"id\":55,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"配置查询（自）\",\"value\":\"55\",\"weight\":100},{\"children\":[],\"id\":56,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"配置列表（自）\",\"value\":\"56\",\"weight\":100},{\"children\":[],\"id\":57,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"配置增加（自）\",\"value\":\"57\",\"weight\":100},{\"children\":[],\"id\":58,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"配置编辑（自）\",\"value\":\"58\",\"weight\":100},{\"children\":[],\"id\":59,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"配置删除（自）\",\"value\":\"59\",\"weight\":100},{\"children\":[],\"id\":60,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"配置详情（自）\",\"value\":\"60\",\"weight\":100},{\"children\":[],\"id\":61,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"发送文本邮件（自）\",\"value\":\"61\",\"weight\":100},{\"children\":[],\"id\":62,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"发送html邮件（自）\",\"value\":\"62\",\"weight\":100},{\"children\":[],\"id\":63,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"短信发送查询（自）\",\"value\":\"63\",\"weight\":100},{\"children\":[],\"id\":64,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"发送验证码短信（自）\",\"value\":\"64\",\"weight\":100},{\"children\":[],\"id\":65,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"验证短信验证码（自）\",\"value\":\"65\",\"weight\":100},{\"children\":[],\"id\":66,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"字典类型查询（自）\",\"value\":\"66\",\"weight\":100},{\"children\":[],\"id\":67,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"字典类型列表（自）\",\"value\":\"67\",\"weight\":100},{\"children\":[],\"id\":68,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"字典类型增加（自）\",\"value\":\"68\",\"weight\":100},{\"children\":[],\"id\":69,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"字典类型删除（自）\",\"value\":\"69\",\"weight\":100},{\"children\":[],\"id\":70,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"字典类型编辑（自）\",\"value\":\"70\",\"weight\":100},{\"children\":[],\"id\":71,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"字典类型详情（自）\",\"value\":\"71\",\"weight\":100},{\"children\":[],\"id\":72,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"字典类型下拉（自）\",\"value\":\"72\",\"weight\":100},{\"children\":[],\"id\":73,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"字典类型修改状态（自）\",\"value\":\"73\",\"weight\":100},{\"children\":[],\"id\":74,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"字典值查询（自）\",\"value\":\"74\",\"weight\":100},{\"children\":[],\"id\":75,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"字典值列表（自）\",\"value\":\"75\",\"weight\":100},{\"children\":[],\"id\":76,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"字典值增加（自）\",\"value\":\"76\",\"weight\":100},{\"children\":[],\"id\":77,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"字典值删除（自）\",\"value\":\"77\",\"weight\":100},{\"children\":[],\"id\":78,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"字典值编辑（自）\",\"value\":\"78\",\"weight\":100},{\"children\":[],\"id\":79,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"字典值详情（自）\",\"value\":\"79\",\"weight\":100},{\"children\":[],\"id\":80,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"字典值修改状态（自）\",\"value\":\"80\",\"weight\":100},{\"children\":[],\"id\":81,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"访问日志查询（自）\",\"value\":\"81\",\"weight\":100},{\"children\":[],\"id\":82,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"访问日志清空（自）\",\"value\":\"82\",\"weight\":100},{\"children\":[],\"id\":83,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"操作日志查询（自）\",\"value\":\"83\",\"weight\":100},{\"children\":[],\"id\":84,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"操作日志清空（自）\",\"value\":\"84\",\"weight\":100},{\"children\":[],\"id\":85,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"服务监控查询（自）\",\"value\":\"85\",\"weight\":100},{\"children\":[],\"id\":86,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"在线用户列表（自）\",\"value\":\"86\",\"weight\":100},{\"children\":[],\"id\":87,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"在线用户强退（自）\",\"value\":\"87\",\"weight\":100},{\"children\":[],\"id\":88,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"公告查询（自）\",\"value\":\"88\",\"weight\":100},{\"children\":[],\"id\":89,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"公告增加（自）\",\"value\":\"89\",\"weight\":100},{\"children\":[],\"id\":90,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"公告编辑（自）\",\"value\":\"90\",\"weight\":100},{\"children\":[],\"id\":91,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"公告删除（自）\",\"value\":\"91\",\"weight\":100},{\"children\":[],\"id\":92,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"公告查看（自）\",\"value\":\"92\",\"weight\":100},{\"children\":[],\"id\":93,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"公告修改状态（自）\",\"value\":\"93\",\"weight\":100},{\"children\":[],\"id\":94,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"已收公告查询（自）\",\"value\":\"94\",\"weight\":100},{\"children\":[],\"id\":95,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"文件查询（自）\",\"value\":\"95\",\"weight\":100},{\"children\":[],\"id\":96,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"文件列表（自）\",\"value\":\"96\",\"weight\":100},{\"children\":[],\"id\":97,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"文件删除（自）\",\"value\":\"97\",\"weight\":100},{\"children\":[],\"id\":98,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"文件详情（自）\",\"value\":\"98\",\"weight\":100},{\"children\":[],\"id\":99,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"文件上传（自）\",\"value\":\"99\",\"weight\":100},{\"children\":[],\"id\":100,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"文件下载（自）\",\"value\":\"100\",\"weight\":100},{\"children\":[],\"id\":101,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"图片预览（自）\",\"value\":\"101\",\"weight\":100},{\"children\":[],\"id\":102,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"定时任务查询（自）\",\"value\":\"102\",\"weight\":100},{\"children\":[],\"id\":103,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"定时任务列表（自）\",\"value\":\"103\",\"weight\":100},{\"children\":[],\"id\":104,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"定时任务详情（自）\",\"value\":\"104\",\"weight\":100},{\"children\":[],\"id\":105,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"定时任务增加（自）\",\"value\":\"105\",\"weight\":100},{\"children\":[],\"id\":106,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"定时任务删除（自）\",\"value\":\"106\",\"weight\":100},{\"children\":[],\"id\":107,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"定时任务编辑（自）\",\"value\":\"107\",\"weight\":100},{\"children\":[],\"id\":108,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"定时任务可执行列表（自）\",\"value\":\"108\",\"weight\":100},{\"children\":[],\"id\":109,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"定时任务启动（自）\",\"value\":\"109\",\"weight\":100},{\"children\":[],\"id\":110,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"定时任务关闭（自）\",\"value\":\"110\",\"weight\":100},{\"children\":[],\"id\":111,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"系统区域列表（自）\",\"value\":\"111\",\"weight\":100},{\"children\":[],\"id\":112,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"报工审批（自）\",\"value\":\"112\",\"weight\":100},{\"children\":[],\"id\":113,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"产品类型树（自）\",\"value\":\"113\",\"weight\":100},{\"children\":[],\"id\":114,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"字段配置新增（自）\",\"value\":\"114\",\"weight\":100},{\"children\":[],\"id\":115,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"客户分类查询（自）\",\"value\":\"115\",\"weight\":100},{\"children\":[],\"id\":116,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"供应商分类新增（自）\",\"value\":\"116\",\"weight\":100},{\"children\":[],\"id\":117,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"采购细明导出（自）\",\"value\":\"117\",\"weight\":100},{\"children\":[],\"id\":118,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"物料清单编辑（自）\",\"value\":\"118\",\"weight\":100},{\"children\":[],\"id\":119,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"生产计划编辑（自）\",\"value\":\"119\",\"weight\":100},{\"children\":[],\"id\":120,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"自定义编号规则查询（自）\",\"value\":\"120\",\"weight\":100},{\"children\":[],\"id\":121,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"工单表新增（自）\",\"value\":\"121\",\"weight\":100},{\"children\":[],\"id\":122,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"产品表列表（自）\",\"value\":\"122\",\"weight\":100},{\"children\":[],\"id\":123,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"采购细明删除（自）\",\"value\":\"123\",\"weight\":100},{\"children\":[],\"id\":124,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"物料清单列表（自）\",\"value\":\"124\",\"weight\":100},{\"children\":[],\"id\":125,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"客户分类导出（自）\",\"value\":\"125\",\"weight\":100},{\"children\":[],\"id\":126,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"出库单列表（自）\",\"value\":\"126\",\"weight\":100},{\"children\":[],\"id\":127,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"出入库明细新增（自）\",\"value\":\"127\",\"weight\":100},{\"children\":[],\"id\":128,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"任务列表（自）\",\"value\":\"128\",\"weight\":100},{\"children\":[],\"id\":129,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"生产计划导出（自）\",\"value\":\"129\",\"weight\":100},{\"children\":[],\"id\":130,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"供应商联系人查询（自）\",\"value\":\"130\",\"weight\":100},{\"children\":[],\"id\":131,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"工艺路线列表（自）\",\"value\":\"131\",\"weight\":100},{\"children\":[],\"id\":132,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"SaaS租户删除（自）\",\"value\":\"132\",\"weight\":100},{\"children\":[],\"id\":133,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"产品类型表列表（自）\",\"value\":\"133\",\"weight\":100},{\"children\":[],\"id\":134,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"采购细明查询（自）\",\"value\":\"134\",\"weight\":100},{\"children\":[],\"id\":135,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\" 供应商资料查询（自）\",\"value\":\"135\",\"weight\":100},{\"children\":[],\"id\":136,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"供应商联系人新增（自）\",\"value\":\"136\",\"weight\":100},{\"children\":[],\"id\":137,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"生产计划新增（自）\",\"value\":\"137\",\"weight\":100},{\"children\":[],\"id\":138,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"工艺路线查询（自）\",\"value\":\"138\",\"weight\":100},{\"children\":[],\"id\":139,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"采购订单删除（自）\",\"value\":\"139\",\"weight\":100},{\"children\":[],\"id\":140,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"客户资料新增（自）\",\"value\":\"140\",\"weight\":100},{\"children\":[],\"id\":141,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"产品表编辑（自）\",\"value\":\"141\",\"weight\":100},{\"children\":[],\"id\":142,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"工单任务关系表删除（自）\",\"value\":\"142\",\"weight\":100},{\"children\":[],\"id\":143,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"客户联系人查看（自）\",\"value\":\"143\",\"weight\":100},{\"children\":[],\"id\":144,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"报工编辑（自）\",\"value\":\"144\",\"weight\":100},{\"children\":[],\"id\":145,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"采购订单编辑（自）\",\"value\":\"145\",\"weight\":100},{\"children\":[],\"id\":146,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"客户资料删除（自）\",\"value\":\"146\",\"weight\":100},{\"children\":[],\"id\":147,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"销售订单删除（自）\",\"value\":\"147\",\"weight\":100},{\"children\":[],\"id\":148,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"库存余额导出（自）\",\"value\":\"148\",\"weight\":100},{\"children\":[],\"id\":149,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"供应商联系人查看（自）\",\"value\":\"149\",\"weight\":100},{\"children\":[],\"id\":150,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"库存余额查询（自）\",\"value\":\"150\",\"weight\":100},{\"children\":[],\"id\":151,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"自定义编号规则新增（自）\",\"value\":\"151\",\"weight\":100},{\"children\":[],\"id\":152,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"生产计划查询（自）\",\"value\":\"152\",\"weight\":100},{\"children\":[],\"id\":153,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"产品类型表导出（自）\",\"value\":\"153\",\"weight\":100},{\"children\":[],\"id\":154,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"仓库管理查询（自）\",\"value\":\"154\",\"weight\":100},{\"children\":[],\"id\":155,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"客户联系人删除（自）\",\"value\":\"155\",\"weight\":100},{\"children\":[],\"id\":156,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"入库单导出（自）\",\"value\":\"156\",\"weight\":100},{\"children\":[],\"id\":157,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"任务编辑（自）\",\"value\":\"157\",\"weight\":100},{\"children\":[],\"id\":158,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"自定义编号规则删除（自）\",\"value\":\"158\",\"weight\":100},{\"children\":[],\"id\":159,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"工单任务关系表查询（自）\",\"value\":\"159\",\"weight\":100},{\"children\":[],\"id\":160,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"工序新增（自）\",\"value\":\"160\",\"weight\":100},{\"children\":[],\"id\":161,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"仓库管理导出（自）\",\"value\":\"161\",\"weight\":100},{\"children\":[],\"id\":162,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"报工列表（自）\",\"value\":\"162\",\"weight\":100},{\"children\":[],\"id\":163,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"工艺路线编辑（自）\",\"value\":\"163\",\"weight\":100},{\"children\":[],\"id\":164,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"仓库管理新增（自）\",\"value\":\"164\",\"weight\":100},{\"children\":[],\"id\":165,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"出入库明细查询（自）\",\"value\":\"165\",\"weight\":100},{\"children\":[],\"id\":166,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"工序路线关系表查询（自）\",\"value\":\"166\",\"weight\":100},{\"children\":[],\"id\":167,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"字段配置导出（自）\",\"value\":\"167\",\"weight\":100},{\"children\":[],\"id\":168,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"客户资料列表（自）\",\"value\":\"168\",\"weight\":100},{\"children\":[],\"id\":169,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"工单任务关系表编辑（自）\",\"value\":\"169\",\"weight\":100},{\"children\":[],\"id\":170,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"字段配置编辑（自）\",\"value\":\"170\",\"weight\":100},{\"children\":[],\"id\":171,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"出库单删除（自）\",\"value\":\"171\",\"weight\":100},{\"children\":[],\"id\":172,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"仓库管理列表（自）\",\"value\":\"172\",\"weight\":100},{\"children\":[],\"id\":173,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"采购订单列表（自）\",\"value\":\"173\",\"weight\":100},{\"children\":[],\"id\":174,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"物料清单导出（自）\",\"value\":\"174\",\"weight\":100},{\"children\":[],\"id\":175,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"工单任务关系表导出（自）\",\"value\":\"175\",\"weight\":100},{\"children\":[],\"id\":176,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"SaaS租户导出（自）\",\"value\":\"176\",\"weight\":100},{\"children\":[],\"id\":177,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"销售订单新增（自）\",\"value\":\"177\",\"weight\":100},{\"children\":[],\"id\":178,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"采购订单新增（自）\",\"value\":\"178\",\"weight\":100},{\"children\":[],\"id\":179,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"工序路线关系表列表（自）\",\"value\":\"179\",\"weight\":100},{\"children\":[],\"id\":180,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"工序路线关系表删除（自）\",\"value\":\"180\",\"weight\":100},{\"children\":[],\"id\":181,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"工单表编辑（自）\",\"value\":\"181\",\"weight\":100},{\"children\":[],\"id\":182,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\" 供应商资料列表（自）\",\"value\":\"182\",\"weight\":100},{\"children\":[],\"id\":183,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"工艺路线删除（自）\",\"value\":\"183\",\"weight\":100},{\"children\":[],\"id\":184,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"物料清单新增（自）\",\"value\":\"184\",\"weight\":100},{\"children\":[],\"id\":185,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"客户联系人编辑（自）\",\"value\":\"185\",\"weight\":100},{\"children\":[],\"id\":186,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"工序删除（自）\",\"value\":\"186\",\"weight\":100},{\"children\":[],\"id\":187,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"销售订单编辑（自）\",\"value\":\"187\",\"weight\":100},{\"children\":[],\"id\":188,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"入库单列表（自）\",\"value\":\"188\",\"weight\":100},{\"children\":[],\"id\":189,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"出入库明细导出（自）\",\"value\":\"189\",\"weight\":100},{\"children\":[],\"id\":190,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"任务查看（自）\",\"value\":\"190\",\"weight\":100},{\"children\":[],\"id\":191,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"出库单新增（自）\",\"value\":\"191\",\"weight\":100},{\"children\":[],\"id\":192,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"工单任务关系表列表（自）\",\"value\":\"192\",\"weight\":100},{\"children\":[],\"id\":193,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"出入库明细查看（自）\",\"value\":\"193\",\"weight\":100},{\"children\":[],\"id\":194,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"销售订单查看（自）\",\"value\":\"194\",\"weight\":100},{\"children\":[],\"id\":195,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"工序路线关系表导出（自）\",\"value\":\"195\",\"weight\":100},{\"children\":[],\"id\":196,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"采购订单查询（自）\",\"value\":\"196\",\"weight\":100},{\"children\":[],\"id\":197,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"客户分类删除（自）\",\"value\":\"197\",\"weight\":100},{\"children\":[],\"id\":198,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"工序导出（自）\",\"value\":\"198\",\"weight\":100},{\"children\":[],\"id\":199,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"供应商联系人删除（自）\",\"value\":\"199\",\"weight\":100},{\"children\":[],\"id\":200,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"供应商联系人列表（自）\",\"value\":\"200\",\"weight\":100},{\"children\":[],\"id\":201,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"产品类型表查看（自）\",\"value\":\"201\",\"weight\":100},{\"children\":[],\"id\":202,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"产品类型表新增（自）\",\"value\":\"202\",\"weight\":100},{\"children\":[],\"id\":203,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"物料清单查询（自）\",\"value\":\"203\",\"weight\":100},{\"children\":[],\"id\":204,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"客户资料导出（自）\",\"value\":\"204\",\"weight\":100},{\"children\":[],\"id\":205,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"供应商联系人编辑（自）\",\"value\":\"205\",\"weight\":100},{\"children\":[],\"id\":206,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"客户资料查询（自）\",\"value\":\"206\",\"weight\":100},{\"children\":[],\"id\":207,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"工序查看（自）\",\"value\":\"207\",\"weight\":100},{\"children\":[],\"id\":208,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"仓库管理查看（自）\",\"value\":\"208\",\"weight\":100},{\"children\":[],\"id\":209,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"采购订单导出（自）\",\"value\":\"209\",\"weight\":100},{\"children\":[],\"id\":210,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"产品表查看（自）\",\"value\":\"210\",\"weight\":100},{\"children\":[],\"id\":211,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"工序路线关系表查看（自）\",\"value\":\"211\",\"weight\":100},{\"children\":[],\"id\":212,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"SaaS租户查询（自）\",\"value\":\"212\",\"weight\":100},{\"children\":[],\"id\":213,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"出库单查询（自）\",\"value\":\"213\",\"weight\":100},{\"children\":[],\"id\":214,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"工序查询（自）\",\"value\":\"214\",\"weight\":100},{\"children\":[],\"id\":215,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"供应商分类删除（自）\",\"value\":\"215\",\"weight\":100},{\"children\":[],\"id\":216,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"供应商分类列表（自）\",\"value\":\"216\",\"weight\":100},{\"children\":[],\"id\":217,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"SaaS租户查看（自）\",\"value\":\"217\",\"weight\":100},{\"children\":[],\"id\":218,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"仓库管理编辑（自）\",\"value\":\"218\",\"weight\":100},{\"children\":[],\"id\":219,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"物料清单查看（自）\",\"value\":\"219\",\"weight\":100},{\"children\":[],\"id\":220,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"供应商分类导出（自）\",\"value\":\"220\",\"weight\":100},{\"children\":[],\"id\":221,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"自定义编号规则列表（自）\",\"value\":\"221\",\"weight\":100},{\"children\":[],\"id\":222,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\" 供应商资料编辑（自）\",\"value\":\"222\",\"weight\":100},{\"children\":[],\"id\":223,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"自定义编号规则导出（自）\",\"value\":\"223\",\"weight\":100},{\"children\":[],\"id\":224,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"入库单删除（自）\",\"value\":\"224\",\"weight\":100},{\"children\":[],\"id\":225,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"自定义编号规则查看（自）\",\"value\":\"225\",\"weight\":100},{\"children\":[],\"id\":226,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"报工新增（自）\",\"value\":\"226\",\"weight\":100},{\"children\":[],\"id\":227,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"供应商分类查看（自）\",\"value\":\"227\",\"weight\":100},{\"children\":[],\"id\":228,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"入库单编辑（自）\",\"value\":\"228\",\"weight\":100},{\"children\":[],\"id\":229,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"出入库明细删除（自）\",\"value\":\"229\",\"weight\":100},{\"children\":[],\"id\":230,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\" 供应商资料新增（自）\",\"value\":\"230\",\"weight\":100},{\"children\":[],\"id\":231,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"客户联系人新增（自）\",\"value\":\"231\",\"weight\":100},{\"children\":[],\"id\":232,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"工序列表（自）\",\"value\":\"232\",\"weight\":100},{\"children\":[],\"id\":233,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"出入库明细编辑（自）\",\"value\":\"233\",\"weight\":100},{\"children\":[],\"id\":234,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"字段配置删除（自）\",\"value\":\"234\",\"weight\":100},{\"children\":[],\"id\":235,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"生产计划删除（自）\",\"value\":\"235\",\"weight\":100},{\"children\":[],\"id\":236,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\" 供应商资料查看（自）\",\"value\":\"236\",\"weight\":100},{\"children\":[],\"id\":237,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"销售订单查询（自）\",\"value\":\"237\",\"weight\":100},{\"children\":[],\"id\":238,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"物料清单删除（自）\",\"value\":\"238\",\"weight\":100},{\"children\":[],\"id\":239,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"客户分类新增（自）\",\"value\":\"239\",\"weight\":100},{\"children\":[],\"id\":240,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"产品类型表查询（自）\",\"value\":\"240\",\"weight\":100},{\"children\":[],\"id\":241,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"出库单查看（自）\",\"value\":\"241\",\"weight\":100},{\"children\":[],\"id\":242,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"客户分类编辑（自）\",\"value\":\"242\",\"weight\":100},{\"children\":[],\"id\":243,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"工单表导出（自）\",\"value\":\"243\",\"weight\":100},{\"children\":[],\"id\":244,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"任务开始\\r\\n（自）\",\"value\":\"244\",\"weight\":100},{\"children\":[],\"id\":245,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"字段配置查看（自）\",\"value\":\"245\",\"weight\":100},{\"children\":[],\"id\":246,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"出入库明细列表（自）\",\"value\":\"246\",\"weight\":100},{\"children\":[],\"id\":247,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"客户联系人查询（自）\",\"value\":\"247\",\"weight\":100},{\"children\":[],\"id\":248,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"库存余额查看（自）\",\"value\":\"248\",\"weight\":100},{\"children\":[],\"id\":249,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"库存余额删除（自）\",\"value\":\"249\",\"weight\":100},{\"children\":[],\"id\":250,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"入库单新增（自）\",\"value\":\"250\",\"weight\":100},{\"children\":[],\"id\":251,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"采购细明编辑（自）\",\"value\":\"251\",\"weight\":100},{\"children\":[],\"id\":252,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"任务查询（自）\",\"value\":\"252\",\"weight\":100},{\"children\":[],\"id\":253,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"客户资料编辑（自）\",\"value\":\"253\",\"weight\":100},{\"children\":[],\"id\":254,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"采购细明查看（自）\",\"value\":\"254\",\"weight\":100},{\"children\":[],\"id\":255,\"parentId\":1560145190851772417,\"pid\":1560145190851772417,\"title\":\"工艺路线导出（自）\",\"value\":\"255\",\"weight\":100}],\"id\":1560145190851772417,\"parentId\":0,\"pid\":0,\"title\":\"接口管理\",\"value\":\"1560145190851772417\",\"weight\":9},{\"children\":[{\"children\":[{\"children\":[],\"id\":6449912099524079616,\"parentId\":4998978684524443692,\"pid\":4998978684524443692,\"title\":\"工序导出\",\"value\":\"6449912099524079616\",\"weight\":100},{\"children\":[],\"id\":5707993274387362828,\"parentId\":4998978684524443692,\"pid\":4998978684524443692,\"title\":\"工序新增\",\"value\":\"5707993274387362828\",\"weight\":100},{\"children\":[],\"id\":8275137972166495794,\"parentId\":4998978684524443692,\"pid\":4998978684524443692,\"title\":\"工序编辑\",\"value\":\"8275137972166495794\",\"weight\":100},{\"children\":[],\"id\":6249597959063106635,\"parentId\":4998978684524443692,\"pid\":4998978684524443692,\"title\":\"工序删除\",\"value\":\"6249597959063106635\",\"weight\":100},{\"children\":[],\"id\":6905244625723471705,\"parentId\":4998978684524443692,\"pid\":4998978684524443692,\"title\":\"工序查询\",\"value\":\"6905244625723471705\",\"weight\":100},{\"children\":[],\"id\":6709768906758836391,\"parentId\":4998978684524443692,\"pid\":4998978684524443692,\"title\":\"工序查看\",\"value\":\"6709768906758836391\",\"weight\":100},{\"children\":[],\"id\":7388361180917918171,\"parentId\":4998978684524443692,\"pid\":4998978684524443692,\"title\":\"工序列表\",\"value\":\"7388361180917918171\",\"weight\":100}],\"id\":4998978684524443692,\"parentId\":1534384317233524738,\"pid\":1534384317233524738,\"title\":\"工序\",\"value\":\"4998978684524443692\",\"weight\":1},{\"children\":[{\"children\":[],\"id\":5243613315131080710,\"parentId\":7702400310012060990,\"pid\":7702400310012060990,\"title\":\"工艺路线查询\",\"value\":\"5243613315131080710\",\"weight\":100},{\"children\":[],\"id\":5079108238558434584,\"parentId\":7702400310012060990,\"pid\":7702400310012060990,\"title\":\"工艺路线列表\",\"value\":\"5079108238558434584\",\"weight\":100},{\"children\":[],\"id\":8562547749529140265,\"parentId\":7702400310012060990,\"pid\":7702400310012060990,\"title\":\"工艺路线查看\",\"value\":\"8562547749529140265\",\"weight\":100},{\"children\":[],\"id\":5788080164048630938,\"parentId\":7702400310012060990,\"pid\":7702400310012060990,\"title\":\"工艺路线编辑\",\"value\":\"5788080164048630938\",\"weight\":100},{\"children\":[],\"id\":7936411315868128942,\"parentId\":7702400310012060990,\"pid\":7702400310012060990,\"title\":\"工艺路线导出\",\"value\":\"7936411315868128942\",\"weight\":100},{\"children\":[],\"id\":7940657177277675482,\"parentId\":7702400310012060990,\"pid\":7702400310012060990,\"title\":\"工艺路线新增\",\"value\":\"7940657177277675482\",\"weight\":100},{\"children\":[],\"id\":6194217070047486945,\"parentId\":7702400310012060990,\"pid\":7702400310012060990,\"title\":\"工艺路线删除\",\"value\":\"6194217070047486945\",\"weight\":100}],\"id\":7702400310012060990,\"parentId\":1534384317233524738,\"pid\":1534384317233524738,\"title\":\"工艺路线\",\"value\":\"7702400310012060990\",\"weight\":2},{\"children\":[{\"children\":[],\"id\":1551480636561321985,\"parentId\":6527167616560933135,\"pid\":6527167616560933135,\"title\":\"产品类型树\",\"value\":\"1551480636561321985\",\"weight\":100},{\"children\":[],\"id\":6570253563256899359,\"parentId\":6527167616560933135,\"pid\":6527167616560933135,\"title\":\"产品类型表新增\",\"value\":\"6570253563256899359\",\"weight\":100},{\"children\":[],\"id\":6547301851685353031,\"parentId\":6527167616560933135,\"pid\":6527167616560933135,\"title\":\"产品类型表查看\",\"value\":\"6547301851685353031\",\"weight\":100},{\"children\":[],\"id\":8197158197771689059,\"parentId\":6527167616560933135,\"pid\":6527167616560933135,\"title\":\"产品类型表编辑\",\"value\":\"8197158197771689059\",\"weight\":100},{\"children\":[],\"id\":9222389351831596218,\"parentId\":6527167616560933135,\"pid\":6527167616560933135,\"title\":\"产品类型表删除\",\"value\":\"9222389351831596218\",\"weight\":100},{\"children\":[],\"id\":5142735407151148234,\"parentId\":6527167616560933135,\"pid\":6527167616560933135,\"title\":\"产品类型表列表\",\"value\":\"5142735407151148234\",\"weight\":100},{\"children\":[],\"id\":7621956982866257638,\"parentId\":6527167616560933135,\"pid\":6527167616560933135,\"title\":\"产品类型表查询\",\"value\":\"7621956982866257638\",\"weight\":100},{\"children\":[],\"id\":5553078347948343545,\"parentId\":6527167616560933135,\"pid\":6527167616560933135,\"title\":\"产品类型表导出\",\"value\":\"5553078347948343545\",\"weight\":100}],\"id\":6527167616560933135,\"parentId\":1534384317233524738,\"pid\":1534384317233524738,\"title\":\"产品类型\",\"value\":\"6527167616560933135\",\"weight\":3},{\"children\":[{\"children\":[],\"id\":8315301036989682205,\"parentId\":6436044181834026359,\"pid\":6436044181834026359,\"title\":\"产品表导出\",\"value\":\"8315301036989682205\",\"weight\":100},{\"children\":[],\"id\":8618504262472492350,\"parentId\":6436044181834026359,\"pid\":6436044181834026359,\"title\":\"产品表查询\",\"value\":\"8618504262472492350\",\"weight\":100},{\"children\":[],\"id\":7950819887425681478,\"parentId\":6436044181834026359,\"pid\":6436044181834026359,\"title\":\"产品表删除\",\"value\":\"7950819887425681478\",\"weight\":100},{\"children\":[],\"id\":4832107133629024589,\"parentId\":6436044181834026359,\"pid\":6436044181834026359,\"title\":\"产品表列表\",\"value\":\"4832107133629024589\",\"weight\":100},{\"children\":[],\"id\":7946795106595000695,\"parentId\":6436044181834026359,\"pid\":6436044181834026359,\"title\":\"产品表新增\",\"value\":\"7946795106595000695\",\"weight\":100},{\"children\":[],\"id\":5279583233581044419,\"parentId\":6436044181834026359,\"pid\":6436044181834026359,\"title\":\"产品表编辑\",\"value\":\"5279583233581044419\",\"weight\":100},{\"children\":[],\"id\":6845666717193712873,\"parentId\":6436044181834026359,\"pid\":6436044181834026359,\"title\":\"产品表查看\",\"value\":\"6845666717193712873\",\"weight\":100}],\"id\":6436044181834026359,\"parentId\":1534384317233524738,\"pid\":1534384317233524738,\"title\":\"产品管理\",\"value\":\"6436044181834026359\",\"weight\":4},{\"children\":[{\"children\":[],\"id\":6103813152661113624,\"parentId\":8426505795261326687,\"pid\":8426505795261326687,\"title\":\"工序路线关系表列表\",\"value\":\"6103813152661113624\",\"weight\":100},{\"children\":[],\"id\":6374585520258378360,\"parentId\":8426505795261326687,\"pid\":8426505795261326687,\"title\":\"工序路线关系表导出\",\"value\":\"6374585520258378360\",\"weight\":100},{\"children\":[],\"id\":8621765388717143178,\"parentId\":8426505795261326687,\"pid\":8426505795261326687,\"title\":\"工序路线关系表新增\",\"value\":\"8621765388717143178\",\"weight\":100},{\"children\":[],\"id\":6116805160244737678,\"parentId\":8426505795261326687,\"pid\":8426505795261326687,\"title\":\"工序路线关系表删除\",\"value\":\"6116805160244737678\",\"weight\":100},{\"children\":[],\"id\":6860312372874164125,\"parentId\":8426505795261326687,\"pid\":8426505795261326687,\"title\":\"工序路线关系表查看\",\"value\":\"6860312372874164125\",\"weight\":100},{\"children\":[],\"id\":5820472386138045917,\"parentId\":8426505795261326687,\"pid\":8426505795261326687,\"title\":\"工序路线关系表查询\",\"value\":\"5820472386138045917\",\"weight\":100},{\"children\":[],\"id\":8181822590852115190,\"parentId\":8426505795261326687,\"pid\":8426505795261326687,\"title\":\"工序路线关系表编辑\",\"value\":\"8181822590852115190\",\"weight\":100}],\"id\":8426505795261326687,\"parentId\":1534384317233524738,\"pid\":1534384317233524738,\"title\":\"工序路线关系\",\"value\":\"8426505795261326687\",\"weight\":6},{\"children\":[{\"children\":[],\"id\":6204862670753579307,\"parentId\":7128085356547940830,\"pid\":7128085356547940830,\"title\":\"物料清单新增\",\"value\":\"6204862670753579307\",\"weight\":100},{\"children\":[],\"id\":4835110481928644952,\"parentId\":7128085356547940830,\"pid\":7128085356547940830,\"title\":\"物料清单列表\",\"value\":\"4835110481928644952\",\"weight\":100},{\"children\":[],\"id\":7047324015553475476,\"parentId\":7128085356547940830,\"pid\":7128085356547940830,\"title\":\"物料清单查看\",\"value\":\"7047324015553475476\",\"weight\":100},{\"children\":[],\"id\":7581916369308597479,\"parentId\":7128085356547940830,\"pid\":7128085356547940830,\"title\":\"物料清单删除\",\"value\":\"7581916369308597479\",\"weight\":100},{\"children\":[],\"id\":6591367648784009715,\"parentId\":7128085356547940830,\"pid\":7128085356547940830,\"title\":\"物料清单查询\",\"value\":\"6591367648784009715\",\"weight\":100},{\"children\":[],\"id\":6015462925549607928,\"parentId\":7128085356547940830,\"pid\":7128085356547940830,\"title\":\"物料清单导出\",\"value\":\"6015462925549607928\",\"weight\":100},{\"children\":[],\"id\":4719172401435180793,\"parentId\":7128085356547940830,\"pid\":7128085356547940830,\"title\":\"物料清单编辑\",\"value\":\"4719172401435180793\",\"weight\":100}],\"id\":7128085356547940830,\"parentId\":1534384317233524738,\"pid\":1534384317233524738,\"title\":\"物料清单\",\"value\":\"7128085356547940830\",\"weight\":100}],\"id\":1534384317233524738,\"parentId\":0,\"pid\":0,\"title\":\"基础数据\",\"value\":\"1534384317233524738\",\"weight\":11},{\"children\":[{\"children\":[{\"children\":[],\"id\":5206868747918117433,\"parentId\":8477499608364957015,\"pid\":8477499608364957015,\"title\":\"生产计划新增\",\"value\":\"5206868747918117433\",\"weight\":100},{\"children\":[],\"id\":4757443910107701590,\"parentId\":8477499608364957015,\"pid\":8477499608364957015,\"title\":\"生产计划编辑\",\"value\":\"4757443910107701590\",\"weight\":100},{\"children\":[],\"id\":5016248121790626902,\"parentId\":8477499608364957015,\"pid\":8477499608364957015,\"title\":\"生产计划导出\",\"value\":\"5016248121790626902\",\"weight\":100},{\"children\":[],\"id\":7516608184768822750,\"parentId\":8477499608364957015,\"pid\":8477499608364957015,\"title\":\"生产计划删除\",\"value\":\"7516608184768822750\",\"weight\":100},{\"children\":[],\"id\":8293588485966810596,\"parentId\":8477499608364957015,\"pid\":8477499608364957015,\"title\":\"生产计划查看\",\"value\":\"8293588485966810596\",\"weight\":100},{\"children\":[],\"id\":5523506981641034218,\"parentId\":8477499608364957015,\"pid\":8477499608364957015,\"title\":\"生产计划查询\",\"value\":\"5523506981641034218\",\"weight\":100},{\"children\":[],\"id\":8494981115107347954,\"parentId\":8477499608364957015,\"pid\":8477499608364957015,\"title\":\"生产计划列表\",\"value\":\"8494981115107347954\",\"weight\":100}],\"id\":8477499608364957015,\"parentId\":1534410297847214081,\"pid\":1534410297847214081,\"title\":\"生产计划\",\"value\":\"8477499608364957015\",\"weight\":1},{\"children\":[{\"children\":[],\"id\":6045763991836034103,\"parentId\":5934688735467584877,\"pid\":5934688735467584877,\"title\":\"销售订单新增\",\"value\":\"6045763991836034103\",\"weight\":100},{\"children\":[],\"id\":7561922286979539034,\"parentId\":5934688735467584877,\"pid\":5934688735467584877,\"title\":\"销售订单查询\",\"value\":\"7561922286979539034\",\"weight\":100},{\"children\":[],\"id\":5457126757845470309,\"parentId\":5934688735467584877,\"pid\":5934688735467584877,\"title\":\"销售订单删除\",\"value\":\"5457126757845470309\",\"weight\":100},{\"children\":[],\"id\":8590002636006568603,\"parentId\":5934688735467584877,\"pid\":5934688735467584877,\"title\":\"销售订单列表\",\"value\":\"8590002636006568603\",\"weight\":100},{\"children\":[],\"id\":6257208246505535137,\"parentId\":5934688735467584877,\"pid\":5934688735467584877,\"title\":\"销售订单编辑\",\"value\":\"6257208246505535137\",\"weight\":100},{\"children\":[],\"id\":9118159149629415380,\"parentId\":5934688735467584877,\"pid\":5934688735467584877,\"title\":\"销售订单导出\",\"value\":\"9118159149629415380\",\"weight\":100},{\"children\":[],\"id\":6354039257511829492,\"parentId\":5934688735467584877,\"pid\":5934688735467584877,\"title\":\"销售订单查看\",\"value\":\"6354039257511829492\",\"weight\":100}],\"id\":5934688735467584877,\"parentId\":1534410297847214081,\"pid\":1534410297847214081,\"title\":\"销售订单\",\"value\":\"5934688735467584877\",\"weight\":2},{\"children\":[{\"children\":[],\"id\":6542835870761280820,\"parentId\":5445032970491536505,\"pid\":5445032970491536505,\"title\":\"装配工单查询\",\"value\":\"6542835870761280820\",\"weight\":100},{\"children\":[],\"id\":8191527329183937863,\"parentId\":5445032970491536505,\"pid\":5445032970491536505,\"title\":\"装配工单查看\",\"value\":\"8191527329183937863\",\"weight\":100},{\"children\":[],\"id\":7357393608825227850,\"parentId\":5445032970491536505,\"pid\":5445032970491536505,\"title\":\"装配工单编辑\",\"value\":\"7357393608825227850\",\"weight\":100},{\"children\":[],\"id\":7447629070175932292,\"parentId\":5445032970491536505,\"pid\":5445032970491536505,\"title\":\"装配工单列表\",\"value\":\"7447629070175932292\",\"weight\":100},{\"children\":[],\"id\":5228961670953770885,\"parentId\":5445032970491536505,\"pid\":5445032970491536505,\"title\":\"装配工单新增\",\"value\":\"5228961670953770885\",\"weight\":100},{\"children\":[],\"id\":5234105579337808561,\"parentId\":5445032970491536505,\"pid\":5445032970491536505,\"title\":\"装配工单删除\",\"value\":\"5234105579337808561\",\"weight\":100},{\"children\":[],\"id\":6457085339840740289,\"parentId\":5445032970491536505,\"pid\":5445032970491536505,\"title\":\"装配工单导出\",\"value\":\"6457085339840740289\",\"weight\":100}],\"id\":5445032970491536505,\"parentId\":1534410297847214081,\"pid\":1534410297847214081,\"title\":\"装配工单\",\"value\":\"5445032970491536505\",\"weight\":2},{\"children\":[{\"children\":[],\"id\":7639361031662168624,\"parentId\":6507903030541782366,\"pid\":6507903030541782366,\"title\":\"工单表导出\",\"value\":\"7639361031662168624\",\"weight\":100},{\"children\":[],\"id\":8562653372099681846,\"parentId\":6507903030541782366,\"pid\":6507903030541782366,\"title\":\"工单表查询\",\"value\":\"8562653372099681846\",\"weight\":100},{\"children\":[],\"id\":4830986669641223230,\"parentId\":6507903030541782366,\"pid\":6507903030541782366,\"title\":\"工单表新增\",\"value\":\"4830986669641223230\",\"weight\":100},{\"children\":[],\"id\":7974141988632466544,\"parentId\":6507903030541782366,\"pid\":6507903030541782366,\"title\":\"工单表删除\",\"value\":\"7974141988632466544\",\"weight\":100},{\"children\":[],\"id\":8313819116019873398,\"parentId\":6507903030541782366,\"pid\":6507903030541782366,\"title\":\"工单表列表\",\"value\":\"8313819116019873398\",\"weight\":100},{\"children\":[],\"id\":8931538668430408569,\"parentId\":6507903030541782366,\"pid\":6507903030541782366,\"title\":\"工单表查看\",\"value\":\"8931538668430408569\",\"weight\":100},{\"children\":[],\"id\":6122595225588020183,\"parentId\":6507903030541782366,\"pid\":6507903030541782366,\"title\":\"工单表编辑\",\"value\":\"6122595225588020183\",\"weight\":100}],\"id\":6507903030541782366,\"parentId\":1534410297847214081,\"pid\":1534410297847214081,\"title\":\"工单\",\"value\":\"6507903030541782366\",\"weight\":3},{\"children\":[{\"children\":[],\"id\":8959792232643188225,\"parentId\":6667327679590719676,\"pid\":6667327679590719676,\"title\":\"工单任务关系表查看\",\"value\":\"8959792232643188225\",\"weight\":100},{\"children\":[],\"id\":5668705457330168859,\"parentId\":6667327679590719676,\"pid\":6667327679590719676,\"title\":\"工单任务关系表查询\",\"value\":\"5668705457330168859\",\"weight\":100},{\"children\":[],\"id\":9180800170765745954,\"parentId\":6667327679590719676,\"pid\":6667327679590719676,\"title\":\"工单任务关系表新增\",\"value\":\"9180800170765745954\",\"weight\":100},{\"children\":[],\"id\":5327083612717101630,\"parentId\":6667327679590719676,\"pid\":6667327679590719676,\"title\":\"工单任务关系表删除\",\"value\":\"5327083612717101630\",\"weight\":100},{\"children\":[],\"id\":6329171771009724483,\"parentId\":6667327679590719676,\"pid\":6667327679590719676,\"title\":\"工单任务关系表列表\",\"value\":\"6329171771009724483\",\"weight\":100},{\"children\":[],\"id\":6020421073902938241,\"parentId\":6667327679590719676,\"pid\":6667327679590719676,\"title\":\"工单任务关系表导出\",\"value\":\"6020421073902938241\",\"weight\":100},{\"children\":[],\"id\":5886355131876064420,\"parentId\":6667327679590719676,\"pid\":6667327679590719676,\"title\":\"工单任务关系表编辑\",\"value\":\"5886355131876064420\",\"weight\":100}],\"id\":6667327679590719676,\"parentId\":1534410297847214081,\"pid\":1534410297847214081,\"title\":\"工单任务关系\",\"value\":\"6667327679590719676\",\"weight\":3},{\"children\":[{\"children\":[],\"id\":1560158326283632641,\"parentId\":9202857208449608897,\"pid\":9202857208449608897,\"title\":\"任务结束\",\"value\":\"1560158326283632641\",\"weight\":100},{\"children\":[],\"id\":1560158090005905410,\"parentId\":9202857208449608897,\"pid\":9202857208449608897,\"title\":\"任务开始\",\"value\":\"1560158090005905410\",\"weight\":100},{\"children\":[],\"id\":7656571819576826411,\"parentId\":9202857208449608897,\"pid\":9202857208449608897,\"title\":\"任务添加\",\"value\":\"7656571819576826411\",\"weight\":100},{\"children\":[],\"id\":5014700545593967407,\"parentId\":9202857208449608897,\"pid\":9202857208449608897,\"title\":\"任务列表\",\"value\":\"5014700545593967407\",\"weight\":100},{\"children\":[],\"id\":8088901678692869692,\"parentId\":9202857208449608897,\"pid\":9202857208449608897,\"title\":\"任务导出\",\"value\":\"8088901678692869692\",\"weight\":100},{\"children\":[],\"id\":6299920358694835309,\"parentId\":9202857208449608897,\"pid\":9202857208449608897,\"title\":\"任务查看\",\"value\":\"6299920358694835309\",\"weight\":100},{\"children\":[],\"id\":5642803668063215805,\"parentId\":9202857208449608897,\"pid\":9202857208449608897,\"title\":\"任务编辑\",\"value\":\"5642803668063215805\",\"weight\":100},{\"children\":[],\"id\":8834719374925492199,\"parentId\":9202857208449608897,\"pid\":9202857208449608897,\"title\":\"任务删除\",\"value\":\"8834719374925492199\",\"weight\":100},{\"children\":[],\"id\":7882898168203081196,\"parentId\":9202857208449608897,\"pid\":9202857208449608897,\"title\":\"任务查询\",\"value\":\"7882898168203081196\",\"weight\":100}],\"id\":9202857208449608897,\"parentId\":1534410297847214081,\"pid\":1534410297847214081,\"title\":\"任务\",\"value\":\"9202857208449608897\",\"weight\":4},{\"children\":[{\"children\":[],\"id\":1542409869531873282,\"parentId\":7346283523793183379,\"pid\":7346283523793183379,\"title\":\"报工审批\",\"value\":\"1542409869531873282\",\"weight\":100},{\"children\":[],\"id\":9208275275751949837,\"parentId\":7346283523793183379,\"pid\":7346283523793183379,\"title\":\"报工查询\",\"value\":\"9208275275751949837\",\"weight\":100},{\"children\":[],\"id\":9173957282399717676,\"parentId\":7346283523793183379,\"pid\":7346283523793183379,\"title\":\"报工删除\",\"value\":\"9173957282399717676\",\"weight\":100},{\"children\":[],\"id\":8973112451110891359,\"parentId\":7346283523793183379,\"pid\":7346283523793183379,\"title\":\"报工导出\",\"value\":\"8973112451110891359\",\"weight\":100},{\"children\":[],\"id\":5753930915061776016,\"parentId\":7346283523793183379,\"pid\":7346283523793183379,\"title\":\"报工列表\",\"value\":\"5753930915061776016\",\"weight\":100},{\"children\":[],\"id\":7288710125399936914,\"parentId\":7346283523793183379,\"pid\":7346283523793183379,\"title\":\"报工新增\",\"value\":\"7288710125399936914\",\"weight\":100},{\"children\":[],\"id\":9022874385071881655,\"parentId\":7346283523793183379,\"pid\":7346283523793183379,\"title\":\"报工查看\",\"value\":\"9022874385071881655\",\"weight\":100},{\"children\":[],\"id\":5378908852735764919,\"parentId\":7346283523793183379,\"pid\":7346283523793183379,\"title\":\"报工编辑\",\"value\":\"5378908852735764919\",\"weight\":100}],\"id\":7346283523793183379,\"parentId\":1534410297847214081,\"pid\":1534410297847214081,\"title\":\"报工\",\"value\":\"7346283523793183379\",\"weight\":5}],\"id\":1534410297847214081,\"parentId\":0,\"pid\":0,\"title\":\"生产管理\",\"value\":\"1534410297847214081\",\"weight\":12},{\"children\":[{\"children\":[{\"children\":[],\"id\":8000986046974593578,\"parentId\":6414357916883420801,\"pid\":6414357916883420801,\"title\":\"供应商联系人导出\",\"value\":\"8000986046974593578\",\"weight\":100},{\"children\":[],\"id\":5197311839117522988,\"parentId\":6414357916883420801,\"pid\":6414357916883420801,\"title\":\"供应商联系人新增\",\"value\":\"5197311839117522988\",\"weight\":100},{\"children\":[],\"id\":6494634442879742520,\"parentId\":6414357916883420801,\"pid\":6414357916883420801,\"title\":\"供应商联系人删除\",\"value\":\"6494634442879742520\",\"weight\":100},{\"children\":[],\"id\":6659298240010835782,\"parentId\":6414357916883420801,\"pid\":6414357916883420801,\"title\":\"供应商联系人编辑\",\"value\":\"6659298240010835782\",\"weight\":100},{\"children\":[],\"id\":5027224879967170377,\"parentId\":6414357916883420801,\"pid\":6414357916883420801,\"title\":\"供应商联系人查询\",\"value\":\"5027224879967170377\",\"weight\":100},{\"children\":[],\"id\":5465134665687105949,\"parentId\":6414357916883420801,\"pid\":6414357916883420801,\"title\":\"供应商联系人查看\",\"value\":\"5465134665687105949\",\"weight\":100},{\"children\":[],\"id\":6535734057645557495,\"parentId\":6414357916883420801,\"pid\":6414357916883420801,\"title\":\"供应商联系人列表\",\"value\":\"6535734057645557495\",\"weight\":100}],\"id\":6414357916883420801,\"parentId\":1558006778241753089,\"pid\":1558006778241753089,\"title\":\"供应商联系人\",\"value\":\"6414357916883420801\",\"weight\":50},{\"children\":[{\"children\":[],\"id\":8424802293696759825,\"parentId\":6134354751483903106,\"pid\":6134354751483903106,\"title\":\" 供应商资料删除\",\"value\":\"8424802293696759825\",\"weight\":100},{\"children\":[],\"id\":6122993178045887049,\"parentId\":6134354751483903106,\"pid\":6134354751483903106,\"title\":\" 供应商资料列表\",\"value\":\"6122993178045887049\",\"weight\":100},{\"children\":[],\"id\":7529678467290942551,\"parentId\":6134354751483903106,\"pid\":6134354751483903106,\"title\":\" 供应商资料查看\",\"value\":\"7529678467290942551\",\"weight\":100},{\"children\":[],\"id\":7209147716433162075,\"parentId\":6134354751483903106,\"pid\":6134354751483903106,\"title\":\" 供应商资料编辑\",\"value\":\"7209147716433162075\",\"weight\":100},{\"children\":[],\"id\":8655981106868069232,\"parentId\":6134354751483903106,\"pid\":6134354751483903106,\"title\":\" 供应商资料导出\",\"value\":\"8655981106868069232\",\"weight\":100},{\"children\":[],\"id\":7337865129337018225,\"parentId\":6134354751483903106,\"pid\":6134354751483903106,\"title\":\" 供应商资料新增\",\"value\":\"7337865129337018225\",\"weight\":100},{\"children\":[],\"id\":5196743176498588333,\"parentId\":6134354751483903106,\"pid\":6134354751483903106,\"title\":\" 供应商资料查询\",\"value\":\"5196743176498588333\",\"weight\":100}],\"id\":6134354751483903106,\"parentId\":1558006778241753089,\"pid\":1558006778241753089,\"title\":\"供应商资料\",\"value\":\"6134354751483903106\",\"weight\":50},{\"children\":[{\"children\":[],\"id\":1574929774689878018,\"parentId\":4794640277308881312,\"pid\":4794640277308881312,\"title\":\"供应商分类树\",\"value\":\"1574929774689878018\",\"weight\":100},{\"children\":[],\"id\":6947194724669964805,\"parentId\":4794640277308881312,\"pid\":4794640277308881312,\"title\":\"供应商分类删除\",\"value\":\"6947194724669964805\",\"weight\":100},{\"children\":[],\"id\":7119232719243003213,\"parentId\":4794640277308881312,\"pid\":4794640277308881312,\"title\":\"供应商分类导出\",\"value\":\"7119232719243003213\",\"weight\":100},{\"children\":[],\"id\":4678695510847297372,\"parentId\":4794640277308881312,\"pid\":4794640277308881312,\"title\":\"供应商分类新增\",\"value\":\"4678695510847297372\",\"weight\":100},{\"children\":[],\"id\":8502207026072268708,\"parentId\":4794640277308881312,\"pid\":4794640277308881312,\"title\":\"供应商分类编辑\",\"value\":\"8502207026072268708\",\"weight\":100},{\"children\":[],\"id\":7294415501551357401,\"parentId\":4794640277308881312,\"pid\":4794640277308881312,\"title\":\"供应商分类查看\",\"value\":\"7294415501551357401\",\"weight\":100},{\"children\":[],\"id\":8175022271486866402,\"parentId\":4794640277308881312,\"pid\":4794640277308881312,\"title\":\"供应商分类查询\",\"value\":\"8175022271486866402\",\"weight\":100},{\"children\":[],\"id\":6996269839693777638,\"parentId\":4794640277308881312,\"pid\":4794640277308881312,\"title\":\"供应商分类列表\",\"value\":\"6996269839693777638\",\"weight\":100}],\"id\":4794640277308881312,\"parentId\":1558006778241753089,\"pid\":1558006778241753089,\"title\":\"供应商分类\",\"value\":\"4794640277308881312\",\"weight\":50}],\"id\":1558006778241753089,\"parentId\":0,\"pid\":0,\"title\":\"供应商管理\",\"value\":\"1558006778241753089\",\"weight\":100},{\"children\":[{\"children\":[{\"children\":[],\"id\":6405406552095053594,\"parentId\":8422209827465537355,\"pid\":8422209827465537355,\"title\":\"采购订单查询\",\"value\":\"6405406552095053594\",\"weight\":100},{\"children\":[],\"id\":8903136733033804117,\"parentId\":8422209827465537355,\"pid\":8422209827465537355,\"title\":\"采购订单查看\",\"value\":\"8903136733033804117\",\"weight\":100},{\"children\":[],\"id\":5382528335658667107,\"parentId\":8422209827465537355,\"pid\":8422209827465537355,\"title\":\"采购订单编辑\",\"value\":\"5382528335658667107\",\"weight\":100},{\"children\":[],\"id\":6046397975825687693,\"parentId\":8422209827465537355,\"pid\":8422209827465537355,\"title\":\"采购订单新增\",\"value\":\"6046397975825687693\",\"weight\":100},{\"children\":[],\"id\":5999927780812383399,\"parentId\":8422209827465537355,\"pid\":8422209827465537355,\"title\":\"采购订单列表\",\"value\":\"5999927780812383399\",\"weight\":100},{\"children\":[],\"id\":5274675770695528925,\"parentId\":8422209827465537355,\"pid\":8422209827465537355,\"title\":\"采购订单删除\",\"value\":\"5274675770695528925\",\"weight\":100},{\"children\":[],\"id\":6813707923637624035,\"parentId\":8422209827465537355,\"pid\":8422209827465537355,\"title\":\"采购订单导出\",\"value\":\"6813707923637624035\",\"weight\":100}],\"id\":8422209827465537355,\"parentId\":1552209067446579201,\"pid\":1552209067446579201,\"title\":\"采购订单\",\"value\":\"8422209827465537355\",\"weight\":100},{\"children\":[{\"children\":[],\"id\":4700141466883779130,\"parentId\":8287951873357908072,\"pid\":8287951873357908072,\"title\":\"采购细明导出\",\"value\":\"4700141466883779130\",\"weight\":100},{\"children\":[],\"id\":4834264129878073409,\"parentId\":8287951873357908072,\"pid\":8287951873357908072,\"title\":\"采购细明删除\",\"value\":\"4834264129878073409\",\"weight\":100},{\"children\":[],\"id\":7845222658049694795,\"parentId\":8287951873357908072,\"pid\":8287951873357908072,\"title\":\"采购细明编辑\",\"value\":\"7845222658049694795\",\"weight\":100},{\"children\":[],\"id\":8946168826263694974,\"parentId\":8287951873357908072,\"pid\":8287951873357908072,\"title\":\"采购细明新增\",\"value\":\"8946168826263694974\",\"weight\":100},{\"children\":[],\"id\":5162865219757254273,\"parentId\":8287951873357908072,\"pid\":8287951873357908072,\"title\":\"采购细明查询\",\"value\":\"5162865219757254273\",\"weight\":100},{\"children\":[],\"id\":9090782745729954273,\"parentId\":8287951873357908072,\"pid\":8287951873357908072,\"title\":\"采购细明列表\",\"value\":\"9090782745729954273\",\"weight\":100},{\"children\":[],\"id\":7909153361025684710,\"parentId\":8287951873357908072,\"pid\":8287951873357908072,\"title\":\"采购细明查看\",\"value\":\"7909153361025684710\",\"weight\":100}],\"id\":8287951873357908072,\"parentId\":1552209067446579201,\"pid\":1552209067446579201,\"title\":\"采购明细\",\"value\":\"8287951873357908072\",\"weight\":100}],\"id\":1552209067446579201,\"parentId\":0,\"pid\":0,\"title\":\"采购管理\",\"value\":\"1552209067446579201\",\"weight\":100},{\"children\":[],\"id\":1342445437296771074,\"parentId\":0,\"pid\":0,\"title\":\"代码生成\",\"value\":\"1342445437296771074\",\"weight\":100},{\"children\":[{\"children\":[{\"children\":[],\"id\":1574930219948802050,\"parentId\":7072750871619746982,\"pid\":7072750871619746982,\"title\":\"客户分类树\",\"value\":\"1574930219948802050\",\"weight\":100},{\"children\":[],\"id\":8107628912599125262,\"parentId\":7072750871619746982,\"pid\":7072750871619746982,\"title\":\"客户分类列表\",\"value\":\"8107628912599125262\",\"weight\":100},{\"children\":[],\"id\":4903707486851837034,\"parentId\":7072750871619746982,\"pid\":7072750871619746982,\"title\":\"客户分类导出\",\"value\":\"4903707486851837034\",\"weight\":100},{\"children\":[],\"id\":7635960329677377171,\"parentId\":7072750871619746982,\"pid\":7072750871619746982,\"title\":\"客户分类编辑\",\"value\":\"7635960329677377171\",\"weight\":100},{\"children\":[],\"id\":7596329328691560085,\"parentId\":7072750871619746982,\"pid\":7072750871619746982,\"title\":\"客户分类新增\",\"value\":\"7596329328691560085\",\"weight\":100},{\"children\":[],\"id\":6429021782911745716,\"parentId\":7072750871619746982,\"pid\":7072750871619746982,\"title\":\"客户分类删除\",\"value\":\"6429021782911745716\",\"weight\":100},{\"children\":[],\"id\":8259333771823873238,\"parentId\":7072750871619746982,\"pid\":7072750871619746982,\"title\":\"客户分类查看\",\"value\":\"8259333771823873238\",\"weight\":100},{\"children\":[],\"id\":4674816360932725215,\"parentId\":7072750871619746982,\"pid\":7072750871619746982,\"title\":\"客户分类查询\",\"value\":\"4674816360932725215\",\"weight\":100}],\"id\":7072750871619746982,\"parentId\":1558006914472747010,\"pid\":1558006914472747010,\"title\":\"客户分类\",\"value\":\"7072750871619746982\",\"weight\":50},{\"children\":[{\"children\":[],\"id\":6696300230304937737,\"parentId\":8436831550169549160,\"pid\":8436831550169549160,\"title\":\"客户资料查询\",\"value\":\"6696300230304937737\",\"weight\":100},{\"children\":[],\"id\":7898061431233899837,\"parentId\":8436831550169549160,\"pid\":8436831550169549160,\"title\":\"客户资料编辑\",\"value\":\"7898061431233899837\",\"weight\":100},{\"children\":[],\"id\":5448185299220002627,\"parentId\":8436831550169549160,\"pid\":8436831550169549160,\"title\":\"客户资料删除\",\"value\":\"5448185299220002627\",\"weight\":100},{\"children\":[],\"id\":9135927743117002076,\"parentId\":8436831550169549160,\"pid\":8436831550169549160,\"title\":\"客户资料查看\",\"value\":\"9135927743117002076\",\"weight\":100},{\"children\":[],\"id\":6655166482691941995,\"parentId\":8436831550169549160,\"pid\":8436831550169549160,\"title\":\"客户资料导出\",\"value\":\"6655166482691941995\",\"weight\":100},{\"children\":[],\"id\":5871841993475141582,\"parentId\":8436831550169549160,\"pid\":8436831550169549160,\"title\":\"客户资料列表\",\"value\":\"5871841993475141582\",\"weight\":100},{\"children\":[],\"id\":5276828216703973340,\"parentId\":8436831550169549160,\"pid\":8436831550169549160,\"title\":\"客户资料新增\",\"value\":\"5276828216703973340\",\"weight\":100}],\"id\":8436831550169549160,\"parentId\":1558006914472747010,\"pid\":1558006914472747010,\"title\":\"客户资料\",\"value\":\"8436831550169549160\",\"weight\":100},{\"children\":[{\"children\":[],\"id\":6238776219082572290,\"parentId\":8700710314151240148,\"pid\":8700710314151240148,\"title\":\"客户联系人编辑\",\"value\":\"6238776219082572290\",\"weight\":100},{\"children\":[],\"id\":8613714595480779524,\"parentId\":8700710314151240148,\"pid\":8700710314151240148,\"title\":\"客户联系人导出\",\"value\":\"8613714595480779524\",\"weight\":100},{\"children\":[],\"id\":5346549595871021092,\"parentId\":8700710314151240148,\"pid\":8700710314151240148,\"title\":\"客户联系人查看\",\"value\":\"5346549595871021092\",\"weight\":100},{\"children\":[],\"id\":8181816557328888214,\"parentId\":8700710314151240148,\"pid\":8700710314151240148,\"title\":\"客户联系人列表\",\"value\":\"8181816557328888214\",\"weight\":100},{\"children\":[],\"id\":5587279777867961498,\"parentId\":8700710314151240148,\"pid\":8700710314151240148,\"title\":\"客户联系人删除\",\"value\":\"5587279777867961498\",\"weight\":100},{\"children\":[],\"id\":7380411400614516902,\"parentId\":8700710314151240148,\"pid\":8700710314151240148,\"title\":\"客户联系人新增\",\"value\":\"7380411400614516902\",\"weight\":100},{\"children\":[],\"id\":7721498005843290304,\"parentId\":8700710314151240148,\"pid\":8700710314151240148,\"title\":\"客户联系人查询\",\"value\":\"7721498005843290304\",\"weight\":100}],\"id\":8700710314151240148,\"parentId\":1558006914472747010,\"pid\":1558006914472747010,\"title\":\"联系人\",\"value\":\"8700710314151240148\",\"weight\":100}],\"id\":1558006914472747010,\"parentId\":0,\"pid\":0,\"title\":\"客户管理\",\"value\":\"1558006914472747010\",\"weight\":100},{\"children\":[{\"children\":[{\"children\":[],\"id\":9152465645130034198,\"parentId\":5012839659883217263,\"pid\":5012839659883217263,\"title\":\"入库单查看\",\"value\":\"9152465645130034198\",\"weight\":100},{\"children\":[],\"id\":6280770118494855200,\"parentId\":5012839659883217263,\"pid\":5012839659883217263,\"title\":\"入库单列表\",\"value\":\"6280770118494855200\",\"weight\":100},{\"children\":[],\"id\":8775511535879974972,\"parentId\":5012839659883217263,\"pid\":5012839659883217263,\"title\":\"入库单查询\",\"value\":\"8775511535879974972\",\"weight\":100},{\"children\":[],\"id\":5624458298951752771,\"parentId\":5012839659883217263,\"pid\":5012839659883217263,\"title\":\"入库单导出\",\"value\":\"5624458298951752771\",\"weight\":100},{\"children\":[],\"id\":7254023492833901715,\"parentId\":5012839659883217263,\"pid\":5012839659883217263,\"title\":\"入库单删除\",\"value\":\"7254023492833901715\",\"weight\":100},{\"children\":[],\"id\":7318591877639154878,\"parentId\":5012839659883217263,\"pid\":5012839659883217263,\"title\":\"入库单编辑\",\"value\":\"7318591877639154878\",\"weight\":100},{\"children\":[],\"id\":7812048842906278623,\"parentId\":5012839659883217263,\"pid\":5012839659883217263,\"title\":\"入库单新增\",\"value\":\"7812048842906278623\",\"weight\":100}],\"id\":5012839659883217263,\"parentId\":1534411830716350466,\"pid\":1534411830716350466,\"title\":\"入库单\",\"value\":\"5012839659883217263\",\"weight\":1},{\"children\":[{\"children\":[],\"id\":5893520734687245059,\"parentId\":8633823152794346306,\"pid\":8633823152794346306,\"title\":\"出库单删除\",\"value\":\"5893520734687245059\",\"weight\":100},{\"children\":[],\"id\":8868921547741003555,\"parentId\":8633823152794346306,\"pid\":8633823152794346306,\"title\":\"出库单导出\",\"value\":\"8868921547741003555\",\"weight\":100},{\"children\":[],\"id\":6326961474985796183,\"parentId\":8633823152794346306,\"pid\":8633823152794346306,\"title\":\"出库单新增\",\"value\":\"6326961474985796183\",\"weight\":100},{\"children\":[],\"id\":8833258242949762178,\"parentId\":8633823152794346306,\"pid\":8633823152794346306,\"title\":\"出库单编辑\",\"value\":\"8833258242949762178\",\"weight\":100},{\"children\":[],\"id\":4967796364303223216,\"parentId\":8633823152794346306,\"pid\":8633823152794346306,\"title\":\"出库单列表\",\"value\":\"4967796364303223216\",\"weight\":100},{\"children\":[],\"id\":6900932255841430981,\"parentId\":8633823152794346306,\"pid\":8633823152794346306,\"title\":\"出库单查询\",\"value\":\"6900932255841430981\",\"weight\":100},{\"children\":[],\"id\":7629147488562302699,\"parentId\":8633823152794346306,\"pid\":8633823152794346306,\"title\":\"出库单查看\",\"value\":\"7629147488562302699\",\"weight\":100}],\"id\":8633823152794346306,\"parentId\":1534411830716350466,\"pid\":1534411830716350466,\"title\":\"出库单\",\"value\":\"8633823152794346306\",\"weight\":2},{\"children\":[{\"children\":[],\"id\":5002055413507195458,\"parentId\":8016744771368544956,\"pid\":8016744771368544956,\"title\":\"出入库明细新增\",\"value\":\"5002055413507195458\",\"weight\":100},{\"children\":[],\"id\":6290232610677682017,\"parentId\":8016744771368544956,\"pid\":8016744771368544956,\"title\":\"出入库明细导出\",\"value\":\"6290232610677682017\",\"weight\":100},{\"children\":[],\"id\":5803272132980049305,\"parentId\":8016744771368544956,\"pid\":8016744771368544956,\"title\":\"出入库明细查询\",\"value\":\"5803272132980049305\",\"weight\":100},{\"children\":[],\"id\":7395287038762629813,\"parentId\":8016744771368544956,\"pid\":8016744771368544956,\"title\":\"出入库明细编辑\",\"value\":\"7395287038762629813\",\"weight\":100},{\"children\":[],\"id\":7323225129555267277,\"parentId\":8016744771368544956,\"pid\":8016744771368544956,\"title\":\"出入库明细删除\",\"value\":\"7323225129555267277\",\"weight\":100},{\"children\":[],\"id\":6347609996552577764,\"parentId\":8016744771368544956,\"pid\":8016744771368544956,\"title\":\"出入库明细查看\",\"value\":\"6347609996552577764\",\"weight\":100},{\"children\":[],\"id\":7682643462888492777,\"parentId\":8016744771368544956,\"pid\":8016744771368544956,\"title\":\"出入库明细列表\",\"value\":\"7682643462888492777\",\"weight\":100}],\"id\":8016744771368544956,\"parentId\":1534411830716350466,\"pid\":1534411830716350466,\"title\":\"出入库明细\",\"value\":\"8016744771368544956\",\"weight\":3},{\"children\":[{\"children\":[],\"id\":5469043739505193743,\"parentId\":5721174223820977430,\"pid\":5721174223820977430,\"title\":\"库存余额查询\",\"value\":\"5469043739505193743\",\"weight\":100},{\"children\":[],\"id\":5459011313540319790,\"parentId\":5721174223820977430,\"pid\":5721174223820977430,\"title\":\"库存余额导出\",\"value\":\"5459011313540319790\",\"weight\":100},{\"children\":[],\"id\":7723724790018395451,\"parentId\":5721174223820977430,\"pid\":5721174223820977430,\"title\":\"库存余额查看\",\"value\":\"7723724790018395451\",\"weight\":100},{\"children\":[],\"id\":7764860386896651377,\"parentId\":5721174223820977430,\"pid\":5721174223820977430,\"title\":\"库存余额删除\",\"value\":\"7764860386896651377\",\"weight\":100},{\"children\":[],\"id\":8254320371052864398,\"parentId\":5721174223820977430,\"pid\":5721174223820977430,\"title\":\"库存余额列表\",\"value\":\"8254320371052864398\",\"weight\":100},{\"children\":[],\"id\":8273410905135864011,\"parentId\":5721174223820977430,\"pid\":5721174223820977430,\"title\":\"库存余额编辑\",\"value\":\"8273410905135864011\",\"weight\":100},{\"children\":[],\"id\":8515665128226371022,\"parentId\":5721174223820977430,\"pid\":5721174223820977430,\"title\":\"库存余额新增\",\"value\":\"8515665128226371022\",\"weight\":100}],\"id\":5721174223820977430,\"parentId\":1534411830716350466,\"pid\":1534411830716350466,\"title\":\"库存余额\",\"value\":\"5721174223820977430\",\"weight\":100},{\"children\":[{\"children\":[],\"id\":5555524248707858432,\"parentId\":5000566847705099773,\"pid\":5000566847705099773,\"title\":\"仓库管理查询\",\"value\":\"5555524248707858432\",\"weight\":100},{\"children\":[],\"id\":1562329827773108225,\"parentId\":5000566847705099773,\"pid\":5000566847705099773,\"title\":\"仓库树\",\"value\":\"1562329827773108225\",\"weight\":100},{\"children\":[],\"id\":5901981891345615661,\"parentId\":5000566847705099773,\"pid\":5000566847705099773,\"title\":\"仓库管理列表\",\"value\":\"5901981891345615661\",\"weight\":100},{\"children\":[],\"id\":8342821565086833982,\"parentId\":5000566847705099773,\"pid\":5000566847705099773,\"title\":\"仓库管理删除\",\"value\":\"8342821565086833982\",\"weight\":100},{\"children\":[],\"id\":7044559182298687818,\"parentId\":5000566847705099773,\"pid\":5000566847705099773,\"title\":\"仓库管理编辑\",\"value\":\"7044559182298687818\",\"weight\":100},{\"children\":[],\"id\":5714641428045333341,\"parentId\":5000566847705099773,\"pid\":5000566847705099773,\"title\":\"仓库管理导出\",\"value\":\"5714641428045333341\",\"weight\":100},{\"children\":[],\"id\":5801879553212456867,\"parentId\":5000566847705099773,\"pid\":5000566847705099773,\"title\":\"仓库管理新增\",\"value\":\"5801879553212456867\",\"weight\":100},{\"children\":[],\"id\":6739113753546148532,\"parentId\":5000566847705099773,\"pid\":5000566847705099773,\"title\":\"仓库管理查看\",\"value\":\"6739113753546148532\",\"weight\":100}],\"id\":5000566847705099773,\"parentId\":1534411830716350466,\"pid\":1534411830716350466,\"title\":\"仓库管理\",\"value\":\"5000566847705099773\",\"weight\":100}],\"id\":1534411830716350466,\"parentId\":0,\"pid\":0,\"title\":\"库存管理\",\"value\":\"1534411830716350466\",\"weight\":100},{\"children\":[{\"children\":[{\"children\":[],\"id\":1264622039642256741,\"parentId\":1264622039642256731,\"pid\":1264622039642256731,\"title\":\"系统区域列表\",\"value\":\"1264622039642256741\",\"weight\":100}],\"id\":1264622039642256731,\"parentId\":1264622039642256721,\"pid\":1264622039642256721,\"title\":\"系统区域\",\"value\":\"1264622039642256731\",\"weight\":100}],\"id\":1264622039642256721,\"parentId\":0,\"pid\":0,\"title\":\"区域管理\",\"value\":\"1264622039642256721\",\"weight\":100},{\"children\":[{\"children\":[],\"id\":1574930637298827265,\"parentId\":8957317986837088422,\"pid\":8957317986837088422,\"title\":\"字段配置信息列表\",\"value\":\"1574930637298827265\",\"weight\":100},{\"children\":[],\"id\":7455112058491820064,\"parentId\":8957317986837088422,\"pid\":8957317986837088422,\"title\":\"字段配置删除\",\"value\":\"7455112058491820064\",\"weight\":100},{\"children\":[],\"id\":4621333945127042674,\"parentId\":8957317986837088422,\"pid\":8957317986837088422,\"title\":\"字段配置新增\",\"value\":\"4621333945127042674\",\"weight\":100},{\"children\":[],\"id\":5886900280768283007,\"parentId\":8957317986837088422,\"pid\":8957317986837088422,\"title\":\"字段配置编辑\",\"value\":\"5886900280768283007\",\"weight\":100},{\"children\":[],\"id\":5846194751422568093,\"parentId\":8957317986837088422,\"pid\":8957317986837088422,\"title\":\"字段配置导出\",\"value\":\"5846194751422568093\",\"weight\":100},{\"children\":[],\"id\":9156093698218665129,\"parentId\":8957317986837088422,\"pid\":8957317986837088422,\"title\":\"字段配置列表\",\"value\":\"9156093698218665129\",\"weight\":100},{\"children\":[],\"id\":7678659259268274624,\"parentId\":8957317986837088422,\"pid\":8957317986837088422,\"title\":\"字段配置查看\",\"value\":\"7678659259268274624\",\"weight\":100},{\"children\":[],\"id\":8099644559700436461,\"parentId\":8957317986837088422,\"pid\":8957317986837088422,\"title\":\"字段配置查询\",\"value\":\"8099644559700436461\",\"weight\":100}],\"id\":8957317986837088422,\"parentId\":0,\"pid\":0,\"title\":\"字段配置\",\"value\":\"8957317986837088422\",\"weight\":100},{\"children\":[{\"children\":[],\"id\":1574930880073531393,\"parentId\":5474507168841280952,\"pid\":5474507168841280952,\"title\":\"编号规则信息列表\",\"value\":\"1574930880073531393\",\"weight\":100},{\"children\":[],\"id\":5653092812632926489,\"parentId\":5474507168841280952,\"pid\":5474507168841280952,\"title\":\"自定义编号规则删除\",\"value\":\"5653092812632926489\",\"weight\":100},{\"children\":[],\"id\":7200474546305867050,\"parentId\":5474507168841280952,\"pid\":5474507168841280952,\"title\":\"自定义编号规则列表\",\"value\":\"7200474546305867050\",\"weight\":100},{\"children\":[],\"id\":7242870236365548592,\"parentId\":5474507168841280952,\"pid\":5474507168841280952,\"title\":\"自定义编号规则导出\",\"value\":\"7242870236365548592\",\"weight\":100},{\"children\":[],\"id\":9150731911657480775,\"parentId\":5474507168841280952,\"pid\":5474507168841280952,\"title\":\"自定义编号规则编辑\",\"value\":\"9150731911657480775\",\"weight\":100},{\"children\":[],\"id\":7287765970469918079,\"parentId\":5474507168841280952,\"pid\":5474507168841280952,\"title\":\"自定义编号规则查看\",\"value\":\"7287765970469918079\",\"weight\":100},{\"children\":[],\"id\":5480200211927228567,\"parentId\":5474507168841280952,\"pid\":5474507168841280952,\"title\":\"自定义编号规则新增\",\"value\":\"5480200211927228567\",\"weight\":100},{\"children\":[],\"id\":4770899845682577376,\"parentId\":5474507168841280952,\"pid\":5474507168841280952,\"title\":\"自定义编号规则查询\",\"value\":\"4770899845682577376\",\"weight\":100}],\"id\":5474507168841280952,\"parentId\":0,\"pid\":0,\"title\":\"编号规则\",\"value\":\"5474507168841280952\",\"weight\":100},{\"children\":[{\"children\":[{\"children\":[],\"id\":1264622039642256641,\"parentId\":1264622039642256621,\"pid\":1264622039642256621,\"title\":\"定时任务列表\",\"value\":\"1264622039642256641\",\"weight\":100},{\"children\":[],\"id\":1264622039642256651,\"parentId\":1264622039642256621,\"pid\":1264622039642256621,\"title\":\"定时任务详情\",\"value\":\"1264622039642256651\",\"weight\":100},{\"children\":[],\"id\":1264622039642256661,\"parentId\":1264622039642256621,\"pid\":1264622039642256621,\"title\":\"定时任务增加\",\"value\":\"1264622039642256661\",\"weight\":100},{\"children\":[],\"id\":1264622039642256671,\"parentId\":1264622039642256621,\"pid\":1264622039642256621,\"title\":\"定时任务删除\",\"value\":\"1264622039642256671\",\"weight\":100},{\"children\":[],\"id\":1264622039642256681,\"parentId\":1264622039642256621,\"pid\":1264622039642256621,\"title\":\"定时任务编辑\",\"value\":\"1264622039642256681\",\"weight\":100},{\"children\":[],\"id\":1264622039642256691,\"parentId\":1264622039642256621,\"pid\":1264622039642256621,\"title\":\"定时任务可执行列表\",\"value\":\"1264622039642256691\",\"weight\":100},{\"children\":[],\"id\":1264622039642256701,\"parentId\":1264622039642256621,\"pid\":1264622039642256621,\"title\":\"定时任务启动\",\"value\":\"1264622039642256701\",\"weight\":100},{\"children\":[],\"id\":1264622039642256711,\"parentId\":1264622039642256621,\"pid\":1264622039642256621,\"title\":\"定时任务关闭\",\"value\":\"1264622039642256711\",\"weight\":100},{\"children\":[],\"id\":1264622039642256631,\"parentId\":1264622039642256621,\"pid\":1264622039642256621,\"title\":\"定时任务查询\",\"value\":\"1264622039642256631\",\"weight\":100}],\"id\":1264622039642256621,\"parentId\":1264622039642256611,\"pid\":1264622039642256611,\"title\":\"任务管理\",\"value\":\"1264622039642256621\",\"weight\":22}],\"id\":1264622039642256611,\"parentId\":0,\"pid\":0,\"title\":\"定时任务\",\"value\":\"1264622039642256611\",\"weight\":100}],\"message\":\"请求成功\",\"success\":true}', '2022-10-26 16:54:34', 'superAdmin', 'e00f1aca0aebf0a0cef062147a2bc0131ec6247f85dffde415ba6ba2b72dc219');
INSERT INTO `sys_op_log` VALUES (1585193268018388993, '系统角色_授权菜单', 10, 'Y', '成功', '127.0.0.1', '-', 'Chrome', 'Windows 10 or Windows Server 2016', '/sysRole/grantMenu', 'vip.xiaonuo.sys.modular.role.controller.SysRoleController', 'grantMenu', 'POST', '{\"grantMenuIdList\":[1538776469361139714,1264622039642256441,1264622039642256431,1264622039642256421,1264622039642256451,1264622039642256461,1264622039642256471,1264622039642256481,1264622039642256491,1264622039642256501,1264622039642256511,1264622039642256541,1264622039642256531,1264622039642256521,1264622039642256551,1264622039642256561,1264622039642256571,1264622039642256581,1264622039642256591,1264622039642256601,1410859007809736705,6570253563256899359,6527167616560933135,1534384317233524738,1551480636561321985,6547301851685353031,8197158197771689059,9222389351831596218,5142735407151148234,7621956982866257638,5553078347948343545,4998978684524443692,6449912099524079616,5707993274387362828,8275137972166495794,6249597959063106635,6905244625723471705,6709768906758836391,7388361180917918171,7702400310012060990,5243613315131080710,5079108238558434584,8562547749529140265,5788080164048630938,7936411315868128942,7940657177277675482,6194217070047486945,6436044181834026359,8315301036989682205,8618504262472492350,7950819887425681478,4832107133629024589,7946795106595000695,5279583233581044419,6845666717193712873,8426505795261326687,6103813152661113624,6374585520258378360,8621765388717143178,6116805160244737678,6860312372874164125,5820472386138045917,8181822590852115190,7128085356547940830,6204862670753579307,4835110481928644952,7047324015553475476,7581916369308597479,6591367648784009715,6015462925549607928,4719172401435180793,7639361031662168624,6507903030541782366,1534410297847214081,8562653372099681846,4830986669641223230,7974141988632466544,8313819116019873398,8931538668430408569,6122595225588020183,8477499608364957015,5206868747918117433,4757443910107701590,5016248121790626902,7516608184768822750,8293588485966810596,5523506981641034218,8494981115107347954,5934688735467584877,6045763991836034103,7561922286979539034,5457126757845470309,8590002636006568603,6257208246505535137,9118159149629415380,6354039257511829492,5445032970491536505,6542835870761280820,8191527329183937863,7357393608825227850,7447629070175932292,5228961670953770885,5234105579337808561,6457085339840740289,6667327679590719676,8959792232643188225,5668705457330168859,9180800170765745954,5327083612717101630,6329171771009724483,6020421073902938241,5886355131876064420,9202857208449608897,1560158326283632641,1560158090005905410,7656571819576826411,5014700545593967407,8088901678692869692,6299920358694835309,5642803668063215805,8834719374925492199,7882898168203081196,7346283523793183379,1542409869531873282,9208275275751949837,9173957282399717676,8973112451110891359,5753930915061776016,7288710125399936914,9022874385071881655,5378908852735764919,9152465645130034198,5012839659883217263,1534411830716350466,6280770118494855200,8775511535879974972,5624458298951752771,7254023492833901715,7318591877639154878,7812048842906278623,8633823152794346306,5893520734687245059,8868921547741003555,6326961474985796183,8833258242949762178,4967796364303223216,6900932255841430981,7629147488562302699,8016744771368544956,5002055413507195458,6290232610677682017,5803272132980049305,7395287038762629813,7323225129555267277,6347609996552577764,7682643462888492777,5721174223820977430,5469043739505193743,5459011313540319790,7723724790018395451,7764860386896651377,8254320371052864398,8273410905135864011,8515665128226371022,5000566847705099773,5555524248707858432,1562329827773108225,5901981891345615661,8342821565086833982,7044559182298687818,5714641428045333341,5801879553212456867,6739113753546148532,6696300230304937737,8436831550169549160,1558006914472747010,7898061431233899837,5448185299220002627,9135927743117002076,6655166482691941995,5871841993475141582,5276828216703973340,7072750871619746982,1574930219948802050,8107628912599125262,4903707486851837034,7635960329677377171,7596329328691560085,6429021782911745716,8259333771823873238,4674816360932725215,8700710314151240148,6238776219082572290,8613714595480779524,5346549595871021092,8181816557328888214,5587279777867961498,7380411400614516902,7721498005843290304,7455112058491820064,8957317986837088422,4621333945127042674,5886900280768283007,5846194751422568093,9156093698218665129,7678659259268274624,8099644559700436461,5653092812632926489,7200474546305867050,7242870236365548592,9150731911657480775,7287765970469918079,5480200211927228567,4770899845682577376,1264622039642256641,1264622039642256621,1264622039642256611,1264622039642256651,1264622039642256661,1264622039642256671,1264622039642256681,1264622039642256691,1264622039642256701,1264622039642256711,1264622039642256631,1264622039642255361,1264622039642255351,1264622039642255341,1264622039642255371,1264622039642255381,1264622039642255391,1264622039642255401,1264622039642255411,1264622039642255421,1264622039642255431,1264622039642255441,1264622039642255451,1264622039642255461,1264622039642255471,1264622039642255481,1264622039642255491,1264622039642255501,1264622039642255511,1264622039642255521,1264622039642255531,1264622039642255541,1264622039642255551,1264622039642255561,1264622039642255571,1264622039642255581,1264622039642255591,1264622039642255601,1264622039642255621,1264622039642255631,1264622039642255641,1264622039642255651,1264622039642255661,1264622039642255611,1264622039642255311,1264622039642255331,1264622039642255321,1264622039642255671,1264622039642255681,1264622039642255691,1264622039642255701,1264622039642255711,1264622039642255721,1264622039642255731,1264622039642255741,1264622039642255751,1264622039642255761,1264622039642255771,1264622039642255781,1264622039642255791,1264622039642255801,1264622039642255811,1264622039642255821,1264622039642255831,1264622039642255841,1264622039642255851,1264622039642255881,1264622039642255891,1264622039642255901,1264622039642255911,1264622039642255921,1264622039642255931,1264622039642255941,1264622039642255951,1264622039642255861,1264622039642255871,1264622039642255961,1264622039642255971,1264622039642255981,1264622039642255991,1264622039642256001,1264622039642256011,1264622039642256021,1264622039642256031,1264622039642256041,1264622039642256051,1264622039642256061,1264622039642256071,1264622039642256081,1264622039642256091,1264622039642256101,1264622039642256111,1264622039642256131,1264622039642256141,1264622039642256151,1264622039642256161,1264622039642256171,1264622039642256181,1264622039642256191,1264622039642256201,1264622039642256211,1264622039642256221,1264622039642256231,1264622039642256241,1264622039642256251,1264622039642256261,1264622039642256121,1264622039642256271,1264622039642256281,1264622039642256291,1264622039642256301,1264622039642256311,1264622039642256321,1264622039642256331,1264622039642256341,1264622039642256351,1264622039642256361,1264622039642256371,1264622039642256381,1264622039642256391,1264622039642256401,1264622039642256411,1560145190851772417,256,1,257,2,258,3,259,4,260,5,261,6,262,7,263,8,264,9,265,10,266,11,267,12,268,13,269,14,270,15,271,16,272,17,273,18,274,19,275,20,276,21,277,22,278,23,279,24,280,25,281,26,282,27,283,28,284,29,285,30,286,31,287,32,288,33,289,34,290,35,291,36,292,37,293,38,294,39,295,40,296,41,297,42,298,43,299,44,300,45,301,46,302,47,303,48,304,49,305,50,306,51,307,52,308,53,309,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255,1558006778241753089,6414357916883420801,8000986046974593578,5197311839117522988,6494634442879742520,6659298240010835782,5027224879967170377,5465134665687105949,6535734057645557495,6134354751483903106,8424802293696759825,6122993178045887049,7529678467290942551,7209147716433162075,8655981106868069232,7337865129337018225,5196743176498588333,4794640277308881312,1574929774689878018,6947194724669964805,7119232719243003213,4678695510847297372,8502207026072268708,7294415501551357401,8175022271486866402,6996269839693777638,1552209067446579201,8422209827465537355,6405406552095053594,8903136733033804117,5382528335658667107,6046397975825687693,5999927780812383399,5274675770695528925,6813707923637624035,8287951873357908072,4700141466883779130,4834264129878073409,7845222658049694795,8946168826263694974,5162865219757254273,9090782745729954273,7909153361025684710,1342445437296771074,1264622039642256721,1264622039642256731,1264622039642256741,1574930637298827265,5474507168841280952,1574930880073531393],\"id\":1551458068714385410}', '{\"code\":200,\"message\":\"请求成功\",\"success\":true}', '2022-10-26 16:55:01', 'superAdmin', '1ded8dd68da938eb57d6f05e52b15679fc9ad0adb65e37b8ce7557ee535b5e53');
INSERT INTO `sys_op_log` VALUES (1585193268806918146, '系统角色_查询', 5, 'Y', '成功', '127.0.0.1', '-', 'Chrome', 'Windows 10 or Windows Server 2016', '/sysRole/page', 'vip.xiaonuo.sys.modular.role.controller.SysRoleController', 'page', 'GET', '{}', '{\"code\":200,\"data\":{\"pageNo\":1,\"pageSize\":10,\"rainbow\":[1],\"rows\":[{\"code\":\"admin\",\"createTime\":1660803289000,\"createUser\":1265476890672672808,\"dataScopeType\":1,\"id\":1560148188390817794,\"name\":\"管理员\",\"sort\":1,\"status\":0},{\"code\":\"manager\",\"createTime\":1660803311000,\"createUser\":1265476890672672808,\"dataScopeType\":1,\"id\":1560148282678771714,\"name\":\"经理\",\"sort\":2,\"status\":0,\"updateTime\":1660803349000,\"updateUser\":1265476890672672808},{\"code\":\"gleader\",\"createTime\":1660803326000,\"createUser\":1265476890672672808,\"dataScopeType\":1,\"id\":1560148341927510018,\"name\":\"小组长\",\"sort\":3,\"status\":0,\"updateTime\":1660803397000,\"updateUser\":1265476890672672808},{\"code\":\"wperson\",\"createTime\":1660803342000,\"createUser\":1265476890672672808,\"dataScopeType\":1,\"id\":1560148409690685442,\"name\":\"生产人员\",\"sort\":4,\"status\":0,\"updateTime\":1660803380000,\"updateUser\":1265476890672672808},{\"code\":\"demo\",\"createTime\":1664326782000,\"createUser\":1265476890672672808,\"dataScopeType\":1,\"id\":1574926790555828225,\"name\":\"测试\",\"sort\":100,\"status\":0},{\"code\":\"test_user\",\"createTime\":1658731403000,\"createUser\":1265476890672672808,\"dataScopeType\":1,\"id\":1551458068714385410,\"name\":\"测试用户\",\"remark\":\"测试用户\",\"sort\":100,\"status\":0},{\"code\":\"ent_manager_role\",\"createTime\":1585826846000,\"createUser\":1265476890672672808,\"dataScopeType\":1,\"id\":1265476890672672817,\"name\":\"组织架构管理员\",\"remark\":\"组织架构管理员\",\"sort\":100,\"status\":0,\"updateTime\":1656573363000,\"updateUser\":1265476890672672808},{\"code\":\"auth_role\",\"createTime\":1585826920000,\"createUser\":1265476890672672808,\"dataScopeType\":1,\"id\":1265476890672672818,\"name\":\"权限管理员\",\"remark\":\"权限管理员\",\"sort\":101,\"status\":0,\"updateTime\":1656573386000,\"updateUser\":1265476890672672808}],\"totalPage\":1,\"totalRows\":8},\"message\":\"请求成功\",\"success\":true}', '2022-10-26 16:55:01', 'superAdmin', '984a45bf345833b2bf99849c57a63cb80913022a0dc551566d3191fa89653cbc');
INSERT INTO `sys_op_log` VALUES (1585193288998297602, '系统组织机构_树', 7, 'Y', '成功', '127.0.0.1', '-', 'Chrome', 'Windows 10 or Windows Server 2016', '/sysOrg/tree', 'vip.xiaonuo.sys.modular.org.controller.SysOrgController', 'tree', 'GET', '{}', '{\"code\":200,\"data\":[{\"children\":[{\"children\":[{\"children\":[],\"id\":1265476890672672771,\"parentId\":1265476890672672769,\"pid\":1265476890672672769,\"title\":\"研发部\",\"value\":\"1265476890672672771\",\"weight\":100},{\"children\":[],\"id\":1265476890672672772,\"parentId\":1265476890672672769,\"pid\":1265476890672672769,\"title\":\"企划部\",\"value\":\"1265476890672672772\",\"weight\":100}],\"id\":1265476890672672769,\"parentId\":1265476890651701250,\"pid\":1265476890651701250,\"title\":\"华夏集团乌鲁木齐分公司\",\"value\":\"1265476890672672769\",\"weight\":100},{\"children\":[{\"children\":[{\"children\":[],\"id\":1265476890672672775,\"parentId\":1265476890672672773,\"pid\":1265476890672672773,\"title\":\"市场部二部\",\"value\":\"1265476890672672775\",\"weight\":100}],\"id\":1265476890672672773,\"parentId\":1265476890672672770,\"pid\":1265476890672672770,\"title\":\"市场部\",\"value\":\"1265476890672672773\",\"weight\":100},{\"children\":[],\"id\":1265476890672672774,\"parentId\":1265476890672672770,\"pid\":1265476890672672770,\"title\":\"财务部\",\"value\":\"1265476890672672774\",\"weight\":100}],\"id\":1265476890672672770,\"parentId\":1265476890651701250,\"pid\":1265476890651701250,\"title\":\"华夏集团成都分公司\",\"value\":\"1265476890672672770\",\"weight\":100}],\"id\":1265476890651701250,\"parentId\":0,\"pid\":0,\"title\":\"华夏集团\",\"value\":\"1265476890651701250\",\"weight\":100}],\"message\":\"请求成功\",\"success\":true}', '2022-10-26 16:55:06', 'superAdmin', '0959bc4bb2c63b8aae9ec29dfe993aea3d6920cdcf9717361451db9e657453d0');
INSERT INTO `sys_op_log` VALUES (1585193290277560321, '系统字典类型_下拉', 5, 'Y', '成功', '127.0.0.1', '-', 'Chrome', 'Windows 10 or Windows Server 2016', '/sysDictType/dropDown', 'vip.xiaonuo.sys.modular.dict.controller.SysDictTypeController', 'dropDown', 'GET', '{\"code\":\"sex\"}', '{\"code\":200,\"data\":[{\"code\":\"1\",\"value\":\"男\"},{\"code\":\"2\",\"value\":\"女\"},{\"code\":\"3\",\"value\":\"未知\"}],\"message\":\"请求成功\",\"success\":true}', '2022-10-26 16:55:07', 'superAdmin', '7dfa2ee3cc48c223aeefec5eb84e49fb62f38f95614737b724ec80942e597c28');
INSERT INTO `sys_op_log` VALUES (1585193290277560322, '系统字典类型_下拉', 5, 'Y', '成功', '127.0.0.1', '-', 'Chrome', 'Windows 10 or Windows Server 2016', '/sysDictType/dropDown', 'vip.xiaonuo.sys.modular.dict.controller.SysDictTypeController', 'dropDown', 'GET', '{\"code\":\"common_status\"}', '{\"code\":200,\"data\":[{\"code\":\"0\",\"value\":\"正常\"},{\"code\":\"1\",\"value\":\"停用\"},{\"code\":\"2\",\"value\":\"删除\"}],\"message\":\"请求成功\",\"success\":true}', '2022-10-26 16:55:07', 'superAdmin', '294f84cec80203b993609cf4eeff0849d461d1577f0a8e1f844109be4e46d859');
INSERT INTO `sys_op_log` VALUES (1585193290550190081, '系统用户_查询', 5, 'Y', '成功', '127.0.0.1', '-', 'Chrome', 'Windows 10 or Windows Server 2016', '/sysUser/page', 'vip.xiaonuo.sys.modular.user.controller.SysUserController', 'page', 'GET', '{}', '{\"code\":200,\"data\":{\"pageNo\":1,\"pageSize\":10,\"rainbow\":[1],\"rows\":[{\"account\":\"yubaoshan\",\"birthday\":718041600000,\"email\":\"await183@qq.com\",\"id\":1275735541155614721,\"name\":\"俞宝山\",\"nickName\":\"俞宝山\",\"phone\":\"1f6e48e5610ba731115d118365dede4a\",\"sex\":1,\"status\":0,\"sysEmpInfo\":{\"jobNum\":\"001\",\"orgId\":1265476890672672769,\"orgName\":\"华夏集团新疆分公司\"},\"tel\":\"\"},{\"account\":\"xuyuxiang\",\"birthday\":1593532800000,\"id\":1280709549107552257,\"name\":\"徐玉祥\",\"nickName\":\"就是那个锅\",\"phone\":\"d5437be867777b7e27893bac3a3605a2\",\"sex\":1,\"status\":0,\"sysEmpInfo\":{\"orgId\":1265476890672672770,\"orgName\":\"华夏集团成都分公司\"}},{\"account\":\"dongxiayu\",\"birthday\":1639584000000,\"id\":1471457941179072513,\"name\":\"董夏雨\",\"nickName\":\"阿董\",\"phone\":\"24c5ce408af1c56078c4d58098ebc218\",\"sex\":1,\"status\":0,\"sysEmpInfo\":{\"orgId\":1265476890672672770,\"orgName\":\"华夏集团成都分公司\"}},{\"account\":\"Admin\",\"id\":1551457178297200641,\"name\":\"测试用户\",\"phone\":\"66664efc7eecc63d93f7c1ab56c690be\",\"sex\":1,\"status\":0,\"sysEmpInfo\":{\"orgId\":1265476890651701250,\"orgName\":\"华夏集团\"}},{\"account\":\"Admin1\",\"id\":1551462379850723329,\"name\":\"Admin\",\"phone\":\"4884cc97e8656d2e7842ead0fb22fa1b\",\"sex\":1,\"status\":0,\"sysEmpInfo\":{\"orgId\":1265476890651701250,\"orgName\":\"华夏集团\"}},{\"account\":\" admin\",\"id\":1560148932795891714,\"name\":\"管理员\",\"nickName\":\"管理员\",\"phone\":\"dc9456435c3f298f662811b8f9b57d5a\",\"sex\":1,\"status\":0,\"sysEmpInfo\":{\"orgId\":1265476890651701250,\"orgName\":\"华夏集团\"}},{\"account\":\"manager\",\"id\":1560149290020569089,\"name\":\"经理\",\"nickName\":\"经理\",\"phone\":\"dc9456435c3f298f662811b8f9b57d5a\",\"sex\":1,\"status\":0,\"sysEmpInfo\":{\"orgId\":1265476890651701250,\"orgName\":\"华夏集团\"}},{\"account\":\"gleader\",\"id\":1560149788173860866,\"name\":\"小组长 \",\"phone\":\"dc9456435c3f298f662811b8f9b57d5a\",\"sex\":1,\"status\":0,\"sysEmpInfo\":{\"orgId\":1265476890651701250,\"orgName\":\"华夏集团\"}},{\"account\":\"wperson\",\"id\":1560149962497523714,\"name\":\"生产人员\",\"phone\":\"dc9456435c3f298f662811b8f9b57d5a\",\"sex\":1,\"status\":0,\"sysEmpInfo\":{\"orgId\":1265476890651701250,\"orgName\":\"华夏集团\"}},{\"account\":\"nhszcy\",\"birthday\":1664294400000,\"id\":1574927397500977154,\"name\":\"南海数字产业\",\"nickName\":\"南海数字产业\",\"phone\":\"fb0e6dadcedfb3eaca597709a5d2304c\",\"sex\":1,\"status\":0,\"sysEmpInfo\":{\"orgId\":1265476890651701250,\"orgName\":\"华夏集团\"}}],\"totalPage\":1,\"totalRows\":10},\"message\":\"请求成功\",\"success\":true}', '2022-10-26 16:55:07', 'superAdmin', '842a39a2cdfedf24c82cd870de82faf9b60cccd9feed999783f6b2c33ba283fd');
INSERT INTO `sys_op_log` VALUES (1585193307893637121, '系统用户_拥有角色', 6, 'Y', '成功', '127.0.0.1', '-', 'Chrome', 'Windows 10 or Windows Server 2016', '/sysUser/ownRole', 'vip.xiaonuo.sys.modular.user.controller.SysUserController', 'ownRole', 'GET', '{\"id\":1574927397500977154}', '{\"code\":200,\"data\":[],\"message\":\"请求成功\",\"success\":true}', '2022-10-26 16:55:11', 'superAdmin', 'bfc01069c9acd39d3cac9abfbdb9320b2023ff5065306ef75da694af580d15aa');
INSERT INTO `sys_op_log` VALUES (1585193308149489665, '系统角色_查询', 5, 'Y', '成功', '127.0.0.1', '-', 'Chrome', 'Windows 10 or Windows Server 2016', '/sysRole/page', 'vip.xiaonuo.sys.modular.role.controller.SysRoleController', 'page', 'GET', '{}', '{\"code\":200,\"data\":{\"pageNo\":1,\"pageSize\":20,\"rainbow\":[1],\"rows\":[{\"code\":\"admin\",\"createTime\":1660803289000,\"createUser\":1265476890672672808,\"dataScopeType\":1,\"id\":1560148188390817794,\"name\":\"管理员\",\"sort\":1,\"status\":0},{\"code\":\"manager\",\"createTime\":1660803311000,\"createUser\":1265476890672672808,\"dataScopeType\":1,\"id\":1560148282678771714,\"name\":\"经理\",\"sort\":2,\"status\":0,\"updateTime\":1660803349000,\"updateUser\":1265476890672672808},{\"code\":\"gleader\",\"createTime\":1660803326000,\"createUser\":1265476890672672808,\"dataScopeType\":1,\"id\":1560148341927510018,\"name\":\"小组长\",\"sort\":3,\"status\":0,\"updateTime\":1660803397000,\"updateUser\":1265476890672672808},{\"code\":\"wperson\",\"createTime\":1660803342000,\"createUser\":1265476890672672808,\"dataScopeType\":1,\"id\":1560148409690685442,\"name\":\"生产人员\",\"sort\":4,\"status\":0,\"updateTime\":1660803380000,\"updateUser\":1265476890672672808},{\"code\":\"demo\",\"createTime\":1664326782000,\"createUser\":1265476890672672808,\"dataScopeType\":1,\"id\":1574926790555828225,\"name\":\"测试\",\"sort\":100,\"status\":0},{\"code\":\"test_user\",\"createTime\":1658731403000,\"createUser\":1265476890672672808,\"dataScopeType\":1,\"id\":1551458068714385410,\"name\":\"测试用户\",\"remark\":\"测试用户\",\"sort\":100,\"status\":0},{\"code\":\"ent_manager_role\",\"createTime\":1585826846000,\"createUser\":1265476890672672808,\"dataScopeType\":1,\"id\":1265476890672672817,\"name\":\"组织架构管理员\",\"remark\":\"组织架构管理员\",\"sort\":100,\"status\":0,\"updateTime\":1656573363000,\"updateUser\":1265476890672672808},{\"code\":\"auth_role\",\"createTime\":1585826920000,\"createUser\":1265476890672672808,\"dataScopeType\":1,\"id\":1265476890672672818,\"name\":\"权限管理员\",\"remark\":\"权限管理员\",\"sort\":101,\"status\":0,\"updateTime\":1656573386000,\"updateUser\":1265476890672672808}],\"totalPage\":1,\"totalRows\":8},\"message\":\"请求成功\",\"success\":true}', '2022-10-26 16:55:11', 'superAdmin', 'edbccbd6c04fed77aaa63ac62f5d5c7f5a42c9808a2800eaadc7dd2b507839c7');
INSERT INTO `sys_op_log` VALUES (1585193319495081985, '系统用户_授权角色', 10, 'Y', '成功', '127.0.0.1', '-', 'Chrome', 'Windows 10 or Windows Server 2016', '/sysUser/grantRole', 'vip.xiaonuo.sys.modular.user.controller.SysUserController', 'grantRole', 'POST', '{\"grantRoleIdList\":[1551458068714385410],\"id\":1574927397500977154}', '{\"code\":200,\"message\":\"请求成功\",\"success\":true}', '2022-10-26 16:55:13', 'superAdmin', '4da6bc333d8dabb00b6f4979a77d29718290c1bc40d8409d174c1ce7594cd575');
INSERT INTO `sys_op_log` VALUES (1585193321395101698, '系统用户_查询', 5, 'Y', '成功', '127.0.0.1', '-', 'Chrome', 'Windows 10 or Windows Server 2016', '/sysUser/page', 'vip.xiaonuo.sys.modular.user.controller.SysUserController', 'page', 'GET', '{}', '{\"code\":200,\"data\":{\"pageNo\":1,\"pageSize\":10,\"rainbow\":[1],\"rows\":[{\"account\":\"yubaoshan\",\"birthday\":718041600000,\"email\":\"await183@qq.com\",\"id\":1275735541155614721,\"name\":\"俞宝山\",\"nickName\":\"俞宝山\",\"phone\":\"1f6e48e5610ba731115d118365dede4a\",\"sex\":1,\"status\":0,\"sysEmpInfo\":{\"jobNum\":\"001\",\"orgId\":1265476890672672769,\"orgName\":\"华夏集团新疆分公司\"},\"tel\":\"\"},{\"account\":\"xuyuxiang\",\"birthday\":1593532800000,\"id\":1280709549107552257,\"name\":\"徐玉祥\",\"nickName\":\"就是那个锅\",\"phone\":\"d5437be867777b7e27893bac3a3605a2\",\"sex\":1,\"status\":0,\"sysEmpInfo\":{\"orgId\":1265476890672672770,\"orgName\":\"华夏集团成都分公司\"}},{\"account\":\"dongxiayu\",\"birthday\":1639584000000,\"id\":1471457941179072513,\"name\":\"董夏雨\",\"nickName\":\"阿董\",\"phone\":\"24c5ce408af1c56078c4d58098ebc218\",\"sex\":1,\"status\":0,\"sysEmpInfo\":{\"orgId\":1265476890672672770,\"orgName\":\"华夏集团成都分公司\"}},{\"account\":\"Admin\",\"id\":1551457178297200641,\"name\":\"测试用户\",\"phone\":\"66664efc7eecc63d93f7c1ab56c690be\",\"sex\":1,\"status\":0,\"sysEmpInfo\":{\"orgId\":1265476890651701250,\"orgName\":\"华夏集团\"}},{\"account\":\"Admin1\",\"id\":1551462379850723329,\"name\":\"Admin\",\"phone\":\"4884cc97e8656d2e7842ead0fb22fa1b\",\"sex\":1,\"status\":0,\"sysEmpInfo\":{\"orgId\":1265476890651701250,\"orgName\":\"华夏集团\"}},{\"account\":\" admin\",\"id\":1560148932795891714,\"name\":\"管理员\",\"nickName\":\"管理员\",\"phone\":\"dc9456435c3f298f662811b8f9b57d5a\",\"sex\":1,\"status\":0,\"sysEmpInfo\":{\"orgId\":1265476890651701250,\"orgName\":\"华夏集团\"}},{\"account\":\"manager\",\"id\":1560149290020569089,\"name\":\"经理\",\"nickName\":\"经理\",\"phone\":\"dc9456435c3f298f662811b8f9b57d5a\",\"sex\":1,\"status\":0,\"sysEmpInfo\":{\"orgId\":1265476890651701250,\"orgName\":\"华夏集团\"}},{\"account\":\"gleader\",\"id\":1560149788173860866,\"name\":\"小组长 \",\"phone\":\"dc9456435c3f298f662811b8f9b57d5a\",\"sex\":1,\"status\":0,\"sysEmpInfo\":{\"orgId\":1265476890651701250,\"orgName\":\"华夏集团\"}},{\"account\":\"wperson\",\"id\":1560149962497523714,\"name\":\"生产人员\",\"phone\":\"dc9456435c3f298f662811b8f9b57d5a\",\"sex\":1,\"status\":0,\"sysEmpInfo\":{\"orgId\":1265476890651701250,\"orgName\":\"华夏集团\"}},{\"account\":\"nhszcy\",\"birthday\":1664294400000,\"id\":1574927397500977154,\"name\":\"南海数字产业\",\"nickName\":\"南海数字产业\",\"phone\":\"fb0e6dadcedfb3eaca597709a5d2304c\",\"sex\":1,\"status\":0,\"sysEmpInfo\":{\"orgId\":1265476890651701250,\"orgName\":\"华夏集团\"}}],\"totalPage\":1,\"totalRows\":10},\"message\":\"请求成功\",\"success\":true}', '2022-10-26 16:55:14', 'superAdmin', '014fe5fc652b8d7c92f6b98fb87d49d199caac2f5c9782b85bcaf8e30f040479');

-- ----------------------------
-- Table structure for sys_org
-- ----------------------------
DROP TABLE IF EXISTS `sys_org`;
CREATE TABLE `sys_org`  (
                            `id` bigint(20) NOT NULL COMMENT '主键',
                            `pid` bigint(20) NOT NULL COMMENT '父id',
                            `pids` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '父ids',
                            `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
                            `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '编码',
                            `sort` int(11) NOT NULL COMMENT '排序',
                            `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
                            `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '状态（字典 0正常 1停用 2删除）',
                            `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                            `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                            `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                            `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                            PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统组织机构表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_org
-- ----------------------------
INSERT INTO `sys_org` VALUES (1265476890651701250, 0, '[0],', '华夏集团', 'hxjt', 100, '华夏集团总公司', 0, '2020-03-26 16:50:53', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_org` VALUES (1265476890672672769, 1265476890651701250, '[0],[1265476890651701250],', '华夏集团乌鲁木齐分公司', 'hxjt_wlmq', 100, '华夏集团乌鲁木齐分公司', 0, '2020-03-26 16:55:42', 1265476890672672808, '2021-12-16 20:29:38', 1265476890672672808);
INSERT INTO `sys_org` VALUES (1265476890672672770, 1265476890651701250, '[0],[1265476890651701250],', '华夏集团成都分公司', 'hxjt_cd', 100, '华夏集团成都分公司', 0, '2020-03-26 16:56:02', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_org` VALUES (1265476890672672771, 1265476890672672769, '[0],[1265476890651701250],[1265476890672672769],', '研发部', 'hxjt_wlmq_yfb', 100, '华夏集团乌鲁木齐分公司研发部', 0, '2020-03-26 16:56:36', 1265476890672672808, '2021-12-16 20:29:38', 1265476890672672808);
INSERT INTO `sys_org` VALUES (1265476890672672772, 1265476890672672769, '[0],[1265476890651701250],[1265476890672672769],', '企划部', 'hxjt_wlmq_qhb', 100, '华夏集团乌鲁木齐分公司企划部', 0, '2020-03-26 16:57:06', 1265476890672672808, '2021-12-16 20:29:38', 1265476890672672808);
INSERT INTO `sys_org` VALUES (1265476890672672773, 1265476890672672770, '[0],[1265476890651701250],[1265476890672672770],', '市场部', 'hxjt_cd_scb', 100, '华夏集团成都分公司市场部', 0, '2020-03-26 16:57:35', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_org` VALUES (1265476890672672774, 1265476890672672770, '[0],[1265476890651701250],[1265476890672672770],', '财务部', 'hxjt_cd_cwb', 100, '华夏集团成都分公司财务部', 0, '2020-03-26 16:58:01', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_org` VALUES (1265476890672672775, 1265476890672672773, '[0],[1265476890651701250],[1265476890672672770],[1265476890672672773],', '市场部二部', 'hxjt_cd_scb_2b', 100, '华夏集团成都分公司市场部二部', 0, '2020-04-06 15:36:50', 1265476890672672808, NULL, NULL);

-- ----------------------------
-- Table structure for sys_pos
-- ----------------------------
DROP TABLE IF EXISTS `sys_pos`;
CREATE TABLE `sys_pos`  (
                            `id` bigint(20) NOT NULL COMMENT '主键',
                            `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
                            `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '编码',
                            `sort` int(11) NOT NULL COMMENT '排序',
                            `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
                            `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '状态（字典 0正常 1停用 2删除）',
                            `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                            `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                            `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                            `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                            PRIMARY KEY (`id`) USING BTREE,
                            UNIQUE INDEX `CODE_UNI`(`code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统职位表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_pos
-- ----------------------------
INSERT INTO `sys_pos` VALUES (1265476890672672787, '总经理', 'zjl', 100, '总经理职位', 0, '2020-03-26 19:28:54', 1265476890672672808, '2020-06-02 21:01:04', 1265476890672672808);
INSERT INTO `sys_pos` VALUES (1265476890672672788, '副总经理', 'fzjl', 100, '副总经理职位', 0, '2020-03-26 19:29:57', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_pos` VALUES (1265476890672672789, '部门经理', 'bmjl', 100, '部门经理职位', 0, '2020-03-26 19:31:49', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_pos` VALUES (1265476890672672790, '工作人员', 'gzry', 100, '工作人员职位', 0, '2020-05-27 11:32:00', 1265476890672672808, '2020-06-01 10:51:35', 1265476890672672808);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
                             `id` bigint(20) NOT NULL COMMENT '主键id',
                             `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
                             `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '编码',
                             `sort` int(11) NOT NULL COMMENT '序号',
                             `data_scope_type` tinyint(4) NOT NULL DEFAULT 1 COMMENT '数据范围类型（字典 1全部数据 2本部门及以下数据 3本部门数据 4仅本人数据 5自定义数据）',
                             `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
                             `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '状态（字典 0正常 1停用 2删除）',
                             `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                             `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                             `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                             `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                             PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统角色表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1265476890672672817, '组织架构管理员', 'ent_manager_role', 100, 1, '组织架构管理员', 0, '2020-04-02 19:27:26', 1265476890672672808, '2022-06-30 15:16:03', 1265476890672672808);
INSERT INTO `sys_role` VALUES (1265476890672672818, '权限管理员', 'auth_role', 101, 1, '权限管理员', 0, '2020-04-02 19:28:40', 1265476890672672808, '2022-06-30 15:16:26', 1265476890672672808);
INSERT INTO `sys_role` VALUES (1551458068714385410, '测试用户', 'test_user', 100, 1, '测试用户', 0, '2022-07-25 14:43:23', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_role` VALUES (1560148188390817794, '管理员', 'admin', 1, 1, NULL, 0, '2022-08-18 14:14:49', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_role` VALUES (1560148282678771714, '经理', 'manager', 2, 1, NULL, 0, '2022-08-18 14:15:11', 1265476890672672808, '2022-08-18 14:15:49', 1265476890672672808);
INSERT INTO `sys_role` VALUES (1560148341927510018, '小组长', 'gleader', 3, 1, NULL, 0, '2022-08-18 14:15:26', 1265476890672672808, '2022-08-18 14:16:37', 1265476890672672808);
INSERT INTO `sys_role` VALUES (1560148409690685442, '生产人员', 'wperson', 4, 1, NULL, 0, '2022-08-18 14:15:42', 1265476890672672808, '2022-08-18 14:16:20', 1265476890672672808);
INSERT INTO `sys_role` VALUES (1574926790555828225, '测试', 'demo', 100, 1, NULL, 0, '2022-09-28 08:59:42', 1265476890672672808, NULL, NULL);

-- ----------------------------
-- Table structure for sys_role_data_scope
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_data_scope`;
CREATE TABLE `sys_role_data_scope`  (
                                        `id` bigint(20) NOT NULL COMMENT '主键',
                                        `role_id` bigint(20) NOT NULL COMMENT '角色id',
                                        `org_id` bigint(20) NOT NULL COMMENT '机构id',
                                        PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统角色数据范围表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_role_data_scope
-- ----------------------------
INSERT INTO `sys_role_data_scope` VALUES (1292060127645429762, 1265476890672672819, 1265476890672672774);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
                                  `id` bigint(20) NOT NULL COMMENT '主键',
                                  `role_id` bigint(20) NOT NULL COMMENT '角色id',
                                  `menu_id` bigint(20) NOT NULL COMMENT '菜单id',
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统角色菜单表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (1542410592785072129, 1265476890672672817, 1264622039642255331);
INSERT INTO `sys_role_menu` VALUES (1542410592818626561, 1265476890672672817, 1264622039642255311);
INSERT INTO `sys_role_menu` VALUES (1542410592839598081, 1265476890672672817, 1264622039642255321);
INSERT INTO `sys_role_menu` VALUES (1542410592868958210, 1265476890672672817, 1264622039642255361);
INSERT INTO `sys_role_menu` VALUES (1542410592885735425, 1265476890672672817, 1264622039642255351);
INSERT INTO `sys_role_menu` VALUES (1542410592919289857, 1265476890672672817, 1264622039642255341);
INSERT INTO `sys_role_menu` VALUES (1542410592936067073, 1265476890672672817, 1264622039642255371);
INSERT INTO `sys_role_menu` VALUES (1542410592961232898, 1265476890672672817, 1264622039642255381);
INSERT INTO `sys_role_menu` VALUES (1542410592982204418, 1265476890672672817, 1264622039642255391);
INSERT INTO `sys_role_menu` VALUES (1542410593007370242, 1265476890672672817, 1264622039642255401);
INSERT INTO `sys_role_menu` VALUES (1542410593032536065, 1265476890672672817, 1264622039642255411);
INSERT INTO `sys_role_menu` VALUES (1542410593053507586, 1265476890672672817, 1264622039642255421);
INSERT INTO `sys_role_menu` VALUES (1542410593103839234, 1265476890672672817, 1264622039642255431);
INSERT INTO `sys_role_menu` VALUES (1542410593129005058, 1265476890672672817, 1264622039642255441);
INSERT INTO `sys_role_menu` VALUES (1542410593154170882, 1265476890672672817, 1264622039642255451);
INSERT INTO `sys_role_menu` VALUES (1542410593179336706, 1265476890672672817, 1264622039642255461);
INSERT INTO `sys_role_menu` VALUES (1542410593204502529, 1265476890672672817, 1264622039642255471);
INSERT INTO `sys_role_menu` VALUES (1542410593225474049, 1265476890672672817, 1264622039642255481);
INSERT INTO `sys_role_menu` VALUES (1542410593246445570, 1265476890672672817, 1264622039642255491);
INSERT INTO `sys_role_menu` VALUES (1542410593271611394, 1265476890672672817, 1264622039642255501);
INSERT INTO `sys_role_menu` VALUES (1542410593296777217, 1265476890672672817, 1264622039642255511);
INSERT INTO `sys_role_menu` VALUES (1542410593309360129, 1265476890672672817, 1264622039642255531);
INSERT INTO `sys_role_menu` VALUES (1542410593342914561, 1265476890672672817, 1264622039642255521);
INSERT INTO `sys_role_menu` VALUES (1542410593363886082, 1265476890672672817, 1264622039642255541);
INSERT INTO `sys_role_menu` VALUES (1542410593389051906, 1265476890672672817, 1264622039642255551);
INSERT INTO `sys_role_menu` VALUES (1542410593414217730, 1265476890672672817, 1264622039642255561);
INSERT INTO `sys_role_menu` VALUES (1542410593439383553, 1265476890672672817, 1264622039642255571);
INSERT INTO `sys_role_menu` VALUES (1542410593456160769, 1265476890672672817, 1264622039642255581);
INSERT INTO `sys_role_menu` VALUES (1542410593502298114, 1265476890672672817, 1264622039642255591);
INSERT INTO `sys_role_menu` VALUES (1542410593519075329, 1265476890672672817, 1264622039642255621);
INSERT INTO `sys_role_menu` VALUES (1542410593544241154, 1265476890672672817, 1264622039642255601);
INSERT INTO `sys_role_menu` VALUES (1542410593590378497, 1265476890672672817, 1264622039642255631);
INSERT INTO `sys_role_menu` VALUES (1542410593611350017, 1265476890672672817, 1264622039642255641);
INSERT INTO `sys_role_menu` VALUES (1542410593636515842, 1265476890672672817, 1264622039642255651);
INSERT INTO `sys_role_menu` VALUES (1542410593661681666, 1265476890672672817, 1264622039642255661);
INSERT INTO `sys_role_menu` VALUES (1542410593682653186, 1265476890672672817, 1264622039642255611);
INSERT INTO `sys_role_menu` VALUES (1542410593699430401, 1265476890672672817, 1264622039642255911);
INSERT INTO `sys_role_menu` VALUES (1542410593720401921, 1265476890672672817, 7639361031662168624);
INSERT INTO `sys_role_menu` VALUES (1542410593737179138, 1265476890672672817, 6507903030541782366);
INSERT INTO `sys_role_menu` VALUES (1542410593770733569, 1265476890672672817, 8562653372099681846);
INSERT INTO `sys_role_menu` VALUES (1542410593787510785, 1265476890672672817, 4830986669641223230);
INSERT INTO `sys_role_menu` VALUES (1542410593812676609, 1265476890672672817, 7974141988632466544);
INSERT INTO `sys_role_menu` VALUES (1542410593829453825, 1265476890672672817, 8313819116019873398);
INSERT INTO `sys_role_menu` VALUES (1542410593854619650, 1265476890672672817, 8931538668430408569);
INSERT INTO `sys_role_menu` VALUES (1542410593879785473, 1265476890672672817, 6122595225588020183);
INSERT INTO `sys_role_menu` VALUES (1542410593900756993, 1265476890672672817, 7656571819576826411);
INSERT INTO `sys_role_menu` VALUES (1542410593934311426, 1265476890672672817, 9202857208449608897);
INSERT INTO `sys_role_menu` VALUES (1542410593951088642, 1265476890672672817, 5014700545593967407);
INSERT INTO `sys_role_menu` VALUES (1542410593972060161, 1265476890672672817, 8088901678692869692);
INSERT INTO `sys_role_menu` VALUES (1542410593997225985, 1265476890672672817, 6299920358694835309);
INSERT INTO `sys_role_menu` VALUES (1542410594022391809, 1265476890672672817, 5642803668063215805);
INSERT INTO `sys_role_menu` VALUES (1542410594039169025, 1265476890672672817, 8834719374925492199);
INSERT INTO `sys_role_menu` VALUES (1542410594064334850, 1265476890672672817, 7882898168203081196);
INSERT INTO `sys_role_menu` VALUES (1542410594081112066, 1265476890672672817, 8959792232643188225);
INSERT INTO `sys_role_menu` VALUES (1542410594102083585, 1265476890672672817, 6667327679590719676);
INSERT INTO `sys_role_menu` VALUES (1542410594123055105, 1265476890672672817, 5668705457330168859);
INSERT INTO `sys_role_menu` VALUES (1542410594139832321, 1265476890672672817, 9180800170765745954);
INSERT INTO `sys_role_menu` VALUES (1542410594169192449, 1265476890672672817, 5327083612717101630);
INSERT INTO `sys_role_menu` VALUES (1542410594248884225, 1265476890672672817, 6329171771009724483);
INSERT INTO `sys_role_menu` VALUES (1542410594282438657, 1265476890672672817, 6020421073902938241);
INSERT INTO `sys_role_menu` VALUES (1542410594311798786, 1265476890672672817, 5886355131876064420);
INSERT INTO `sys_role_menu` VALUES (1542410594378907649, 1265476890672672817, 9208275275751949837);
INSERT INTO `sys_role_menu` VALUES (1542410594412462081, 1265476890672672817, 9173957282399717676);
INSERT INTO `sys_role_menu` VALUES (1542410594429239297, 1265476890672672817, 8973112451110891359);
INSERT INTO `sys_role_menu` VALUES (1542410594454405122, 1265476890672672817, 5753930915061776016);
INSERT INTO `sys_role_menu` VALUES (1542410594471182338, 1265476890672672817, 7288710125399936914);
INSERT INTO `sys_role_menu` VALUES (1542410594496348162, 1265476890672672817, 9022874385071881655);
INSERT INTO `sys_role_menu` VALUES (1542410594513125377, 1265476890672672817, 5378908852735764919);
INSERT INTO `sys_role_menu` VALUES (1542410594559262722, 1265476890672672817, 6449912099524079616);
INSERT INTO `sys_role_menu` VALUES (1542410594584428546, 1265476890672672817, 4998978684524443692);
INSERT INTO `sys_role_menu` VALUES (1542410594609594370, 1265476890672672817, 1534384317233524738);
INSERT INTO `sys_role_menu` VALUES (1542410594634760194, 1265476890672672817, 5707993274387362828);
INSERT INTO `sys_role_menu` VALUES (1542410594651537410, 1265476890672672817, 8275137972166495794);
INSERT INTO `sys_role_menu` VALUES (1542410594672508929, 1265476890672672817, 6249597959063106635);
INSERT INTO `sys_role_menu` VALUES (1542410594685091842, 1265476890672672817, 6905244625723471705);
INSERT INTO `sys_role_menu` VALUES (1542410594710257665, 1265476890672672817, 6709768906758836391);
INSERT INTO `sys_role_menu` VALUES (1542410594731229186, 1265476890672672817, 7388361180917918171);
INSERT INTO `sys_role_menu` VALUES (1542410594748006401, 1265476890672672817, 6570253563256899359);
INSERT INTO `sys_role_menu` VALUES (1542410594781560833, 1265476890672672817, 6527167616560933135);
INSERT INTO `sys_role_menu` VALUES (1542410594806726658, 1265476890672672817, 6547301851685353031);
INSERT INTO `sys_role_menu` VALUES (1542410594836086786, 1265476890672672817, 8197158197771689059);
INSERT INTO `sys_role_menu` VALUES (1542410594848669698, 1265476890672672817, 9222389351831596218);
INSERT INTO `sys_role_menu` VALUES (1542410594882224130, 1265476890672672817, 5142735407151148234);
INSERT INTO `sys_role_menu` VALUES (1542410594899001345, 1265476890672672817, 7621956982866257638);
INSERT INTO `sys_role_menu` VALUES (1542410594932555777, 1265476890672672817, 5553078347948343545);
INSERT INTO `sys_role_menu` VALUES (1542410594957721602, 1265476890672672817, 8315301036989682205);
INSERT INTO `sys_role_menu` VALUES (1542410594982887426, 1265476890672672817, 6436044181834026359);
INSERT INTO `sys_role_menu` VALUES (1542410595008053250, 1265476890672672817, 8618504262472492350);
INSERT INTO `sys_role_menu` VALUES (1542410595024830465, 1265476890672672817, 7950819887425681478);
INSERT INTO `sys_role_menu` VALUES (1542410595049996289, 1265476890672672817, 4832107133629024589);
INSERT INTO `sys_role_menu` VALUES (1542410595075162114, 1265476890672672817, 7946795106595000695);
INSERT INTO `sys_role_menu` VALUES (1542410595091939329, 1265476890672672817, 5279583233581044419);
INSERT INTO `sys_role_menu` VALUES (1542410595108716546, 1265476890672672817, 6845666717193712873);
INSERT INTO `sys_role_menu` VALUES (1542410595125493762, 1265476890672672817, 5243613315131080710);
INSERT INTO `sys_role_menu` VALUES (1542410595142270977, 1265476890672672817, 7702400310012060990);
INSERT INTO `sys_role_menu` VALUES (1542410595167436802, 1265476890672672817, 5079108238558434584);
INSERT INTO `sys_role_menu` VALUES (1542410595184214017, 1265476890672672817, 8562547749529140265);
INSERT INTO `sys_role_menu` VALUES (1542410595217768450, 1265476890672672817, 5788080164048630938);
INSERT INTO `sys_role_menu` VALUES (1542410595234545665, 1265476890672672817, 7936411315868128942);
INSERT INTO `sys_role_menu` VALUES (1542410595259711489, 1265476890672672817, 7940657177277675482);
INSERT INTO `sys_role_menu` VALUES (1542410595276488706, 1265476890672672817, 6194217070047486945);
INSERT INTO `sys_role_menu` VALUES (1542410595301654530, 1265476890672672817, 6103813152661113624);
INSERT INTO `sys_role_menu` VALUES (1542410595331014658, 1265476890672672817, 8426505795261326687);
INSERT INTO `sys_role_menu` VALUES (1542410595368763394, 1265476890672672817, 6374585520258378360);
INSERT INTO `sys_role_menu` VALUES (1542410595389734913, 1265476890672672817, 8621765388717143178);
INSERT INTO `sys_role_menu` VALUES (1542410595414900737, 1265476890672672817, 6116805160244737678);
INSERT INTO `sys_role_menu` VALUES (1542410595440066561, 1265476890672672817, 6860312372874164125);
INSERT INTO `sys_role_menu` VALUES (1542410595473620994, 1265476890672672817, 5820472386138045917);
INSERT INTO `sys_role_menu` VALUES (1542410595498786818, 1265476890672672817, 8181822590852115190);
INSERT INTO `sys_role_menu` VALUES (1542410595523952642, 1265476890672672817, 7346283523793183379);
INSERT INTO `sys_role_menu` VALUES (1542410595540729858, 1265476890672672817, 1542409869531873282);
INSERT INTO `sys_role_menu` VALUES (1542410595570089985, 1265476890672672817, 1534410297847214081);
INSERT INTO `sys_role_menu` VALUES (1542410595599450114, 1265476890672672817, 1264622039642255851);
INSERT INTO `sys_role_menu` VALUES (1542410595624615937, 1265476890672672817, 1264622039642255671);
INSERT INTO `sys_role_menu` VALUES (1549008510768009218, 1265476890672672818, 7656571819576826411);
INSERT INTO `sys_role_menu` VALUES (1549008510768009219, 1265476890672672818, 9202857208449608897);
INSERT INTO `sys_role_menu` VALUES (1549008510768009220, 1265476890672672818, 5014700545593967407);
INSERT INTO `sys_role_menu` VALUES (1549008510768009221, 1265476890672672818, 8088901678692869692);
INSERT INTO `sys_role_menu` VALUES (1549008510768009222, 1265476890672672818, 6299920358694835309);
INSERT INTO `sys_role_menu` VALUES (1549008510768009223, 1265476890672672818, 5642803668063215805);
INSERT INTO `sys_role_menu` VALUES (1549008510768009224, 1265476890672672818, 8834719374925492199);
INSERT INTO `sys_role_menu` VALUES (1549008510768009225, 1265476890672672818, 7882898168203081196);
INSERT INTO `sys_role_menu` VALUES (1549008510768009226, 1265476890672672818, 1534410297847214081);
INSERT INTO `sys_role_menu` VALUES (1560150302139678722, 1560148188390817794, 1560145190851772417);
INSERT INTO `sys_role_menu` VALUES (1560150302194204674, 1560148188390817794, 256);
INSERT INTO `sys_role_menu` VALUES (1560150302261313538, 1560148188390817794, 1);
INSERT INTO `sys_role_menu` VALUES (1560150302282285058, 1560148188390817794, 257);
INSERT INTO `sys_role_menu` VALUES (1560150302307450881, 1560148188390817794, 2);
INSERT INTO `sys_role_menu` VALUES (1560150302403919873, 1560148188390817794, 258);
INSERT INTO `sys_role_menu` VALUES (1560150302487805954, 1560148188390817794, 3);
INSERT INTO `sys_role_menu` VALUES (1560150302529748993, 1560148188390817794, 259);
INSERT INTO `sys_role_menu` VALUES (1560150304727564290, 1560148188390817794, 4);
INSERT INTO `sys_role_menu` VALUES (1560150304849199105, 1560148188390817794, 260);
INSERT INTO `sys_role_menu` VALUES (1560150304899530754, 1560148188390817794, 5);
INSERT INTO `sys_role_menu` VALUES (1560150304949862401, 1560148188390817794, 261);
INSERT INTO `sys_role_menu` VALUES (1560150304962445313, 1560148188390817794, 6);
INSERT INTO `sys_role_menu` VALUES (1560150305029554178, 1560148188390817794, 262);
INSERT INTO `sys_role_menu` VALUES (1560150305054720001, 1560148188390817794, 7);
INSERT INTO `sys_role_menu` VALUES (1560150305100857345, 1560148188390817794, 263);
INSERT INTO `sys_role_menu` VALUES (1560150305155383297, 1560148188390817794, 8);
INSERT INTO `sys_role_menu` VALUES (1560150305222492161, 1560148188390817794, 264);
INSERT INTO `sys_role_menu` VALUES (1560150305251852289, 1560148188390817794, 9);
INSERT INTO `sys_role_menu` VALUES (1560150305281212417, 1560148188390817794, 265);
INSERT INTO `sys_role_menu` VALUES (1560150306216542210, 1560148188390817794, 10);
INSERT INTO `sys_role_menu` VALUES (1560150306602418177, 1560148188390817794, 266);
INSERT INTO `sys_role_menu` VALUES (1560150306656944130, 1560148188390817794, 11);
INSERT INTO `sys_role_menu` VALUES (1560150306677915650, 1560148188390817794, 267);
INSERT INTO `sys_role_menu` VALUES (1560150307609051138, 1560148188390817794, 12);
INSERT INTO `sys_role_menu` VALUES (1560150307671965697, 1560148188390817794, 268);
INSERT INTO `sys_role_menu` VALUES (1560150307671965698, 1560148188390817794, 13);
INSERT INTO `sys_role_menu` VALUES (1560150307739074562, 1560148188390817794, 269);
INSERT INTO `sys_role_menu` VALUES (1560150307739074563, 1560148188390817794, 14);
INSERT INTO `sys_role_menu` VALUES (1560150307801989121, 1560148188390817794, 270);
INSERT INTO `sys_role_menu` VALUES (1560150307869097986, 1560148188390817794, 15);
INSERT INTO `sys_role_menu` VALUES (1560150307869097987, 1560148188390817794, 271);
INSERT INTO `sys_role_menu` VALUES (1560150308066230273, 1560148188390817794, 16);
INSERT INTO `sys_role_menu` VALUES (1560150308401774593, 1560148188390817794, 272);
INSERT INTO `sys_role_menu` VALUES (1560150310654115842, 1560148188390817794, 17);
INSERT INTO `sys_role_menu` VALUES (1560150315410456577, 1560148188390817794, 273);
INSERT INTO `sys_role_menu` VALUES (1560150315410456578, 1560148188390817794, 18);
INSERT INTO `sys_role_menu` VALUES (1560150316349980674, 1560148188390817794, 274);
INSERT INTO `sys_role_menu` VALUES (1560150317285310466, 1560148188390817794, 19);
INSERT INTO `sys_role_menu` VALUES (1560150318208057346, 1560148188390817794, 275);
INSERT INTO `sys_role_menu` VALUES (1560150319126609921, 1560148188390817794, 20);
INSERT INTO `sys_role_menu` VALUES (1560150320124854273, 1560148188390817794, 276);
INSERT INTO `sys_role_menu` VALUES (1560150320133242882, 1560148188390817794, 21);
INSERT INTO `sys_role_menu` VALUES (1560150320158408705, 1560148188390817794, 277);
INSERT INTO `sys_role_menu` VALUES (1560150320204546049, 1560148188390817794, 22);
INSERT INTO `sys_role_menu` VALUES (1560150320225517570, 1560148188390817794, 278);
INSERT INTO `sys_role_menu` VALUES (1560150320238100482, 1560148188390817794, 23);
INSERT INTO `sys_role_menu` VALUES (1560150321135681538, 1560148188390817794, 279);
INSERT INTO `sys_role_menu` VALUES (1560150321194401793, 1560148188390817794, 24);
INSERT INTO `sys_role_menu` VALUES (1560150321206984705, 1560148188390817794, 280);
INSERT INTO `sys_role_menu` VALUES (1560150321261510658, 1560148188390817794, 25);
INSERT INTO `sys_role_menu` VALUES (1560150321303453697, 1560148188390817794, 281);
INSERT INTO `sys_role_menu` VALUES (1560150321370562561, 1560148188390817794, 26);
INSERT INTO `sys_role_menu` VALUES (1560150321441865730, 1560148188390817794, 282);
INSERT INTO `sys_role_menu` VALUES (1560150321471225857, 1560148188390817794, 27);
INSERT INTO `sys_role_menu` VALUES (1560150321504780289, 1560148188390817794, 283);
INSERT INTO `sys_role_menu` VALUES (1560150321622220801, 1560148188390817794, 28);
INSERT INTO `sys_role_menu` VALUES (1560150321731272706, 1560148188390817794, 284);
INSERT INTO `sys_role_menu` VALUES (1560150321789992962, 1560148188390817794, 29);
INSERT INTO `sys_role_menu` VALUES (1560150321840324610, 1560148188390817794, 285);
INSERT INTO `sys_role_menu` VALUES (1560150321911627777, 1560148188390817794, 30);
INSERT INTO `sys_role_menu` VALUES (1560150321928404994, 1560148188390817794, 286);
INSERT INTO `sys_role_menu` VALUES (1560150321945182209, 1560148188390817794, 31);
INSERT INTO `sys_role_menu` VALUES (1560150321995513857, 1560148188390817794, 287);
INSERT INTO `sys_role_menu` VALUES (1560150323295748097, 1560148188390817794, 32);
INSERT INTO `sys_role_menu` VALUES (1560150325254488065, 1560148188390817794, 288);
INSERT INTO `sys_role_menu` VALUES (1560150325489369090, 1560148188390817794, 33);
INSERT INTO `sys_role_menu` VALUES (1560150325665529857, 1560148188390817794, 289);
INSERT INTO `sys_role_menu` VALUES (1560150325728444418, 1560148188390817794, 34);
INSERT INTO `sys_role_menu` VALUES (1560150330858078210, 1560148188390817794, 290);
INSERT INTO `sys_role_menu` VALUES (1560150332984590338, 1560148188390817794, 35);
INSERT INTO `sys_role_menu` VALUES (1560150336079986690, 1560148188390817794, 291);
INSERT INTO `sys_role_menu` VALUES (1560150341641633793, 1560148188390817794, 36);
INSERT INTO `sys_role_menu` VALUES (1560150341838766081, 1560148188390817794, 292);
INSERT INTO `sys_role_menu` VALUES (1560150341977178114, 1560148188390817794, 37);
INSERT INTO `sys_role_menu` VALUES (1560150343289995265, 1560148188390817794, 293);
INSERT INTO `sys_role_menu` VALUES (1560150343357104130, 1560148188390817794, 38);
INSERT INTO `sys_role_menu` VALUES (1560150343420018690, 1560148188390817794, 294);
INSERT INTO `sys_role_menu` VALUES (1560150343487127554, 1560148188390817794, 39);
INSERT INTO `sys_role_menu` VALUES (1560150344472788994, 1560148188390817794, 295);
INSERT INTO `sys_role_menu` VALUES (1560150344539897857, 1560148188390817794, 40);
INSERT INTO `sys_role_menu` VALUES (1560150344539897858, 1560148188390817794, 296);
INSERT INTO `sys_role_menu` VALUES (1560150344728641538, 1560148188390817794, 41);
INSERT INTO `sys_role_menu` VALUES (1560150345970155521, 1560148188390817794, 297);
INSERT INTO `sys_role_menu` VALUES (1560150346708353025, 1560148188390817794, 42);
INSERT INTO `sys_role_menu` VALUES (1560150346720935937, 1560148188390817794, 298);
INSERT INTO `sys_role_menu` VALUES (1560150346767073282, 1560148188390817794, 43);
INSERT INTO `sys_role_menu` VALUES (1560150346871930881, 1560148188390817794, 299);
INSERT INTO `sys_role_menu` VALUES (1560150346905485313, 1560148188390817794, 44);
INSERT INTO `sys_role_menu` VALUES (1560150346922262530, 1560148188390817794, 300);
INSERT INTO `sys_role_menu` VALUES (1560150346951622657, 1560148188390817794, 45);
INSERT INTO `sys_role_menu` VALUES (1560150346972594177, 1560148188390817794, 301);
INSERT INTO `sys_role_menu` VALUES (1560150346989371393, 1560148188390817794, 46);
INSERT INTO `sys_role_menu` VALUES (1560150347006148609, 1560148188390817794, 302);
INSERT INTO `sys_role_menu` VALUES (1560150347048091650, 1560148188390817794, 47);
INSERT INTO `sys_role_menu` VALUES (1560150347060674561, 1560148188390817794, 303);
INSERT INTO `sys_role_menu` VALUES (1560150347069063169, 1560148188390817794, 48);
INSERT INTO `sys_role_menu` VALUES (1560150347379441666, 1560148188390817794, 304);
INSERT INTO `sys_role_menu` VALUES (1560150347450744833, 1560148188390817794, 49);
INSERT INTO `sys_role_menu` VALUES (1560150347496882178, 1560148188390817794, 305);
INSERT INTO `sys_role_menu` VALUES (1560150347522048002, 1560148188390817794, 50);
INSERT INTO `sys_role_menu` VALUES (1560150347576573953, 1560148188390817794, 306);
INSERT INTO `sys_role_menu` VALUES (1560150347618516994, 1560148188390817794, 51);
INSERT INTO `sys_role_menu` VALUES (1560150347794677761, 1560148188390817794, 307);
INSERT INTO `sys_role_menu` VALUES (1560150347828232193, 1560148188390817794, 52);
INSERT INTO `sys_role_menu` VALUES (1560150347840815106, 1560148188390817794, 308);
INSERT INTO `sys_role_menu` VALUES (1560150347895341058, 1560148188390817794, 53);
INSERT INTO `sys_role_menu` VALUES (1560150347912118274, 1560148188390817794, 309);
INSERT INTO `sys_role_menu` VALUES (1560150347920506881, 1560148188390817794, 54);
INSERT INTO `sys_role_menu` VALUES (1560150347941478402, 1560148188390817794, 55);
INSERT INTO `sys_role_menu` VALUES (1560150347996004354, 1560148188390817794, 56);
INSERT INTO `sys_role_menu` VALUES (1560150348037947393, 1560148188390817794, 57);
INSERT INTO `sys_role_menu` VALUES (1560150348050530305, 1560148188390817794, 58);
INSERT INTO `sys_role_menu` VALUES (1560150348608372737, 1560148188390817794, 59);
INSERT INTO `sys_role_menu` VALUES (1560150349124272130, 1560148188390817794, 60);
INSERT INTO `sys_role_menu` VALUES (1560150349484982274, 1560148188390817794, 61);
INSERT INTO `sys_role_menu` VALUES (1560150349640171522, 1560148188390817794, 62);
INSERT INTO `sys_role_menu` VALUES (1560150349715668994, 1560148188390817794, 63);
INSERT INTO `sys_role_menu` VALUES (1560150349745029121, 1560148188390817794, 64);
INSERT INTO `sys_role_menu` VALUES (1560150349791166466, 1560148188390817794, 65);
INSERT INTO `sys_role_menu` VALUES (1560150349803749378, 1560148188390817794, 66);
INSERT INTO `sys_role_menu` VALUES (1560150349812137986, 1560148188390817794, 67);
INSERT INTO `sys_role_menu` VALUES (1560150349828915202, 1560148188390817794, 68);
INSERT INTO `sys_role_menu` VALUES (1560150349854081025, 1560148188390817794, 69);
INSERT INTO `sys_role_menu` VALUES (1560150349891829761, 1560148188390817794, 70);
INSERT INTO `sys_role_menu` VALUES (1560150350147682305, 1560148188390817794, 71);
INSERT INTO `sys_role_menu` VALUES (1560150350198013953, 1560148188390817794, 72);
INSERT INTO `sys_role_menu` VALUES (1560150350231568385, 1560148188390817794, 73);
INSERT INTO `sys_role_menu` VALUES (1560150353318576130, 1560148188390817794, 74);
INSERT INTO `sys_role_menu` VALUES (1560150353368907778, 1560148188390817794, 75);
INSERT INTO `sys_role_menu` VALUES (1560150353515708417, 1560148188390817794, 76);
INSERT INTO `sys_role_menu` VALUES (1560150353654120449, 1560148188390817794, 77);
INSERT INTO `sys_role_menu` VALUES (1560150353687674882, 1560148188390817794, 78);
INSERT INTO `sys_role_menu` VALUES (1560150353729617921, 1560148188390817794, 79);
INSERT INTO `sys_role_menu` VALUES (1560150353830281218, 1560148188390817794, 80);
INSERT INTO `sys_role_menu` VALUES (1560150353838669826, 1560148188390817794, 81);
INSERT INTO `sys_role_menu` VALUES (1560150353905778690, 1560148188390817794, 82);
INSERT INTO `sys_role_menu` VALUES (1560150354006441985, 1560148188390817794, 83);
INSERT INTO `sys_role_menu` VALUES (1560150354249711617, 1560148188390817794, 84);
INSERT INTO `sys_role_menu` VALUES (1560150354266488833, 1560148188390817794, 85);
INSERT INTO `sys_role_menu` VALUES (1560150354283266049, 1560148188390817794, 86);
INSERT INTO `sys_role_menu` VALUES (1560150355034046465, 1560148188390817794, 87);
INSERT INTO `sys_role_menu` VALUES (1560150355067600897, 1560148188390817794, 88);
INSERT INTO `sys_role_menu` VALUES (1560150355201818626, 1560148188390817794, 89);
INSERT INTO `sys_role_menu` VALUES (1560150355268927490, 1560148188390817794, 90);
INSERT INTO `sys_role_menu` VALUES (1560150355336036354, 1560148188390817794, 91);
INSERT INTO `sys_role_menu` VALUES (1560150356598521858, 1560148188390817794, 92);
INSERT INTO `sys_role_menu` VALUES (1560150357131198466, 1560148188390817794, 93);
INSERT INTO `sys_role_menu` VALUES (1560150357189918722, 1560148188390817794, 94);
INSERT INTO `sys_role_menu` VALUES (1560150358456598530, 1560148188390817794, 95);
INSERT INTO `sys_role_menu` VALUES (1560150360377589762, 1560148188390817794, 96);
INSERT INTO `sys_role_menu` VALUES (1560150365108764674, 1560148188390817794, 97);
INSERT INTO `sys_role_menu` VALUES (1560150367734398977, 1560148188390817794, 98);
INSERT INTO `sys_role_menu` VALUES (1560150371760930817, 1560148188390817794, 99);
INSERT INTO `sys_role_menu` VALUES (1560150377041559553, 1560148188390817794, 100);
INSERT INTO `sys_role_menu` VALUES (1560150384792633346, 1560148188390817794, 101);
INSERT INTO `sys_role_menu` VALUES (1560150385581162498, 1560148188390817794, 102);
INSERT INTO `sys_role_menu` VALUES (1560150388269711362, 1560148188390817794, 103);
INSERT INTO `sys_role_menu` VALUES (1560150388559118338, 1560148188390817794, 104);
INSERT INTO `sys_role_menu` VALUES (1560150388588478466, 1560148188390817794, 105);
INSERT INTO `sys_role_menu` VALUES (1560150388643004417, 1560148188390817794, 106);
INSERT INTO `sys_role_menu` VALUES (1560150388919828481, 1560148188390817794, 107);
INSERT INTO `sys_role_menu` VALUES (1560150389490253826, 1560148188390817794, 108);
INSERT INTO `sys_role_menu` VALUES (1560150390450749442, 1560148188390817794, 109);
INSERT INTO `sys_role_menu` VALUES (1560150390480109570, 1560148188390817794, 110);
INSERT INTO `sys_role_menu` VALUES (1560150390551412738, 1560148188390817794, 111);
INSERT INTO `sys_role_menu` VALUES (1560150390924705793, 1560148188390817794, 112);
INSERT INTO `sys_role_menu` VALUES (1560150390991814657, 1560148188390817794, 113);
INSERT INTO `sys_role_menu` VALUES (1560150391033757698, 1560148188390817794, 114);
INSERT INTO `sys_role_menu` VALUES (1560150391159586818, 1560148188390817794, 115);
INSERT INTO `sys_role_menu` VALUES (1560150391197335553, 1560148188390817794, 116);
INSERT INTO `sys_role_menu` VALUES (1560150391230889985, 1560148188390817794, 117);
INSERT INTO `sys_role_menu` VALUES (1560150391348330498, 1560148188390817794, 118);
INSERT INTO `sys_role_menu` VALUES (1560150391360913410, 1560148188390817794, 119);
INSERT INTO `sys_role_menu` VALUES (1560150391436410881, 1560148188390817794, 120);
INSERT INTO `sys_role_menu` VALUES (1560150391574822914, 1560148188390817794, 121);
INSERT INTO `sys_role_menu` VALUES (1560150391583211522, 1560148188390817794, 122);
INSERT INTO `sys_role_menu` VALUES (1560150391599988738, 1560148188390817794, 123);
INSERT INTO `sys_role_menu` VALUES (1560150392354963458, 1560148188390817794, 124);
INSERT INTO `sys_role_menu` VALUES (1560150392434655234, 1560148188390817794, 125);
INSERT INTO `sys_role_menu` VALUES (1560150392845697026, 1560148188390817794, 126);
INSERT INTO `sys_role_menu` VALUES (1560150392992497666, 1560148188390817794, 127);
INSERT INTO `sys_role_menu` VALUES (1560150393059606529, 1560148188390817794, 128);
INSERT INTO `sys_role_menu` VALUES (1560150393072189441, 1560148188390817794, 129);
INSERT INTO `sys_role_menu` VALUES (1560150393332236289, 1560148188390817794, 130);
INSERT INTO `sys_role_menu` VALUES (1560150393361596418, 1560148188390817794, 131);
INSERT INTO `sys_role_menu` VALUES (1560150394674413570, 1560148188390817794, 132);
INSERT INTO `sys_role_menu` VALUES (1560150394695385089, 1560148188390817794, 133);
INSERT INTO `sys_role_menu` VALUES (1560150396670902273, 1560148188390817794, 134);
INSERT INTO `sys_role_menu` VALUES (1560150397266493442, 1560148188390817794, 135);
INSERT INTO `sys_role_menu` VALUES (1560150397463625730, 1560148188390817794, 136);
INSERT INTO `sys_role_menu` VALUES (1560150397463625731, 1560148188390817794, 137);
INSERT INTO `sys_role_menu` VALUES (1560150397530734594, 1560148188390817794, 138);
INSERT INTO `sys_role_menu` VALUES (1560150397664952322, 1560148188390817794, 139);
INSERT INTO `sys_role_menu` VALUES (1560150397732061186, 1560148188390817794, 140);
INSERT INTO `sys_role_menu` VALUES (1560150397794975746, 1560148188390817794, 141);
INSERT INTO `sys_role_menu` VALUES (1560150397794975747, 1560148188390817794, 142);
INSERT INTO `sys_role_menu` VALUES (1560150397794975748, 1560148188390817794, 143);
INSERT INTO `sys_role_menu` VALUES (1560150397794975749, 1560148188390817794, 144);
INSERT INTO `sys_role_menu` VALUES (1560150397857890305, 1560148188390817794, 145);
INSERT INTO `sys_role_menu` VALUES (1560150397920804866, 1560148188390817794, 146);
INSERT INTO `sys_role_menu` VALUES (1560150397920804867, 1560148188390817794, 147);
INSERT INTO `sys_role_menu` VALUES (1560150397987913730, 1560148188390817794, 148);
INSERT INTO `sys_role_menu` VALUES (1560150397987913731, 1560148188390817794, 149);
INSERT INTO `sys_role_menu` VALUES (1560150399304925186, 1560148188390817794, 150);
INSERT INTO `sys_role_menu` VALUES (1560150399313313794, 1560148188390817794, 151);
INSERT INTO `sys_role_menu` VALUES (1560150399330091009, 1560148188390817794, 152);
INSERT INTO `sys_role_menu` VALUES (1560150399363645441, 1560148188390817794, 153);
INSERT INTO `sys_role_menu` VALUES (1560150399401394178, 1560148188390817794, 154);
INSERT INTO `sys_role_menu` VALUES (1560150399472697346, 1560148188390817794, 155);
INSERT INTO `sys_role_menu` VALUES (1560150399535611905, 1560148188390817794, 156);
INSERT INTO `sys_role_menu` VALUES (1560150399900516353, 1560148188390817794, 157);
INSERT INTO `sys_role_menu` VALUES (1560150400961675266, 1560148188390817794, 158);
INSERT INTO `sys_role_menu` VALUES (1560150400986841089, 1560148188390817794, 159);
INSERT INTO `sys_role_menu` VALUES (1560150401360134145, 1560148188390817794, 160);
INSERT INTO `sys_role_menu` VALUES (1560150401368522754, 1560148188390817794, 161);
INSERT INTO `sys_role_menu` VALUES (1560150401515323394, 1560148188390817794, 162);
INSERT INTO `sys_role_menu` VALUES (1560150401532100610, 1560148188390817794, 163);
INSERT INTO `sys_role_menu` VALUES (1560150401720844290, 1560148188390817794, 164);
INSERT INTO `sys_role_menu` VALUES (1560150401729232897, 1560148188390817794, 165);
INSERT INTO `sys_role_menu` VALUES (1560150401779564546, 1560148188390817794, 166);
INSERT INTO `sys_role_menu` VALUES (1560150401842479105, 1560148188390817794, 167);
INSERT INTO `sys_role_menu` VALUES (1560150401922170882, 1560148188390817794, 168);
INSERT INTO `sys_role_menu` VALUES (1560150402081554433, 1560148188390817794, 169);
INSERT INTO `sys_role_menu` VALUES (1560150402148663298, 1560148188390817794, 170);
INSERT INTO `sys_role_menu` VALUES (1560150402249326594, 1560148188390817794, 171);
INSERT INTO `sys_role_menu` VALUES (1560150402446458881, 1560148188390817794, 172);
INSERT INTO `sys_role_menu` VALUES (1560150402459041794, 1560148188390817794, 173);
INSERT INTO `sys_role_menu` VALUES (1560150402542927874, 1560148188390817794, 174);
INSERT INTO `sys_role_menu` VALUES (1560150402618425345, 1560148188390817794, 175);
INSERT INTO `sys_role_menu` VALUES (1560150402702311426, 1560148188390817794, 176);
INSERT INTO `sys_role_menu` VALUES (1560150402949775361, 1560148188390817794, 177);
INSERT INTO `sys_role_menu` VALUES (1560150403121741826, 1560148188390817794, 178);
INSERT INTO `sys_role_menu` VALUES (1560150403553755137, 1560148188390817794, 179);
INSERT INTO `sys_role_menu` VALUES (1560150403721527298, 1560148188390817794, 180);
INSERT INTO `sys_role_menu` VALUES (1560150403927048194, 1560148188390817794, 181);
INSERT INTO `sys_role_menu` VALUES (1560150404052877313, 1560148188390817794, 182);
INSERT INTO `sys_role_menu` VALUES (1560150404086431745, 1560148188390817794, 183);
INSERT INTO `sys_role_menu` VALUES (1560150404241620994, 1560148188390817794, 184);
INSERT INTO `sys_role_menu` VALUES (1560150404321312770, 1560148188390817794, 185);
INSERT INTO `sys_role_menu` VALUES (1560150404409393153, 1560148188390817794, 186);
INSERT INTO `sys_role_menu` VALUES (1560150404476502018, 1560148188390817794, 187);
INSERT INTO `sys_role_menu` VALUES (1560150404551999490, 1560148188390817794, 188);
INSERT INTO `sys_role_menu` VALUES (1560150404992401410, 1560148188390817794, 189);
INSERT INTO `sys_role_menu` VALUES (1560150405139202049, 1560148188390817794, 190);
INSERT INTO `sys_role_menu` VALUES (1560150405218893825, 1560148188390817794, 191);
INSERT INTO `sys_role_menu` VALUES (1560150405361500162, 1560148188390817794, 192);
INSERT INTO `sys_role_menu` VALUES (1560150405407637506, 1560148188390817794, 193);
INSERT INTO `sys_role_menu` VALUES (1560150405546049537, 1560148188390817794, 194);
INSERT INTO `sys_role_menu` VALUES (1560150405638324225, 1560148188390817794, 195);
INSERT INTO `sys_role_menu` VALUES (1560150405764153346, 1560148188390817794, 196);
INSERT INTO `sys_role_menu` VALUES (1560150405936119809, 1560148188390817794, 197);
INSERT INTO `sys_role_menu` VALUES (1560150405978062849, 1560148188390817794, 198);
INSERT INTO `sys_role_menu` VALUES (1560150406036783105, 1560148188390817794, 199);
INSERT INTO `sys_role_menu` VALUES (1560150406116474881, 1560148188390817794, 200);
INSERT INTO `sys_role_menu` VALUES (1560150406141640705, 1560148188390817794, 201);
INSERT INTO `sys_role_menu` VALUES (1560150406175195137, 1560148188390817794, 202);
INSERT INTO `sys_role_menu` VALUES (1560150406208749569, 1560148188390817794, 203);
INSERT INTO `sys_role_menu` VALUES (1560150406217138177, 1560148188390817794, 204);
INSERT INTO `sys_role_menu` VALUES (1560150406233915394, 1560148188390817794, 205);
INSERT INTO `sys_role_menu` VALUES (1560150406242304001, 1560148188390817794, 206);
INSERT INTO `sys_role_menu` VALUES (1560150406280052737, 1560148188390817794, 207);
INSERT INTO `sys_role_menu` VALUES (1560150406301024258, 1560148188390817794, 208);
INSERT INTO `sys_role_menu` VALUES (1560150406317801473, 1560148188390817794, 209);
INSERT INTO `sys_role_menu` VALUES (1560150406380716034, 1560148188390817794, 210);
INSERT INTO `sys_role_menu` VALUES (1560150406435241985, 1560148188390817794, 211);
INSERT INTO `sys_role_menu` VALUES (1560150406460407809, 1560148188390817794, 212);
INSERT INTO `sys_role_menu` VALUES (1560150406485573633, 1560148188390817794, 213);
INSERT INTO `sys_role_menu` VALUES (1560150406519128066, 1560148188390817794, 214);
INSERT INTO `sys_role_menu` VALUES (1560150406531710978, 1560148188390817794, 215);
INSERT INTO `sys_role_menu` VALUES (1560150406582042626, 1560148188390817794, 216);
INSERT INTO `sys_role_menu` VALUES (1560150406611402753, 1560148188390817794, 217);
INSERT INTO `sys_role_menu` VALUES (1560150406657540098, 1560148188390817794, 218);
INSERT INTO `sys_role_menu` VALUES (1560150406758203394, 1560148188390817794, 219);
INSERT INTO `sys_role_menu` VALUES (1560150406774980610, 1560148188390817794, 220);
INSERT INTO `sys_role_menu` VALUES (1560150406787563522, 1560148188390817794, 221);
INSERT INTO `sys_role_menu` VALUES (1560150406846283778, 1560148188390817794, 222);
INSERT INTO `sys_role_menu` VALUES (1560150406925975553, 1560148188390817794, 223);
INSERT INTO `sys_role_menu` VALUES (1560150406942752770, 1560148188390817794, 224);
INSERT INTO `sys_role_menu` VALUES (1560150406959529985, 1560148188390817794, 225);
INSERT INTO `sys_role_menu` VALUES (1560150406972112898, 1560148188390817794, 226);
INSERT INTO `sys_role_menu` VALUES (1560150406980501506, 1560148188390817794, 227);
INSERT INTO `sys_role_menu` VALUES (1560150406993084417, 1560148188390817794, 228);
INSERT INTO `sys_role_menu` VALUES (1560150407018250241, 1560148188390817794, 229);
INSERT INTO `sys_role_menu` VALUES (1560150407030833153, 1560148188390817794, 230);
INSERT INTO `sys_role_menu` VALUES (1560150407047610370, 1560148188390817794, 231);
INSERT INTO `sys_role_menu` VALUES (1560150407068581890, 1560148188390817794, 232);
INSERT INTO `sys_role_menu` VALUES (1560150407093747713, 1560148188390817794, 233);
INSERT INTO `sys_role_menu` VALUES (1560150407110524930, 1560148188390817794, 234);
INSERT INTO `sys_role_menu` VALUES (1560150407169245185, 1560148188390817794, 235);
INSERT INTO `sys_role_menu` VALUES (1560150407206993922, 1560148188390817794, 236);
INSERT INTO `sys_role_menu` VALUES (1560150407253131265, 1560148188390817794, 237);
INSERT INTO `sys_role_menu` VALUES (1560150407274102785, 1560148188390817794, 238);
INSERT INTO `sys_role_menu` VALUES (1560150407303462914, 1560148188390817794, 239);
INSERT INTO `sys_role_menu` VALUES (1560150407659978753, 1560148188390817794, 240);
INSERT INTO `sys_role_menu` VALUES (1560150407659978754, 1560148188390817794, 241);
INSERT INTO `sys_role_menu` VALUES (1560150407722893314, 1560148188390817794, 242);
INSERT INTO `sys_role_menu` VALUES (1560150407790002177, 1560148188390817794, 243);
INSERT INTO `sys_role_menu` VALUES (1560150407790002178, 1560148188390817794, 244);
INSERT INTO `sys_role_menu` VALUES (1560150407857111042, 1560148188390817794, 245);
INSERT INTO `sys_role_menu` VALUES (1560150408058437634, 1560148188390817794, 246);
INSERT INTO `sys_role_menu` VALUES (1560150408058437635, 1560148188390817794, 247);
INSERT INTO `sys_role_menu` VALUES (1560150408121352193, 1560148188390817794, 248);
INSERT INTO `sys_role_menu` VALUES (1560150408121352194, 1560148188390817794, 249);
INSERT INTO `sys_role_menu` VALUES (1560150408188461057, 1560148188390817794, 250);
INSERT INTO `sys_role_menu` VALUES (1560150408255569922, 1560148188390817794, 251);
INSERT INTO `sys_role_menu` VALUES (1560150410453385218, 1560148188390817794, 252);
INSERT INTO `sys_role_menu` VALUES (1560150415142617090, 1560148188390817794, 253);
INSERT INTO `sys_role_menu` VALUES (1560150415151005698, 1560148188390817794, 254);
INSERT INTO `sys_role_menu` VALUES (1560150415171977217, 1560148188390817794, 255);
INSERT INTO `sys_role_menu` VALUES (1560150415192948738, 1560148188390817794, 4998978684524443692);
INSERT INTO `sys_role_menu` VALUES (1560150415201337345, 1560148188390817794, 6449912099524079616);
INSERT INTO `sys_role_menu` VALUES (1560150415218114562, 1560148188390817794, 5707993274387362828);
INSERT INTO `sys_role_menu` VALUES (1560150415234891777, 1560148188390817794, 8275137972166495794);
INSERT INTO `sys_role_menu` VALUES (1560150415251668994, 1560148188390817794, 6249597959063106635);
INSERT INTO `sys_role_menu` VALUES (1560150415264251906, 1560148188390817794, 6905244625723471705);
INSERT INTO `sys_role_menu` VALUES (1560150415272640514, 1560148188390817794, 6709768906758836391);
INSERT INTO `sys_role_menu` VALUES (1560150415289417730, 1560148188390817794, 7388361180917918171);
INSERT INTO `sys_role_menu` VALUES (1560150415297806337, 1560148188390817794, 1534384317233524738);
INSERT INTO `sys_role_menu` VALUES (1560150415314583553, 1560148188390817794, 7702400310012060990);
INSERT INTO `sys_role_menu` VALUES (1560150415327166466, 1560148188390817794, 5243613315131080710);
INSERT INTO `sys_role_menu` VALUES (1560150415352332289, 1560148188390817794, 5079108238558434584);
INSERT INTO `sys_role_menu` VALUES (1560150415390081026, 1560148188390817794, 8562547749529140265);
INSERT INTO `sys_role_menu` VALUES (1560150415398469633, 1560148188390817794, 5788080164048630938);
INSERT INTO `sys_role_menu` VALUES (1560150415415246850, 1560148188390817794, 7936411315868128942);
INSERT INTO `sys_role_menu` VALUES (1560150415448801281, 1560148188390817794, 7940657177277675482);
INSERT INTO `sys_role_menu` VALUES (1560150415490744322, 1560148188390817794, 6194217070047486945);
INSERT INTO `sys_role_menu` VALUES (1560150415515910145, 1560148188390817794, 6527167616560933135);
INSERT INTO `sys_role_menu` VALUES (1560150415541075970, 1560148188390817794, 1551480636561321985);
INSERT INTO `sys_role_menu` VALUES (1560150415574630401, 1560148188390817794, 6570253563256899359);
INSERT INTO `sys_role_menu` VALUES (1560150415587213313, 1560148188390817794, 6547301851685353031);
INSERT INTO `sys_role_menu` VALUES (1560150415612379137, 1560148188390817794, 8197158197771689059);
INSERT INTO `sys_role_menu` VALUES (1560150415624962049, 1560148188390817794, 9222389351831596218);
INSERT INTO `sys_role_menu` VALUES (1560150415658516482, 1560148188390817794, 5142735407151148234);
INSERT INTO `sys_role_menu` VALUES (1560150415675293698, 1560148188390817794, 7621956982866257638);
INSERT INTO `sys_role_menu` VALUES (1560150415687876610, 1560148188390817794, 5553078347948343545);
INSERT INTO `sys_role_menu` VALUES (1560150415721431042, 1560148188390817794, 6436044181834026359);
INSERT INTO `sys_role_menu` VALUES (1560150415792734210, 1560148188390817794, 8315301036989682205);
INSERT INTO `sys_role_menu` VALUES (1560150415834677250, 1560148188390817794, 8618504262472492350);
INSERT INTO `sys_role_menu` VALUES (1560150415897591810, 1560148188390817794, 7950819887425681478);
INSERT INTO `sys_role_menu` VALUES (1560150415897591811, 1560148188390817794, 4832107133629024589);
INSERT INTO `sys_role_menu` VALUES (1560150415935340546, 1560148188390817794, 7946795106595000695);
INSERT INTO `sys_role_menu` VALUES (1560150415935340547, 1560148188390817794, 5279583233581044419);
INSERT INTO `sys_role_menu` VALUES (1560150416002449409, 1560148188390817794, 6845666717193712873);
INSERT INTO `sys_role_menu` VALUES (1560150416002449410, 1560148188390817794, 8426505795261326687);
INSERT INTO `sys_role_menu` VALUES (1560150416069558274, 1560148188390817794, 6103813152661113624);
INSERT INTO `sys_role_menu` VALUES (1560150416329605122, 1560148188390817794, 6374585520258378360);
INSERT INTO `sys_role_menu` VALUES (1560150416396713986, 1560148188390817794, 8621765388717143178);
INSERT INTO `sys_role_menu` VALUES (1560150416396713987, 1560148188390817794, 6116805160244737678);
INSERT INTO `sys_role_menu` VALUES (1560150416463822849, 1560148188390817794, 6860312372874164125);
INSERT INTO `sys_role_menu` VALUES (1560150416463822850, 1560148188390817794, 5820472386138045917);
INSERT INTO `sys_role_menu` VALUES (1560150416530931713, 1560148188390817794, 8181822590852115190);
INSERT INTO `sys_role_menu` VALUES (1560150416530931714, 1560148188390817794, 7128085356547940830);
INSERT INTO `sys_role_menu` VALUES (1560150416530931715, 1560148188390817794, 6204862670753579307);
INSERT INTO `sys_role_menu` VALUES (1560150416598040577, 1560148188390817794, 4835110481928644952);
INSERT INTO `sys_role_menu` VALUES (1560150417990549506, 1560148188390817794, 7047324015553475476);
INSERT INTO `sys_role_menu` VALUES (1560150418057658369, 1560148188390817794, 7581916369308597479);
INSERT INTO `sys_role_menu` VALUES (1560150418187681794, 1560148188390817794, 6591367648784009715);
INSERT INTO `sys_role_menu` VALUES (1560150419110428673, 1560148188390817794, 6015462925549607928);
INSERT INTO `sys_role_menu` VALUES (1560150419177537538, 1560148188390817794, 4719172401435180793);
INSERT INTO `sys_role_menu` VALUES (1560150419500498945, 1560148188390817794, 5474507168841280952);
INSERT INTO `sys_role_menu` VALUES (1560150420687486977, 1560148188390817794, 5653092812632926489);
INSERT INTO `sys_role_menu` VALUES (1560150420716847105, 1560148188390817794, 7200474546305867050);
INSERT INTO `sys_role_menu` VALUES (1560150420771373058, 1560148188390817794, 7242870236365548592);
INSERT INTO `sys_role_menu` VALUES (1560150420809121793, 1560148188390817794, 9150731911657480775);
INSERT INTO `sys_role_menu` VALUES (1560150420897202178, 1560148188390817794, 7287765970469918079);
INSERT INTO `sys_role_menu` VALUES (1560150420918173697, 1560148188390817794, 5480200211927228567);
INSERT INTO `sys_role_menu` VALUES (1560150421819949058, 1560148188390817794, 4770899845682577376);
INSERT INTO `sys_role_menu` VALUES (1560150421882863618, 1560148188390817794, 8957317986837088422);
INSERT INTO `sys_role_menu` VALUES (1560150421924806658, 1560148188390817794, 7455112058491820064);
INSERT INTO `sys_role_menu` VALUES (1560150421966749698, 1560148188390817794, 4621333945127042674);
INSERT INTO `sys_role_menu` VALUES (1560150422268739585, 1560148188390817794, 5886900280768283007);
INSERT INTO `sys_role_menu` VALUES (1560150422289711106, 1560148188390817794, 5846194751422568093);
INSERT INTO `sys_role_menu` VALUES (1560150422331654146, 1560148188390817794, 9156093698218665129);
INSERT INTO `sys_role_menu` VALUES (1560150422344237057, 1560148188390817794, 7678659259268274624);
INSERT INTO `sys_role_menu` VALUES (1560150422365208578, 1560148188390817794, 8099644559700436461);
INSERT INTO `sys_role_menu` VALUES (1560153026453372930, 1560148282678771714, 1538776469361139714);
INSERT INTO `sys_role_menu` VALUES (1560153026486927361, 1560148282678771714, 256);
INSERT INTO `sys_role_menu` VALUES (1560153026570813441, 1560148282678771714, 1560145190851772417);
INSERT INTO `sys_role_menu` VALUES (1560153026595979266, 1560148282678771714, 1);
INSERT INTO `sys_role_menu` VALUES (1560153026663088129, 1560148282678771714, 257);
INSERT INTO `sys_role_menu` VALUES (1560153026679865346, 1560148282678771714, 2);
INSERT INTO `sys_role_menu` VALUES (1560153026713419777, 1560148282678771714, 258);
INSERT INTO `sys_role_menu` VALUES (1560153026730196994, 1560148282678771714, 3);
INSERT INTO `sys_role_menu` VALUES (1560153026746974210, 1560148282678771714, 259);
INSERT INTO `sys_role_menu` VALUES (1560153026755362818, 1560148282678771714, 4);
INSERT INTO `sys_role_menu` VALUES (1560153026772140034, 1560148282678771714, 260);
INSERT INTO `sys_role_menu` VALUES (1560153026788917250, 1560148282678771714, 5);
INSERT INTO `sys_role_menu` VALUES (1560153026797305857, 1560148282678771714, 261);
INSERT INTO `sys_role_menu` VALUES (1560153026814083073, 1560148282678771714, 6);
INSERT INTO `sys_role_menu` VALUES (1560153026822471682, 1560148282678771714, 262);
INSERT INTO `sys_role_menu` VALUES (1560153026839248898, 1560148282678771714, 7);
INSERT INTO `sys_role_menu` VALUES (1560153026847637505, 1560148282678771714, 263);
INSERT INTO `sys_role_menu` VALUES (1560153026864414722, 1560148282678771714, 8);
INSERT INTO `sys_role_menu` VALUES (1560153026872803330, 1560148282678771714, 264);
INSERT INTO `sys_role_menu` VALUES (1560153026889580545, 1560148282678771714, 9);
INSERT INTO `sys_role_menu` VALUES (1560153026902163457, 1560148282678771714, 265);
INSERT INTO `sys_role_menu` VALUES (1560153026918940673, 1560148282678771714, 10);
INSERT INTO `sys_role_menu` VALUES (1560153026927329282, 1560148282678771714, 266);
INSERT INTO `sys_role_menu` VALUES (1560153026944106497, 1560148282678771714, 11);
INSERT INTO `sys_role_menu` VALUES (1560153026960883713, 1560148282678771714, 267);
INSERT INTO `sys_role_menu` VALUES (1560153026969272322, 1560148282678771714, 12);
INSERT INTO `sys_role_menu` VALUES (1560153026986049538, 1560148282678771714, 268);
INSERT INTO `sys_role_menu` VALUES (1560153026994438145, 1560148282678771714, 13);
INSERT INTO `sys_role_menu` VALUES (1560153027007021057, 1560148282678771714, 269);
INSERT INTO `sys_role_menu` VALUES (1560153027019603970, 1560148282678771714, 14);
INSERT INTO `sys_role_menu` VALUES (1560153027036381185, 1560148282678771714, 270);
INSERT INTO `sys_role_menu` VALUES (1560153027044769794, 1560148282678771714, 15);
INSERT INTO `sys_role_menu` VALUES (1560153027053158401, 1560148282678771714, 271);
INSERT INTO `sys_role_menu` VALUES (1560153027069935617, 1560148282678771714, 16);
INSERT INTO `sys_role_menu` VALUES (1560153027086712833, 1560148282678771714, 272);
INSERT INTO `sys_role_menu` VALUES (1560153027095101442, 1560148282678771714, 17);
INSERT INTO `sys_role_menu` VALUES (1560153027111878657, 1560148282678771714, 273);
INSERT INTO `sys_role_menu` VALUES (1560153027120267266, 1560148282678771714, 18);
INSERT INTO `sys_role_menu` VALUES (1560153027137044482, 1560148282678771714, 274);
INSERT INTO `sys_role_menu` VALUES (1560153027149627393, 1560148282678771714, 19);
INSERT INTO `sys_role_menu` VALUES (1560153027162210305, 1560148282678771714, 275);
INSERT INTO `sys_role_menu` VALUES (1560153027174793217, 1560148282678771714, 20);
INSERT INTO `sys_role_menu` VALUES (1560153027187376129, 1560148282678771714, 276);
INSERT INTO `sys_role_menu` VALUES (1560153027195764738, 1560148282678771714, 21);
INSERT INTO `sys_role_menu` VALUES (1560153027204153346, 1560148282678771714, 277);
INSERT INTO `sys_role_menu` VALUES (1560153027216736258, 1560148282678771714, 22);
INSERT INTO `sys_role_menu` VALUES (1560153027233513474, 1560148282678771714, 278);
INSERT INTO `sys_role_menu` VALUES (1560153027246096385, 1560148282678771714, 23);
INSERT INTO `sys_role_menu` VALUES (1560153027279650817, 1560148282678771714, 279);
INSERT INTO `sys_role_menu` VALUES (1560153027292233730, 1560148282678771714, 24);
INSERT INTO `sys_role_menu` VALUES (1560153027296428034, 1560148282678771714, 280);
INSERT INTO `sys_role_menu` VALUES (1560153027313205250, 1560148282678771714, 25);
INSERT INTO `sys_role_menu` VALUES (1560153027334176770, 1560148282678771714, 281);
INSERT INTO `sys_role_menu` VALUES (1560153027350953985, 1560148282678771714, 26);
INSERT INTO `sys_role_menu` VALUES (1560153027359342593, 1560148282678771714, 282);
INSERT INTO `sys_role_menu` VALUES (1560153027376119810, 1560148282678771714, 27);
INSERT INTO `sys_role_menu` VALUES (1560153027392897026, 1560148282678771714, 283);
INSERT INTO `sys_role_menu` VALUES (1560153027401285633, 1560148282678771714, 28);
INSERT INTO `sys_role_menu` VALUES (1560153027413868546, 1560148282678771714, 284);
INSERT INTO `sys_role_menu` VALUES (1560153027422257153, 1560148282678771714, 29);
INSERT INTO `sys_role_menu` VALUES (1560153027439034369, 1560148282678771714, 285);
INSERT INTO `sys_role_menu` VALUES (1560153027447422978, 1560148282678771714, 30);
INSERT INTO `sys_role_menu` VALUES (1560153027464200194, 1560148282678771714, 286);
INSERT INTO `sys_role_menu` VALUES (1560153027476783105, 1560148282678771714, 31);
INSERT INTO `sys_role_menu` VALUES (1560153027489366018, 1560148282678771714, 287);
INSERT INTO `sys_role_menu` VALUES (1560153027501948929, 1560148282678771714, 32);
INSERT INTO `sys_role_menu` VALUES (1560153027514531841, 1560148282678771714, 288);
INSERT INTO `sys_role_menu` VALUES (1560153027522920449, 1560148282678771714, 33);
INSERT INTO `sys_role_menu` VALUES (1560153027543891969, 1560148282678771714, 289);
INSERT INTO `sys_role_menu` VALUES (1560153027556474881, 1560148282678771714, 34);
INSERT INTO `sys_role_menu` VALUES (1560153027569057794, 1560148282678771714, 290);
INSERT INTO `sys_role_menu` VALUES (1560153027581640706, 1560148282678771714, 35);
INSERT INTO `sys_role_menu` VALUES (1560153027594223618, 1560148282678771714, 291);
INSERT INTO `sys_role_menu` VALUES (1560153027602612225, 1560148282678771714, 36);
INSERT INTO `sys_role_menu` VALUES (1560153027615195138, 1560148282678771714, 292);
INSERT INTO `sys_role_menu` VALUES (1560153027631972353, 1560148282678771714, 37);
INSERT INTO `sys_role_menu` VALUES (1560153027640360962, 1560148282678771714, 293);
INSERT INTO `sys_role_menu` VALUES (1560153027657138178, 1560148282678771714, 38);
INSERT INTO `sys_role_menu` VALUES (1560153027665526785, 1560148282678771714, 294);
INSERT INTO `sys_role_menu` VALUES (1560153027682304002, 1560148282678771714, 39);
INSERT INTO `sys_role_menu` VALUES (1560153027690692610, 1560148282678771714, 295);
INSERT INTO `sys_role_menu` VALUES (1560153027703275522, 1560148282678771714, 40);
INSERT INTO `sys_role_menu` VALUES (1560153027720052738, 1560148282678771714, 296);
INSERT INTO `sys_role_menu` VALUES (1560153027728441345, 1560148282678771714, 41);
INSERT INTO `sys_role_menu` VALUES (1560153027745218561, 1560148282678771714, 297);
INSERT INTO `sys_role_menu` VALUES (1560153027757801474, 1560148282678771714, 42);
INSERT INTO `sys_role_menu` VALUES (1560153027766190082, 1560148282678771714, 298);
INSERT INTO `sys_role_menu` VALUES (1560153027774578690, 1560148282678771714, 43);
INSERT INTO `sys_role_menu` VALUES (1560153027791355905, 1560148282678771714, 299);
INSERT INTO `sys_role_menu` VALUES (1560153027803938817, 1560148282678771714, 44);
INSERT INTO `sys_role_menu` VALUES (1560153027812327426, 1560148282678771714, 300);
INSERT INTO `sys_role_menu` VALUES (1560153027829104641, 1560148282678771714, 45);
INSERT INTO `sys_role_menu` VALUES (1560153027845881857, 1560148282678771714, 301);
INSERT INTO `sys_role_menu` VALUES (1560153027854270466, 1560148282678771714, 46);
INSERT INTO `sys_role_menu` VALUES (1560153027871047681, 1560148282678771714, 302);
INSERT INTO `sys_role_menu` VALUES (1560153027883630594, 1560148282678771714, 47);
INSERT INTO `sys_role_menu` VALUES (1560153027904602114, 1560148282678771714, 303);
INSERT INTO `sys_role_menu` VALUES (1560153027912990722, 1560148282678771714, 48);
INSERT INTO `sys_role_menu` VALUES (1560153027929767937, 1560148282678771714, 304);
INSERT INTO `sys_role_menu` VALUES (1560153027946545154, 1560148282678771714, 49);
INSERT INTO `sys_role_menu` VALUES (1560153027954933761, 1560148282678771714, 305);
INSERT INTO `sys_role_menu` VALUES (1560153027975905282, 1560148282678771714, 50);
INSERT INTO `sys_role_menu` VALUES (1560153027992682498, 1560148282678771714, 306);
INSERT INTO `sys_role_menu` VALUES (1560153028001071106, 1560148282678771714, 51);
INSERT INTO `sys_role_menu` VALUES (1560153028013654017, 1560148282678771714, 307);
INSERT INTO `sys_role_menu` VALUES (1560153028022042626, 1560148282678771714, 52);
INSERT INTO `sys_role_menu` VALUES (1560153028047208449, 1560148282678771714, 308);
INSERT INTO `sys_role_menu` VALUES (1560153028055597057, 1560148282678771714, 53);
INSERT INTO `sys_role_menu` VALUES (1560153028072374273, 1560148282678771714, 309);
INSERT INTO `sys_role_menu` VALUES (1560153028084957186, 1560148282678771714, 54);
INSERT INTO `sys_role_menu` VALUES (1560153028093345793, 1560148282678771714, 55);
INSERT INTO `sys_role_menu` VALUES (1560153028110123010, 1560148282678771714, 56);
INSERT INTO `sys_role_menu` VALUES (1560153028118511618, 1560148282678771714, 57);
INSERT INTO `sys_role_menu` VALUES (1560153028135288834, 1560148282678771714, 58);
INSERT INTO `sys_role_menu` VALUES (1560153028143677442, 1560148282678771714, 59);
INSERT INTO `sys_role_menu` VALUES (1560153028160454657, 1560148282678771714, 60);
INSERT INTO `sys_role_menu` VALUES (1560153028168843265, 1560148282678771714, 61);
INSERT INTO `sys_role_menu` VALUES (1560153028185620482, 1560148282678771714, 62);
INSERT INTO `sys_role_menu` VALUES (1560153028202397698, 1560148282678771714, 63);
INSERT INTO `sys_role_menu` VALUES (1560153028210786305, 1560148282678771714, 64);
INSERT INTO `sys_role_menu` VALUES (1560153028235952130, 1560148282678771714, 65);
INSERT INTO `sys_role_menu` VALUES (1560153028244340737, 1560148282678771714, 66);
INSERT INTO `sys_role_menu` VALUES (1560153028273700865, 1560148282678771714, 67);
INSERT INTO `sys_role_menu` VALUES (1560153028298866690, 1560148282678771714, 68);
INSERT INTO `sys_role_menu` VALUES (1560153028315643906, 1560148282678771714, 69);
INSERT INTO `sys_role_menu` VALUES (1560153028332421121, 1560148282678771714, 70);
INSERT INTO `sys_role_menu` VALUES (1560153028340809730, 1560148282678771714, 71);
INSERT INTO `sys_role_menu` VALUES (1560153028357586945, 1560148282678771714, 72);
INSERT INTO `sys_role_menu` VALUES (1560153028374364161, 1560148282678771714, 73);
INSERT INTO `sys_role_menu` VALUES (1560153028395335681, 1560148282678771714, 74);
INSERT INTO `sys_role_menu` VALUES (1560153028412112898, 1560148282678771714, 75);
INSERT INTO `sys_role_menu` VALUES (1560153028420501506, 1560148282678771714, 76);
INSERT INTO `sys_role_menu` VALUES (1560153028433084418, 1560148282678771714, 77);
INSERT INTO `sys_role_menu` VALUES (1560153028441473026, 1560148282678771714, 78);
INSERT INTO `sys_role_menu` VALUES (1560153028458250241, 1560148282678771714, 79);
INSERT INTO `sys_role_menu` VALUES (1560153028475027457, 1560148282678771714, 80);
INSERT INTO `sys_role_menu` VALUES (1560153028483416066, 1560148282678771714, 81);
INSERT INTO `sys_role_menu` VALUES (1560153028491804673, 1560148282678771714, 82);
INSERT INTO `sys_role_menu` VALUES (1560153028508581889, 1560148282678771714, 83);
INSERT INTO `sys_role_menu` VALUES (1560153028516970498, 1560148282678771714, 84);
INSERT INTO `sys_role_menu` VALUES (1560153028533747713, 1560148282678771714, 85);
INSERT INTO `sys_role_menu` VALUES (1560153028542136321, 1560148282678771714, 86);
INSERT INTO `sys_role_menu` VALUES (1560153028558913537, 1560148282678771714, 87);
INSERT INTO `sys_role_menu` VALUES (1560153028567302145, 1560148282678771714, 88);
INSERT INTO `sys_role_menu` VALUES (1560153028584079361, 1560148282678771714, 89);
INSERT INTO `sys_role_menu` VALUES (1560153028592467970, 1560148282678771714, 90);
INSERT INTO `sys_role_menu` VALUES (1560153028609245185, 1560148282678771714, 91);
INSERT INTO `sys_role_menu` VALUES (1560153028626022401, 1560148282678771714, 92);
INSERT INTO `sys_role_menu` VALUES (1560153028642799618, 1560148282678771714, 93);
INSERT INTO `sys_role_menu` VALUES (1560153028651188226, 1560148282678771714, 94);
INSERT INTO `sys_role_menu` VALUES (1560153028667965441, 1560148282678771714, 95);
INSERT INTO `sys_role_menu` VALUES (1560153028676354050, 1560148282678771714, 96);
INSERT INTO `sys_role_menu` VALUES (1560153028693131265, 1560148282678771714, 97);
INSERT INTO `sys_role_menu` VALUES (1560153028701519873, 1560148282678771714, 98);
INSERT INTO `sys_role_menu` VALUES (1560153028718297089, 1560148282678771714, 99);
INSERT INTO `sys_role_menu` VALUES (1560153028735074306, 1560148282678771714, 100);
INSERT INTO `sys_role_menu` VALUES (1560153028743462913, 1560148282678771714, 101);
INSERT INTO `sys_role_menu` VALUES (1560153028751851522, 1560148282678771714, 102);
INSERT INTO `sys_role_menu` VALUES (1560153028764434434, 1560148282678771714, 103);
INSERT INTO `sys_role_menu` VALUES (1560153028772823042, 1560148282678771714, 104);
INSERT INTO `sys_role_menu` VALUES (1560153028789600258, 1560148282678771714, 105);
INSERT INTO `sys_role_menu` VALUES (1560153028797988866, 1560148282678771714, 106);
INSERT INTO `sys_role_menu` VALUES (1560153028814766081, 1560148282678771714, 107);
INSERT INTO `sys_role_menu` VALUES (1560153028823154690, 1560148282678771714, 108);
INSERT INTO `sys_role_menu` VALUES (1560153028839931905, 1560148282678771714, 109);
INSERT INTO `sys_role_menu` VALUES (1560153028856709122, 1560148282678771714, 110);
INSERT INTO `sys_role_menu` VALUES (1560153028873486338, 1560148282678771714, 111);
INSERT INTO `sys_role_menu` VALUES (1560153028881874945, 1560148282678771714, 112);
INSERT INTO `sys_role_menu` VALUES (1560153028890263554, 1560148282678771714, 113);
INSERT INTO `sys_role_menu` VALUES (1560153028907040769, 1560148282678771714, 114);
INSERT INTO `sys_role_menu` VALUES (1560153028915429377, 1560148282678771714, 115);
INSERT INTO `sys_role_menu` VALUES (1560153028932206593, 1560148282678771714, 116);
INSERT INTO `sys_role_menu` VALUES (1560153028948983809, 1560148282678771714, 117);
INSERT INTO `sys_role_menu` VALUES (1560153028948983810, 1560148282678771714, 118);
INSERT INTO `sys_role_menu` VALUES (1560153028974149634, 1560148282678771714, 119);
INSERT INTO `sys_role_menu` VALUES (1560153028974149635, 1560148282678771714, 120);
INSERT INTO `sys_role_menu` VALUES (1560153028974149636, 1560148282678771714, 121);
INSERT INTO `sys_role_menu` VALUES (1560153028974149637, 1560148282678771714, 122);
INSERT INTO `sys_role_menu` VALUES (1560153028974149638, 1560148282678771714, 123);
INSERT INTO `sys_role_menu` VALUES (1560153029041258497, 1560148282678771714, 124);
INSERT INTO `sys_role_menu` VALUES (1560153029041258498, 1560148282678771714, 125);
INSERT INTO `sys_role_menu` VALUES (1560153029041258499, 1560148282678771714, 126);
INSERT INTO `sys_role_menu` VALUES (1560153029041258500, 1560148282678771714, 127);
INSERT INTO `sys_role_menu` VALUES (1560153029041258501, 1560148282678771714, 128);
INSERT INTO `sys_role_menu` VALUES (1560153029041258502, 1560148282678771714, 129);
INSERT INTO `sys_role_menu` VALUES (1560153029238390785, 1560148282678771714, 130);
INSERT INTO `sys_role_menu` VALUES (1560153029238390786, 1560148282678771714, 131);
INSERT INTO `sys_role_menu` VALUES (1560153029305499649, 1560148282678771714, 132);
INSERT INTO `sys_role_menu` VALUES (1560153029305499650, 1560148282678771714, 133);
INSERT INTO `sys_role_menu` VALUES (1560153029305499651, 1560148282678771714, 134);
INSERT INTO `sys_role_menu` VALUES (1560153029305499652, 1560148282678771714, 135);
INSERT INTO `sys_role_menu` VALUES (1560153029305499653, 1560148282678771714, 136);
INSERT INTO `sys_role_menu` VALUES (1560153029305499654, 1560148282678771714, 137);
INSERT INTO `sys_role_menu` VALUES (1560153029372608514, 1560148282678771714, 138);
INSERT INTO `sys_role_menu` VALUES (1560153029372608515, 1560148282678771714, 139);
INSERT INTO `sys_role_menu` VALUES (1560153029372608516, 1560148282678771714, 140);
INSERT INTO `sys_role_menu` VALUES (1560153029372608517, 1560148282678771714, 141);
INSERT INTO `sys_role_menu` VALUES (1560153029372608518, 1560148282678771714, 142);
INSERT INTO `sys_role_menu` VALUES (1560153029439717377, 1560148282678771714, 143);
INSERT INTO `sys_role_menu` VALUES (1560153029439717378, 1560148282678771714, 144);
INSERT INTO `sys_role_menu` VALUES (1560153029439717379, 1560148282678771714, 145);
INSERT INTO `sys_role_menu` VALUES (1560153029439717380, 1560148282678771714, 146);
INSERT INTO `sys_role_menu` VALUES (1560153029439717381, 1560148282678771714, 147);
INSERT INTO `sys_role_menu` VALUES (1560153029506826241, 1560148282678771714, 148);
INSERT INTO `sys_role_menu` VALUES (1560153029506826242, 1560148282678771714, 149);
INSERT INTO `sys_role_menu` VALUES (1560153029506826243, 1560148282678771714, 150);
INSERT INTO `sys_role_menu` VALUES (1560153029506826244, 1560148282678771714, 151);
INSERT INTO `sys_role_menu` VALUES (1560153029506826245, 1560148282678771714, 152);
INSERT INTO `sys_role_menu` VALUES (1560153029573935105, 1560148282678771714, 153);
INSERT INTO `sys_role_menu` VALUES (1560153029573935106, 1560148282678771714, 154);
INSERT INTO `sys_role_menu` VALUES (1560153029573935107, 1560148282678771714, 155);
INSERT INTO `sys_role_menu` VALUES (1560153029573935108, 1560148282678771714, 156);
INSERT INTO `sys_role_menu` VALUES (1560153029573935109, 1560148282678771714, 157);
INSERT INTO `sys_role_menu` VALUES (1560153029641043969, 1560148282678771714, 158);
INSERT INTO `sys_role_menu` VALUES (1560153029641043970, 1560148282678771714, 159);
INSERT INTO `sys_role_menu` VALUES (1560153029641043971, 1560148282678771714, 160);
INSERT INTO `sys_role_menu` VALUES (1560153029641043972, 1560148282678771714, 161);
INSERT INTO `sys_role_menu` VALUES (1560153029708152833, 1560148282678771714, 162);
INSERT INTO `sys_role_menu` VALUES (1560153029708152834, 1560148282678771714, 163);
INSERT INTO `sys_role_menu` VALUES (1560153029708152835, 1560148282678771714, 164);
INSERT INTO `sys_role_menu` VALUES (1560153029708152836, 1560148282678771714, 165);
INSERT INTO `sys_role_menu` VALUES (1560153029708152837, 1560148282678771714, 166);
INSERT INTO `sys_role_menu` VALUES (1560153029775261698, 1560148282678771714, 167);
INSERT INTO `sys_role_menu` VALUES (1560153029775261699, 1560148282678771714, 168);
INSERT INTO `sys_role_menu` VALUES (1560153029775261700, 1560148282678771714, 169);
INSERT INTO `sys_role_menu` VALUES (1560153029775261701, 1560148282678771714, 170);
INSERT INTO `sys_role_menu` VALUES (1560153029775261702, 1560148282678771714, 171);
INSERT INTO `sys_role_menu` VALUES (1560153029842370561, 1560148282678771714, 172);
INSERT INTO `sys_role_menu` VALUES (1560153029842370562, 1560148282678771714, 173);
INSERT INTO `sys_role_menu` VALUES (1560153029842370563, 1560148282678771714, 174);
INSERT INTO `sys_role_menu` VALUES (1560153029842370564, 1560148282678771714, 175);
INSERT INTO `sys_role_menu` VALUES (1560153029842370565, 1560148282678771714, 176);
INSERT INTO `sys_role_menu` VALUES (1560153029842370566, 1560148282678771714, 177);
INSERT INTO `sys_role_menu` VALUES (1560153029909479426, 1560148282678771714, 178);
INSERT INTO `sys_role_menu` VALUES (1560153029909479427, 1560148282678771714, 179);
INSERT INTO `sys_role_menu` VALUES (1560153029909479428, 1560148282678771714, 180);
INSERT INTO `sys_role_menu` VALUES (1560153029909479429, 1560148282678771714, 181);
INSERT INTO `sys_role_menu` VALUES (1560153029909479430, 1560148282678771714, 182);
INSERT INTO `sys_role_menu` VALUES (1560153029976588290, 1560148282678771714, 183);
INSERT INTO `sys_role_menu` VALUES (1560153029976588291, 1560148282678771714, 184);
INSERT INTO `sys_role_menu` VALUES (1560153029976588292, 1560148282678771714, 185);
INSERT INTO `sys_role_menu` VALUES (1560153029976588293, 1560148282678771714, 186);
INSERT INTO `sys_role_menu` VALUES (1560153029976588294, 1560148282678771714, 187);
INSERT INTO `sys_role_menu` VALUES (1560153030043697154, 1560148282678771714, 188);
INSERT INTO `sys_role_menu` VALUES (1560153030043697155, 1560148282678771714, 189);
INSERT INTO `sys_role_menu` VALUES (1560153030043697156, 1560148282678771714, 190);
INSERT INTO `sys_role_menu` VALUES (1560153030043697157, 1560148282678771714, 191);
INSERT INTO `sys_role_menu` VALUES (1560153030043697158, 1560148282678771714, 192);
INSERT INTO `sys_role_menu` VALUES (1560153030043697159, 1560148282678771714, 193);
INSERT INTO `sys_role_menu` VALUES (1560153030110806017, 1560148282678771714, 194);
INSERT INTO `sys_role_menu` VALUES (1560153030110806018, 1560148282678771714, 195);
INSERT INTO `sys_role_menu` VALUES (1560153030110806019, 1560148282678771714, 196);
INSERT INTO `sys_role_menu` VALUES (1560153030110806020, 1560148282678771714, 197);
INSERT INTO `sys_role_menu` VALUES (1560153030110806021, 1560148282678771714, 198);
INSERT INTO `sys_role_menu` VALUES (1560153030173720577, 1560148282678771714, 199);
INSERT INTO `sys_role_menu` VALUES (1560153030173720578, 1560148282678771714, 200);
INSERT INTO `sys_role_menu` VALUES (1560153030173720579, 1560148282678771714, 201);
INSERT INTO `sys_role_menu` VALUES (1560153030173720580, 1560148282678771714, 202);
INSERT INTO `sys_role_menu` VALUES (1560153030173720581, 1560148282678771714, 203);
INSERT INTO `sys_role_menu` VALUES (1560153030240829441, 1560148282678771714, 204);
INSERT INTO `sys_role_menu` VALUES (1560153030240829442, 1560148282678771714, 205);
INSERT INTO `sys_role_menu` VALUES (1560153030240829443, 1560148282678771714, 206);
INSERT INTO `sys_role_menu` VALUES (1560153030240829444, 1560148282678771714, 207);
INSERT INTO `sys_role_menu` VALUES (1560153030240829445, 1560148282678771714, 208);
INSERT INTO `sys_role_menu` VALUES (1560153030240829446, 1560148282678771714, 209);
INSERT INTO `sys_role_menu` VALUES (1560153030307938306, 1560148282678771714, 210);
INSERT INTO `sys_role_menu` VALUES (1560153030307938307, 1560148282678771714, 211);
INSERT INTO `sys_role_menu` VALUES (1560153030307938308, 1560148282678771714, 212);
INSERT INTO `sys_role_menu` VALUES (1560153030307938309, 1560148282678771714, 213);
INSERT INTO `sys_role_menu` VALUES (1560153030307938310, 1560148282678771714, 214);
INSERT INTO `sys_role_menu` VALUES (1560153030375047169, 1560148282678771714, 215);
INSERT INTO `sys_role_menu` VALUES (1560153030375047170, 1560148282678771714, 216);
INSERT INTO `sys_role_menu` VALUES (1560153030375047171, 1560148282678771714, 217);
INSERT INTO `sys_role_menu` VALUES (1560153030375047172, 1560148282678771714, 218);
INSERT INTO `sys_role_menu` VALUES (1560153030375047173, 1560148282678771714, 219);
INSERT INTO `sys_role_menu` VALUES (1560153030442156033, 1560148282678771714, 220);
INSERT INTO `sys_role_menu` VALUES (1560153030442156034, 1560148282678771714, 221);
INSERT INTO `sys_role_menu` VALUES (1560153030442156035, 1560148282678771714, 222);
INSERT INTO `sys_role_menu` VALUES (1560153030442156036, 1560148282678771714, 223);
INSERT INTO `sys_role_menu` VALUES (1560153030442156037, 1560148282678771714, 224);
INSERT INTO `sys_role_menu` VALUES (1560153030509264898, 1560148282678771714, 225);
INSERT INTO `sys_role_menu` VALUES (1560153030509264899, 1560148282678771714, 226);
INSERT INTO `sys_role_menu` VALUES (1560153030509264900, 1560148282678771714, 227);
INSERT INTO `sys_role_menu` VALUES (1560153030509264901, 1560148282678771714, 228);
INSERT INTO `sys_role_menu` VALUES (1560153030509264902, 1560148282678771714, 229);
INSERT INTO `sys_role_menu` VALUES (1560153030509264903, 1560148282678771714, 230);
INSERT INTO `sys_role_menu` VALUES (1560153030576373761, 1560148282678771714, 231);
INSERT INTO `sys_role_menu` VALUES (1560153030576373762, 1560148282678771714, 232);
INSERT INTO `sys_role_menu` VALUES (1560153030576373763, 1560148282678771714, 233);
INSERT INTO `sys_role_menu` VALUES (1560153030576373764, 1560148282678771714, 234);
INSERT INTO `sys_role_menu` VALUES (1560153030576373765, 1560148282678771714, 235);
INSERT INTO `sys_role_menu` VALUES (1560153030639288321, 1560148282678771714, 236);
INSERT INTO `sys_role_menu` VALUES (1560153030639288322, 1560148282678771714, 237);
INSERT INTO `sys_role_menu` VALUES (1560153030639288323, 1560148282678771714, 238);
INSERT INTO `sys_role_menu` VALUES (1560153030639288324, 1560148282678771714, 239);
INSERT INTO `sys_role_menu` VALUES (1560153030639288325, 1560148282678771714, 240);
INSERT INTO `sys_role_menu` VALUES (1560153030706397186, 1560148282678771714, 241);
INSERT INTO `sys_role_menu` VALUES (1560153030706397187, 1560148282678771714, 242);
INSERT INTO `sys_role_menu` VALUES (1560153030706397188, 1560148282678771714, 243);
INSERT INTO `sys_role_menu` VALUES (1560153030773506049, 1560148282678771714, 244);
INSERT INTO `sys_role_menu` VALUES (1560153030773506050, 1560148282678771714, 245);
INSERT INTO `sys_role_menu` VALUES (1560153030773506051, 1560148282678771714, 246);
INSERT INTO `sys_role_menu` VALUES (1560153030773506052, 1560148282678771714, 247);
INSERT INTO `sys_role_menu` VALUES (1560153030773506053, 1560148282678771714, 248);
INSERT INTO `sys_role_menu` VALUES (1560153030773506054, 1560148282678771714, 249);
INSERT INTO `sys_role_menu` VALUES (1560153030840614913, 1560148282678771714, 250);
INSERT INTO `sys_role_menu` VALUES (1560153030840614914, 1560148282678771714, 251);
INSERT INTO `sys_role_menu` VALUES (1560153030840614915, 1560148282678771714, 252);
INSERT INTO `sys_role_menu` VALUES (1560153030840614916, 1560148282678771714, 253);
INSERT INTO `sys_role_menu` VALUES (1560153030840614917, 1560148282678771714, 254);
INSERT INTO `sys_role_menu` VALUES (1560153030907723777, 1560148282678771714, 255);
INSERT INTO `sys_role_menu` VALUES (1560153030907723778, 1560148282678771714, 5206868747918117433);
INSERT INTO `sys_role_menu` VALUES (1560153030907723779, 1560148282678771714, 8477499608364957015);
INSERT INTO `sys_role_menu` VALUES (1560153030907723780, 1560148282678771714, 4757443910107701590);
INSERT INTO `sys_role_menu` VALUES (1560153030907723781, 1560148282678771714, 5016248121790626902);
INSERT INTO `sys_role_menu` VALUES (1560153030907723782, 1560148282678771714, 7516608184768822750);
INSERT INTO `sys_role_menu` VALUES (1560153030974832642, 1560148282678771714, 8293588485966810596);
INSERT INTO `sys_role_menu` VALUES (1560153030974832643, 1560148282678771714, 5523506981641034218);
INSERT INTO `sys_role_menu` VALUES (1560153030974832644, 1560148282678771714, 8494981115107347954);
INSERT INTO `sys_role_menu` VALUES (1560153030974832645, 1560148282678771714, 6045763991836034103);
INSERT INTO `sys_role_menu` VALUES (1560153031041941506, 1560148282678771714, 5934688735467584877);
INSERT INTO `sys_role_menu` VALUES (1560153031041941507, 1560148282678771714, 7561922286979539034);
INSERT INTO `sys_role_menu` VALUES (1560153031041941508, 1560148282678771714, 5457126757845470309);
INSERT INTO `sys_role_menu` VALUES (1560153031041941509, 1560148282678771714, 8590002636006568603);
INSERT INTO `sys_role_menu` VALUES (1560153031109050370, 1560148282678771714, 6257208246505535137);
INSERT INTO `sys_role_menu` VALUES (1560153031109050371, 1560148282678771714, 9118159149629415380);
INSERT INTO `sys_role_menu` VALUES (1560153031109050372, 1560148282678771714, 6354039257511829492);
INSERT INTO `sys_role_menu` VALUES (1560153031176159233, 1560148282678771714, 7639361031662168624);
INSERT INTO `sys_role_menu` VALUES (1560153031176159234, 1560148282678771714, 6507903030541782366);
INSERT INTO `sys_role_menu` VALUES (1560153031176159235, 1560148282678771714, 8562653372099681846);
INSERT INTO `sys_role_menu` VALUES (1560153031176159236, 1560148282678771714, 4830986669641223230);
INSERT INTO `sys_role_menu` VALUES (1560153031176159237, 1560148282678771714, 7974141988632466544);
INSERT INTO `sys_role_menu` VALUES (1560153031243268097, 1560148282678771714, 8313819116019873398);
INSERT INTO `sys_role_menu` VALUES (1560153031243268098, 1560148282678771714, 8931538668430408569);
INSERT INTO `sys_role_menu` VALUES (1560153031243268099, 1560148282678771714, 6122595225588020183);
INSERT INTO `sys_role_menu` VALUES (1560153031243268100, 1560148282678771714, 9152465645130034198);
INSERT INTO `sys_role_menu` VALUES (1560153031310376961, 1560148282678771714, 5012839659883217263);
INSERT INTO `sys_role_menu` VALUES (1560153031310376962, 1560148282678771714, 1534411830716350466);
INSERT INTO `sys_role_menu` VALUES (1560153031310376963, 1560148282678771714, 6280770118494855200);
INSERT INTO `sys_role_menu` VALUES (1560153031310376964, 1560148282678771714, 8775511535879974972);
INSERT INTO `sys_role_menu` VALUES (1560153031310376965, 1560148282678771714, 5624458298951752771);
INSERT INTO `sys_role_menu` VALUES (1560153031377485826, 1560148282678771714, 7254023492833901715);
INSERT INTO `sys_role_menu` VALUES (1560153031377485827, 1560148282678771714, 7318591877639154878);
INSERT INTO `sys_role_menu` VALUES (1560153031377485828, 1560148282678771714, 7812048842906278623);
INSERT INTO `sys_role_menu` VALUES (1560153031377485829, 1560148282678771714, 8633823152794346306);
INSERT INTO `sys_role_menu` VALUES (1560153031377485830, 1560148282678771714, 5893520734687245059);
INSERT INTO `sys_role_menu` VALUES (1560153031444594689, 1560148282678771714, 8868921547741003555);
INSERT INTO `sys_role_menu` VALUES (1560153031444594690, 1560148282678771714, 6326961474985796183);
INSERT INTO `sys_role_menu` VALUES (1560153031444594691, 1560148282678771714, 8833258242949762178);
INSERT INTO `sys_role_menu` VALUES (1560153031444594692, 1560148282678771714, 4967796364303223216);
INSERT INTO `sys_role_menu` VALUES (1560153031511703554, 1560148282678771714, 6900932255841430981);
INSERT INTO `sys_role_menu` VALUES (1560153031511703555, 1560148282678771714, 7629147488562302699);
INSERT INTO `sys_role_menu` VALUES (1560153031511703556, 1560148282678771714, 8016744771368544956);
INSERT INTO `sys_role_menu` VALUES (1560153031511703557, 1560148282678771714, 5002055413507195458);
INSERT INTO `sys_role_menu` VALUES (1560153031574618113, 1560148282678771714, 6290232610677682017);
INSERT INTO `sys_role_menu` VALUES (1560153031574618114, 1560148282678771714, 5803272132980049305);
INSERT INTO `sys_role_menu` VALUES (1560153031574618115, 1560148282678771714, 7395287038762629813);
INSERT INTO `sys_role_menu` VALUES (1560153031574618116, 1560148282678771714, 7323225129555267277);
INSERT INTO `sys_role_menu` VALUES (1560153031574618117, 1560148282678771714, 6347609996552577764);
INSERT INTO `sys_role_menu` VALUES (1560153031574618118, 1560148282678771714, 7682643462888492777);
INSERT INTO `sys_role_menu` VALUES (1560153031641726978, 1560148282678771714, 5721174223820977430);
INSERT INTO `sys_role_menu` VALUES (1560153031641726979, 1560148282678771714, 5469043739505193743);
INSERT INTO `sys_role_menu` VALUES (1560153031641726980, 1560148282678771714, 5459011313540319790);
INSERT INTO `sys_role_menu` VALUES (1560153031641726981, 1560148282678771714, 7723724790018395451);
INSERT INTO `sys_role_menu` VALUES (1560153031641726982, 1560148282678771714, 7764860386896651377);
INSERT INTO `sys_role_menu` VALUES (1560153031641726983, 1560148282678771714, 8254320371052864398);
INSERT INTO `sys_role_menu` VALUES (1560153031704641537, 1560148282678771714, 8273410905135864011);
INSERT INTO `sys_role_menu` VALUES (1560153031704641538, 1560148282678771714, 8515665128226371022);
INSERT INTO `sys_role_menu` VALUES (1560153031704641539, 1560148282678771714, 5000566847705099773);
INSERT INTO `sys_role_menu` VALUES (1560153031704641540, 1560148282678771714, 5555524248707858432);
INSERT INTO `sys_role_menu` VALUES (1560153031771750401, 1560148282678771714, 5901981891345615661);
INSERT INTO `sys_role_menu` VALUES (1560153031771750402, 1560148282678771714, 8342821565086833982);
INSERT INTO `sys_role_menu` VALUES (1560153031771750403, 1560148282678771714, 7044559182298687818);
INSERT INTO `sys_role_menu` VALUES (1560153031771750404, 1560148282678771714, 5714641428045333341);
INSERT INTO `sys_role_menu` VALUES (1560153031771750405, 1560148282678771714, 5801879553212456867);
INSERT INTO `sys_role_menu` VALUES (1560153031838859265, 1560148282678771714, 6739113753546148532);
INSERT INTO `sys_role_menu` VALUES (1560153031838859266, 1560148282678771714, 1534410297847214081);
INSERT INTO `sys_role_menu` VALUES (1560158451487801345, 1560148341927510018, 256);
INSERT INTO `sys_role_menu` VALUES (1560158451521355777, 1560148341927510018, 1560145190851772417);
INSERT INTO `sys_role_menu` VALUES (1560158451529744386, 1560148341927510018, 1);
INSERT INTO `sys_role_menu` VALUES (1560158451550715905, 1560148341927510018, 257);
INSERT INTO `sys_role_menu` VALUES (1560158451563298817, 1560148341927510018, 2);
INSERT INTO `sys_role_menu` VALUES (1560158451584270337, 1560148341927510018, 258);
INSERT INTO `sys_role_menu` VALUES (1560158451592658945, 1560148341927510018, 3);
INSERT INTO `sys_role_menu` VALUES (1560158451609436162, 1560148341927510018, 259);
INSERT INTO `sys_role_menu` VALUES (1560158451626213377, 1560148341927510018, 4);
INSERT INTO `sys_role_menu` VALUES (1560158451638796289, 1560148341927510018, 260);
INSERT INTO `sys_role_menu` VALUES (1560158451663962114, 1560148341927510018, 5);
INSERT INTO `sys_role_menu` VALUES (1560158451701710850, 1560148341927510018, 261);
INSERT INTO `sys_role_menu` VALUES (1560158451718488065, 1560148341927510018, 6);
INSERT INTO `sys_role_menu` VALUES (1560158451739459586, 1560148341927510018, 262);
INSERT INTO `sys_role_menu` VALUES (1560158451789791233, 1560148341927510018, 7);
INSERT INTO `sys_role_menu` VALUES (1560158451798179841, 1560148341927510018, 263);
INSERT INTO `sys_role_menu` VALUES (1560158451810762753, 1560148341927510018, 8);
INSERT INTO `sys_role_menu` VALUES (1560158451823345666, 1560148341927510018, 264);
INSERT INTO `sys_role_menu` VALUES (1560158451831734273, 1560148341927510018, 9);
INSERT INTO `sys_role_menu` VALUES (1560158451873677314, 1560148341927510018, 265);
INSERT INTO `sys_role_menu` VALUES (1560158451898843137, 1560148341927510018, 10);
INSERT INTO `sys_role_menu` VALUES (1560158451907231746, 1560148341927510018, 266);
INSERT INTO `sys_role_menu` VALUES (1560158451915620354, 1560148341927510018, 11);
INSERT INTO `sys_role_menu` VALUES (1560158451932397569, 1560148341927510018, 267);
INSERT INTO `sys_role_menu` VALUES (1560158451944980481, 1560148341927510018, 12);
INSERT INTO `sys_role_menu` VALUES (1560158451974340610, 1560148341927510018, 268);
INSERT INTO `sys_role_menu` VALUES (1560158451986923521, 1560148341927510018, 13);
INSERT INTO `sys_role_menu` VALUES (1560158452007895041, 1560148341927510018, 269);
INSERT INTO `sys_role_menu` VALUES (1560158452016283649, 1560148341927510018, 14);
INSERT INTO `sys_role_menu` VALUES (1560158452024672257, 1560148341927510018, 270);
INSERT INTO `sys_role_menu` VALUES (1560158452041449474, 1560148341927510018, 15);
INSERT INTO `sys_role_menu` VALUES (1560158452070809602, 1560148341927510018, 271);
INSERT INTO `sys_role_menu` VALUES (1560158452112752642, 1560148341927510018, 16);
INSERT INTO `sys_role_menu` VALUES (1560158452125335554, 1560148341927510018, 272);
INSERT INTO `sys_role_menu` VALUES (1560158452200833026, 1560148341927510018, 17);
INSERT INTO `sys_role_menu` VALUES (1560158452217610242, 1560148341927510018, 273);
INSERT INTO `sys_role_menu` VALUES (1560158452225998850, 1560148341927510018, 18);
INSERT INTO `sys_role_menu` VALUES (1560158452242776066, 1560148341927510018, 274);
INSERT INTO `sys_role_menu` VALUES (1560158452251164674, 1560148341927510018, 19);
INSERT INTO `sys_role_menu` VALUES (1560158452259553282, 1560148341927510018, 275);
INSERT INTO `sys_role_menu` VALUES (1560158452272136194, 1560148341927510018, 20);
INSERT INTO `sys_role_menu` VALUES (1560158452280524802, 1560148341927510018, 276);
INSERT INTO `sys_role_menu` VALUES (1560158452293107714, 1560148341927510018, 21);
INSERT INTO `sys_role_menu` VALUES (1560158452301496322, 1560148341927510018, 277);
INSERT INTO `sys_role_menu` VALUES (1560158452309884930, 1560148341927510018, 22);
INSERT INTO `sys_role_menu` VALUES (1560158452318273538, 1560148341927510018, 278);
INSERT INTO `sys_role_menu` VALUES (1560158452322467842, 1560148341927510018, 23);
INSERT INTO `sys_role_menu` VALUES (1560158452339245057, 1560148341927510018, 279);
INSERT INTO `sys_role_menu` VALUES (1560158452347633666, 1560148341927510018, 24);
INSERT INTO `sys_role_menu` VALUES (1560158452356022273, 1560148341927510018, 280);
INSERT INTO `sys_role_menu` VALUES (1560158452368605186, 1560148341927510018, 25);
INSERT INTO `sys_role_menu` VALUES (1560158452376993793, 1560148341927510018, 281);
INSERT INTO `sys_role_menu` VALUES (1560158452397965313, 1560148341927510018, 26);
INSERT INTO `sys_role_menu` VALUES (1560158452410548225, 1560148341927510018, 282);
INSERT INTO `sys_role_menu` VALUES (1560158452423131137, 1560148341927510018, 27);
INSERT INTO `sys_role_menu` VALUES (1560158452439908354, 1560148341927510018, 283);
INSERT INTO `sys_role_menu` VALUES (1560158452448296962, 1560148341927510018, 28);
INSERT INTO `sys_role_menu` VALUES (1560158452611874817, 1560148341927510018, 284);
INSERT INTO `sys_role_menu` VALUES (1560158452628652033, 1560148341927510018, 29);
INSERT INTO `sys_role_menu` VALUES (1560158452637040641, 1560148341927510018, 285);
INSERT INTO `sys_role_menu` VALUES (1560158452645429250, 1560148341927510018, 30);
INSERT INTO `sys_role_menu` VALUES (1560158452662206465, 1560148341927510018, 286);
INSERT INTO `sys_role_menu` VALUES (1560158452678983682, 1560148341927510018, 31);
INSERT INTO `sys_role_menu` VALUES (1560158452687372289, 1560148341927510018, 287);
INSERT INTO `sys_role_menu` VALUES (1560158452695760898, 1560148341927510018, 32);
INSERT INTO `sys_role_menu` VALUES (1560158452704149506, 1560148341927510018, 288);
INSERT INTO `sys_role_menu` VALUES (1560158452720926722, 1560148341927510018, 33);
INSERT INTO `sys_role_menu` VALUES (1560158452729315329, 1560148341927510018, 289);
INSERT INTO `sys_role_menu` VALUES (1560158452737703938, 1560148341927510018, 34);
INSERT INTO `sys_role_menu` VALUES (1560158452754481153, 1560148341927510018, 290);
INSERT INTO `sys_role_menu` VALUES (1560158452762869762, 1560148341927510018, 35);
INSERT INTO `sys_role_menu` VALUES (1560158452779646977, 1560148341927510018, 291);
INSERT INTO `sys_role_menu` VALUES (1560158452788035586, 1560148341927510018, 36);
INSERT INTO `sys_role_menu` VALUES (1560158452813201410, 1560148341927510018, 292);
INSERT INTO `sys_role_menu` VALUES (1560158452821590017, 1560148341927510018, 37);
INSERT INTO `sys_role_menu` VALUES (1560158452838367234, 1560148341927510018, 293);
INSERT INTO `sys_role_menu` VALUES (1560158452846755841, 1560148341927510018, 38);
INSERT INTO `sys_role_menu` VALUES (1560158452863533057, 1560148341927510018, 294);
INSERT INTO `sys_role_menu` VALUES (1560158452871921666, 1560148341927510018, 39);
INSERT INTO `sys_role_menu` VALUES (1560158452888698882, 1560148341927510018, 295);
INSERT INTO `sys_role_menu` VALUES (1560158452897087489, 1560148341927510018, 40);
INSERT INTO `sys_role_menu` VALUES (1560158452913864705, 1560148341927510018, 296);
INSERT INTO `sys_role_menu` VALUES (1560158452922253314, 1560148341927510018, 41);
INSERT INTO `sys_role_menu` VALUES (1560158452939030529, 1560148341927510018, 297);
INSERT INTO `sys_role_menu` VALUES (1560158452955807746, 1560148341927510018, 42);
INSERT INTO `sys_role_menu` VALUES (1560158452989362178, 1560148341927510018, 298);
INSERT INTO `sys_role_menu` VALUES (1560158453022916610, 1560148341927510018, 43);
INSERT INTO `sys_role_menu` VALUES (1560158453048082434, 1560148341927510018, 299);
INSERT INTO `sys_role_menu` VALUES (1560158453056471041, 1560148341927510018, 44);
INSERT INTO `sys_role_menu` VALUES (1560158453077442561, 1560148341927510018, 300);
INSERT INTO `sys_role_menu` VALUES (1560158453085831169, 1560148341927510018, 45);
INSERT INTO `sys_role_menu` VALUES (1560158453098414081, 1560148341927510018, 301);
INSERT INTO `sys_role_menu` VALUES (1560158453115191297, 1560148341927510018, 46);
INSERT INTO `sys_role_menu` VALUES (1560158453127774210, 1560148341927510018, 302);
INSERT INTO `sys_role_menu` VALUES (1560158453136162817, 1560148341927510018, 47);
INSERT INTO `sys_role_menu` VALUES (1560158453148745729, 1560148341927510018, 303);
INSERT INTO `sys_role_menu` VALUES (1560158453157134337, 1560148341927510018, 48);
INSERT INTO `sys_role_menu` VALUES (1560158453165522946, 1560148341927510018, 304);
INSERT INTO `sys_role_menu` VALUES (1560158453182300161, 1560148341927510018, 49);
INSERT INTO `sys_role_menu` VALUES (1560158453190688769, 1560148341927510018, 305);
INSERT INTO `sys_role_menu` VALUES (1560158453203271681, 1560148341927510018, 50);
INSERT INTO `sys_role_menu` VALUES (1560158453211660289, 1560148341927510018, 306);
INSERT INTO `sys_role_menu` VALUES (1560158453228437505, 1560148341927510018, 51);
INSERT INTO `sys_role_menu` VALUES (1560158453232631810, 1560148341927510018, 307);
INSERT INTO `sys_role_menu` VALUES (1560158453245214721, 1560148341927510018, 52);
INSERT INTO `sys_role_menu` VALUES (1560158453261991937, 1560148341927510018, 308);
INSERT INTO `sys_role_menu` VALUES (1560158453270380546, 1560148341927510018, 53);
INSERT INTO `sys_role_menu` VALUES (1560158453278769154, 1560148341927510018, 309);
INSERT INTO `sys_role_menu` VALUES (1560158453287157762, 1560148341927510018, 54);
INSERT INTO `sys_role_menu` VALUES (1560158453303934978, 1560148341927510018, 55);
INSERT INTO `sys_role_menu` VALUES (1560158453312323585, 1560148341927510018, 56);
INSERT INTO `sys_role_menu` VALUES (1560158453320712193, 1560148341927510018, 57);
INSERT INTO `sys_role_menu` VALUES (1560158453337489409, 1560148341927510018, 58);
INSERT INTO `sys_role_menu` VALUES (1560158453345878017, 1560148341927510018, 59);
INSERT INTO `sys_role_menu` VALUES (1560158453354266626, 1560148341927510018, 60);
INSERT INTO `sys_role_menu` VALUES (1560158453379432450, 1560148341927510018, 61);
INSERT INTO `sys_role_menu` VALUES (1560158453404598274, 1560148341927510018, 62);
INSERT INTO `sys_role_menu` VALUES (1560158453412986881, 1560148341927510018, 63);
INSERT INTO `sys_role_menu` VALUES (1560158453450735617, 1560148341927510018, 64);
INSERT INTO `sys_role_menu` VALUES (1560158453480095746, 1560148341927510018, 65);
INSERT INTO `sys_role_menu` VALUES (1560158453496872961, 1560148341927510018, 66);
INSERT INTO `sys_role_menu` VALUES (1560158453505261569, 1560148341927510018, 67);
INSERT INTO `sys_role_menu` VALUES (1560158453517844481, 1560148341927510018, 68);
INSERT INTO `sys_role_menu` VALUES (1560158453530427393, 1560148341927510018, 69);
INSERT INTO `sys_role_menu` VALUES (1560158453563981825, 1560148341927510018, 70);
INSERT INTO `sys_role_menu` VALUES (1560158453572370434, 1560148341927510018, 71);
INSERT INTO `sys_role_menu` VALUES (1560158453589147649, 1560148341927510018, 72);
INSERT INTO `sys_role_menu` VALUES (1560158453597536257, 1560148341927510018, 73);
INSERT INTO `sys_role_menu` VALUES (1560158453605924865, 1560148341927510018, 74);
INSERT INTO `sys_role_menu` VALUES (1560158453618507777, 1560148341927510018, 75);
INSERT INTO `sys_role_menu` VALUES (1560158453626896385, 1560148341927510018, 76);
INSERT INTO `sys_role_menu` VALUES (1560158453635284993, 1560148341927510018, 77);
INSERT INTO `sys_role_menu` VALUES (1560158453652062209, 1560148341927510018, 78);
INSERT INTO `sys_role_menu` VALUES (1560158453664645122, 1560148341927510018, 79);
INSERT INTO `sys_role_menu` VALUES (1560158453673033729, 1560148341927510018, 80);
INSERT INTO `sys_role_menu` VALUES (1560158453689810946, 1560148341927510018, 81);
INSERT INTO `sys_role_menu` VALUES (1560158453698199553, 1560148341927510018, 82);
INSERT INTO `sys_role_menu` VALUES (1560158453706588161, 1560148341927510018, 83);
INSERT INTO `sys_role_menu` VALUES (1560158453723365378, 1560148341927510018, 84);
INSERT INTO `sys_role_menu` VALUES (1560158453731753985, 1560148341927510018, 85);
INSERT INTO `sys_role_menu` VALUES (1560158453740142594, 1560148341927510018, 86);
INSERT INTO `sys_role_menu` VALUES (1560158453748531201, 1560148341927510018, 87);
INSERT INTO `sys_role_menu` VALUES (1560158453761114114, 1560148341927510018, 88);
INSERT INTO `sys_role_menu` VALUES (1560158453777891330, 1560148341927510018, 89);
INSERT INTO `sys_role_menu` VALUES (1560158453786279937, 1560148341927510018, 90);
INSERT INTO `sys_role_menu` VALUES (1560158453807251457, 1560148341927510018, 91);
INSERT INTO `sys_role_menu` VALUES (1560158453845000194, 1560148341927510018, 92);
INSERT INTO `sys_role_menu` VALUES (1560158453912109058, 1560148341927510018, 93);
INSERT INTO `sys_role_menu` VALUES (1560158453920497666, 1560148341927510018, 94);
INSERT INTO `sys_role_menu` VALUES (1560158453928886273, 1560148341927510018, 95);
INSERT INTO `sys_role_menu` VALUES (1560158453945663489, 1560148341927510018, 96);
INSERT INTO `sys_role_menu` VALUES (1560158453954052097, 1560148341927510018, 97);
INSERT INTO `sys_role_menu` VALUES (1560158453970829313, 1560148341927510018, 98);
INSERT INTO `sys_role_menu` VALUES (1560158453979217922, 1560148341927510018, 99);
INSERT INTO `sys_role_menu` VALUES (1560158453987606529, 1560148341927510018, 100);
INSERT INTO `sys_role_menu` VALUES (1560158453995995138, 1560148341927510018, 101);
INSERT INTO `sys_role_menu` VALUES (1560158454012772354, 1560148341927510018, 102);
INSERT INTO `sys_role_menu` VALUES (1560158454021160962, 1560148341927510018, 103);
INSERT INTO `sys_role_menu` VALUES (1560158454037938177, 1560148341927510018, 104);
INSERT INTO `sys_role_menu` VALUES (1560158454046326786, 1560148341927510018, 105);
INSERT INTO `sys_role_menu` VALUES (1560158454054715394, 1560148341927510018, 106);
INSERT INTO `sys_role_menu` VALUES (1560158454071492609, 1560148341927510018, 107);
INSERT INTO `sys_role_menu` VALUES (1560158454079881217, 1560148341927510018, 108);
INSERT INTO `sys_role_menu` VALUES (1560158454088269825, 1560148341927510018, 109);
INSERT INTO `sys_role_menu` VALUES (1560158454096658434, 1560148341927510018, 110);
INSERT INTO `sys_role_menu` VALUES (1560158454105047042, 1560148341927510018, 111);
INSERT INTO `sys_role_menu` VALUES (1560158454121824257, 1560148341927510018, 112);
INSERT INTO `sys_role_menu` VALUES (1560158454130212865, 1560148341927510018, 113);
INSERT INTO `sys_role_menu` VALUES (1560158454146990081, 1560148341927510018, 114);
INSERT INTO `sys_role_menu` VALUES (1560158454163767298, 1560148341927510018, 115);
INSERT INTO `sys_role_menu` VALUES (1560158454172155906, 1560148341927510018, 116);
INSERT INTO `sys_role_menu` VALUES (1560158454180544513, 1560148341927510018, 117);
INSERT INTO `sys_role_menu` VALUES (1560158454193127425, 1560148341927510018, 118);
INSERT INTO `sys_role_menu` VALUES (1560158454214098945, 1560148341927510018, 119);
INSERT INTO `sys_role_menu` VALUES (1560158454230876162, 1560148341927510018, 120);
INSERT INTO `sys_role_menu` VALUES (1560158454239264770, 1560148341927510018, 121);
INSERT INTO `sys_role_menu` VALUES (1560158454264430594, 1560148341927510018, 122);
INSERT INTO `sys_role_menu` VALUES (1560158454272819202, 1560148341927510018, 123);
INSERT INTO `sys_role_menu` VALUES (1560158454323150849, 1560148341927510018, 124);
INSERT INTO `sys_role_menu` VALUES (1560158454331539457, 1560148341927510018, 125);
INSERT INTO `sys_role_menu` VALUES (1560158454348316673, 1560148341927510018, 126);
INSERT INTO `sys_role_menu` VALUES (1560158454356705282, 1560148341927510018, 127);
INSERT INTO `sys_role_menu` VALUES (1560158454369288193, 1560148341927510018, 128);
INSERT INTO `sys_role_menu` VALUES (1560158454381871105, 1560148341927510018, 129);
INSERT INTO `sys_role_menu` VALUES (1560158454398648321, 1560148341927510018, 130);
INSERT INTO `sys_role_menu` VALUES (1560158454411231234, 1560148341927510018, 131);
INSERT INTO `sys_role_menu` VALUES (1560158454419619842, 1560148341927510018, 132);
INSERT INTO `sys_role_menu` VALUES (1560158454432202754, 1560148341927510018, 133);
INSERT INTO `sys_role_menu` VALUES (1560158454444785665, 1560148341927510018, 134);
INSERT INTO `sys_role_menu` VALUES (1560158454453174274, 1560148341927510018, 135);
INSERT INTO `sys_role_menu` VALUES (1560158454478340097, 1560148341927510018, 136);
INSERT INTO `sys_role_menu` VALUES (1560158454486728706, 1560148341927510018, 137);
INSERT INTO `sys_role_menu` VALUES (1560158454495117313, 1560148341927510018, 138);
INSERT INTO `sys_role_menu` VALUES (1560158454507700225, 1560148341927510018, 139);
INSERT INTO `sys_role_menu` VALUES (1560158454516088833, 1560148341927510018, 140);
INSERT INTO `sys_role_menu` VALUES (1560158454524477442, 1560148341927510018, 141);
INSERT INTO `sys_role_menu` VALUES (1560158454541254658, 1560148341927510018, 142);
INSERT INTO `sys_role_menu` VALUES (1560158454549643266, 1560148341927510018, 143);
INSERT INTO `sys_role_menu` VALUES (1560158454558031874, 1560148341927510018, 144);
INSERT INTO `sys_role_menu` VALUES (1560158454574809090, 1560148341927510018, 145);
INSERT INTO `sys_role_menu` VALUES (1560158454583197698, 1560148341927510018, 146);
INSERT INTO `sys_role_menu` VALUES (1560158454599974913, 1560148341927510018, 147);
INSERT INTO `sys_role_menu` VALUES (1560158454625140737, 1560148341927510018, 148);
INSERT INTO `sys_role_menu` VALUES (1560158454667083777, 1560148341927510018, 149);
INSERT INTO `sys_role_menu` VALUES (1560158454692249602, 1560148341927510018, 150);
INSERT INTO `sys_role_menu` VALUES (1560158454709026817, 1560148341927510018, 151);
INSERT INTO `sys_role_menu` VALUES (1560158454725804034, 1560148341927510018, 152);
INSERT INTO `sys_role_menu` VALUES (1560158454750969857, 1560148341927510018, 153);
INSERT INTO `sys_role_menu` VALUES (1560158454784524289, 1560148341927510018, 154);
INSERT INTO `sys_role_menu` VALUES (1560158454809690114, 1560148341927510018, 155);
INSERT INTO `sys_role_menu` VALUES (1560158454818078721, 1560148341927510018, 156);
INSERT INTO `sys_role_menu` VALUES (1560158454826467330, 1560148341927510018, 157);
INSERT INTO `sys_role_menu` VALUES (1560158454843244546, 1560148341927510018, 158);
INSERT INTO `sys_role_menu` VALUES (1560158454860021761, 1560148341927510018, 159);
INSERT INTO `sys_role_menu` VALUES (1560158454868410369, 1560148341927510018, 160);
INSERT INTO `sys_role_menu` VALUES (1560158454885187586, 1560148341927510018, 161);
INSERT INTO `sys_role_menu` VALUES (1560158454893576193, 1560148341927510018, 162);
INSERT INTO `sys_role_menu` VALUES (1560158454910353409, 1560148341927510018, 163);
INSERT INTO `sys_role_menu` VALUES (1560158454918742017, 1560148341927510018, 164);
INSERT INTO `sys_role_menu` VALUES (1560158454927130625, 1560148341927510018, 165);
INSERT INTO `sys_role_menu` VALUES (1560158454935519233, 1560148341927510018, 166);
INSERT INTO `sys_role_menu` VALUES (1560158454948102145, 1560148341927510018, 167);
INSERT INTO `sys_role_menu` VALUES (1560158454969073665, 1560148341927510018, 168);
INSERT INTO `sys_role_menu` VALUES (1560158454981656578, 1560148341927510018, 169);
INSERT INTO `sys_role_menu` VALUES (1560158455006822401, 1560148341927510018, 170);
INSERT INTO `sys_role_menu` VALUES (1560158455019405314, 1560148341927510018, 171);
INSERT INTO `sys_role_menu` VALUES (1560158455036182529, 1560148341927510018, 172);
INSERT INTO `sys_role_menu` VALUES (1560158455078125570, 1560148341927510018, 173);
INSERT INTO `sys_role_menu` VALUES (1560158455094902785, 1560148341927510018, 174);
INSERT INTO `sys_role_menu` VALUES (1560158455120068610, 1560148341927510018, 175);
INSERT INTO `sys_role_menu` VALUES (1560158455128457217, 1560148341927510018, 176);
INSERT INTO `sys_role_menu` VALUES (1560158455136845825, 1560148341927510018, 177);
INSERT INTO `sys_role_menu` VALUES (1560158455145234433, 1560148341927510018, 178);
INSERT INTO `sys_role_menu` VALUES (1560158455195566082, 1560148341927510018, 179);
INSERT INTO `sys_role_menu` VALUES (1560158455203954689, 1560148341927510018, 180);
INSERT INTO `sys_role_menu` VALUES (1560158455212343297, 1560148341927510018, 181);
INSERT INTO `sys_role_menu` VALUES (1560158455229120514, 1560148341927510018, 182);
INSERT INTO `sys_role_menu` VALUES (1560158455237509122, 1560148341927510018, 183);
INSERT INTO `sys_role_menu` VALUES (1560158455245897730, 1560148341927510018, 184);
INSERT INTO `sys_role_menu` VALUES (1560158455275257857, 1560148341927510018, 185);
INSERT INTO `sys_role_menu` VALUES (1560158455292035073, 1560148341927510018, 186);
INSERT INTO `sys_role_menu` VALUES (1560158455300423682, 1560148341927510018, 187);
INSERT INTO `sys_role_menu` VALUES (1560158455317200897, 1560148341927510018, 188);
INSERT INTO `sys_role_menu` VALUES (1560158455325589505, 1560148341927510018, 189);
INSERT INTO `sys_role_menu` VALUES (1560158455333978113, 1560148341927510018, 190);
INSERT INTO `sys_role_menu` VALUES (1560158455350755330, 1560148341927510018, 191);
INSERT INTO `sys_role_menu` VALUES (1560158455359143938, 1560148341927510018, 192);
INSERT INTO `sys_role_menu` VALUES (1560158455367532545, 1560148341927510018, 193);
INSERT INTO `sys_role_menu` VALUES (1560158455375921153, 1560148341927510018, 194);
INSERT INTO `sys_role_menu` VALUES (1560158455388504066, 1560148341927510018, 195);
INSERT INTO `sys_role_menu` VALUES (1560158455405281282, 1560148341927510018, 196);
INSERT INTO `sys_role_menu` VALUES (1560158455413669890, 1560148341927510018, 197);
INSERT INTO `sys_role_menu` VALUES (1560158455422058498, 1560148341927510018, 198);
INSERT INTO `sys_role_menu` VALUES (1560158455434641410, 1560148341927510018, 199);
INSERT INTO `sys_role_menu` VALUES (1560158455438835713, 1560148341927510018, 200);
INSERT INTO `sys_role_menu` VALUES (1560158455447224321, 1560148341927510018, 201);
INSERT INTO `sys_role_menu` VALUES (1560158455455612930, 1560148341927510018, 202);
INSERT INTO `sys_role_menu` VALUES (1560158455468195841, 1560148341927510018, 203);
INSERT INTO `sys_role_menu` VALUES (1560158455484973057, 1560148341927510018, 204);
INSERT INTO `sys_role_menu` VALUES (1560158455497555969, 1560148341927510018, 205);
INSERT INTO `sys_role_menu` VALUES (1560158455505944577, 1560148341927510018, 206);
INSERT INTO `sys_role_menu` VALUES (1560158455522721794, 1560148341927510018, 207);
INSERT INTO `sys_role_menu` VALUES (1560158455568859137, 1560148341927510018, 208);
INSERT INTO `sys_role_menu` VALUES (1560158455619190786, 1560148341927510018, 209);
INSERT INTO `sys_role_menu` VALUES (1560158455627579394, 1560148341927510018, 210);
INSERT INTO `sys_role_menu` VALUES (1560158455635968001, 1560148341927510018, 211);
INSERT INTO `sys_role_menu` VALUES (1560158455644356610, 1560148341927510018, 212);
INSERT INTO `sys_role_menu` VALUES (1560158455661133826, 1560148341927510018, 213);
INSERT INTO `sys_role_menu` VALUES (1560158455669522434, 1560148341927510018, 214);
INSERT INTO `sys_role_menu` VALUES (1560158455677911041, 1560148341927510018, 215);
INSERT INTO `sys_role_menu` VALUES (1560158455694688258, 1560148341927510018, 216);
INSERT INTO `sys_role_menu` VALUES (1560158455703076865, 1560148341927510018, 217);
INSERT INTO `sys_role_menu` VALUES (1560158455761797121, 1560148341927510018, 218);
INSERT INTO `sys_role_menu` VALUES (1560158455770185730, 1560148341927510018, 219);
INSERT INTO `sys_role_menu` VALUES (1560158455778574337, 1560148341927510018, 220);
INSERT INTO `sys_role_menu` VALUES (1560158455786962945, 1560148341927510018, 221);
INSERT INTO `sys_role_menu` VALUES (1560158455795351554, 1560148341927510018, 222);
INSERT INTO `sys_role_menu` VALUES (1560158455803740162, 1560148341927510018, 223);
INSERT INTO `sys_role_menu` VALUES (1560158455812128770, 1560148341927510018, 224);
INSERT INTO `sys_role_menu` VALUES (1560158455820517378, 1560148341927510018, 225);
INSERT INTO `sys_role_menu` VALUES (1560158455828905985, 1560148341927510018, 226);
INSERT INTO `sys_role_menu` VALUES (1560158455837294594, 1560148341927510018, 227);
INSERT INTO `sys_role_menu` VALUES (1560158455845683201, 1560148341927510018, 228);
INSERT INTO `sys_role_menu` VALUES (1560158455854071809, 1560148341927510018, 229);
INSERT INTO `sys_role_menu` VALUES (1560158455862460417, 1560148341927510018, 230);
INSERT INTO `sys_role_menu` VALUES (1560158455879237634, 1560148341927510018, 231);
INSERT INTO `sys_role_menu` VALUES (1560158455912792065, 1560148341927510018, 232);
INSERT INTO `sys_role_menu` VALUES (1560158455921180673, 1560148341927510018, 233);
INSERT INTO `sys_role_menu` VALUES (1560158455946346498, 1560148341927510018, 234);
INSERT INTO `sys_role_menu` VALUES (1560158455971512321, 1560148341927510018, 235);
INSERT INTO `sys_role_menu` VALUES (1560158455988289538, 1560148341927510018, 236);
INSERT INTO `sys_role_menu` VALUES (1560158456013455362, 1560148341927510018, 237);
INSERT INTO `sys_role_menu` VALUES (1560158456038621186, 1560148341927510018, 238);
INSERT INTO `sys_role_menu` VALUES (1560158456055398402, 1560148341927510018, 239);
INSERT INTO `sys_role_menu` VALUES (1560158456063787009, 1560148341927510018, 240);
INSERT INTO `sys_role_menu` VALUES (1560158456076369922, 1560148341927510018, 241);
INSERT INTO `sys_role_menu` VALUES (1560158456093147137, 1560148341927510018, 242);
INSERT INTO `sys_role_menu` VALUES (1560158456101535746, 1560148341927510018, 243);
INSERT INTO `sys_role_menu` VALUES (1560158456109924353, 1560148341927510018, 244);
INSERT INTO `sys_role_menu` VALUES (1560158456126701569, 1560148341927510018, 245);
INSERT INTO `sys_role_menu` VALUES (1560158456135090177, 1560148341927510018, 246);
INSERT INTO `sys_role_menu` VALUES (1560158456147673090, 1560148341927510018, 247);
INSERT INTO `sys_role_menu` VALUES (1560158456164450305, 1560148341927510018, 248);
INSERT INTO `sys_role_menu` VALUES (1560158456181227521, 1560148341927510018, 249);
INSERT INTO `sys_role_menu` VALUES (1560158456198004738, 1560148341927510018, 250);
INSERT INTO `sys_role_menu` VALUES (1560158456206393345, 1560148341927510018, 251);
INSERT INTO `sys_role_menu` VALUES (1560158456214781954, 1560148341927510018, 252);
INSERT INTO `sys_role_menu` VALUES (1560158456231559170, 1560148341927510018, 253);
INSERT INTO `sys_role_menu` VALUES (1560158456244142081, 1560148341927510018, 254);
INSERT INTO `sys_role_menu` VALUES (1560158456252530690, 1560148341927510018, 255);
INSERT INTO `sys_role_menu` VALUES (1560158456265113602, 1560148341927510018, 7656571819576826411);
INSERT INTO `sys_role_menu` VALUES (1560158456273502210, 1560148341927510018, 5014700545593967407);
INSERT INTO `sys_role_menu` VALUES (1560158456286085121, 1560148341927510018, 8088901678692869692);
INSERT INTO `sys_role_menu` VALUES (1560158456307056642, 1560148341927510018, 6299920358694835309);
INSERT INTO `sys_role_menu` VALUES (1560158456315445250, 1560148341927510018, 5642803668063215805);
INSERT INTO `sys_role_menu` VALUES (1560158456328028161, 1560148341927510018, 8834719374925492199);
INSERT INTO `sys_role_menu` VALUES (1560158456336416769, 1560148341927510018, 7882898168203081196);
INSERT INTO `sys_role_menu` VALUES (1560158456382554113, 1560148341927510018, 1542409869531873282);
INSERT INTO `sys_role_menu` VALUES (1560158456382554114, 1560148341927510018, 7346283523793183379);
INSERT INTO `sys_role_menu` VALUES (1560158456424497153, 1560148341927510018, 9208275275751949837);
INSERT INTO `sys_role_menu` VALUES (1560158456424497154, 1560148341927510018, 9173957282399717676);
INSERT INTO `sys_role_menu` VALUES (1560158456491606017, 1560148341927510018, 8973112451110891359);
INSERT INTO `sys_role_menu` VALUES (1560158456491606018, 1560148341927510018, 5753930915061776016);
INSERT INTO `sys_role_menu` VALUES (1560158456491606019, 1560148341927510018, 7288710125399936914);
INSERT INTO `sys_role_menu` VALUES (1560158456491606020, 1560148341927510018, 9022874385071881655);
INSERT INTO `sys_role_menu` VALUES (1560158456491606021, 1560148341927510018, 5378908852735764919);
INSERT INTO `sys_role_menu` VALUES (1560158456491606022, 1560148341927510018, 9202857208449608897);
INSERT INTO `sys_role_menu` VALUES (1560158456558714882, 1560148341927510018, 1560158326283632641);
INSERT INTO `sys_role_menu` VALUES (1560158456558714883, 1560148341927510018, 1560158090005905410);
INSERT INTO `sys_role_menu` VALUES (1560158456558714884, 1560148341927510018, 1534410297847214081);
INSERT INTO `sys_role_menu` VALUES (1561562668102426626, 1560148409690685442, 256);
INSERT INTO `sys_role_menu` VALUES (1561562668102426627, 1560148409690685442, 1);
INSERT INTO `sys_role_menu` VALUES (1561562668169535490, 1560148409690685442, 257);
INSERT INTO `sys_role_menu` VALUES (1561562668169535491, 1560148409690685442, 2);
INSERT INTO `sys_role_menu` VALUES (1561562668169535492, 1560148409690685442, 258);
INSERT INTO `sys_role_menu` VALUES (1561562668236644353, 1560148409690685442, 3);
INSERT INTO `sys_role_menu` VALUES (1561562668236644354, 1560148409690685442, 259);
INSERT INTO `sys_role_menu` VALUES (1561562668236644355, 1560148409690685442, 4);
INSERT INTO `sys_role_menu` VALUES (1561562668303753218, 1560148409690685442, 260);
INSERT INTO `sys_role_menu` VALUES (1561562668303753219, 1560148409690685442, 5);
INSERT INTO `sys_role_menu` VALUES (1561562668303753220, 1560148409690685442, 261);
INSERT INTO `sys_role_menu` VALUES (1561562668303753221, 1560148409690685442, 6);
INSERT INTO `sys_role_menu` VALUES (1561562668370862082, 1560148409690685442, 262);
INSERT INTO `sys_role_menu` VALUES (1561562668370862083, 1560148409690685442, 7);
INSERT INTO `sys_role_menu` VALUES (1561562668370862084, 1560148409690685442, 263);
INSERT INTO `sys_role_menu` VALUES (1561562668437970946, 1560148409690685442, 8);
INSERT INTO `sys_role_menu` VALUES (1561562668437970947, 1560148409690685442, 264);
INSERT INTO `sys_role_menu` VALUES (1561562668437970948, 1560148409690685442, 9);
INSERT INTO `sys_role_menu` VALUES (1561562668500885506, 1560148409690685442, 265);
INSERT INTO `sys_role_menu` VALUES (1561562668500885507, 1560148409690685442, 10);
INSERT INTO `sys_role_menu` VALUES (1561562668500885508, 1560148409690685442, 266);
INSERT INTO `sys_role_menu` VALUES (1561562668567994369, 1560148409690685442, 11);
INSERT INTO `sys_role_menu` VALUES (1561562668567994370, 1560148409690685442, 267);
INSERT INTO `sys_role_menu` VALUES (1561562668567994371, 1560148409690685442, 12);
INSERT INTO `sys_role_menu` VALUES (1561562668630908930, 1560148409690685442, 268);
INSERT INTO `sys_role_menu` VALUES (1561562668630908931, 1560148409690685442, 13);
INSERT INTO `sys_role_menu` VALUES (1561562668630908932, 1560148409690685442, 269);
INSERT INTO `sys_role_menu` VALUES (1561562668698017793, 1560148409690685442, 14);
INSERT INTO `sys_role_menu` VALUES (1561562668698017794, 1560148409690685442, 270);
INSERT INTO `sys_role_menu` VALUES (1561562668765126657, 1560148409690685442, 15);
INSERT INTO `sys_role_menu` VALUES (1561562668765126658, 1560148409690685442, 271);
INSERT INTO `sys_role_menu` VALUES (1561562668765126659, 1560148409690685442, 16);
INSERT INTO `sys_role_menu` VALUES (1561562668765126660, 1560148409690685442, 272);
INSERT INTO `sys_role_menu` VALUES (1561562668832235522, 1560148409690685442, 17);
INSERT INTO `sys_role_menu` VALUES (1561562668832235523, 1560148409690685442, 273);
INSERT INTO `sys_role_menu` VALUES (1561562668899344385, 1560148409690685442, 18);
INSERT INTO `sys_role_menu` VALUES (1561562668899344386, 1560148409690685442, 274);
INSERT INTO `sys_role_menu` VALUES (1561562668899344387, 1560148409690685442, 19);
INSERT INTO `sys_role_menu` VALUES (1561562668962258945, 1560148409690685442, 275);
INSERT INTO `sys_role_menu` VALUES (1561562668962258946, 1560148409690685442, 20);
INSERT INTO `sys_role_menu` VALUES (1561562668962258947, 1560148409690685442, 276);
INSERT INTO `sys_role_menu` VALUES (1561562668962258948, 1560148409690685442, 21);
INSERT INTO `sys_role_menu` VALUES (1561562669033562114, 1560148409690685442, 277);
INSERT INTO `sys_role_menu` VALUES (1561562669033562115, 1560148409690685442, 22);
INSERT INTO `sys_role_menu` VALUES (1561562669033562116, 1560148409690685442, 278);
INSERT INTO `sys_role_menu` VALUES (1561562669033562117, 1560148409690685442, 23);
INSERT INTO `sys_role_menu` VALUES (1561562669096476674, 1560148409690685442, 279);
INSERT INTO `sys_role_menu` VALUES (1561562669096476675, 1560148409690685442, 24);
INSERT INTO `sys_role_menu` VALUES (1561562669159391234, 1560148409690685442, 280);
INSERT INTO `sys_role_menu` VALUES (1561562669159391235, 1560148409690685442, 25);
INSERT INTO `sys_role_menu` VALUES (1561562669159391236, 1560148409690685442, 281);
INSERT INTO `sys_role_menu` VALUES (1561562669159391237, 1560148409690685442, 26);
INSERT INTO `sys_role_menu` VALUES (1561562669226500097, 1560148409690685442, 282);
INSERT INTO `sys_role_menu` VALUES (1561562669226500098, 1560148409690685442, 27);
INSERT INTO `sys_role_menu` VALUES (1561562669226500099, 1560148409690685442, 283);
INSERT INTO `sys_role_menu` VALUES (1561562669289414658, 1560148409690685442, 28);
INSERT INTO `sys_role_menu` VALUES (1561562669289414659, 1560148409690685442, 284);
INSERT INTO `sys_role_menu` VALUES (1561562669356523522, 1560148409690685442, 29);
INSERT INTO `sys_role_menu` VALUES (1561562669356523523, 1560148409690685442, 285);
INSERT INTO `sys_role_menu` VALUES (1561562669356523524, 1560148409690685442, 30);
INSERT INTO `sys_role_menu` VALUES (1561562669423632386, 1560148409690685442, 286);
INSERT INTO `sys_role_menu` VALUES (1561562669423632387, 1560148409690685442, 31);
INSERT INTO `sys_role_menu` VALUES (1561562669423632388, 1560148409690685442, 287);
INSERT INTO `sys_role_menu` VALUES (1561562669490741250, 1560148409690685442, 32);
INSERT INTO `sys_role_menu` VALUES (1561562669490741251, 1560148409690685442, 288);
INSERT INTO `sys_role_menu` VALUES (1561562669490741252, 1560148409690685442, 33);
INSERT INTO `sys_role_menu` VALUES (1561562669557850113, 1560148409690685442, 289);
INSERT INTO `sys_role_menu` VALUES (1561562669557850114, 1560148409690685442, 34);
INSERT INTO `sys_role_menu` VALUES (1561562669557850115, 1560148409690685442, 290);
INSERT INTO `sys_role_menu` VALUES (1561562669557850116, 1560148409690685442, 35);
INSERT INTO `sys_role_menu` VALUES (1561562669624958977, 1560148409690685442, 291);
INSERT INTO `sys_role_menu` VALUES (1561562669624958978, 1560148409690685442, 36);
INSERT INTO `sys_role_menu` VALUES (1561562669692067842, 1560148409690685442, 292);
INSERT INTO `sys_role_menu` VALUES (1561562669692067843, 1560148409690685442, 37);
INSERT INTO `sys_role_menu` VALUES (1561562669754982401, 1560148409690685442, 293);
INSERT INTO `sys_role_menu` VALUES (1561562669754982402, 1560148409690685442, 38);
INSERT INTO `sys_role_menu` VALUES (1561562669754982403, 1560148409690685442, 294);
INSERT INTO `sys_role_menu` VALUES (1561562669817896961, 1560148409690685442, 39);
INSERT INTO `sys_role_menu` VALUES (1561562669817896962, 1560148409690685442, 295);
INSERT INTO `sys_role_menu` VALUES (1561562669885005826, 1560148409690685442, 40);
INSERT INTO `sys_role_menu` VALUES (1561562669885005827, 1560148409690685442, 296);
INSERT INTO `sys_role_menu` VALUES (1561562669947920385, 1560148409690685442, 41);
INSERT INTO `sys_role_menu` VALUES (1561562669947920386, 1560148409690685442, 297);
INSERT INTO `sys_role_menu` VALUES (1561562669947920387, 1560148409690685442, 42);
INSERT INTO `sys_role_menu` VALUES (1561562669947920388, 1560148409690685442, 298);
INSERT INTO `sys_role_menu` VALUES (1561562670015029249, 1560148409690685442, 43);
INSERT INTO `sys_role_menu` VALUES (1561562670015029250, 1560148409690685442, 299);
INSERT INTO `sys_role_menu` VALUES (1561562670015029251, 1560148409690685442, 44);
INSERT INTO `sys_role_menu` VALUES (1561562670015029252, 1560148409690685442, 300);
INSERT INTO `sys_role_menu` VALUES (1561562670077943809, 1560148409690685442, 45);
INSERT INTO `sys_role_menu` VALUES (1561562670077943810, 1560148409690685442, 301);
INSERT INTO `sys_role_menu` VALUES (1561562670145052674, 1560148409690685442, 46);
INSERT INTO `sys_role_menu` VALUES (1561562670145052675, 1560148409690685442, 302);
INSERT INTO `sys_role_menu` VALUES (1561562670207967233, 1560148409690685442, 47);
INSERT INTO `sys_role_menu` VALUES (1561562670207967234, 1560148409690685442, 303);
INSERT INTO `sys_role_menu` VALUES (1561562670207967235, 1560148409690685442, 48);
INSERT INTO `sys_role_menu` VALUES (1561562670207967236, 1560148409690685442, 304);
INSERT INTO `sys_role_menu` VALUES (1561562670275076098, 1560148409690685442, 49);
INSERT INTO `sys_role_menu` VALUES (1561562670275076099, 1560148409690685442, 305);
INSERT INTO `sys_role_menu` VALUES (1561562670275076100, 1560148409690685442, 50);
INSERT INTO `sys_role_menu` VALUES (1561562670337990658, 1560148409690685442, 51);
INSERT INTO `sys_role_menu` VALUES (1561562670337990659, 1560148409690685442, 307);
INSERT INTO `sys_role_menu` VALUES (1561562670337990660, 1560148409690685442, 52);
INSERT INTO `sys_role_menu` VALUES (1561562670405099522, 1560148409690685442, 308);
INSERT INTO `sys_role_menu` VALUES (1561562670405099523, 1560148409690685442, 53);
INSERT INTO `sys_role_menu` VALUES (1561562670405099524, 1560148409690685442, 309);
INSERT INTO `sys_role_menu` VALUES (1561562670405099525, 1560148409690685442, 54);
INSERT INTO `sys_role_menu` VALUES (1561562670472208386, 1560148409690685442, 55);
INSERT INTO `sys_role_menu` VALUES (1561562670472208387, 1560148409690685442, 56);
INSERT INTO `sys_role_menu` VALUES (1561562670472208388, 1560148409690685442, 57);
INSERT INTO `sys_role_menu` VALUES (1561562670539317249, 1560148409690685442, 58);
INSERT INTO `sys_role_menu` VALUES (1561562670539317250, 1560148409690685442, 59);
INSERT INTO `sys_role_menu` VALUES (1561562670602231810, 1560148409690685442, 60);
INSERT INTO `sys_role_menu` VALUES (1561562670602231811, 1560148409690685442, 61);
INSERT INTO `sys_role_menu` VALUES (1561562670602231812, 1560148409690685442, 62);
INSERT INTO `sys_role_menu` VALUES (1561562670669340674, 1560148409690685442, 63);
INSERT INTO `sys_role_menu` VALUES (1561562670669340675, 1560148409690685442, 64);
INSERT INTO `sys_role_menu` VALUES (1561562670669340676, 1560148409690685442, 65);
INSERT INTO `sys_role_menu` VALUES (1561562670736449537, 1560148409690685442, 66);
INSERT INTO `sys_role_menu` VALUES (1561562670736449538, 1560148409690685442, 67);
INSERT INTO `sys_role_menu` VALUES (1561562670736449539, 1560148409690685442, 68);
INSERT INTO `sys_role_menu` VALUES (1561562670736449540, 1560148409690685442, 69);
INSERT INTO `sys_role_menu` VALUES (1561562670803558402, 1560148409690685442, 70);
INSERT INTO `sys_role_menu` VALUES (1561562670803558403, 1560148409690685442, 71);
INSERT INTO `sys_role_menu` VALUES (1561562670803558404, 1560148409690685442, 72);
INSERT INTO `sys_role_menu` VALUES (1561562670866472962, 1560148409690685442, 73);
INSERT INTO `sys_role_menu` VALUES (1561562670866472963, 1560148409690685442, 74);
INSERT INTO `sys_role_menu` VALUES (1561562670866472964, 1560148409690685442, 75);
INSERT INTO `sys_role_menu` VALUES (1561562670866472965, 1560148409690685442, 76);
INSERT INTO `sys_role_menu` VALUES (1561562670933581825, 1560148409690685442, 77);
INSERT INTO `sys_role_menu` VALUES (1561562670933581826, 1560148409690685442, 78);
INSERT INTO `sys_role_menu` VALUES (1561562670996496386, 1560148409690685442, 79);
INSERT INTO `sys_role_menu` VALUES (1561562671063605250, 1560148409690685442, 80);
INSERT INTO `sys_role_menu` VALUES (1561562671126519810, 1560148409690685442, 81);
INSERT INTO `sys_role_menu` VALUES (1561562671126519811, 1560148409690685442, 82);
INSERT INTO `sys_role_menu` VALUES (1561562671126519812, 1560148409690685442, 83);
INSERT INTO `sys_role_menu` VALUES (1561562671193628673, 1560148409690685442, 84);
INSERT INTO `sys_role_menu` VALUES (1561562671193628674, 1560148409690685442, 85);
INSERT INTO `sys_role_menu` VALUES (1561562671193628675, 1560148409690685442, 86);
INSERT INTO `sys_role_menu` VALUES (1561562671256543233, 1560148409690685442, 87);
INSERT INTO `sys_role_menu` VALUES (1561562671256543234, 1560148409690685442, 88);
INSERT INTO `sys_role_menu` VALUES (1561562671256543235, 1560148409690685442, 89);
INSERT INTO `sys_role_menu` VALUES (1561562671256543236, 1560148409690685442, 90);
INSERT INTO `sys_role_menu` VALUES (1561562671323652097, 1560148409690685442, 91);
INSERT INTO `sys_role_menu` VALUES (1561562671323652098, 1560148409690685442, 92);
INSERT INTO `sys_role_menu` VALUES (1561562671323652099, 1560148409690685442, 93);
INSERT INTO `sys_role_menu` VALUES (1561562671386566657, 1560148409690685442, 94);
INSERT INTO `sys_role_menu` VALUES (1561562671386566658, 1560148409690685442, 95);
INSERT INTO `sys_role_menu` VALUES (1561562671386566659, 1560148409690685442, 96);
INSERT INTO `sys_role_menu` VALUES (1561562671453675521, 1560148409690685442, 97);
INSERT INTO `sys_role_menu` VALUES (1561562671453675522, 1560148409690685442, 98);
INSERT INTO `sys_role_menu` VALUES (1561562671453675523, 1560148409690685442, 99);
INSERT INTO `sys_role_menu` VALUES (1561562671520784385, 1560148409690685442, 100);
INSERT INTO `sys_role_menu` VALUES (1561562671520784386, 1560148409690685442, 101);
INSERT INTO `sys_role_menu` VALUES (1561562671520784387, 1560148409690685442, 102);
INSERT INTO `sys_role_menu` VALUES (1561562671583698945, 1560148409690685442, 103);
INSERT INTO `sys_role_menu` VALUES (1561562671583698946, 1560148409690685442, 104);
INSERT INTO `sys_role_menu` VALUES (1561562671583698947, 1560148409690685442, 105);
INSERT INTO `sys_role_menu` VALUES (1561562671655002113, 1560148409690685442, 106);
INSERT INTO `sys_role_menu` VALUES (1561562671680167937, 1560148409690685442, 107);
INSERT INTO `sys_role_menu` VALUES (1561562671701139457, 1560148409690685442, 108);
INSERT INTO `sys_role_menu` VALUES (1561562671726305281, 1560148409690685442, 109);
INSERT INTO `sys_role_menu` VALUES (1561562671738888194, 1560148409690685442, 110);
INSERT INTO `sys_role_menu` VALUES (1561562671755665410, 1560148409690685442, 111);
INSERT INTO `sys_role_menu` VALUES (1561562671772442626, 1560148409690685442, 113);
INSERT INTO `sys_role_menu` VALUES (1561562671793414145, 1560148409690685442, 114);
INSERT INTO `sys_role_menu` VALUES (1561562671822774273, 1560148409690685442, 115);
INSERT INTO `sys_role_menu` VALUES (1561562671843745793, 1560148409690685442, 116);
INSERT INTO `sys_role_menu` VALUES (1561562671898271745, 1560148409690685442, 117);
INSERT INTO `sys_role_menu` VALUES (1561562671936020481, 1560148409690685442, 118);
INSERT INTO `sys_role_menu` VALUES (1561562671952797697, 1560148409690685442, 119);
INSERT INTO `sys_role_menu` VALUES (1561562671961186305, 1560148409690685442, 120);
INSERT INTO `sys_role_menu` VALUES (1561562671986352129, 1560148409690685442, 121);
INSERT INTO `sys_role_menu` VALUES (1561562672007323650, 1560148409690685442, 122);
INSERT INTO `sys_role_menu` VALUES (1561562672036683777, 1560148409690685442, 123);
INSERT INTO `sys_role_menu` VALUES (1561562672053460993, 1560148409690685442, 124);
INSERT INTO `sys_role_menu` VALUES (1561562672074432514, 1560148409690685442, 125);
INSERT INTO `sys_role_menu` VALUES (1561562672095404033, 1560148409690685442, 126);
INSERT INTO `sys_role_menu` VALUES (1561562672120569857, 1560148409690685442, 127);
INSERT INTO `sys_role_menu` VALUES (1561562672133152769, 1560148409690685442, 128);
INSERT INTO `sys_role_menu` VALUES (1561562672170901506, 1560148409690685442, 129);
INSERT INTO `sys_role_menu` VALUES (1561562672191873026, 1560148409690685442, 130);
INSERT INTO `sys_role_menu` VALUES (1561562672204455938, 1560148409690685442, 131);
INSERT INTO `sys_role_menu` VALUES (1561562672233816065, 1560148409690685442, 132);
INSERT INTO `sys_role_menu` VALUES (1561562672246398978, 1560148409690685442, 133);
INSERT INTO `sys_role_menu` VALUES (1561562672263176193, 1560148409690685442, 134);
INSERT INTO `sys_role_menu` VALUES (1561562672279953409, 1560148409690685442, 135);
INSERT INTO `sys_role_menu` VALUES (1561562672330285058, 1560148409690685442, 136);
INSERT INTO `sys_role_menu` VALUES (1561562672347062273, 1560148409690685442, 137);
INSERT INTO `sys_role_menu` VALUES (1561562672372228098, 1560148409690685442, 138);
INSERT INTO `sys_role_menu` VALUES (1561562672389005313, 1560148409690685442, 139);
INSERT INTO `sys_role_menu` VALUES (1561562672405782529, 1560148409690685442, 140);
INSERT INTO `sys_role_menu` VALUES (1561562672586137601, 1560148409690685442, 141);
INSERT INTO `sys_role_menu` VALUES (1561562672602914818, 1560148409690685442, 142);
INSERT INTO `sys_role_menu` VALUES (1561562672623886337, 1560148409690685442, 143);
INSERT INTO `sys_role_menu` VALUES (1561562672644857857, 1560148409690685442, 145);
INSERT INTO `sys_role_menu` VALUES (1561562672661635073, 1560148409690685442, 146);
INSERT INTO `sys_role_menu` VALUES (1561562672682606593, 1560148409690685442, 147);
INSERT INTO `sys_role_menu` VALUES (1561562672699383809, 1560148409690685442, 148);
INSERT INTO `sys_role_menu` VALUES (1561562672720355329, 1560148409690685442, 149);
INSERT INTO `sys_role_menu` VALUES (1561562672779075586, 1560148409690685442, 150);
INSERT INTO `sys_role_menu` VALUES (1561562672804241409, 1560148409690685442, 151);
INSERT INTO `sys_role_menu` VALUES (1561562672821018626, 1560148409690685442, 152);
INSERT INTO `sys_role_menu` VALUES (1561562672846184449, 1560148409690685442, 153);
INSERT INTO `sys_role_menu` VALUES (1561562672871350273, 1560148409690685442, 154);
INSERT INTO `sys_role_menu` VALUES (1561562672896516098, 1560148409690685442, 155);
INSERT INTO `sys_role_menu` VALUES (1561562672913293313, 1560148409690685442, 156);
INSERT INTO `sys_role_menu` VALUES (1561562672934264834, 1560148409690685442, 157);
INSERT INTO `sys_role_menu` VALUES (1561562672955236354, 1560148409690685442, 158);
INSERT INTO `sys_role_menu` VALUES (1561562672972013570, 1560148409690685442, 159);
INSERT INTO `sys_role_menu` VALUES (1561562672997179394, 1560148409690685442, 160);
INSERT INTO `sys_role_menu` VALUES (1561562673013956609, 1560148409690685442, 161);
INSERT INTO `sys_role_menu` VALUES (1561562673030733825, 1560148409690685442, 162);
INSERT INTO `sys_role_menu` VALUES (1561562673051705345, 1560148409690685442, 163);
INSERT INTO `sys_role_menu` VALUES (1561562673064288257, 1560148409690685442, 164);
INSERT INTO `sys_role_menu` VALUES (1561562673085259777, 1560148409690685442, 165);
INSERT INTO `sys_role_menu` VALUES (1561562673110425602, 1560148409690685442, 166);
INSERT INTO `sys_role_menu` VALUES (1561562673131397122, 1560148409690685442, 167);
INSERT INTO `sys_role_menu` VALUES (1561562673160757249, 1560148409690685442, 168);
INSERT INTO `sys_role_menu` VALUES (1561562673181728770, 1560148409690685442, 169);
INSERT INTO `sys_role_menu` VALUES (1561562673282392066, 1560148409690685442, 170);
INSERT INTO `sys_role_menu` VALUES (1561562673303363585, 1560148409690685442, 171);
INSERT INTO `sys_role_menu` VALUES (1561562673328529410, 1560148409690685442, 172);
INSERT INTO `sys_role_menu` VALUES (1561562673349500930, 1560148409690685442, 173);
INSERT INTO `sys_role_menu` VALUES (1561562673370472449, 1560148409690685442, 174);
INSERT INTO `sys_role_menu` VALUES (1561562673383055361, 1560148409690685442, 175);
INSERT INTO `sys_role_menu` VALUES (1561562673404026882, 1560148409690685442, 176);
INSERT INTO `sys_role_menu` VALUES (1561562673416609793, 1560148409690685442, 177);
INSERT INTO `sys_role_menu` VALUES (1561562673433387009, 1560148409690685442, 178);
INSERT INTO `sys_role_menu` VALUES (1561562673454358529, 1560148409690685442, 179);
INSERT INTO `sys_role_menu` VALUES (1561562673462747138, 1560148409690685442, 180);
INSERT INTO `sys_role_menu` VALUES (1561562673487912962, 1560148409690685442, 181);
INSERT INTO `sys_role_menu` VALUES (1561562673496301569, 1560148409690685442, 182);
INSERT INTO `sys_role_menu` VALUES (1561562673521467394, 1560148409690685442, 183);
INSERT INTO `sys_role_menu` VALUES (1561562673550827522, 1560148409690685442, 184);
INSERT INTO `sys_role_menu` VALUES (1561562673571799042, 1560148409690685442, 185);
INSERT INTO `sys_role_menu` VALUES (1561562673588576258, 1560148409690685442, 186);
INSERT INTO `sys_role_menu` VALUES (1561562673655685121, 1560148409690685442, 187);
INSERT INTO `sys_role_menu` VALUES (1561562673668268034, 1560148409690685442, 188);
INSERT INTO `sys_role_menu` VALUES (1561562673685045250, 1560148409690685442, 189);
INSERT INTO `sys_role_menu` VALUES (1561562673710211073, 1560148409690685442, 190);
INSERT INTO `sys_role_menu` VALUES (1561562673731182594, 1560148409690685442, 191);
INSERT INTO `sys_role_menu` VALUES (1561562673747959809, 1560148409690685442, 192);
INSERT INTO `sys_role_menu` VALUES (1561562673768931330, 1560148409690685442, 193);
INSERT INTO `sys_role_menu` VALUES (1561562673785708546, 1560148409690685442, 194);
INSERT INTO `sys_role_menu` VALUES (1561562673802485762, 1560148409690685442, 195);
INSERT INTO `sys_role_menu` VALUES (1561562673819262978, 1560148409690685442, 196);
INSERT INTO `sys_role_menu` VALUES (1561562673836040193, 1560148409690685442, 197);
INSERT INTO `sys_role_menu` VALUES (1561562673857011714, 1560148409690685442, 198);
INSERT INTO `sys_role_menu` VALUES (1561562673928314882, 1560148409690685442, 199);
INSERT INTO `sys_role_menu` VALUES (1561562673953480705, 1560148409690685442, 200);
INSERT INTO `sys_role_menu` VALUES (1561562673966063617, 1560148409690685442, 201);
INSERT INTO `sys_role_menu` VALUES (1561562673987035137, 1560148409690685442, 202);
INSERT INTO `sys_role_menu` VALUES (1561562674024783874, 1560148409690685442, 203);
INSERT INTO `sys_role_menu` VALUES (1561562674062532609, 1560148409690685442, 204);
INSERT INTO `sys_role_menu` VALUES (1561562674087698433, 1560148409690685442, 205);
INSERT INTO `sys_role_menu` VALUES (1561562674104475649, 1560148409690685442, 206);
INSERT INTO `sys_role_menu` VALUES (1561562674125447169, 1560148409690685442, 207);
INSERT INTO `sys_role_menu` VALUES (1561562674142224386, 1560148409690685442, 208);
INSERT INTO `sys_role_menu` VALUES (1561562674167390209, 1560148409690685442, 209);
INSERT INTO `sys_role_menu` VALUES (1561562674188361730, 1560148409690685442, 210);
INSERT INTO `sys_role_menu` VALUES (1561562674200944641, 1560148409690685442, 211);
INSERT INTO `sys_role_menu` VALUES (1561562674226110466, 1560148409690685442, 212);
INSERT INTO `sys_role_menu` VALUES (1561562674255470593, 1560148409690685442, 213);
INSERT INTO `sys_role_menu` VALUES (1561562674276442113, 1560148409690685442, 214);
INSERT INTO `sys_role_menu` VALUES (1561562674289025025, 1560148409690685442, 215);
INSERT INTO `sys_role_menu` VALUES (1561562674305802241, 1560148409690685442, 216);
INSERT INTO `sys_role_menu` VALUES (1561562674330968066, 1560148409690685442, 217);
INSERT INTO `sys_role_menu` VALUES (1561562674351939586, 1560148409690685442, 218);
INSERT INTO `sys_role_menu` VALUES (1561562674368716801, 1560148409690685442, 219);
INSERT INTO `sys_role_menu` VALUES (1561562674385494018, 1560148409690685442, 220);
INSERT INTO `sys_role_menu` VALUES (1561562674410659842, 1560148409690685442, 221);
INSERT INTO `sys_role_menu` VALUES (1561562674427437057, 1560148409690685442, 222);
INSERT INTO `sys_role_menu` VALUES (1561562674469380098, 1560148409690685442, 223);
INSERT INTO `sys_role_menu` VALUES (1561562674498740225, 1560148409690685442, 224);
INSERT INTO `sys_role_menu` VALUES (1561562674515517441, 1560148409690685442, 225);
INSERT INTO `sys_role_menu` VALUES (1561562674603597826, 1560148409690685442, 226);
INSERT INTO `sys_role_menu` VALUES (1561562674620375041, 1560148409690685442, 227);
INSERT INTO `sys_role_menu` VALUES (1561562674641346561, 1560148409690685442, 228);
INSERT INTO `sys_role_menu` VALUES (1561562674658123778, 1560148409690685442, 229);
INSERT INTO `sys_role_menu` VALUES (1561562674674900994, 1560148409690685442, 230);
INSERT INTO `sys_role_menu` VALUES (1561562674704261122, 1560148409690685442, 231);
INSERT INTO `sys_role_menu` VALUES (1561562674716844034, 1560148409690685442, 232);
INSERT INTO `sys_role_menu` VALUES (1561562674742009857, 1560148409690685442, 233);
INSERT INTO `sys_role_menu` VALUES (1561562674762981378, 1560148409690685442, 234);
INSERT INTO `sys_role_menu` VALUES (1561562674779758593, 1560148409690685442, 235);
INSERT INTO `sys_role_menu` VALUES (1561562674792341506, 1560148409690685442, 236);
INSERT INTO `sys_role_menu` VALUES (1561562674817507329, 1560148409690685442, 237);
INSERT INTO `sys_role_menu` VALUES (1561562674834284545, 1560148409690685442, 238);
INSERT INTO `sys_role_menu` VALUES (1561562674846867457, 1560148409690685442, 239);
INSERT INTO `sys_role_menu` VALUES (1561562674876227586, 1560148409690685442, 240);
INSERT INTO `sys_role_menu` VALUES (1561562674926559233, 1560148409690685442, 241);
INSERT INTO `sys_role_menu` VALUES (1561562674947530753, 1560148409690685442, 242);
INSERT INTO `sys_role_menu` VALUES (1561562674964307970, 1560148409690685442, 243);
INSERT INTO `sys_role_menu` VALUES (1561562674976890881, 1560148409690685442, 244);
INSERT INTO `sys_role_menu` VALUES (1561562675002056706, 1560148409690685442, 245);
INSERT INTO `sys_role_menu` VALUES (1561562675027222529, 1560148409690685442, 246);
INSERT INTO `sys_role_menu` VALUES (1561562675048194050, 1560148409690685442, 247);
INSERT INTO `sys_role_menu` VALUES (1561562675064971266, 1560148409690685442, 248);
INSERT INTO `sys_role_menu` VALUES (1561562675085942785, 1560148409690685442, 249);
INSERT INTO `sys_role_menu` VALUES (1561562675111108610, 1560148409690685442, 250);
INSERT INTO `sys_role_menu` VALUES (1561562675132080129, 1560148409690685442, 251);
INSERT INTO `sys_role_menu` VALUES (1561562675144663042, 1560148409690685442, 252);
INSERT INTO `sys_role_menu` VALUES (1561562675161440258, 1560148409690685442, 253);
INSERT INTO `sys_role_menu` VALUES (1561562675178217474, 1560148409690685442, 254);
INSERT INTO `sys_role_menu` VALUES (1561562675274686466, 1560148409690685442, 255);
INSERT INTO `sys_role_menu` VALUES (1561562675291463681, 1560148409690685442, 9208275275751949837);
INSERT INTO `sys_role_menu` VALUES (1561562675316629506, 1560148409690685442, 8973112451110891359);
INSERT INTO `sys_role_menu` VALUES (1561562675341795329, 1560148409690685442, 5753930915061776016);
INSERT INTO `sys_role_menu` VALUES (1561562675379544066, 1560148409690685442, 7288710125399936914);
INSERT INTO `sys_role_menu` VALUES (1561562675396321281, 1560148409690685442, 9022874385071881655);
INSERT INTO `sys_role_menu` VALUES (1561562675417292801, 1560148409690685442, 1560145190851772417);
INSERT INTO `sys_role_menu` VALUES (1561562675438264322, 1560148409690685442, 7346283523793183379);
INSERT INTO `sys_role_menu` VALUES (1561562675459235841, 1560148409690685442, 1534410297847214081);
INSERT INTO `sys_role_menu` VALUES (1574931382458875905, 1574926790555828225, 1538776469361139714);
INSERT INTO `sys_role_menu` VALUES (1574931382458875906, 1574926790555828225, 256);
INSERT INTO `sys_role_menu` VALUES (1574931382458875907, 1574926790555828225, 1560145190851772417);
INSERT INTO `sys_role_menu` VALUES (1574931382467264513, 1574926790555828225, 1);
INSERT INTO `sys_role_menu` VALUES (1574931382467264514, 1574926790555828225, 257);
INSERT INTO `sys_role_menu` VALUES (1574931382467264515, 1574926790555828225, 2);
INSERT INTO `sys_role_menu` VALUES (1574931382467264516, 1574926790555828225, 258);
INSERT INTO `sys_role_menu` VALUES (1574931382467264517, 1574926790555828225, 3);
INSERT INTO `sys_role_menu` VALUES (1574931382467264518, 1574926790555828225, 259);
INSERT INTO `sys_role_menu` VALUES (1574931382467264519, 1574926790555828225, 4);
INSERT INTO `sys_role_menu` VALUES (1574931382467264520, 1574926790555828225, 260);
INSERT INTO `sys_role_menu` VALUES (1574931382467264521, 1574926790555828225, 5);
INSERT INTO `sys_role_menu` VALUES (1574931382467264522, 1574926790555828225, 261);
INSERT INTO `sys_role_menu` VALUES (1574931382467264523, 1574926790555828225, 6);
INSERT INTO `sys_role_menu` VALUES (1574931382467264524, 1574926790555828225, 262);
INSERT INTO `sys_role_menu` VALUES (1574931382467264525, 1574926790555828225, 7);
INSERT INTO `sys_role_menu` VALUES (1574931382467264526, 1574926790555828225, 263);
INSERT INTO `sys_role_menu` VALUES (1574931382467264527, 1574926790555828225, 8);
INSERT INTO `sys_role_menu` VALUES (1574931382467264528, 1574926790555828225, 264);
INSERT INTO `sys_role_menu` VALUES (1574931382467264529, 1574926790555828225, 9);
INSERT INTO `sys_role_menu` VALUES (1574931382467264530, 1574926790555828225, 265);
INSERT INTO `sys_role_menu` VALUES (1574931382467264531, 1574926790555828225, 10);
INSERT INTO `sys_role_menu` VALUES (1574931382467264532, 1574926790555828225, 266);
INSERT INTO `sys_role_menu` VALUES (1574931382467264533, 1574926790555828225, 11);
INSERT INTO `sys_role_menu` VALUES (1574931382467264534, 1574926790555828225, 267);
INSERT INTO `sys_role_menu` VALUES (1574931382467264535, 1574926790555828225, 12);
INSERT INTO `sys_role_menu` VALUES (1574931382479847426, 1574926790555828225, 268);
INSERT INTO `sys_role_menu` VALUES (1574931382479847427, 1574926790555828225, 13);
INSERT INTO `sys_role_menu` VALUES (1574931382479847428, 1574926790555828225, 269);
INSERT INTO `sys_role_menu` VALUES (1574931382479847429, 1574926790555828225, 14);
INSERT INTO `sys_role_menu` VALUES (1574931382479847430, 1574926790555828225, 270);
INSERT INTO `sys_role_menu` VALUES (1574931382479847431, 1574926790555828225, 15);
INSERT INTO `sys_role_menu` VALUES (1574931382479847432, 1574926790555828225, 271);
INSERT INTO `sys_role_menu` VALUES (1574931382479847433, 1574926790555828225, 16);
INSERT INTO `sys_role_menu` VALUES (1574931382479847434, 1574926790555828225, 272);
INSERT INTO `sys_role_menu` VALUES (1574931382479847435, 1574926790555828225, 17);
INSERT INTO `sys_role_menu` VALUES (1574931382479847436, 1574926790555828225, 273);
INSERT INTO `sys_role_menu` VALUES (1574931382479847437, 1574926790555828225, 18);
INSERT INTO `sys_role_menu` VALUES (1574931382479847438, 1574926790555828225, 274);
INSERT INTO `sys_role_menu` VALUES (1574931382479847439, 1574926790555828225, 19);
INSERT INTO `sys_role_menu` VALUES (1574931382479847440, 1574926790555828225, 275);
INSERT INTO `sys_role_menu` VALUES (1574931382479847441, 1574926790555828225, 20);
INSERT INTO `sys_role_menu` VALUES (1574931382479847442, 1574926790555828225, 276);
INSERT INTO `sys_role_menu` VALUES (1574931382479847443, 1574926790555828225, 21);
INSERT INTO `sys_role_menu` VALUES (1574931382479847444, 1574926790555828225, 277);
INSERT INTO `sys_role_menu` VALUES (1574931382479847445, 1574926790555828225, 22);
INSERT INTO `sys_role_menu` VALUES (1574931382479847446, 1574926790555828225, 278);
INSERT INTO `sys_role_menu` VALUES (1574931382479847447, 1574926790555828225, 23);
INSERT INTO `sys_role_menu` VALUES (1574931382479847448, 1574926790555828225, 279);
INSERT INTO `sys_role_menu` VALUES (1574931382479847449, 1574926790555828225, 24);
INSERT INTO `sys_role_menu` VALUES (1574931382488236033, 1574926790555828225, 280);
INSERT INTO `sys_role_menu` VALUES (1574931382488236034, 1574926790555828225, 25);
INSERT INTO `sys_role_menu` VALUES (1574931382488236035, 1574926790555828225, 281);
INSERT INTO `sys_role_menu` VALUES (1574931382488236036, 1574926790555828225, 26);
INSERT INTO `sys_role_menu` VALUES (1574931382488236037, 1574926790555828225, 282);
INSERT INTO `sys_role_menu` VALUES (1574931382488236038, 1574926790555828225, 27);
INSERT INTO `sys_role_menu` VALUES (1574931382488236039, 1574926790555828225, 283);
INSERT INTO `sys_role_menu` VALUES (1574931382488236040, 1574926790555828225, 28);
INSERT INTO `sys_role_menu` VALUES (1574931382488236041, 1574926790555828225, 284);
INSERT INTO `sys_role_menu` VALUES (1574931382488236042, 1574926790555828225, 29);
INSERT INTO `sys_role_menu` VALUES (1574931382488236043, 1574926790555828225, 285);
INSERT INTO `sys_role_menu` VALUES (1574931382488236044, 1574926790555828225, 30);
INSERT INTO `sys_role_menu` VALUES (1574931382488236045, 1574926790555828225, 286);
INSERT INTO `sys_role_menu` VALUES (1574931382488236046, 1574926790555828225, 31);
INSERT INTO `sys_role_menu` VALUES (1574931382488236047, 1574926790555828225, 287);
INSERT INTO `sys_role_menu` VALUES (1574931382488236048, 1574926790555828225, 32);
INSERT INTO `sys_role_menu` VALUES (1574931382488236049, 1574926790555828225, 288);
INSERT INTO `sys_role_menu` VALUES (1574931382488236050, 1574926790555828225, 33);
INSERT INTO `sys_role_menu` VALUES (1574931382488236051, 1574926790555828225, 289);
INSERT INTO `sys_role_menu` VALUES (1574931382488236052, 1574926790555828225, 34);
INSERT INTO `sys_role_menu` VALUES (1574931382488236053, 1574926790555828225, 290);
INSERT INTO `sys_role_menu` VALUES (1574931382488236054, 1574926790555828225, 35);
INSERT INTO `sys_role_menu` VALUES (1574931382488236055, 1574926790555828225, 291);
INSERT INTO `sys_role_menu` VALUES (1574931382488236056, 1574926790555828225, 36);
INSERT INTO `sys_role_menu` VALUES (1574931382500818945, 1574926790555828225, 292);
INSERT INTO `sys_role_menu` VALUES (1574931382500818946, 1574926790555828225, 37);
INSERT INTO `sys_role_menu` VALUES (1574931382500818947, 1574926790555828225, 293);
INSERT INTO `sys_role_menu` VALUES (1574931382500818948, 1574926790555828225, 38);
INSERT INTO `sys_role_menu` VALUES (1574931382500818949, 1574926790555828225, 294);
INSERT INTO `sys_role_menu` VALUES (1574931382500818950, 1574926790555828225, 39);
INSERT INTO `sys_role_menu` VALUES (1574931382500818951, 1574926790555828225, 295);
INSERT INTO `sys_role_menu` VALUES (1574931382500818952, 1574926790555828225, 40);
INSERT INTO `sys_role_menu` VALUES (1574931382500818953, 1574926790555828225, 296);
INSERT INTO `sys_role_menu` VALUES (1574931382500818954, 1574926790555828225, 41);
INSERT INTO `sys_role_menu` VALUES (1574931382500818955, 1574926790555828225, 297);
INSERT INTO `sys_role_menu` VALUES (1574931382500818956, 1574926790555828225, 42);
INSERT INTO `sys_role_menu` VALUES (1574931382500818957, 1574926790555828225, 298);
INSERT INTO `sys_role_menu` VALUES (1574931382500818958, 1574926790555828225, 43);
INSERT INTO `sys_role_menu` VALUES (1574931382500818959, 1574926790555828225, 299);
INSERT INTO `sys_role_menu` VALUES (1574931382500818960, 1574926790555828225, 44);
INSERT INTO `sys_role_menu` VALUES (1574931382500818961, 1574926790555828225, 300);
INSERT INTO `sys_role_menu` VALUES (1574931382500818962, 1574926790555828225, 45);
INSERT INTO `sys_role_menu` VALUES (1574931382500818963, 1574926790555828225, 301);
INSERT INTO `sys_role_menu` VALUES (1574931382500818964, 1574926790555828225, 46);
INSERT INTO `sys_role_menu` VALUES (1574931382500818965, 1574926790555828225, 302);
INSERT INTO `sys_role_menu` VALUES (1574931382500818966, 1574926790555828225, 47);
INSERT INTO `sys_role_menu` VALUES (1574931382500818967, 1574926790555828225, 303);
INSERT INTO `sys_role_menu` VALUES (1574931382500818968, 1574926790555828225, 48);
INSERT INTO `sys_role_menu` VALUES (1574931382500818969, 1574926790555828225, 304);
INSERT INTO `sys_role_menu` VALUES (1574931382500818970, 1574926790555828225, 49);
INSERT INTO `sys_role_menu` VALUES (1574931382513401857, 1574926790555828225, 305);
INSERT INTO `sys_role_menu` VALUES (1574931382513401858, 1574926790555828225, 50);
INSERT INTO `sys_role_menu` VALUES (1574931382513401859, 1574926790555828225, 306);
INSERT INTO `sys_role_menu` VALUES (1574931382513401860, 1574926790555828225, 51);
INSERT INTO `sys_role_menu` VALUES (1574931382513401861, 1574926790555828225, 307);
INSERT INTO `sys_role_menu` VALUES (1574931382513401862, 1574926790555828225, 52);
INSERT INTO `sys_role_menu` VALUES (1574931382513401863, 1574926790555828225, 308);
INSERT INTO `sys_role_menu` VALUES (1574931382513401864, 1574926790555828225, 53);
INSERT INTO `sys_role_menu` VALUES (1574931382513401865, 1574926790555828225, 309);
INSERT INTO `sys_role_menu` VALUES (1574931382513401866, 1574926790555828225, 54);
INSERT INTO `sys_role_menu` VALUES (1574931382513401867, 1574926790555828225, 55);
INSERT INTO `sys_role_menu` VALUES (1574931382513401868, 1574926790555828225, 56);
INSERT INTO `sys_role_menu` VALUES (1574931382513401869, 1574926790555828225, 57);
INSERT INTO `sys_role_menu` VALUES (1574931382513401870, 1574926790555828225, 58);
INSERT INTO `sys_role_menu` VALUES (1574931382513401871, 1574926790555828225, 59);
INSERT INTO `sys_role_menu` VALUES (1574931382513401872, 1574926790555828225, 60);
INSERT INTO `sys_role_menu` VALUES (1574931382513401873, 1574926790555828225, 61);
INSERT INTO `sys_role_menu` VALUES (1574931382513401874, 1574926790555828225, 62);
INSERT INTO `sys_role_menu` VALUES (1574931382513401875, 1574926790555828225, 63);
INSERT INTO `sys_role_menu` VALUES (1574931382513401876, 1574926790555828225, 64);
INSERT INTO `sys_role_menu` VALUES (1574931382513401877, 1574926790555828225, 65);
INSERT INTO `sys_role_menu` VALUES (1574931382513401878, 1574926790555828225, 66);
INSERT INTO `sys_role_menu` VALUES (1574931382513401879, 1574926790555828225, 67);
INSERT INTO `sys_role_menu` VALUES (1574931382513401880, 1574926790555828225, 68);
INSERT INTO `sys_role_menu` VALUES (1574931382513401881, 1574926790555828225, 69);
INSERT INTO `sys_role_menu` VALUES (1574931382513401882, 1574926790555828225, 70);
INSERT INTO `sys_role_menu` VALUES (1574931382513401883, 1574926790555828225, 71);
INSERT INTO `sys_role_menu` VALUES (1574931382525984769, 1574926790555828225, 72);
INSERT INTO `sys_role_menu` VALUES (1574931382525984770, 1574926790555828225, 73);
INSERT INTO `sys_role_menu` VALUES (1574931382525984771, 1574926790555828225, 74);
INSERT INTO `sys_role_menu` VALUES (1574931382525984772, 1574926790555828225, 75);
INSERT INTO `sys_role_menu` VALUES (1574931382525984773, 1574926790555828225, 76);
INSERT INTO `sys_role_menu` VALUES (1574931382525984774, 1574926790555828225, 77);
INSERT INTO `sys_role_menu` VALUES (1574931382525984775, 1574926790555828225, 78);
INSERT INTO `sys_role_menu` VALUES (1574931382525984776, 1574926790555828225, 79);
INSERT INTO `sys_role_menu` VALUES (1574931382525984777, 1574926790555828225, 80);
INSERT INTO `sys_role_menu` VALUES (1574931382525984778, 1574926790555828225, 81);
INSERT INTO `sys_role_menu` VALUES (1574931382525984779, 1574926790555828225, 82);
INSERT INTO `sys_role_menu` VALUES (1574931382525984780, 1574926790555828225, 83);
INSERT INTO `sys_role_menu` VALUES (1574931382525984781, 1574926790555828225, 84);
INSERT INTO `sys_role_menu` VALUES (1574931382525984782, 1574926790555828225, 85);
INSERT INTO `sys_role_menu` VALUES (1574931382525984783, 1574926790555828225, 86);
INSERT INTO `sys_role_menu` VALUES (1574931382525984784, 1574926790555828225, 87);
INSERT INTO `sys_role_menu` VALUES (1574931382525984785, 1574926790555828225, 88);
INSERT INTO `sys_role_menu` VALUES (1574931382525984786, 1574926790555828225, 89);
INSERT INTO `sys_role_menu` VALUES (1574931382525984787, 1574926790555828225, 90);
INSERT INTO `sys_role_menu` VALUES (1574931382525984788, 1574926790555828225, 91);
INSERT INTO `sys_role_menu` VALUES (1574931382525984789, 1574926790555828225, 92);
INSERT INTO `sys_role_menu` VALUES (1574931382525984790, 1574926790555828225, 93);
INSERT INTO `sys_role_menu` VALUES (1574931382525984791, 1574926790555828225, 94);
INSERT INTO `sys_role_menu` VALUES (1574931382525984792, 1574926790555828225, 95);
INSERT INTO `sys_role_menu` VALUES (1574931382525984793, 1574926790555828225, 96);
INSERT INTO `sys_role_menu` VALUES (1574931382525984794, 1574926790555828225, 97);
INSERT INTO `sys_role_menu` VALUES (1574931382525984795, 1574926790555828225, 98);
INSERT INTO `sys_role_menu` VALUES (1574931382525984796, 1574926790555828225, 99);
INSERT INTO `sys_role_menu` VALUES (1574931382525984797, 1574926790555828225, 100);
INSERT INTO `sys_role_menu` VALUES (1574931382525984798, 1574926790555828225, 101);
INSERT INTO `sys_role_menu` VALUES (1574931382525984799, 1574926790555828225, 102);
INSERT INTO `sys_role_menu` VALUES (1574931382525984800, 1574926790555828225, 103);
INSERT INTO `sys_role_menu` VALUES (1574931382525984801, 1574926790555828225, 104);
INSERT INTO `sys_role_menu` VALUES (1574931382525984802, 1574926790555828225, 105);
INSERT INTO `sys_role_menu` VALUES (1574931382525984803, 1574926790555828225, 106);
INSERT INTO `sys_role_menu` VALUES (1574931382525984804, 1574926790555828225, 107);
INSERT INTO `sys_role_menu` VALUES (1574931382525984805, 1574926790555828225, 108);
INSERT INTO `sys_role_menu` VALUES (1574931382525984806, 1574926790555828225, 109);
INSERT INTO `sys_role_menu` VALUES (1574931382525984807, 1574926790555828225, 110);
INSERT INTO `sys_role_menu` VALUES (1574931382525984808, 1574926790555828225, 111);
INSERT INTO `sys_role_menu` VALUES (1574931382525984809, 1574926790555828225, 112);
INSERT INTO `sys_role_menu` VALUES (1574931382525984810, 1574926790555828225, 113);
INSERT INTO `sys_role_menu` VALUES (1574931382525984811, 1574926790555828225, 114);
INSERT INTO `sys_role_menu` VALUES (1574931382525984812, 1574926790555828225, 115);
INSERT INTO `sys_role_menu` VALUES (1574931382525984813, 1574926790555828225, 116);
INSERT INTO `sys_role_menu` VALUES (1574931382525984814, 1574926790555828225, 117);
INSERT INTO `sys_role_menu` VALUES (1574931382525984815, 1574926790555828225, 118);
INSERT INTO `sys_role_menu` VALUES (1574931382525984816, 1574926790555828225, 119);
INSERT INTO `sys_role_menu` VALUES (1574931382525984817, 1574926790555828225, 120);
INSERT INTO `sys_role_menu` VALUES (1574931382525984818, 1574926790555828225, 121);
INSERT INTO `sys_role_menu` VALUES (1574931382525984819, 1574926790555828225, 122);
INSERT INTO `sys_role_menu` VALUES (1574931382525984820, 1574926790555828225, 123);
INSERT INTO `sys_role_menu` VALUES (1574931382525984821, 1574926790555828225, 124);
INSERT INTO `sys_role_menu` VALUES (1574931382525984822, 1574926790555828225, 125);
INSERT INTO `sys_role_menu` VALUES (1574931382525984823, 1574926790555828225, 126);
INSERT INTO `sys_role_menu` VALUES (1574931382525984824, 1574926790555828225, 127);
INSERT INTO `sys_role_menu` VALUES (1574931382525984825, 1574926790555828225, 128);
INSERT INTO `sys_role_menu` VALUES (1574931382525984826, 1574926790555828225, 129);
INSERT INTO `sys_role_menu` VALUES (1574931382525984827, 1574926790555828225, 130);
INSERT INTO `sys_role_menu` VALUES (1574931382525984828, 1574926790555828225, 131);
INSERT INTO `sys_role_menu` VALUES (1574931382525984829, 1574926790555828225, 132);
INSERT INTO `sys_role_menu` VALUES (1574931382525984830, 1574926790555828225, 133);
INSERT INTO `sys_role_menu` VALUES (1574931382525984831, 1574926790555828225, 134);
INSERT INTO `sys_role_menu` VALUES (1574931382525984832, 1574926790555828225, 135);
INSERT INTO `sys_role_menu` VALUES (1574931382525984833, 1574926790555828225, 136);
INSERT INTO `sys_role_menu` VALUES (1574931382525984834, 1574926790555828225, 137);
INSERT INTO `sys_role_menu` VALUES (1574931382525984835, 1574926790555828225, 138);
INSERT INTO `sys_role_menu` VALUES (1574931382525984836, 1574926790555828225, 139);
INSERT INTO `sys_role_menu` VALUES (1574931382525984837, 1574926790555828225, 140);
INSERT INTO `sys_role_menu` VALUES (1574931382525984838, 1574926790555828225, 141);
INSERT INTO `sys_role_menu` VALUES (1574931382525984839, 1574926790555828225, 142);
INSERT INTO `sys_role_menu` VALUES (1574931382525984840, 1574926790555828225, 143);
INSERT INTO `sys_role_menu` VALUES (1574931382525984841, 1574926790555828225, 144);
INSERT INTO `sys_role_menu` VALUES (1574931382525984842, 1574926790555828225, 145);
INSERT INTO `sys_role_menu` VALUES (1574931382525984843, 1574926790555828225, 146);
INSERT INTO `sys_role_menu` VALUES (1574931382525984844, 1574926790555828225, 147);
INSERT INTO `sys_role_menu` VALUES (1574931382525984845, 1574926790555828225, 148);
INSERT INTO `sys_role_menu` VALUES (1574931382525984846, 1574926790555828225, 149);
INSERT INTO `sys_role_menu` VALUES (1574931382525984847, 1574926790555828225, 150);
INSERT INTO `sys_role_menu` VALUES (1574931382525984848, 1574926790555828225, 151);
INSERT INTO `sys_role_menu` VALUES (1574931382525984849, 1574926790555828225, 152);
INSERT INTO `sys_role_menu` VALUES (1574931382525984850, 1574926790555828225, 153);
INSERT INTO `sys_role_menu` VALUES (1574931382525984851, 1574926790555828225, 154);
INSERT INTO `sys_role_menu` VALUES (1574931382525984852, 1574926790555828225, 155);
INSERT INTO `sys_role_menu` VALUES (1574931382525984853, 1574926790555828225, 156);
INSERT INTO `sys_role_menu` VALUES (1574931382525984854, 1574926790555828225, 157);
INSERT INTO `sys_role_menu` VALUES (1574931382525984855, 1574926790555828225, 158);
INSERT INTO `sys_role_menu` VALUES (1574931382525984856, 1574926790555828225, 159);
INSERT INTO `sys_role_menu` VALUES (1574931382525984857, 1574926790555828225, 160);
INSERT INTO `sys_role_menu` VALUES (1574931382525984858, 1574926790555828225, 161);
INSERT INTO `sys_role_menu` VALUES (1574931382525984859, 1574926790555828225, 162);
INSERT INTO `sys_role_menu` VALUES (1574931382525984860, 1574926790555828225, 163);
INSERT INTO `sys_role_menu` VALUES (1574931382525984861, 1574926790555828225, 164);
INSERT INTO `sys_role_menu` VALUES (1574931382525984862, 1574926790555828225, 165);
INSERT INTO `sys_role_menu` VALUES (1574931382525984863, 1574926790555828225, 166);
INSERT INTO `sys_role_menu` VALUES (1574931382525984864, 1574926790555828225, 167);
INSERT INTO `sys_role_menu` VALUES (1574931382525984865, 1574926790555828225, 168);
INSERT INTO `sys_role_menu` VALUES (1574931382525984866, 1574926790555828225, 169);
INSERT INTO `sys_role_menu` VALUES (1574931382525984867, 1574926790555828225, 170);
INSERT INTO `sys_role_menu` VALUES (1574931382525984868, 1574926790555828225, 171);
INSERT INTO `sys_role_menu` VALUES (1574931382525984869, 1574926790555828225, 172);
INSERT INTO `sys_role_menu` VALUES (1574931382525984870, 1574926790555828225, 173);
INSERT INTO `sys_role_menu` VALUES (1574931382525984871, 1574926790555828225, 174);
INSERT INTO `sys_role_menu` VALUES (1574931382525984872, 1574926790555828225, 175);
INSERT INTO `sys_role_menu` VALUES (1574931382525984873, 1574926790555828225, 176);
INSERT INTO `sys_role_menu` VALUES (1574931382525984874, 1574926790555828225, 177);
INSERT INTO `sys_role_menu` VALUES (1574931382525984875, 1574926790555828225, 178);
INSERT INTO `sys_role_menu` VALUES (1574931382525984876, 1574926790555828225, 179);
INSERT INTO `sys_role_menu` VALUES (1574931382525984877, 1574926790555828225, 180);
INSERT INTO `sys_role_menu` VALUES (1574931382525984878, 1574926790555828225, 181);
INSERT INTO `sys_role_menu` VALUES (1574931382525984879, 1574926790555828225, 182);
INSERT INTO `sys_role_menu` VALUES (1574931382525984880, 1574926790555828225, 183);
INSERT INTO `sys_role_menu` VALUES (1574931382525984881, 1574926790555828225, 184);
INSERT INTO `sys_role_menu` VALUES (1574931382525984882, 1574926790555828225, 185);
INSERT INTO `sys_role_menu` VALUES (1574931382525984883, 1574926790555828225, 186);
INSERT INTO `sys_role_menu` VALUES (1574931382525984884, 1574926790555828225, 187);
INSERT INTO `sys_role_menu` VALUES (1574931382525984885, 1574926790555828225, 188);
INSERT INTO `sys_role_menu` VALUES (1574931382525984886, 1574926790555828225, 189);
INSERT INTO `sys_role_menu` VALUES (1574931382525984887, 1574926790555828225, 190);
INSERT INTO `sys_role_menu` VALUES (1574931382525984888, 1574926790555828225, 191);
INSERT INTO `sys_role_menu` VALUES (1574931382525984889, 1574926790555828225, 192);
INSERT INTO `sys_role_menu` VALUES (1574931382525984890, 1574926790555828225, 193);
INSERT INTO `sys_role_menu` VALUES (1574931382525984891, 1574926790555828225, 194);
INSERT INTO `sys_role_menu` VALUES (1574931382525984892, 1574926790555828225, 195);
INSERT INTO `sys_role_menu` VALUES (1574931382525984893, 1574926790555828225, 196);
INSERT INTO `sys_role_menu` VALUES (1574931382525984894, 1574926790555828225, 197);
INSERT INTO `sys_role_menu` VALUES (1574931382525984895, 1574926790555828225, 198);
INSERT INTO `sys_role_menu` VALUES (1574931382525984896, 1574926790555828225, 199);
INSERT INTO `sys_role_menu` VALUES (1574931382525984897, 1574926790555828225, 200);
INSERT INTO `sys_role_menu` VALUES (1574931382525984898, 1574926790555828225, 201);
INSERT INTO `sys_role_menu` VALUES (1574931382525984899, 1574926790555828225, 202);
INSERT INTO `sys_role_menu` VALUES (1574931382525984900, 1574926790555828225, 203);
INSERT INTO `sys_role_menu` VALUES (1574931382525984901, 1574926790555828225, 204);
INSERT INTO `sys_role_menu` VALUES (1574931382525984902, 1574926790555828225, 205);
INSERT INTO `sys_role_menu` VALUES (1574931382525984903, 1574926790555828225, 206);
INSERT INTO `sys_role_menu` VALUES (1574931382525984904, 1574926790555828225, 207);
INSERT INTO `sys_role_menu` VALUES (1574931382525984905, 1574926790555828225, 208);
INSERT INTO `sys_role_menu` VALUES (1574931382525984906, 1574926790555828225, 209);
INSERT INTO `sys_role_menu` VALUES (1574931382525984907, 1574926790555828225, 210);
INSERT INTO `sys_role_menu` VALUES (1574931382525984908, 1574926790555828225, 211);
INSERT INTO `sys_role_menu` VALUES (1574931382525984909, 1574926790555828225, 212);
INSERT INTO `sys_role_menu` VALUES (1574931382525984910, 1574926790555828225, 213);
INSERT INTO `sys_role_menu` VALUES (1574931382525984911, 1574926790555828225, 214);
INSERT INTO `sys_role_menu` VALUES (1574931382588899329, 1574926790555828225, 215);
INSERT INTO `sys_role_menu` VALUES (1574931382588899330, 1574926790555828225, 216);
INSERT INTO `sys_role_menu` VALUES (1574931382588899331, 1574926790555828225, 217);
INSERT INTO `sys_role_menu` VALUES (1574931382588899332, 1574926790555828225, 218);
INSERT INTO `sys_role_menu` VALUES (1574931382588899333, 1574926790555828225, 219);
INSERT INTO `sys_role_menu` VALUES (1574931382588899334, 1574926790555828225, 220);
INSERT INTO `sys_role_menu` VALUES (1574931382588899335, 1574926790555828225, 221);
INSERT INTO `sys_role_menu` VALUES (1574931382588899336, 1574926790555828225, 222);
INSERT INTO `sys_role_menu` VALUES (1574931382588899337, 1574926790555828225, 223);
INSERT INTO `sys_role_menu` VALUES (1574931382588899338, 1574926790555828225, 224);
INSERT INTO `sys_role_menu` VALUES (1574931382588899339, 1574926790555828225, 225);
INSERT INTO `sys_role_menu` VALUES (1574931382588899340, 1574926790555828225, 226);
INSERT INTO `sys_role_menu` VALUES (1574931382588899341, 1574926790555828225, 227);
INSERT INTO `sys_role_menu` VALUES (1574931382588899342, 1574926790555828225, 228);
INSERT INTO `sys_role_menu` VALUES (1574931382588899343, 1574926790555828225, 229);
INSERT INTO `sys_role_menu` VALUES (1574931382588899344, 1574926790555828225, 230);
INSERT INTO `sys_role_menu` VALUES (1574931382588899345, 1574926790555828225, 231);
INSERT INTO `sys_role_menu` VALUES (1574931382588899346, 1574926790555828225, 232);
INSERT INTO `sys_role_menu` VALUES (1574931382588899347, 1574926790555828225, 233);
INSERT INTO `sys_role_menu` VALUES (1574931382588899348, 1574926790555828225, 234);
INSERT INTO `sys_role_menu` VALUES (1574931382588899349, 1574926790555828225, 235);
INSERT INTO `sys_role_menu` VALUES (1574931382588899350, 1574926790555828225, 236);
INSERT INTO `sys_role_menu` VALUES (1574931382588899351, 1574926790555828225, 237);
INSERT INTO `sys_role_menu` VALUES (1574931382588899352, 1574926790555828225, 238);
INSERT INTO `sys_role_menu` VALUES (1574931382588899353, 1574926790555828225, 239);
INSERT INTO `sys_role_menu` VALUES (1574931382588899354, 1574926790555828225, 240);
INSERT INTO `sys_role_menu` VALUES (1574931382588899355, 1574926790555828225, 241);
INSERT INTO `sys_role_menu` VALUES (1574931382588899356, 1574926790555828225, 242);
INSERT INTO `sys_role_menu` VALUES (1574931382588899357, 1574926790555828225, 243);
INSERT INTO `sys_role_menu` VALUES (1574931382588899358, 1574926790555828225, 244);
INSERT INTO `sys_role_menu` VALUES (1574931382588899359, 1574926790555828225, 245);
INSERT INTO `sys_role_menu` VALUES (1574931382588899360, 1574926790555828225, 246);
INSERT INTO `sys_role_menu` VALUES (1574931382588899361, 1574926790555828225, 247);
INSERT INTO `sys_role_menu` VALUES (1574931382588899362, 1574926790555828225, 248);
INSERT INTO `sys_role_menu` VALUES (1574931382588899363, 1574926790555828225, 249);
INSERT INTO `sys_role_menu` VALUES (1574931382588899364, 1574926790555828225, 250);
INSERT INTO `sys_role_menu` VALUES (1574931382588899365, 1574926790555828225, 251);
INSERT INTO `sys_role_menu` VALUES (1574931382588899366, 1574926790555828225, 252);
INSERT INTO `sys_role_menu` VALUES (1574931382588899367, 1574926790555828225, 253);
INSERT INTO `sys_role_menu` VALUES (1574931382588899368, 1574926790555828225, 254);
INSERT INTO `sys_role_menu` VALUES (1574931382588899369, 1574926790555828225, 255);
INSERT INTO `sys_role_menu` VALUES (1574931382588899370, 1574926790555828225, 6449912099524079616);
INSERT INTO `sys_role_menu` VALUES (1574931382588899371, 1574926790555828225, 4998978684524443692);
INSERT INTO `sys_role_menu` VALUES (1574931382588899372, 1574926790555828225, 1534384317233524738);
INSERT INTO `sys_role_menu` VALUES (1574931382588899373, 1574926790555828225, 5707993274387362828);
INSERT INTO `sys_role_menu` VALUES (1574931382588899374, 1574926790555828225, 8275137972166495794);
INSERT INTO `sys_role_menu` VALUES (1574931382588899375, 1574926790555828225, 6249597959063106635);
INSERT INTO `sys_role_menu` VALUES (1574931382588899376, 1574926790555828225, 6905244625723471705);
INSERT INTO `sys_role_menu` VALUES (1574931382588899377, 1574926790555828225, 6709768906758836391);
INSERT INTO `sys_role_menu` VALUES (1574931382588899378, 1574926790555828225, 7388361180917918171);
INSERT INTO `sys_role_menu` VALUES (1574931382588899379, 1574926790555828225, 7702400310012060990);
INSERT INTO `sys_role_menu` VALUES (1574931382588899380, 1574926790555828225, 5243613315131080710);
INSERT INTO `sys_role_menu` VALUES (1574931382588899381, 1574926790555828225, 5079108238558434584);
INSERT INTO `sys_role_menu` VALUES (1574931382588899382, 1574926790555828225, 8562547749529140265);
INSERT INTO `sys_role_menu` VALUES (1574931382588899383, 1574926790555828225, 5788080164048630938);
INSERT INTO `sys_role_menu` VALUES (1574931382588899384, 1574926790555828225, 7936411315868128942);
INSERT INTO `sys_role_menu` VALUES (1574931382588899385, 1574926790555828225, 7940657177277675482);
INSERT INTO `sys_role_menu` VALUES (1574931382588899386, 1574926790555828225, 6194217070047486945);
INSERT INTO `sys_role_menu` VALUES (1574931382588899387, 1574926790555828225, 6527167616560933135);
INSERT INTO `sys_role_menu` VALUES (1574931382588899388, 1574926790555828225, 1551480636561321985);
INSERT INTO `sys_role_menu` VALUES (1574931382588899389, 1574926790555828225, 6570253563256899359);
INSERT INTO `sys_role_menu` VALUES (1574931382588899390, 1574926790555828225, 6547301851685353031);
INSERT INTO `sys_role_menu` VALUES (1574931382588899391, 1574926790555828225, 8197158197771689059);
INSERT INTO `sys_role_menu` VALUES (1574931382588899392, 1574926790555828225, 9222389351831596218);
INSERT INTO `sys_role_menu` VALUES (1574931382588899393, 1574926790555828225, 5142735407151148234);
INSERT INTO `sys_role_menu` VALUES (1574931382588899394, 1574926790555828225, 7621956982866257638);
INSERT INTO `sys_role_menu` VALUES (1574931382588899395, 1574926790555828225, 5553078347948343545);
INSERT INTO `sys_role_menu` VALUES (1574931382588899396, 1574926790555828225, 6436044181834026359);
INSERT INTO `sys_role_menu` VALUES (1574931382588899397, 1574926790555828225, 8315301036989682205);
INSERT INTO `sys_role_menu` VALUES (1574931382588899398, 1574926790555828225, 8618504262472492350);
INSERT INTO `sys_role_menu` VALUES (1574931382588899399, 1574926790555828225, 7950819887425681478);
INSERT INTO `sys_role_menu` VALUES (1574931382588899400, 1574926790555828225, 4832107133629024589);
INSERT INTO `sys_role_menu` VALUES (1574931382588899401, 1574926790555828225, 7946795106595000695);
INSERT INTO `sys_role_menu` VALUES (1574931382588899402, 1574926790555828225, 5279583233581044419);
INSERT INTO `sys_role_menu` VALUES (1574931382588899403, 1574926790555828225, 6845666717193712873);
INSERT INTO `sys_role_menu` VALUES (1574931382588899404, 1574926790555828225, 8426505795261326687);
INSERT INTO `sys_role_menu` VALUES (1574931382588899405, 1574926790555828225, 6103813152661113624);
INSERT INTO `sys_role_menu` VALUES (1574931382588899406, 1574926790555828225, 6374585520258378360);
INSERT INTO `sys_role_menu` VALUES (1574931382588899407, 1574926790555828225, 8621765388717143178);
INSERT INTO `sys_role_menu` VALUES (1574931382588899408, 1574926790555828225, 6116805160244737678);
INSERT INTO `sys_role_menu` VALUES (1574931382588899409, 1574926790555828225, 6860312372874164125);
INSERT INTO `sys_role_menu` VALUES (1574931382588899410, 1574926790555828225, 5820472386138045917);
INSERT INTO `sys_role_menu` VALUES (1574931382588899411, 1574926790555828225, 8181822590852115190);
INSERT INTO `sys_role_menu` VALUES (1574931382588899412, 1574926790555828225, 7128085356547940830);
INSERT INTO `sys_role_menu` VALUES (1574931382588899413, 1574926790555828225, 6204862670753579307);
INSERT INTO `sys_role_menu` VALUES (1574931382588899414, 1574926790555828225, 4835110481928644952);
INSERT INTO `sys_role_menu` VALUES (1574931382588899415, 1574926790555828225, 7047324015553475476);
INSERT INTO `sys_role_menu` VALUES (1574931382588899416, 1574926790555828225, 7581916369308597479);
INSERT INTO `sys_role_menu` VALUES (1574931382588899417, 1574926790555828225, 6591367648784009715);
INSERT INTO `sys_role_menu` VALUES (1574931382588899418, 1574926790555828225, 6015462925549607928);
INSERT INTO `sys_role_menu` VALUES (1574931382588899419, 1574926790555828225, 4719172401435180793);
INSERT INTO `sys_role_menu` VALUES (1574931382588899420, 1574926790555828225, 5206868747918117433);
INSERT INTO `sys_role_menu` VALUES (1574931382588899421, 1574926790555828225, 8477499608364957015);
INSERT INTO `sys_role_menu` VALUES (1574931382588899422, 1574926790555828225, 1534410297847214081);
INSERT INTO `sys_role_menu` VALUES (1574931382588899423, 1574926790555828225, 4757443910107701590);
INSERT INTO `sys_role_menu` VALUES (1574931382588899424, 1574926790555828225, 5016248121790626902);
INSERT INTO `sys_role_menu` VALUES (1574931382588899425, 1574926790555828225, 7516608184768822750);
INSERT INTO `sys_role_menu` VALUES (1574931382588899426, 1574926790555828225, 8293588485966810596);
INSERT INTO `sys_role_menu` VALUES (1574931382588899427, 1574926790555828225, 5523506981641034218);
INSERT INTO `sys_role_menu` VALUES (1574931382588899428, 1574926790555828225, 8494981115107347954);
INSERT INTO `sys_role_menu` VALUES (1574931382588899429, 1574926790555828225, 5934688735467584877);
INSERT INTO `sys_role_menu` VALUES (1574931382588899430, 1574926790555828225, 6045763991836034103);
INSERT INTO `sys_role_menu` VALUES (1574931382588899431, 1574926790555828225, 7561922286979539034);
INSERT INTO `sys_role_menu` VALUES (1574931382588899432, 1574926790555828225, 5457126757845470309);
INSERT INTO `sys_role_menu` VALUES (1574931382588899433, 1574926790555828225, 8590002636006568603);
INSERT INTO `sys_role_menu` VALUES (1574931382588899434, 1574926790555828225, 6257208246505535137);
INSERT INTO `sys_role_menu` VALUES (1574931382588899435, 1574926790555828225, 9118159149629415380);
INSERT INTO `sys_role_menu` VALUES (1574931382588899436, 1574926790555828225, 6354039257511829492);
INSERT INTO `sys_role_menu` VALUES (1574931382588899437, 1574926790555828225, 5445032970491536505);
INSERT INTO `sys_role_menu` VALUES (1574931382588899438, 1574926790555828225, 6542835870761280820);
INSERT INTO `sys_role_menu` VALUES (1574931382588899439, 1574926790555828225, 8191527329183937863);
INSERT INTO `sys_role_menu` VALUES (1574931382588899440, 1574926790555828225, 7357393608825227850);
INSERT INTO `sys_role_menu` VALUES (1574931382588899441, 1574926790555828225, 7447629070175932292);
INSERT INTO `sys_role_menu` VALUES (1574931382588899442, 1574926790555828225, 5228961670953770885);
INSERT INTO `sys_role_menu` VALUES (1574931382588899443, 1574926790555828225, 5234105579337808561);
INSERT INTO `sys_role_menu` VALUES (1574931382588899444, 1574926790555828225, 6457085339840740289);
INSERT INTO `sys_role_menu` VALUES (1574931382588899445, 1574926790555828225, 6507903030541782366);
INSERT INTO `sys_role_menu` VALUES (1574931382588899446, 1574926790555828225, 7639361031662168624);
INSERT INTO `sys_role_menu` VALUES (1574931382588899447, 1574926790555828225, 8562653372099681846);
INSERT INTO `sys_role_menu` VALUES (1574931382588899448, 1574926790555828225, 4830986669641223230);
INSERT INTO `sys_role_menu` VALUES (1574931382588899449, 1574926790555828225, 7974141988632466544);
INSERT INTO `sys_role_menu` VALUES (1574931382588899450, 1574926790555828225, 8313819116019873398);
INSERT INTO `sys_role_menu` VALUES (1574931382647619586, 1574926790555828225, 8931538668430408569);
INSERT INTO `sys_role_menu` VALUES (1574931382647619587, 1574926790555828225, 6122595225588020183);
INSERT INTO `sys_role_menu` VALUES (1574931382647619588, 1574926790555828225, 6667327679590719676);
INSERT INTO `sys_role_menu` VALUES (1574931382647619589, 1574926790555828225, 8959792232643188225);
INSERT INTO `sys_role_menu` VALUES (1574931382647619590, 1574926790555828225, 5668705457330168859);
INSERT INTO `sys_role_menu` VALUES (1574931382647619591, 1574926790555828225, 9180800170765745954);
INSERT INTO `sys_role_menu` VALUES (1574931382647619592, 1574926790555828225, 5327083612717101630);
INSERT INTO `sys_role_menu` VALUES (1574931382651813890, 1574926790555828225, 6329171771009724483);
INSERT INTO `sys_role_menu` VALUES (1574931382651813891, 1574926790555828225, 6020421073902938241);
INSERT INTO `sys_role_menu` VALUES (1574931382651813892, 1574926790555828225, 5886355131876064420);
INSERT INTO `sys_role_menu` VALUES (1574931382651813893, 1574926790555828225, 9202857208449608897);
INSERT INTO `sys_role_menu` VALUES (1574931382651813894, 1574926790555828225, 1560158326283632641);
INSERT INTO `sys_role_menu` VALUES (1574931382651813895, 1574926790555828225, 1560158090005905410);
INSERT INTO `sys_role_menu` VALUES (1574931382651813896, 1574926790555828225, 7656571819576826411);
INSERT INTO `sys_role_menu` VALUES (1574931382651813897, 1574926790555828225, 5014700545593967407);
INSERT INTO `sys_role_menu` VALUES (1574931382651813898, 1574926790555828225, 8088901678692869692);
INSERT INTO `sys_role_menu` VALUES (1574931382651813899, 1574926790555828225, 6299920358694835309);
INSERT INTO `sys_role_menu` VALUES (1574931382651813900, 1574926790555828225, 5642803668063215805);
INSERT INTO `sys_role_menu` VALUES (1574931382651813901, 1574926790555828225, 8834719374925492199);
INSERT INTO `sys_role_menu` VALUES (1574931382651813902, 1574926790555828225, 7882898168203081196);
INSERT INTO `sys_role_menu` VALUES (1574931382651813903, 1574926790555828225, 7346283523793183379);
INSERT INTO `sys_role_menu` VALUES (1574931382651813904, 1574926790555828225, 1542409869531873282);
INSERT INTO `sys_role_menu` VALUES (1574931382651813905, 1574926790555828225, 9208275275751949837);
INSERT INTO `sys_role_menu` VALUES (1574931382651813906, 1574926790555828225, 9173957282399717676);
INSERT INTO `sys_role_menu` VALUES (1574931382651813907, 1574926790555828225, 8973112451110891359);
INSERT INTO `sys_role_menu` VALUES (1574931382651813908, 1574926790555828225, 5753930915061776016);
INSERT INTO `sys_role_menu` VALUES (1574931382651813909, 1574926790555828225, 7288710125399936914);
INSERT INTO `sys_role_menu` VALUES (1574931382651813910, 1574926790555828225, 9022874385071881655);
INSERT INTO `sys_role_menu` VALUES (1574931382651813911, 1574926790555828225, 5378908852735764919);
INSERT INTO `sys_role_menu` VALUES (1574931382651813912, 1574926790555828225, 8000986046974593578);
INSERT INTO `sys_role_menu` VALUES (1574931382651813913, 1574926790555828225, 6414357916883420801);
INSERT INTO `sys_role_menu` VALUES (1574931382664396802, 1574926790555828225, 1558006778241753089);
INSERT INTO `sys_role_menu` VALUES (1574931382664396803, 1574926790555828225, 5197311839117522988);
INSERT INTO `sys_role_menu` VALUES (1574931382664396804, 1574926790555828225, 6494634442879742520);
INSERT INTO `sys_role_menu` VALUES (1574931382664396805, 1574926790555828225, 6659298240010835782);
INSERT INTO `sys_role_menu` VALUES (1574931382664396806, 1574926790555828225, 5027224879967170377);
INSERT INTO `sys_role_menu` VALUES (1574931382664396807, 1574926790555828225, 5465134665687105949);
INSERT INTO `sys_role_menu` VALUES (1574931382664396808, 1574926790555828225, 6535734057645557495);
INSERT INTO `sys_role_menu` VALUES (1574931382664396809, 1574926790555828225, 6134354751483903106);
INSERT INTO `sys_role_menu` VALUES (1574931382664396810, 1574926790555828225, 8424802293696759825);
INSERT INTO `sys_role_menu` VALUES (1574931382664396811, 1574926790555828225, 6122993178045887049);
INSERT INTO `sys_role_menu` VALUES (1574931382664396812, 1574926790555828225, 7529678467290942551);
INSERT INTO `sys_role_menu` VALUES (1574931382664396813, 1574926790555828225, 7209147716433162075);
INSERT INTO `sys_role_menu` VALUES (1574931382664396814, 1574926790555828225, 8655981106868069232);
INSERT INTO `sys_role_menu` VALUES (1574931382664396815, 1574926790555828225, 7337865129337018225);
INSERT INTO `sys_role_menu` VALUES (1574931382664396816, 1574926790555828225, 5196743176498588333);
INSERT INTO `sys_role_menu` VALUES (1574931382664396817, 1574926790555828225, 4794640277308881312);
INSERT INTO `sys_role_menu` VALUES (1574931382664396818, 1574926790555828225, 1574929774689878018);
INSERT INTO `sys_role_menu` VALUES (1574931382664396819, 1574926790555828225, 6947194724669964805);
INSERT INTO `sys_role_menu` VALUES (1574931382664396820, 1574926790555828225, 7119232719243003213);
INSERT INTO `sys_role_menu` VALUES (1574931382664396821, 1574926790555828225, 4678695510847297372);
INSERT INTO `sys_role_menu` VALUES (1574931382664396822, 1574926790555828225, 8502207026072268708);
INSERT INTO `sys_role_menu` VALUES (1574931382664396823, 1574926790555828225, 7294415501551357401);
INSERT INTO `sys_role_menu` VALUES (1574931382664396824, 1574926790555828225, 8175022271486866402);
INSERT INTO `sys_role_menu` VALUES (1574931382664396825, 1574926790555828225, 6996269839693777638);
INSERT INTO `sys_role_menu` VALUES (1574931382676979713, 1574926790555828225, 6405406552095053594);
INSERT INTO `sys_role_menu` VALUES (1574931382676979714, 1574926790555828225, 8422209827465537355);
INSERT INTO `sys_role_menu` VALUES (1574931382676979715, 1574926790555828225, 1552209067446579201);
INSERT INTO `sys_role_menu` VALUES (1574931382676979716, 1574926790555828225, 8903136733033804117);
INSERT INTO `sys_role_menu` VALUES (1574931382676979717, 1574926790555828225, 5382528335658667107);
INSERT INTO `sys_role_menu` VALUES (1574931382676979718, 1574926790555828225, 6046397975825687693);
INSERT INTO `sys_role_menu` VALUES (1574931382676979719, 1574926790555828225, 5999927780812383399);
INSERT INTO `sys_role_menu` VALUES (1574931382676979720, 1574926790555828225, 5274675770695528925);
INSERT INTO `sys_role_menu` VALUES (1574931382676979721, 1574926790555828225, 6813707923637624035);
INSERT INTO `sys_role_menu` VALUES (1574931382676979722, 1574926790555828225, 8287951873357908072);
INSERT INTO `sys_role_menu` VALUES (1574931382676979723, 1574926790555828225, 4700141466883779130);
INSERT INTO `sys_role_menu` VALUES (1574931382676979724, 1574926790555828225, 4834264129878073409);
INSERT INTO `sys_role_menu` VALUES (1574931382676979725, 1574926790555828225, 7845222658049694795);
INSERT INTO `sys_role_menu` VALUES (1574931382676979726, 1574926790555828225, 8946168826263694974);
INSERT INTO `sys_role_menu` VALUES (1574931382676979727, 1574926790555828225, 5162865219757254273);
INSERT INTO `sys_role_menu` VALUES (1574931382676979728, 1574926790555828225, 9090782745729954273);
INSERT INTO `sys_role_menu` VALUES (1574931382676979729, 1574926790555828225, 7909153361025684710);
INSERT INTO `sys_role_menu` VALUES (1574931382676979730, 1574926790555828225, 8107628912599125262);
INSERT INTO `sys_role_menu` VALUES (1574931382676979731, 1574926790555828225, 7072750871619746982);
INSERT INTO `sys_role_menu` VALUES (1574931382676979732, 1574926790555828225, 1558006914472747010);
INSERT INTO `sys_role_menu` VALUES (1574931382676979733, 1574926790555828225, 1574930219948802050);
INSERT INTO `sys_role_menu` VALUES (1574931382676979734, 1574926790555828225, 4903707486851837034);
INSERT INTO `sys_role_menu` VALUES (1574931382676979735, 1574926790555828225, 7635960329677377171);
INSERT INTO `sys_role_menu` VALUES (1574931382689562626, 1574926790555828225, 7596329328691560085);
INSERT INTO `sys_role_menu` VALUES (1574931382689562627, 1574926790555828225, 6429021782911745716);
INSERT INTO `sys_role_menu` VALUES (1574931382689562628, 1574926790555828225, 8259333771823873238);
INSERT INTO `sys_role_menu` VALUES (1574931382689562629, 1574926790555828225, 4674816360932725215);
INSERT INTO `sys_role_menu` VALUES (1574931382689562630, 1574926790555828225, 8436831550169549160);
INSERT INTO `sys_role_menu` VALUES (1574931382689562631, 1574926790555828225, 6696300230304937737);
INSERT INTO `sys_role_menu` VALUES (1574931382689562632, 1574926790555828225, 7898061431233899837);
INSERT INTO `sys_role_menu` VALUES (1574931382689562633, 1574926790555828225, 5448185299220002627);
INSERT INTO `sys_role_menu` VALUES (1574931382689562634, 1574926790555828225, 9135927743117002076);
INSERT INTO `sys_role_menu` VALUES (1574931382689562635, 1574926790555828225, 6655166482691941995);
INSERT INTO `sys_role_menu` VALUES (1574931382689562636, 1574926790555828225, 5871841993475141582);
INSERT INTO `sys_role_menu` VALUES (1574931382689562637, 1574926790555828225, 5276828216703973340);
INSERT INTO `sys_role_menu` VALUES (1574931382689562638, 1574926790555828225, 8700710314151240148);
INSERT INTO `sys_role_menu` VALUES (1574931382689562639, 1574926790555828225, 6238776219082572290);
INSERT INTO `sys_role_menu` VALUES (1574931382689562640, 1574926790555828225, 8613714595480779524);
INSERT INTO `sys_role_menu` VALUES (1574931382689562641, 1574926790555828225, 5346549595871021092);
INSERT INTO `sys_role_menu` VALUES (1574931382689562642, 1574926790555828225, 8181816557328888214);
INSERT INTO `sys_role_menu` VALUES (1574931382689562643, 1574926790555828225, 5587279777867961498);
INSERT INTO `sys_role_menu` VALUES (1574931382689562644, 1574926790555828225, 7380411400614516902);
INSERT INTO `sys_role_menu` VALUES (1574931382689562645, 1574926790555828225, 7721498005843290304);
INSERT INTO `sys_role_menu` VALUES (1574931382689562646, 1574926790555828225, 9152465645130034198);
INSERT INTO `sys_role_menu` VALUES (1574931382689562647, 1574926790555828225, 5012839659883217263);
INSERT INTO `sys_role_menu` VALUES (1574931382689562648, 1574926790555828225, 1534411830716350466);
INSERT INTO `sys_role_menu` VALUES (1574931382697951233, 1574926790555828225, 6280770118494855200);
INSERT INTO `sys_role_menu` VALUES (1574931382697951234, 1574926790555828225, 8775511535879974972);
INSERT INTO `sys_role_menu` VALUES (1574931382697951235, 1574926790555828225, 5624458298951752771);
INSERT INTO `sys_role_menu` VALUES (1574931382697951236, 1574926790555828225, 7254023492833901715);
INSERT INTO `sys_role_menu` VALUES (1574931382697951237, 1574926790555828225, 7318591877639154878);
INSERT INTO `sys_role_menu` VALUES (1574931382697951238, 1574926790555828225, 7812048842906278623);
INSERT INTO `sys_role_menu` VALUES (1574931382697951239, 1574926790555828225, 8633823152794346306);
INSERT INTO `sys_role_menu` VALUES (1574931382697951240, 1574926790555828225, 5893520734687245059);
INSERT INTO `sys_role_menu` VALUES (1574931382697951241, 1574926790555828225, 8868921547741003555);
INSERT INTO `sys_role_menu` VALUES (1574931382697951242, 1574926790555828225, 6326961474985796183);
INSERT INTO `sys_role_menu` VALUES (1574931382697951243, 1574926790555828225, 8833258242949762178);
INSERT INTO `sys_role_menu` VALUES (1574931382697951244, 1574926790555828225, 4967796364303223216);
INSERT INTO `sys_role_menu` VALUES (1574931382697951245, 1574926790555828225, 6900932255841430981);
INSERT INTO `sys_role_menu` VALUES (1574931382697951246, 1574926790555828225, 7629147488562302699);
INSERT INTO `sys_role_menu` VALUES (1574931382697951247, 1574926790555828225, 8016744771368544956);
INSERT INTO `sys_role_menu` VALUES (1574931382697951248, 1574926790555828225, 5002055413507195458);
INSERT INTO `sys_role_menu` VALUES (1574931382697951249, 1574926790555828225, 6290232610677682017);
INSERT INTO `sys_role_menu` VALUES (1574931382697951250, 1574926790555828225, 5803272132980049305);
INSERT INTO `sys_role_menu` VALUES (1574931382697951251, 1574926790555828225, 7395287038762629813);
INSERT INTO `sys_role_menu` VALUES (1574931382697951252, 1574926790555828225, 7323225129555267277);
INSERT INTO `sys_role_menu` VALUES (1574931382697951253, 1574926790555828225, 6347609996552577764);
INSERT INTO `sys_role_menu` VALUES (1574931382697951254, 1574926790555828225, 7682643462888492777);
INSERT INTO `sys_role_menu` VALUES (1574931382697951255, 1574926790555828225, 5721174223820977430);
INSERT INTO `sys_role_menu` VALUES (1574931382697951256, 1574926790555828225, 5469043739505193743);
INSERT INTO `sys_role_menu` VALUES (1574931382697951257, 1574926790555828225, 5459011313540319790);
INSERT INTO `sys_role_menu` VALUES (1574931382710534146, 1574926790555828225, 7723724790018395451);
INSERT INTO `sys_role_menu` VALUES (1574931382710534147, 1574926790555828225, 7764860386896651377);
INSERT INTO `sys_role_menu` VALUES (1574931382710534148, 1574926790555828225, 8254320371052864398);
INSERT INTO `sys_role_menu` VALUES (1574931382710534149, 1574926790555828225, 8273410905135864011);
INSERT INTO `sys_role_menu` VALUES (1574931382710534150, 1574926790555828225, 8515665128226371022);
INSERT INTO `sys_role_menu` VALUES (1574931382710534151, 1574926790555828225, 5000566847705099773);
INSERT INTO `sys_role_menu` VALUES (1574931382710534152, 1574926790555828225, 5555524248707858432);
INSERT INTO `sys_role_menu` VALUES (1574931382710534153, 1574926790555828225, 1562329827773108225);
INSERT INTO `sys_role_menu` VALUES (1574931382710534154, 1574926790555828225, 5901981891345615661);
INSERT INTO `sys_role_menu` VALUES (1574931382710534155, 1574926790555828225, 8342821565086833982);
INSERT INTO `sys_role_menu` VALUES (1574931382710534156, 1574926790555828225, 7044559182298687818);
INSERT INTO `sys_role_menu` VALUES (1574931382710534157, 1574926790555828225, 5714641428045333341);
INSERT INTO `sys_role_menu` VALUES (1574931382710534158, 1574926790555828225, 5801879553212456867);
INSERT INTO `sys_role_menu` VALUES (1574931382710534159, 1574926790555828225, 6739113753546148532);
INSERT INTO `sys_role_menu` VALUES (1574931382710534160, 1574926790555828225, 7455112058491820064);
INSERT INTO `sys_role_menu` VALUES (1574931382710534161, 1574926790555828225, 8957317986837088422);
INSERT INTO `sys_role_menu` VALUES (1574931382710534162, 1574926790555828225, 4621333945127042674);
INSERT INTO `sys_role_menu` VALUES (1574931382710534163, 1574926790555828225, 5886900280768283007);
INSERT INTO `sys_role_menu` VALUES (1574931382710534164, 1574926790555828225, 5846194751422568093);
INSERT INTO `sys_role_menu` VALUES (1574931382710534165, 1574926790555828225, 9156093698218665129);
INSERT INTO `sys_role_menu` VALUES (1574931382710534166, 1574926790555828225, 7678659259268274624);
INSERT INTO `sys_role_menu` VALUES (1574931382710534167, 1574926790555828225, 8099644559700436461);
INSERT INTO `sys_role_menu` VALUES (1574931382723117057, 1574926790555828225, 5653092812632926489);
INSERT INTO `sys_role_menu` VALUES (1574931382723117058, 1574926790555828225, 7200474546305867050);
INSERT INTO `sys_role_menu` VALUES (1574931382723117059, 1574926790555828225, 7242870236365548592);
INSERT INTO `sys_role_menu` VALUES (1574931382723117060, 1574926790555828225, 9150731911657480775);
INSERT INTO `sys_role_menu` VALUES (1574931382723117061, 1574926790555828225, 7287765970469918079);
INSERT INTO `sys_role_menu` VALUES (1574931382723117062, 1574926790555828225, 5480200211927228567);
INSERT INTO `sys_role_menu` VALUES (1574931382723117063, 1574926790555828225, 4770899845682577376);
INSERT INTO `sys_role_menu` VALUES (1574931382723117064, 1574926790555828225, 1264622039642255331);
INSERT INTO `sys_role_menu` VALUES (1574931382723117065, 1574926790555828225, 1264622039642255311);
INSERT INTO `sys_role_menu` VALUES (1574931382723117066, 1574926790555828225, 1264622039642255321);
INSERT INTO `sys_role_menu` VALUES (1574931382723117067, 1574926790555828225, 1264622039642255361);
INSERT INTO `sys_role_menu` VALUES (1574931382723117068, 1574926790555828225, 1264622039642255351);
INSERT INTO `sys_role_menu` VALUES (1574931382723117069, 1574926790555828225, 1264622039642255341);
INSERT INTO `sys_role_menu` VALUES (1574931382723117070, 1574926790555828225, 1264622039642255371);
INSERT INTO `sys_role_menu` VALUES (1574931382723117071, 1574926790555828225, 1264622039642255381);
INSERT INTO `sys_role_menu` VALUES (1574931382723117072, 1574926790555828225, 1264622039642255391);
INSERT INTO `sys_role_menu` VALUES (1574931382723117073, 1574926790555828225, 1264622039642255401);
INSERT INTO `sys_role_menu` VALUES (1574931382723117074, 1574926790555828225, 1264622039642255411);
INSERT INTO `sys_role_menu` VALUES (1574931382723117075, 1574926790555828225, 1264622039642255421);
INSERT INTO `sys_role_menu` VALUES (1574931382723117076, 1574926790555828225, 1264622039642255431);
INSERT INTO `sys_role_menu` VALUES (1574931382723117077, 1574926790555828225, 1264622039642255441);
INSERT INTO `sys_role_menu` VALUES (1574931382723117078, 1574926790555828225, 1264622039642255451);
INSERT INTO `sys_role_menu` VALUES (1574931382723117079, 1574926790555828225, 1264622039642255461);
INSERT INTO `sys_role_menu` VALUES (1574931382723117080, 1574926790555828225, 1264622039642255471);
INSERT INTO `sys_role_menu` VALUES (1574931382723117081, 1574926790555828225, 1264622039642255481);
INSERT INTO `sys_role_menu` VALUES (1574931382735699970, 1574926790555828225, 1264622039642255491);
INSERT INTO `sys_role_menu` VALUES (1574931382735699971, 1574926790555828225, 1264622039642255501);
INSERT INTO `sys_role_menu` VALUES (1574931382735699972, 1574926790555828225, 1264622039642255511);
INSERT INTO `sys_role_menu` VALUES (1574931382735699973, 1574926790555828225, 1264622039642255521);
INSERT INTO `sys_role_menu` VALUES (1574931382735699974, 1574926790555828225, 1264622039642255531);
INSERT INTO `sys_role_menu` VALUES (1574931382735699975, 1574926790555828225, 1264622039642255541);
INSERT INTO `sys_role_menu` VALUES (1574931382735699976, 1574926790555828225, 1264622039642255551);
INSERT INTO `sys_role_menu` VALUES (1574931382735699977, 1574926790555828225, 1264622039642255561);
INSERT INTO `sys_role_menu` VALUES (1574931382735699978, 1574926790555828225, 1264622039642255571);
INSERT INTO `sys_role_menu` VALUES (1574931382735699979, 1574926790555828225, 1264622039642255581);
INSERT INTO `sys_role_menu` VALUES (1574931382735699980, 1574926790555828225, 1264622039642255591);
INSERT INTO `sys_role_menu` VALUES (1574931382735699981, 1574926790555828225, 1264622039642255601);
INSERT INTO `sys_role_menu` VALUES (1574931382735699982, 1574926790555828225, 1264622039642255621);
INSERT INTO `sys_role_menu` VALUES (1574931382735699983, 1574926790555828225, 1264622039642255631);
INSERT INTO `sys_role_menu` VALUES (1574931382735699984, 1574926790555828225, 1264622039642255641);
INSERT INTO `sys_role_menu` VALUES (1574931382735699985, 1574926790555828225, 1264622039642255651);
INSERT INTO `sys_role_menu` VALUES (1574931382735699986, 1574926790555828225, 1264622039642255661);
INSERT INTO `sys_role_menu` VALUES (1574931382735699987, 1574926790555828225, 1264622039642255611);
INSERT INTO `sys_role_menu` VALUES (1574931382735699988, 1574926790555828225, 1264622039642255691);
INSERT INTO `sys_role_menu` VALUES (1574931382735699989, 1574926790555828225, 1264622039642255681);
INSERT INTO `sys_role_menu` VALUES (1574931382735699990, 1574926790555828225, 1264622039642255671);
INSERT INTO `sys_role_menu` VALUES (1574931382735699991, 1574926790555828225, 1264622039642255701);
INSERT INTO `sys_role_menu` VALUES (1574931382735699992, 1574926790555828225, 1264622039642255711);
INSERT INTO `sys_role_menu` VALUES (1574931382735699993, 1574926790555828225, 1264622039642255721);
INSERT INTO `sys_role_menu` VALUES (1574931382735699994, 1574926790555828225, 1264622039642255731);
INSERT INTO `sys_role_menu` VALUES (1574931382735699995, 1574926790555828225, 1264622039642255741);
INSERT INTO `sys_role_menu` VALUES (1574931382744088578, 1574926790555828225, 1264622039642255751);
INSERT INTO `sys_role_menu` VALUES (1574931382744088579, 1574926790555828225, 1264622039642255761);
INSERT INTO `sys_role_menu` VALUES (1574931382744088580, 1574926790555828225, 1264622039642255771);
INSERT INTO `sys_role_menu` VALUES (1574931382744088581, 1574926790555828225, 1264622039642255781);
INSERT INTO `sys_role_menu` VALUES (1574931382744088582, 1574926790555828225, 1264622039642255791);
INSERT INTO `sys_role_menu` VALUES (1574931382744088583, 1574926790555828225, 1264622039642255801);
INSERT INTO `sys_role_menu` VALUES (1574931382744088584, 1574926790555828225, 1264622039642255811);
INSERT INTO `sys_role_menu` VALUES (1574931382744088585, 1574926790555828225, 1264622039642255821);
INSERT INTO `sys_role_menu` VALUES (1574931382744088586, 1574926790555828225, 1264622039642255831);
INSERT INTO `sys_role_menu` VALUES (1574931382744088587, 1574926790555828225, 1264622039642255841);
INSERT INTO `sys_role_menu` VALUES (1574931382744088588, 1574926790555828225, 1264622039642255851);
INSERT INTO `sys_role_menu` VALUES (1574931382744088589, 1574926790555828225, 1264622039642255881);
INSERT INTO `sys_role_menu` VALUES (1574931382744088590, 1574926790555828225, 1264622039642255891);
INSERT INTO `sys_role_menu` VALUES (1574931382744088591, 1574926790555828225, 1264622039642255901);
INSERT INTO `sys_role_menu` VALUES (1574931382744088592, 1574926790555828225, 1264622039642255911);
INSERT INTO `sys_role_menu` VALUES (1574931382744088593, 1574926790555828225, 1264622039642255921);
INSERT INTO `sys_role_menu` VALUES (1574931382744088594, 1574926790555828225, 1264622039642255931);
INSERT INTO `sys_role_menu` VALUES (1574931382744088595, 1574926790555828225, 1264622039642255941);
INSERT INTO `sys_role_menu` VALUES (1574931382744088596, 1574926790555828225, 1264622039642255951);
INSERT INTO `sys_role_menu` VALUES (1574931382744088597, 1574926790555828225, 1264622039642255861);
INSERT INTO `sys_role_menu` VALUES (1574931382744088598, 1574926790555828225, 1264622039642255871);
INSERT INTO `sys_role_menu` VALUES (1574931382744088599, 1574926790555828225, 1264622039642255981);
INSERT INTO `sys_role_menu` VALUES (1574931382744088600, 1574926790555828225, 1264622039642255971);
INSERT INTO `sys_role_menu` VALUES (1574931382744088601, 1574926790555828225, 1264622039642255961);
INSERT INTO `sys_role_menu` VALUES (1574931382744088602, 1574926790555828225, 1264622039642255991);
INSERT INTO `sys_role_menu` VALUES (1574931382744088603, 1574926790555828225, 1264622039642256001);
INSERT INTO `sys_role_menu` VALUES (1574931382744088604, 1574926790555828225, 1264622039642256011);
INSERT INTO `sys_role_menu` VALUES (1574931382744088605, 1574926790555828225, 1264622039642256021);
INSERT INTO `sys_role_menu` VALUES (1574931382744088606, 1574926790555828225, 1264622039642256031);
INSERT INTO `sys_role_menu` VALUES (1574931382744088607, 1574926790555828225, 1264622039642256041);
INSERT INTO `sys_role_menu` VALUES (1574931382744088608, 1574926790555828225, 1264622039642256051);
INSERT INTO `sys_role_menu` VALUES (1574931382744088609, 1574926790555828225, 1264622039642256061);
INSERT INTO `sys_role_menu` VALUES (1574931382744088610, 1574926790555828225, 1264622039642256071);
INSERT INTO `sys_role_menu` VALUES (1574931382744088611, 1574926790555828225, 1264622039642256081);
INSERT INTO `sys_role_menu` VALUES (1574931382744088612, 1574926790555828225, 1264622039642256091);
INSERT INTO `sys_role_menu` VALUES (1574931382744088613, 1574926790555828225, 1264622039642256101);
INSERT INTO `sys_role_menu` VALUES (1574931382744088614, 1574926790555828225, 1264622039642256111);
INSERT INTO `sys_role_menu` VALUES (1574931382744088615, 1574926790555828225, 1264622039642256131);
INSERT INTO `sys_role_menu` VALUES (1574931382744088616, 1574926790555828225, 1264622039642256141);
INSERT INTO `sys_role_menu` VALUES (1574931382744088617, 1574926790555828225, 1264622039642256151);
INSERT INTO `sys_role_menu` VALUES (1574931382744088618, 1574926790555828225, 1264622039642256161);
INSERT INTO `sys_role_menu` VALUES (1574931382744088619, 1574926790555828225, 1264622039642256171);
INSERT INTO `sys_role_menu` VALUES (1574931382744088620, 1574926790555828225, 1264622039642256181);
INSERT INTO `sys_role_menu` VALUES (1574931382744088621, 1574926790555828225, 1264622039642256191);
INSERT INTO `sys_role_menu` VALUES (1574931382744088622, 1574926790555828225, 1264622039642256201);
INSERT INTO `sys_role_menu` VALUES (1574931382744088623, 1574926790555828225, 1264622039642256211);
INSERT INTO `sys_role_menu` VALUES (1574931382744088624, 1574926790555828225, 1264622039642256221);
INSERT INTO `sys_role_menu` VALUES (1574931382744088625, 1574926790555828225, 1264622039642256231);
INSERT INTO `sys_role_menu` VALUES (1574931382744088626, 1574926790555828225, 1264622039642256241);
INSERT INTO `sys_role_menu` VALUES (1574931382744088627, 1574926790555828225, 1264622039642256251);
INSERT INTO `sys_role_menu` VALUES (1574931382744088628, 1574926790555828225, 1264622039642256261);
INSERT INTO `sys_role_menu` VALUES (1574931382744088629, 1574926790555828225, 1264622039642256121);
INSERT INTO `sys_role_menu` VALUES (1574931382744088630, 1574926790555828225, 1264622039642256271);
INSERT INTO `sys_role_menu` VALUES (1574931382744088631, 1574926790555828225, 1264622039642256301);
INSERT INTO `sys_role_menu` VALUES (1574931382744088632, 1574926790555828225, 1264622039642256291);
INSERT INTO `sys_role_menu` VALUES (1574931382744088633, 1574926790555828225, 1264622039642256281);
INSERT INTO `sys_role_menu` VALUES (1574931382744088634, 1574926790555828225, 1264622039642256311);
INSERT INTO `sys_role_menu` VALUES (1574931382744088635, 1574926790555828225, 1264622039642256321);
INSERT INTO `sys_role_menu` VALUES (1574931382744088636, 1574926790555828225, 1264622039642256331);
INSERT INTO `sys_role_menu` VALUES (1574931382744088637, 1574926790555828225, 1264622039642256341);
INSERT INTO `sys_role_menu` VALUES (1574931382744088638, 1574926790555828225, 1264622039642256371);
INSERT INTO `sys_role_menu` VALUES (1574931382744088639, 1574926790555828225, 1264622039642256361);
INSERT INTO `sys_role_menu` VALUES (1574931382744088640, 1574926790555828225, 1264622039642256351);
INSERT INTO `sys_role_menu` VALUES (1574931382744088641, 1574926790555828225, 1264622039642256381);
INSERT INTO `sys_role_menu` VALUES (1574931382744088642, 1574926790555828225, 1264622039642256391);
INSERT INTO `sys_role_menu` VALUES (1574931382744088643, 1574926790555828225, 1264622039642256401);
INSERT INTO `sys_role_menu` VALUES (1574931382744088644, 1574926790555828225, 1264622039642256411);
INSERT INTO `sys_role_menu` VALUES (1574931382744088645, 1574926790555828225, 1264622039642256441);
INSERT INTO `sys_role_menu` VALUES (1574931382744088646, 1574926790555828225, 1264622039642256431);
INSERT INTO `sys_role_menu` VALUES (1574931382744088647, 1574926790555828225, 1264622039642256421);
INSERT INTO `sys_role_menu` VALUES (1574931382744088648, 1574926790555828225, 1264622039642256451);
INSERT INTO `sys_role_menu` VALUES (1574931382744088649, 1574926790555828225, 1264622039642256461);
INSERT INTO `sys_role_menu` VALUES (1574931382744088650, 1574926790555828225, 1264622039642256471);
INSERT INTO `sys_role_menu` VALUES (1574931382744088651, 1574926790555828225, 1264622039642256481);
INSERT INTO `sys_role_menu` VALUES (1574931382744088652, 1574926790555828225, 1264622039642256491);
INSERT INTO `sys_role_menu` VALUES (1574931382744088653, 1574926790555828225, 1264622039642256501);
INSERT INTO `sys_role_menu` VALUES (1574931382744088654, 1574926790555828225, 1264622039642256511);
INSERT INTO `sys_role_menu` VALUES (1574931382744088655, 1574926790555828225, 1264622039642256541);
INSERT INTO `sys_role_menu` VALUES (1574931382744088656, 1574926790555828225, 1264622039642256531);
INSERT INTO `sys_role_menu` VALUES (1574931382744088657, 1574926790555828225, 1264622039642256521);
INSERT INTO `sys_role_menu` VALUES (1574931382744088658, 1574926790555828225, 1264622039642256551);
INSERT INTO `sys_role_menu` VALUES (1574931382744088659, 1574926790555828225, 1264622039642256561);
INSERT INTO `sys_role_menu` VALUES (1574931382744088660, 1574926790555828225, 1264622039642256571);
INSERT INTO `sys_role_menu` VALUES (1574931382744088661, 1574926790555828225, 1264622039642256581);
INSERT INTO `sys_role_menu` VALUES (1574931382744088662, 1574926790555828225, 1264622039642256591);
INSERT INTO `sys_role_menu` VALUES (1574931382744088663, 1574926790555828225, 1264622039642256601);
INSERT INTO `sys_role_menu` VALUES (1574931382744088664, 1574926790555828225, 1410859007809736705);
INSERT INTO `sys_role_menu` VALUES (1574931382744088665, 1574926790555828225, 1264622039642256641);
INSERT INTO `sys_role_menu` VALUES (1574931382744088666, 1574926790555828225, 1264622039642256621);
INSERT INTO `sys_role_menu` VALUES (1574931382744088667, 1574926790555828225, 1264622039642256611);
INSERT INTO `sys_role_menu` VALUES (1574931382744088668, 1574926790555828225, 1264622039642256651);
INSERT INTO `sys_role_menu` VALUES (1574931382744088669, 1574926790555828225, 1264622039642256661);
INSERT INTO `sys_role_menu` VALUES (1574931382744088670, 1574926790555828225, 1264622039642256671);
INSERT INTO `sys_role_menu` VALUES (1574931382744088671, 1574926790555828225, 1264622039642256681);
INSERT INTO `sys_role_menu` VALUES (1574931382744088672, 1574926790555828225, 1264622039642256691);
INSERT INTO `sys_role_menu` VALUES (1574931382744088673, 1574926790555828225, 1264622039642256701);
INSERT INTO `sys_role_menu` VALUES (1574931382744088674, 1574926790555828225, 1264622039642256711);
INSERT INTO `sys_role_menu` VALUES (1574931382744088675, 1574926790555828225, 1264622039642256631);
INSERT INTO `sys_role_menu` VALUES (1574931382744088676, 1574926790555828225, 1264622039642256741);
INSERT INTO `sys_role_menu` VALUES (1574931382744088677, 1574926790555828225, 1264622039642256731);
INSERT INTO `sys_role_menu` VALUES (1574931382744088678, 1574926790555828225, 1264622039642256721);
INSERT INTO `sys_role_menu` VALUES (1574931382744088679, 1574926790555828225, 1342445437296771074);
INSERT INTO `sys_role_menu` VALUES (1574931382744088680, 1574926790555828225, 1574930637298827265);
INSERT INTO `sys_role_menu` VALUES (1574931382744088681, 1574926790555828225, 5474507168841280952);
INSERT INTO `sys_role_menu` VALUES (1574931382744088682, 1574926790555828225, 1574930880073531393);
INSERT INTO `sys_role_menu` VALUES (1585193265841545218, 1551458068714385410, 1538776469361139714);
INSERT INTO `sys_role_menu` VALUES (1585193265841545219, 1551458068714385410, 1264622039642256441);
INSERT INTO `sys_role_menu` VALUES (1585193265841545220, 1551458068714385410, 1264622039642256431);
INSERT INTO `sys_role_menu` VALUES (1585193265841545221, 1551458068714385410, 1264622039642256421);
INSERT INTO `sys_role_menu` VALUES (1585193265841545222, 1551458068714385410, 1264622039642256451);
INSERT INTO `sys_role_menu` VALUES (1585193265841545223, 1551458068714385410, 1264622039642256461);
INSERT INTO `sys_role_menu` VALUES (1585193265841545224, 1551458068714385410, 1264622039642256471);
INSERT INTO `sys_role_menu` VALUES (1585193265841545225, 1551458068714385410, 1264622039642256481);
INSERT INTO `sys_role_menu` VALUES (1585193265841545226, 1551458068714385410, 1264622039642256491);
INSERT INTO `sys_role_menu` VALUES (1585193265841545227, 1551458068714385410, 1264622039642256501);
INSERT INTO `sys_role_menu` VALUES (1585193265841545228, 1551458068714385410, 1264622039642256511);
INSERT INTO `sys_role_menu` VALUES (1585193265841545229, 1551458068714385410, 1264622039642256541);
INSERT INTO `sys_role_menu` VALUES (1585193265841545230, 1551458068714385410, 1264622039642256531);
INSERT INTO `sys_role_menu` VALUES (1585193265841545231, 1551458068714385410, 1264622039642256521);
INSERT INTO `sys_role_menu` VALUES (1585193265841545232, 1551458068714385410, 1264622039642256551);
INSERT INTO `sys_role_menu` VALUES (1585193265841545233, 1551458068714385410, 1264622039642256561);
INSERT INTO `sys_role_menu` VALUES (1585193265841545234, 1551458068714385410, 1264622039642256571);
INSERT INTO `sys_role_menu` VALUES (1585193265841545235, 1551458068714385410, 1264622039642256581);
INSERT INTO `sys_role_menu` VALUES (1585193265841545236, 1551458068714385410, 1264622039642256591);
INSERT INTO `sys_role_menu` VALUES (1585193265841545237, 1551458068714385410, 1264622039642256601);
INSERT INTO `sys_role_menu` VALUES (1585193265841545238, 1551458068714385410, 1410859007809736705);
INSERT INTO `sys_role_menu` VALUES (1585193265841545239, 1551458068714385410, 6570253563256899359);
INSERT INTO `sys_role_menu` VALUES (1585193265841545240, 1551458068714385410, 6527167616560933135);
INSERT INTO `sys_role_menu` VALUES (1585193265841545241, 1551458068714385410, 1534384317233524738);
INSERT INTO `sys_role_menu` VALUES (1585193265841545242, 1551458068714385410, 1551480636561321985);
INSERT INTO `sys_role_menu` VALUES (1585193265841545243, 1551458068714385410, 6547301851685353031);
INSERT INTO `sys_role_menu` VALUES (1585193265841545244, 1551458068714385410, 8197158197771689059);
INSERT INTO `sys_role_menu` VALUES (1585193265841545245, 1551458068714385410, 9222389351831596218);
INSERT INTO `sys_role_menu` VALUES (1585193265841545246, 1551458068714385410, 5142735407151148234);
INSERT INTO `sys_role_menu` VALUES (1585193265841545247, 1551458068714385410, 7621956982866257638);
INSERT INTO `sys_role_menu` VALUES (1585193265841545248, 1551458068714385410, 5553078347948343545);
INSERT INTO `sys_role_menu` VALUES (1585193265904459777, 1551458068714385410, 4998978684524443692);
INSERT INTO `sys_role_menu` VALUES (1585193265904459778, 1551458068714385410, 6449912099524079616);
INSERT INTO `sys_role_menu` VALUES (1585193265904459779, 1551458068714385410, 5707993274387362828);
INSERT INTO `sys_role_menu` VALUES (1585193265904459780, 1551458068714385410, 8275137972166495794);
INSERT INTO `sys_role_menu` VALUES (1585193265904459781, 1551458068714385410, 6249597959063106635);
INSERT INTO `sys_role_menu` VALUES (1585193265904459782, 1551458068714385410, 6905244625723471705);
INSERT INTO `sys_role_menu` VALUES (1585193265904459783, 1551458068714385410, 6709768906758836391);
INSERT INTO `sys_role_menu` VALUES (1585193265904459784, 1551458068714385410, 7388361180917918171);
INSERT INTO `sys_role_menu` VALUES (1585193265904459785, 1551458068714385410, 7702400310012060990);
INSERT INTO `sys_role_menu` VALUES (1585193265904459786, 1551458068714385410, 5243613315131080710);
INSERT INTO `sys_role_menu` VALUES (1585193265904459787, 1551458068714385410, 5079108238558434584);
INSERT INTO `sys_role_menu` VALUES (1585193265904459788, 1551458068714385410, 8562547749529140265);
INSERT INTO `sys_role_menu` VALUES (1585193265904459789, 1551458068714385410, 5788080164048630938);
INSERT INTO `sys_role_menu` VALUES (1585193265904459790, 1551458068714385410, 7936411315868128942);
INSERT INTO `sys_role_menu` VALUES (1585193265904459791, 1551458068714385410, 7940657177277675482);
INSERT INTO `sys_role_menu` VALUES (1585193265904459792, 1551458068714385410, 6194217070047486945);
INSERT INTO `sys_role_menu` VALUES (1585193265904459793, 1551458068714385410, 6436044181834026359);
INSERT INTO `sys_role_menu` VALUES (1585193265904459794, 1551458068714385410, 8315301036989682205);
INSERT INTO `sys_role_menu` VALUES (1585193265904459795, 1551458068714385410, 8618504262472492350);
INSERT INTO `sys_role_menu` VALUES (1585193265904459796, 1551458068714385410, 7950819887425681478);
INSERT INTO `sys_role_menu` VALUES (1585193265904459797, 1551458068714385410, 4832107133629024589);
INSERT INTO `sys_role_menu` VALUES (1585193265904459798, 1551458068714385410, 7946795106595000695);
INSERT INTO `sys_role_menu` VALUES (1585193265904459799, 1551458068714385410, 5279583233581044419);
INSERT INTO `sys_role_menu` VALUES (1585193265904459800, 1551458068714385410, 6845666717193712873);
INSERT INTO `sys_role_menu` VALUES (1585193265904459801, 1551458068714385410, 8426505795261326687);
INSERT INTO `sys_role_menu` VALUES (1585193265904459802, 1551458068714385410, 6103813152661113624);
INSERT INTO `sys_role_menu` VALUES (1585193265904459803, 1551458068714385410, 6374585520258378360);
INSERT INTO `sys_role_menu` VALUES (1585193265904459804, 1551458068714385410, 8621765388717143178);
INSERT INTO `sys_role_menu` VALUES (1585193265904459805, 1551458068714385410, 6116805160244737678);
INSERT INTO `sys_role_menu` VALUES (1585193265904459806, 1551458068714385410, 6860312372874164125);
INSERT INTO `sys_role_menu` VALUES (1585193265904459807, 1551458068714385410, 5820472386138045917);
INSERT INTO `sys_role_menu` VALUES (1585193265904459808, 1551458068714385410, 8181822590852115190);
INSERT INTO `sys_role_menu` VALUES (1585193265904459809, 1551458068714385410, 7128085356547940830);
INSERT INTO `sys_role_menu` VALUES (1585193265967374337, 1551458068714385410, 6204862670753579307);
INSERT INTO `sys_role_menu` VALUES (1585193265967374338, 1551458068714385410, 4835110481928644952);
INSERT INTO `sys_role_menu` VALUES (1585193265967374339, 1551458068714385410, 7047324015553475476);
INSERT INTO `sys_role_menu` VALUES (1585193265967374340, 1551458068714385410, 7581916369308597479);
INSERT INTO `sys_role_menu` VALUES (1585193265967374341, 1551458068714385410, 6591367648784009715);
INSERT INTO `sys_role_menu` VALUES (1585193265967374342, 1551458068714385410, 6015462925549607928);
INSERT INTO `sys_role_menu` VALUES (1585193265967374343, 1551458068714385410, 4719172401435180793);
INSERT INTO `sys_role_menu` VALUES (1585193265967374344, 1551458068714385410, 7639361031662168624);
INSERT INTO `sys_role_menu` VALUES (1585193265967374345, 1551458068714385410, 6507903030541782366);
INSERT INTO `sys_role_menu` VALUES (1585193265967374346, 1551458068714385410, 1534410297847214081);
INSERT INTO `sys_role_menu` VALUES (1585193265967374347, 1551458068714385410, 8562653372099681846);
INSERT INTO `sys_role_menu` VALUES (1585193265967374348, 1551458068714385410, 4830986669641223230);
INSERT INTO `sys_role_menu` VALUES (1585193265967374349, 1551458068714385410, 7974141988632466544);
INSERT INTO `sys_role_menu` VALUES (1585193265967374350, 1551458068714385410, 8313819116019873398);
INSERT INTO `sys_role_menu` VALUES (1585193265967374351, 1551458068714385410, 8931538668430408569);
INSERT INTO `sys_role_menu` VALUES (1585193265967374352, 1551458068714385410, 6122595225588020183);
INSERT INTO `sys_role_menu` VALUES (1585193265967374353, 1551458068714385410, 8477499608364957015);
INSERT INTO `sys_role_menu` VALUES (1585193265967374354, 1551458068714385410, 5206868747918117433);
INSERT INTO `sys_role_menu` VALUES (1585193265967374355, 1551458068714385410, 4757443910107701590);
INSERT INTO `sys_role_menu` VALUES (1585193265967374356, 1551458068714385410, 5016248121790626902);
INSERT INTO `sys_role_menu` VALUES (1585193265967374357, 1551458068714385410, 7516608184768822750);
INSERT INTO `sys_role_menu` VALUES (1585193265967374358, 1551458068714385410, 8293588485966810596);
INSERT INTO `sys_role_menu` VALUES (1585193265967374359, 1551458068714385410, 5523506981641034218);
INSERT INTO `sys_role_menu` VALUES (1585193265967374360, 1551458068714385410, 8494981115107347954);
INSERT INTO `sys_role_menu` VALUES (1585193265967374361, 1551458068714385410, 5934688735467584877);
INSERT INTO `sys_role_menu` VALUES (1585193265967374362, 1551458068714385410, 6045763991836034103);
INSERT INTO `sys_role_menu` VALUES (1585193265967374363, 1551458068714385410, 7561922286979539034);
INSERT INTO `sys_role_menu` VALUES (1585193265967374364, 1551458068714385410, 5457126757845470309);
INSERT INTO `sys_role_menu` VALUES (1585193265967374365, 1551458068714385410, 8590002636006568603);
INSERT INTO `sys_role_menu` VALUES (1585193265967374366, 1551458068714385410, 6257208246505535137);
INSERT INTO `sys_role_menu` VALUES (1585193265967374367, 1551458068714385410, 9118159149629415380);
INSERT INTO `sys_role_menu` VALUES (1585193265967374368, 1551458068714385410, 6354039257511829492);
INSERT INTO `sys_role_menu` VALUES (1585193265967374369, 1551458068714385410, 5445032970491536505);
INSERT INTO `sys_role_menu` VALUES (1585193266038677505, 1551458068714385410, 6542835870761280820);
INSERT INTO `sys_role_menu` VALUES (1585193266038677506, 1551458068714385410, 8191527329183937863);
INSERT INTO `sys_role_menu` VALUES (1585193266038677507, 1551458068714385410, 7357393608825227850);
INSERT INTO `sys_role_menu` VALUES (1585193266038677508, 1551458068714385410, 7447629070175932292);
INSERT INTO `sys_role_menu` VALUES (1585193266038677509, 1551458068714385410, 5228961670953770885);
INSERT INTO `sys_role_menu` VALUES (1585193266038677510, 1551458068714385410, 5234105579337808561);
INSERT INTO `sys_role_menu` VALUES (1585193266038677511, 1551458068714385410, 6457085339840740289);
INSERT INTO `sys_role_menu` VALUES (1585193266038677512, 1551458068714385410, 6667327679590719676);
INSERT INTO `sys_role_menu` VALUES (1585193266038677513, 1551458068714385410, 8959792232643188225);
INSERT INTO `sys_role_menu` VALUES (1585193266038677514, 1551458068714385410, 5668705457330168859);
INSERT INTO `sys_role_menu` VALUES (1585193266038677515, 1551458068714385410, 9180800170765745954);
INSERT INTO `sys_role_menu` VALUES (1585193266038677516, 1551458068714385410, 5327083612717101630);
INSERT INTO `sys_role_menu` VALUES (1585193266038677517, 1551458068714385410, 6329171771009724483);
INSERT INTO `sys_role_menu` VALUES (1585193266038677518, 1551458068714385410, 6020421073902938241);
INSERT INTO `sys_role_menu` VALUES (1585193266038677519, 1551458068714385410, 5886355131876064420);
INSERT INTO `sys_role_menu` VALUES (1585193266038677520, 1551458068714385410, 9202857208449608897);
INSERT INTO `sys_role_menu` VALUES (1585193266038677521, 1551458068714385410, 1560158326283632641);
INSERT INTO `sys_role_menu` VALUES (1585193266038677522, 1551458068714385410, 1560158090005905410);
INSERT INTO `sys_role_menu` VALUES (1585193266038677523, 1551458068714385410, 7656571819576826411);
INSERT INTO `sys_role_menu` VALUES (1585193266038677524, 1551458068714385410, 5014700545593967407);
INSERT INTO `sys_role_menu` VALUES (1585193266038677525, 1551458068714385410, 8088901678692869692);
INSERT INTO `sys_role_menu` VALUES (1585193266038677526, 1551458068714385410, 6299920358694835309);
INSERT INTO `sys_role_menu` VALUES (1585193266038677527, 1551458068714385410, 5642803668063215805);
INSERT INTO `sys_role_menu` VALUES (1585193266038677528, 1551458068714385410, 8834719374925492199);
INSERT INTO `sys_role_menu` VALUES (1585193266038677529, 1551458068714385410, 7882898168203081196);
INSERT INTO `sys_role_menu` VALUES (1585193266038677530, 1551458068714385410, 7346283523793183379);
INSERT INTO `sys_role_menu` VALUES (1585193266038677531, 1551458068714385410, 1542409869531873282);
INSERT INTO `sys_role_menu` VALUES (1585193266038677532, 1551458068714385410, 9208275275751949837);
INSERT INTO `sys_role_menu` VALUES (1585193266038677533, 1551458068714385410, 9173957282399717676);
INSERT INTO `sys_role_menu` VALUES (1585193266038677534, 1551458068714385410, 8973112451110891359);
INSERT INTO `sys_role_menu` VALUES (1585193266038677535, 1551458068714385410, 5753930915061776016);
INSERT INTO `sys_role_menu` VALUES (1585193266038677536, 1551458068714385410, 7288710125399936914);
INSERT INTO `sys_role_menu` VALUES (1585193266038677537, 1551458068714385410, 9022874385071881655);
INSERT INTO `sys_role_menu` VALUES (1585193266038677538, 1551458068714385410, 5378908852735764919);
INSERT INTO `sys_role_menu` VALUES (1585193266038677539, 1551458068714385410, 9152465645130034198);
INSERT INTO `sys_role_menu` VALUES (1585193266038677540, 1551458068714385410, 5012839659883217263);
INSERT INTO `sys_role_menu` VALUES (1585193266038677541, 1551458068714385410, 1534411830716350466);
INSERT INTO `sys_role_menu` VALUES (1585193266038677542, 1551458068714385410, 6280770118494855200);
INSERT INTO `sys_role_menu` VALUES (1585193266038677543, 1551458068714385410, 8775511535879974972);
INSERT INTO `sys_role_menu` VALUES (1585193266038677544, 1551458068714385410, 5624458298951752771);
INSERT INTO `sys_role_menu` VALUES (1585193266101592065, 1551458068714385410, 7254023492833901715);
INSERT INTO `sys_role_menu` VALUES (1585193266101592066, 1551458068714385410, 7318591877639154878);
INSERT INTO `sys_role_menu` VALUES (1585193266101592067, 1551458068714385410, 7812048842906278623);
INSERT INTO `sys_role_menu` VALUES (1585193266101592068, 1551458068714385410, 8633823152794346306);
INSERT INTO `sys_role_menu` VALUES (1585193266101592069, 1551458068714385410, 5893520734687245059);
INSERT INTO `sys_role_menu` VALUES (1585193266101592070, 1551458068714385410, 8868921547741003555);
INSERT INTO `sys_role_menu` VALUES (1585193266101592071, 1551458068714385410, 6326961474985796183);
INSERT INTO `sys_role_menu` VALUES (1585193266101592072, 1551458068714385410, 8833258242949762178);
INSERT INTO `sys_role_menu` VALUES (1585193266101592073, 1551458068714385410, 4967796364303223216);
INSERT INTO `sys_role_menu` VALUES (1585193266101592074, 1551458068714385410, 6900932255841430981);
INSERT INTO `sys_role_menu` VALUES (1585193266101592075, 1551458068714385410, 7629147488562302699);
INSERT INTO `sys_role_menu` VALUES (1585193266101592076, 1551458068714385410, 8016744771368544956);
INSERT INTO `sys_role_menu` VALUES (1585193266101592077, 1551458068714385410, 5002055413507195458);
INSERT INTO `sys_role_menu` VALUES (1585193266101592078, 1551458068714385410, 6290232610677682017);
INSERT INTO `sys_role_menu` VALUES (1585193266101592079, 1551458068714385410, 5803272132980049305);
INSERT INTO `sys_role_menu` VALUES (1585193266101592080, 1551458068714385410, 7395287038762629813);
INSERT INTO `sys_role_menu` VALUES (1585193266101592081, 1551458068714385410, 7323225129555267277);
INSERT INTO `sys_role_menu` VALUES (1585193266101592082, 1551458068714385410, 6347609996552577764);
INSERT INTO `sys_role_menu` VALUES (1585193266101592083, 1551458068714385410, 7682643462888492777);
INSERT INTO `sys_role_menu` VALUES (1585193266101592084, 1551458068714385410, 5721174223820977430);
INSERT INTO `sys_role_menu` VALUES (1585193266101592085, 1551458068714385410, 5469043739505193743);
INSERT INTO `sys_role_menu` VALUES (1585193266101592086, 1551458068714385410, 5459011313540319790);
INSERT INTO `sys_role_menu` VALUES (1585193266101592087, 1551458068714385410, 7723724790018395451);
INSERT INTO `sys_role_menu` VALUES (1585193266101592088, 1551458068714385410, 7764860386896651377);
INSERT INTO `sys_role_menu` VALUES (1585193266101592089, 1551458068714385410, 8254320371052864398);
INSERT INTO `sys_role_menu` VALUES (1585193266101592090, 1551458068714385410, 8273410905135864011);
INSERT INTO `sys_role_menu` VALUES (1585193266101592091, 1551458068714385410, 8515665128226371022);
INSERT INTO `sys_role_menu` VALUES (1585193266101592092, 1551458068714385410, 5000566847705099773);
INSERT INTO `sys_role_menu` VALUES (1585193266101592093, 1551458068714385410, 5555524248707858432);
INSERT INTO `sys_role_menu` VALUES (1585193266101592094, 1551458068714385410, 1562329827773108225);
INSERT INTO `sys_role_menu` VALUES (1585193266101592095, 1551458068714385410, 5901981891345615661);
INSERT INTO `sys_role_menu` VALUES (1585193266101592096, 1551458068714385410, 8342821565086833982);
INSERT INTO `sys_role_menu` VALUES (1585193266101592097, 1551458068714385410, 7044559182298687818);
INSERT INTO `sys_role_menu` VALUES (1585193266101592098, 1551458068714385410, 5714641428045333341);
INSERT INTO `sys_role_menu` VALUES (1585193266101592099, 1551458068714385410, 5801879553212456867);
INSERT INTO `sys_role_menu` VALUES (1585193266101592100, 1551458068714385410, 6739113753546148532);
INSERT INTO `sys_role_menu` VALUES (1585193266101592101, 1551458068714385410, 6696300230304937737);
INSERT INTO `sys_role_menu` VALUES (1585193266101592102, 1551458068714385410, 8436831550169549160);
INSERT INTO `sys_role_menu` VALUES (1585193266101592103, 1551458068714385410, 1558006914472747010);
INSERT INTO `sys_role_menu` VALUES (1585193266101592104, 1551458068714385410, 7898061431233899837);
INSERT INTO `sys_role_menu` VALUES (1585193266101592105, 1551458068714385410, 5448185299220002627);
INSERT INTO `sys_role_menu` VALUES (1585193266101592106, 1551458068714385410, 9135927743117002076);
INSERT INTO `sys_role_menu` VALUES (1585193266101592107, 1551458068714385410, 6655166482691941995);
INSERT INTO `sys_role_menu` VALUES (1585193266101592108, 1551458068714385410, 5871841993475141582);
INSERT INTO `sys_role_menu` VALUES (1585193266172895233, 1551458068714385410, 5276828216703973340);
INSERT INTO `sys_role_menu` VALUES (1585193266172895234, 1551458068714385410, 7072750871619746982);
INSERT INTO `sys_role_menu` VALUES (1585193266172895235, 1551458068714385410, 1574930219948802050);
INSERT INTO `sys_role_menu` VALUES (1585193266172895236, 1551458068714385410, 8107628912599125262);
INSERT INTO `sys_role_menu` VALUES (1585193266172895237, 1551458068714385410, 4903707486851837034);
INSERT INTO `sys_role_menu` VALUES (1585193266172895238, 1551458068714385410, 7635960329677377171);
INSERT INTO `sys_role_menu` VALUES (1585193266172895239, 1551458068714385410, 7596329328691560085);
INSERT INTO `sys_role_menu` VALUES (1585193266172895240, 1551458068714385410, 6429021782911745716);
INSERT INTO `sys_role_menu` VALUES (1585193266172895241, 1551458068714385410, 8259333771823873238);
INSERT INTO `sys_role_menu` VALUES (1585193266172895242, 1551458068714385410, 4674816360932725215);
INSERT INTO `sys_role_menu` VALUES (1585193266172895243, 1551458068714385410, 8700710314151240148);
INSERT INTO `sys_role_menu` VALUES (1585193266172895244, 1551458068714385410, 6238776219082572290);
INSERT INTO `sys_role_menu` VALUES (1585193266172895245, 1551458068714385410, 8613714595480779524);
INSERT INTO `sys_role_menu` VALUES (1585193266172895246, 1551458068714385410, 5346549595871021092);
INSERT INTO `sys_role_menu` VALUES (1585193266172895247, 1551458068714385410, 8181816557328888214);
INSERT INTO `sys_role_menu` VALUES (1585193266172895248, 1551458068714385410, 5587279777867961498);
INSERT INTO `sys_role_menu` VALUES (1585193266172895249, 1551458068714385410, 7380411400614516902);
INSERT INTO `sys_role_menu` VALUES (1585193266172895250, 1551458068714385410, 7721498005843290304);
INSERT INTO `sys_role_menu` VALUES (1585193266172895251, 1551458068714385410, 7455112058491820064);
INSERT INTO `sys_role_menu` VALUES (1585193266172895252, 1551458068714385410, 8957317986837088422);
INSERT INTO `sys_role_menu` VALUES (1585193266172895253, 1551458068714385410, 4621333945127042674);
INSERT INTO `sys_role_menu` VALUES (1585193266172895254, 1551458068714385410, 5886900280768283007);
INSERT INTO `sys_role_menu` VALUES (1585193266172895255, 1551458068714385410, 5846194751422568093);
INSERT INTO `sys_role_menu` VALUES (1585193266172895256, 1551458068714385410, 9156093698218665129);
INSERT INTO `sys_role_menu` VALUES (1585193266172895257, 1551458068714385410, 7678659259268274624);
INSERT INTO `sys_role_menu` VALUES (1585193266172895258, 1551458068714385410, 8099644559700436461);
INSERT INTO `sys_role_menu` VALUES (1585193266172895259, 1551458068714385410, 5653092812632926489);
INSERT INTO `sys_role_menu` VALUES (1585193266172895260, 1551458068714385410, 7200474546305867050);
INSERT INTO `sys_role_menu` VALUES (1585193266172895261, 1551458068714385410, 7242870236365548592);
INSERT INTO `sys_role_menu` VALUES (1585193266172895262, 1551458068714385410, 9150731911657480775);
INSERT INTO `sys_role_menu` VALUES (1585193266172895263, 1551458068714385410, 7287765970469918079);
INSERT INTO `sys_role_menu` VALUES (1585193266172895264, 1551458068714385410, 5480200211927228567);
INSERT INTO `sys_role_menu` VALUES (1585193266172895265, 1551458068714385410, 4770899845682577376);
INSERT INTO `sys_role_menu` VALUES (1585193266172895266, 1551458068714385410, 1264622039642256641);
INSERT INTO `sys_role_menu` VALUES (1585193266172895267, 1551458068714385410, 1264622039642256621);
INSERT INTO `sys_role_menu` VALUES (1585193266172895268, 1551458068714385410, 1264622039642256611);
INSERT INTO `sys_role_menu` VALUES (1585193266172895269, 1551458068714385410, 1264622039642256651);
INSERT INTO `sys_role_menu` VALUES (1585193266172895270, 1551458068714385410, 1264622039642256661);
INSERT INTO `sys_role_menu` VALUES (1585193266172895271, 1551458068714385410, 1264622039642256671);
INSERT INTO `sys_role_menu` VALUES (1585193266172895272, 1551458068714385410, 1264622039642256681);
INSERT INTO `sys_role_menu` VALUES (1585193266172895273, 1551458068714385410, 1264622039642256691);
INSERT INTO `sys_role_menu` VALUES (1585193266172895274, 1551458068714385410, 1264622039642256701);
INSERT INTO `sys_role_menu` VALUES (1585193266172895275, 1551458068714385410, 1264622039642256711);
INSERT INTO `sys_role_menu` VALUES (1585193266172895276, 1551458068714385410, 1264622039642256631);
INSERT INTO `sys_role_menu` VALUES (1585193266172895277, 1551458068714385410, 1264622039642255361);
INSERT INTO `sys_role_menu` VALUES (1585193266172895278, 1551458068714385410, 1264622039642255351);
INSERT INTO `sys_role_menu` VALUES (1585193266172895279, 1551458068714385410, 1264622039642255341);
INSERT INTO `sys_role_menu` VALUES (1585193266172895280, 1551458068714385410, 1264622039642255371);
INSERT INTO `sys_role_menu` VALUES (1585193266172895281, 1551458068714385410, 1264622039642255381);
INSERT INTO `sys_role_menu` VALUES (1585193266172895282, 1551458068714385410, 1264622039642255391);
INSERT INTO `sys_role_menu` VALUES (1585193266172895283, 1551458068714385410, 1264622039642255401);
INSERT INTO `sys_role_menu` VALUES (1585193266172895284, 1551458068714385410, 1264622039642255411);
INSERT INTO `sys_role_menu` VALUES (1585193266235809793, 1551458068714385410, 1264622039642255421);
INSERT INTO `sys_role_menu` VALUES (1585193266235809794, 1551458068714385410, 1264622039642255431);
INSERT INTO `sys_role_menu` VALUES (1585193266235809795, 1551458068714385410, 1264622039642255441);
INSERT INTO `sys_role_menu` VALUES (1585193266235809796, 1551458068714385410, 1264622039642255451);
INSERT INTO `sys_role_menu` VALUES (1585193266235809797, 1551458068714385410, 1264622039642255461);
INSERT INTO `sys_role_menu` VALUES (1585193266235809798, 1551458068714385410, 1264622039642255471);
INSERT INTO `sys_role_menu` VALUES (1585193266235809799, 1551458068714385410, 1264622039642255481);
INSERT INTO `sys_role_menu` VALUES (1585193266235809800, 1551458068714385410, 1264622039642255491);
INSERT INTO `sys_role_menu` VALUES (1585193266235809801, 1551458068714385410, 1264622039642255501);
INSERT INTO `sys_role_menu` VALUES (1585193266235809802, 1551458068714385410, 1264622039642255511);
INSERT INTO `sys_role_menu` VALUES (1585193266235809803, 1551458068714385410, 1264622039642255521);
INSERT INTO `sys_role_menu` VALUES (1585193266235809804, 1551458068714385410, 1264622039642255531);
INSERT INTO `sys_role_menu` VALUES (1585193266235809805, 1551458068714385410, 1264622039642255541);
INSERT INTO `sys_role_menu` VALUES (1585193266235809806, 1551458068714385410, 1264622039642255551);
INSERT INTO `sys_role_menu` VALUES (1585193266235809807, 1551458068714385410, 1264622039642255561);
INSERT INTO `sys_role_menu` VALUES (1585193266235809808, 1551458068714385410, 1264622039642255571);
INSERT INTO `sys_role_menu` VALUES (1585193266235809809, 1551458068714385410, 1264622039642255581);
INSERT INTO `sys_role_menu` VALUES (1585193266235809810, 1551458068714385410, 1264622039642255591);
INSERT INTO `sys_role_menu` VALUES (1585193266235809811, 1551458068714385410, 1264622039642255601);
INSERT INTO `sys_role_menu` VALUES (1585193266235809812, 1551458068714385410, 1264622039642255621);
INSERT INTO `sys_role_menu` VALUES (1585193266235809813, 1551458068714385410, 1264622039642255631);
INSERT INTO `sys_role_menu` VALUES (1585193266235809814, 1551458068714385410, 1264622039642255641);
INSERT INTO `sys_role_menu` VALUES (1585193266235809815, 1551458068714385410, 1264622039642255651);
INSERT INTO `sys_role_menu` VALUES (1585193266235809816, 1551458068714385410, 1264622039642255661);
INSERT INTO `sys_role_menu` VALUES (1585193266235809817, 1551458068714385410, 1264622039642255611);
INSERT INTO `sys_role_menu` VALUES (1585193266235809818, 1551458068714385410, 1264622039642255311);
INSERT INTO `sys_role_menu` VALUES (1585193266235809819, 1551458068714385410, 1264622039642255331);
INSERT INTO `sys_role_menu` VALUES (1585193266235809820, 1551458068714385410, 1264622039642255321);
INSERT INTO `sys_role_menu` VALUES (1585193266235809821, 1551458068714385410, 1264622039642255671);
INSERT INTO `sys_role_menu` VALUES (1585193266235809822, 1551458068714385410, 1264622039642255681);
INSERT INTO `sys_role_menu` VALUES (1585193266235809823, 1551458068714385410, 1264622039642255691);
INSERT INTO `sys_role_menu` VALUES (1585193266235809824, 1551458068714385410, 1264622039642255701);
INSERT INTO `sys_role_menu` VALUES (1585193266235809825, 1551458068714385410, 1264622039642255711);
INSERT INTO `sys_role_menu` VALUES (1585193266235809826, 1551458068714385410, 1264622039642255721);
INSERT INTO `sys_role_menu` VALUES (1585193266235809827, 1551458068714385410, 1264622039642255731);
INSERT INTO `sys_role_menu` VALUES (1585193266235809828, 1551458068714385410, 1264622039642255741);
INSERT INTO `sys_role_menu` VALUES (1585193266235809829, 1551458068714385410, 1264622039642255751);
INSERT INTO `sys_role_menu` VALUES (1585193266235809830, 1551458068714385410, 1264622039642255761);
INSERT INTO `sys_role_menu` VALUES (1585193266235809831, 1551458068714385410, 1264622039642255771);
INSERT INTO `sys_role_menu` VALUES (1585193266235809832, 1551458068714385410, 1264622039642255781);
INSERT INTO `sys_role_menu` VALUES (1585193266235809833, 1551458068714385410, 1264622039642255791);
INSERT INTO `sys_role_menu` VALUES (1585193266235809834, 1551458068714385410, 1264622039642255801);
INSERT INTO `sys_role_menu` VALUES (1585193266235809835, 1551458068714385410, 1264622039642255811);
INSERT INTO `sys_role_menu` VALUES (1585193266235809836, 1551458068714385410, 1264622039642255821);
INSERT INTO `sys_role_menu` VALUES (1585193266235809837, 1551458068714385410, 1264622039642255831);
INSERT INTO `sys_role_menu` VALUES (1585193266235809838, 1551458068714385410, 1264622039642255841);
INSERT INTO `sys_role_menu` VALUES (1585193266235809839, 1551458068714385410, 1264622039642255851);
INSERT INTO `sys_role_menu` VALUES (1585193266235809840, 1551458068714385410, 1264622039642255881);
INSERT INTO `sys_role_menu` VALUES (1585193266235809841, 1551458068714385410, 1264622039642255891);
INSERT INTO `sys_role_menu` VALUES (1585193266235809842, 1551458068714385410, 1264622039642255901);
INSERT INTO `sys_role_menu` VALUES (1585193266235809843, 1551458068714385410, 1264622039642255911);
INSERT INTO `sys_role_menu` VALUES (1585193266302918657, 1551458068714385410, 1264622039642255921);
INSERT INTO `sys_role_menu` VALUES (1585193266302918658, 1551458068714385410, 1264622039642255931);
INSERT INTO `sys_role_menu` VALUES (1585193266302918659, 1551458068714385410, 1264622039642255941);
INSERT INTO `sys_role_menu` VALUES (1585193266302918660, 1551458068714385410, 1264622039642255951);
INSERT INTO `sys_role_menu` VALUES (1585193266302918661, 1551458068714385410, 1264622039642255861);
INSERT INTO `sys_role_menu` VALUES (1585193266302918662, 1551458068714385410, 1264622039642255871);
INSERT INTO `sys_role_menu` VALUES (1585193266302918663, 1551458068714385410, 1264622039642255961);
INSERT INTO `sys_role_menu` VALUES (1585193266302918664, 1551458068714385410, 1264622039642255971);
INSERT INTO `sys_role_menu` VALUES (1585193266302918665, 1551458068714385410, 1264622039642255981);
INSERT INTO `sys_role_menu` VALUES (1585193266302918666, 1551458068714385410, 1264622039642255991);
INSERT INTO `sys_role_menu` VALUES (1585193266302918667, 1551458068714385410, 1264622039642256001);
INSERT INTO `sys_role_menu` VALUES (1585193266302918668, 1551458068714385410, 1264622039642256011);
INSERT INTO `sys_role_menu` VALUES (1585193266302918669, 1551458068714385410, 1264622039642256021);
INSERT INTO `sys_role_menu` VALUES (1585193266302918670, 1551458068714385410, 1264622039642256031);
INSERT INTO `sys_role_menu` VALUES (1585193266302918671, 1551458068714385410, 1264622039642256041);
INSERT INTO `sys_role_menu` VALUES (1585193266302918672, 1551458068714385410, 1264622039642256051);
INSERT INTO `sys_role_menu` VALUES (1585193266302918673, 1551458068714385410, 1264622039642256061);
INSERT INTO `sys_role_menu` VALUES (1585193266302918674, 1551458068714385410, 1264622039642256071);
INSERT INTO `sys_role_menu` VALUES (1585193266302918675, 1551458068714385410, 1264622039642256081);
INSERT INTO `sys_role_menu` VALUES (1585193266302918676, 1551458068714385410, 1264622039642256091);
INSERT INTO `sys_role_menu` VALUES (1585193266302918677, 1551458068714385410, 1264622039642256101);
INSERT INTO `sys_role_menu` VALUES (1585193266302918678, 1551458068714385410, 1264622039642256111);
INSERT INTO `sys_role_menu` VALUES (1585193266302918679, 1551458068714385410, 1264622039642256131);
INSERT INTO `sys_role_menu` VALUES (1585193266302918680, 1551458068714385410, 1264622039642256141);
INSERT INTO `sys_role_menu` VALUES (1585193266302918681, 1551458068714385410, 1264622039642256151);
INSERT INTO `sys_role_menu` VALUES (1585193266302918682, 1551458068714385410, 1264622039642256161);
INSERT INTO `sys_role_menu` VALUES (1585193266302918683, 1551458068714385410, 1264622039642256171);
INSERT INTO `sys_role_menu` VALUES (1585193266302918684, 1551458068714385410, 1264622039642256181);
INSERT INTO `sys_role_menu` VALUES (1585193266302918685, 1551458068714385410, 1264622039642256191);
INSERT INTO `sys_role_menu` VALUES (1585193266302918686, 1551458068714385410, 1264622039642256201);
INSERT INTO `sys_role_menu` VALUES (1585193266302918687, 1551458068714385410, 1264622039642256211);
INSERT INTO `sys_role_menu` VALUES (1585193266432942082, 1551458068714385410, 1264622039642256221);
INSERT INTO `sys_role_menu` VALUES (1585193266432942083, 1551458068714385410, 1264622039642256231);
INSERT INTO `sys_role_menu` VALUES (1585193266449719297, 1551458068714385410, 1264622039642256241);
INSERT INTO `sys_role_menu` VALUES (1585193266449719298, 1551458068714385410, 1264622039642256251);
INSERT INTO `sys_role_menu` VALUES (1585193266449719299, 1551458068714385410, 1264622039642256261);
INSERT INTO `sys_role_menu` VALUES (1585193266449719300, 1551458068714385410, 1264622039642256121);
INSERT INTO `sys_role_menu` VALUES (1585193266449719301, 1551458068714385410, 1264622039642256271);
INSERT INTO `sys_role_menu` VALUES (1585193266449719302, 1551458068714385410, 1264622039642256281);
INSERT INTO `sys_role_menu` VALUES (1585193266449719303, 1551458068714385410, 1264622039642256291);
INSERT INTO `sys_role_menu` VALUES (1585193266449719304, 1551458068714385410, 1264622039642256301);
INSERT INTO `sys_role_menu` VALUES (1585193266449719305, 1551458068714385410, 1264622039642256311);
INSERT INTO `sys_role_menu` VALUES (1585193266449719306, 1551458068714385410, 1264622039642256321);
INSERT INTO `sys_role_menu` VALUES (1585193266449719307, 1551458068714385410, 1264622039642256331);
INSERT INTO `sys_role_menu` VALUES (1585193266449719308, 1551458068714385410, 1264622039642256341);
INSERT INTO `sys_role_menu` VALUES (1585193266449719309, 1551458068714385410, 1264622039642256351);
INSERT INTO `sys_role_menu` VALUES (1585193266449719310, 1551458068714385410, 1264622039642256361);
INSERT INTO `sys_role_menu` VALUES (1585193266449719311, 1551458068714385410, 1264622039642256371);
INSERT INTO `sys_role_menu` VALUES (1585193266449719312, 1551458068714385410, 1264622039642256381);
INSERT INTO `sys_role_menu` VALUES (1585193266449719313, 1551458068714385410, 1264622039642256391);
INSERT INTO `sys_role_menu` VALUES (1585193266449719314, 1551458068714385410, 1264622039642256401);
INSERT INTO `sys_role_menu` VALUES (1585193266449719315, 1551458068714385410, 1264622039642256411);
INSERT INTO `sys_role_menu` VALUES (1585193266449719316, 1551458068714385410, 1560145190851772417);
INSERT INTO `sys_role_menu` VALUES (1585193266449719317, 1551458068714385410, 256);
INSERT INTO `sys_role_menu` VALUES (1585193266449719318, 1551458068714385410, 1);
INSERT INTO `sys_role_menu` VALUES (1585193266449719319, 1551458068714385410, 257);
INSERT INTO `sys_role_menu` VALUES (1585193266449719320, 1551458068714385410, 2);
INSERT INTO `sys_role_menu` VALUES (1585193266449719321, 1551458068714385410, 258);
INSERT INTO `sys_role_menu` VALUES (1585193266449719322, 1551458068714385410, 3);
INSERT INTO `sys_role_menu` VALUES (1585193266449719323, 1551458068714385410, 259);
INSERT INTO `sys_role_menu` VALUES (1585193266449719324, 1551458068714385410, 4);
INSERT INTO `sys_role_menu` VALUES (1585193266449719325, 1551458068714385410, 260);
INSERT INTO `sys_role_menu` VALUES (1585193266449719326, 1551458068714385410, 5);
INSERT INTO `sys_role_menu` VALUES (1585193266449719327, 1551458068714385410, 261);
INSERT INTO `sys_role_menu` VALUES (1585193266449719328, 1551458068714385410, 6);
INSERT INTO `sys_role_menu` VALUES (1585193266449719329, 1551458068714385410, 262);
INSERT INTO `sys_role_menu` VALUES (1585193266449719330, 1551458068714385410, 7);
INSERT INTO `sys_role_menu` VALUES (1585193266449719331, 1551458068714385410, 263);
INSERT INTO `sys_role_menu` VALUES (1585193266449719332, 1551458068714385410, 8);
INSERT INTO `sys_role_menu` VALUES (1585193266449719333, 1551458068714385410, 264);
INSERT INTO `sys_role_menu` VALUES (1585193266449719334, 1551458068714385410, 9);
INSERT INTO `sys_role_menu` VALUES (1585193266449719335, 1551458068714385410, 265);
INSERT INTO `sys_role_menu` VALUES (1585193266449719336, 1551458068714385410, 10);
INSERT INTO `sys_role_menu` VALUES (1585193266449719337, 1551458068714385410, 266);
INSERT INTO `sys_role_menu` VALUES (1585193266449719338, 1551458068714385410, 11);
INSERT INTO `sys_role_menu` VALUES (1585193266449719339, 1551458068714385410, 267);
INSERT INTO `sys_role_menu` VALUES (1585193266449719340, 1551458068714385410, 12);
INSERT INTO `sys_role_menu` VALUES (1585193266449719341, 1551458068714385410, 268);
INSERT INTO `sys_role_menu` VALUES (1585193266449719342, 1551458068714385410, 13);
INSERT INTO `sys_role_menu` VALUES (1585193266449719343, 1551458068714385410, 269);
INSERT INTO `sys_role_menu` VALUES (1585193266449719344, 1551458068714385410, 14);
INSERT INTO `sys_role_menu` VALUES (1585193266449719345, 1551458068714385410, 270);
INSERT INTO `sys_role_menu` VALUES (1585193266449719346, 1551458068714385410, 15);
INSERT INTO `sys_role_menu` VALUES (1585193266449719347, 1551458068714385410, 271);
INSERT INTO `sys_role_menu` VALUES (1585193266449719348, 1551458068714385410, 16);
INSERT INTO `sys_role_menu` VALUES (1585193266449719349, 1551458068714385410, 272);
INSERT INTO `sys_role_menu` VALUES (1585193266449719350, 1551458068714385410, 17);
INSERT INTO `sys_role_menu` VALUES (1585193266508439554, 1551458068714385410, 273);
INSERT INTO `sys_role_menu` VALUES (1585193266508439555, 1551458068714385410, 18);
INSERT INTO `sys_role_menu` VALUES (1585193266508439556, 1551458068714385410, 274);
INSERT INTO `sys_role_menu` VALUES (1585193266508439557, 1551458068714385410, 19);
INSERT INTO `sys_role_menu` VALUES (1585193266508439558, 1551458068714385410, 275);
INSERT INTO `sys_role_menu` VALUES (1585193266508439559, 1551458068714385410, 20);
INSERT INTO `sys_role_menu` VALUES (1585193266508439560, 1551458068714385410, 276);
INSERT INTO `sys_role_menu` VALUES (1585193266508439561, 1551458068714385410, 21);
INSERT INTO `sys_role_menu` VALUES (1585193266508439562, 1551458068714385410, 277);
INSERT INTO `sys_role_menu` VALUES (1585193266508439563, 1551458068714385410, 22);
INSERT INTO `sys_role_menu` VALUES (1585193266508439564, 1551458068714385410, 278);
INSERT INTO `sys_role_menu` VALUES (1585193266508439565, 1551458068714385410, 23);
INSERT INTO `sys_role_menu` VALUES (1585193266508439566, 1551458068714385410, 279);
INSERT INTO `sys_role_menu` VALUES (1585193266508439567, 1551458068714385410, 24);
INSERT INTO `sys_role_menu` VALUES (1585193266508439568, 1551458068714385410, 280);
INSERT INTO `sys_role_menu` VALUES (1585193266508439569, 1551458068714385410, 25);
INSERT INTO `sys_role_menu` VALUES (1585193266508439570, 1551458068714385410, 281);
INSERT INTO `sys_role_menu` VALUES (1585193266508439571, 1551458068714385410, 26);
INSERT INTO `sys_role_menu` VALUES (1585193266508439572, 1551458068714385410, 282);
INSERT INTO `sys_role_menu` VALUES (1585193266508439573, 1551458068714385410, 27);
INSERT INTO `sys_role_menu` VALUES (1585193266508439574, 1551458068714385410, 283);
INSERT INTO `sys_role_menu` VALUES (1585193266508439575, 1551458068714385410, 28);
INSERT INTO `sys_role_menu` VALUES (1585193266508439576, 1551458068714385410, 284);
INSERT INTO `sys_role_menu` VALUES (1585193266508439577, 1551458068714385410, 29);
INSERT INTO `sys_role_menu` VALUES (1585193266508439578, 1551458068714385410, 285);
INSERT INTO `sys_role_menu` VALUES (1585193266508439579, 1551458068714385410, 30);
INSERT INTO `sys_role_menu` VALUES (1585193266508439580, 1551458068714385410, 286);
INSERT INTO `sys_role_menu` VALUES (1585193266508439581, 1551458068714385410, 31);
INSERT INTO `sys_role_menu` VALUES (1585193266508439582, 1551458068714385410, 287);
INSERT INTO `sys_role_menu` VALUES (1585193266508439583, 1551458068714385410, 32);
INSERT INTO `sys_role_menu` VALUES (1585193266508439584, 1551458068714385410, 288);
INSERT INTO `sys_role_menu` VALUES (1585193266508439585, 1551458068714385410, 33);
INSERT INTO `sys_role_menu` VALUES (1585193266508439586, 1551458068714385410, 289);
INSERT INTO `sys_role_menu` VALUES (1585193266508439587, 1551458068714385410, 34);
INSERT INTO `sys_role_menu` VALUES (1585193266508439588, 1551458068714385410, 290);
INSERT INTO `sys_role_menu` VALUES (1585193266508439589, 1551458068714385410, 35);
INSERT INTO `sys_role_menu` VALUES (1585193266508439590, 1551458068714385410, 291);
INSERT INTO `sys_role_menu` VALUES (1585193266508439591, 1551458068714385410, 36);
INSERT INTO `sys_role_menu` VALUES (1585193266508439592, 1551458068714385410, 292);
INSERT INTO `sys_role_menu` VALUES (1585193266508439593, 1551458068714385410, 37);
INSERT INTO `sys_role_menu` VALUES (1585193266508439594, 1551458068714385410, 293);
INSERT INTO `sys_role_menu` VALUES (1585193266508439595, 1551458068714385410, 38);
INSERT INTO `sys_role_menu` VALUES (1585193266508439596, 1551458068714385410, 294);
INSERT INTO `sys_role_menu` VALUES (1585193266508439597, 1551458068714385410, 39);
INSERT INTO `sys_role_menu` VALUES (1585193266508439598, 1551458068714385410, 295);
INSERT INTO `sys_role_menu` VALUES (1585193266508439599, 1551458068714385410, 40);
INSERT INTO `sys_role_menu` VALUES (1585193266508439600, 1551458068714385410, 296);
INSERT INTO `sys_role_menu` VALUES (1585193266508439601, 1551458068714385410, 41);
INSERT INTO `sys_role_menu` VALUES (1585193266508439602, 1551458068714385410, 297);
INSERT INTO `sys_role_menu` VALUES (1585193266508439603, 1551458068714385410, 42);
INSERT INTO `sys_role_menu` VALUES (1585193266508439604, 1551458068714385410, 298);
INSERT INTO `sys_role_menu` VALUES (1585193266508439605, 1551458068714385410, 43);
INSERT INTO `sys_role_menu` VALUES (1585193266508439606, 1551458068714385410, 299);
INSERT INTO `sys_role_menu` VALUES (1585193266508439607, 1551458068714385410, 44);
INSERT INTO `sys_role_menu` VALUES (1585193266508439608, 1551458068714385410, 300);
INSERT INTO `sys_role_menu` VALUES (1585193266508439609, 1551458068714385410, 45);
INSERT INTO `sys_role_menu` VALUES (1585193266508439610, 1551458068714385410, 301);
INSERT INTO `sys_role_menu` VALUES (1585193266508439611, 1551458068714385410, 46);
INSERT INTO `sys_role_menu` VALUES (1585193266508439612, 1551458068714385410, 302);
INSERT INTO `sys_role_menu` VALUES (1585193266508439613, 1551458068714385410, 47);
INSERT INTO `sys_role_menu` VALUES (1585193266508439614, 1551458068714385410, 303);
INSERT INTO `sys_role_menu` VALUES (1585193266508439615, 1551458068714385410, 48);
INSERT INTO `sys_role_menu` VALUES (1585193266508439616, 1551458068714385410, 304);
INSERT INTO `sys_role_menu` VALUES (1585193266508439617, 1551458068714385410, 49);
INSERT INTO `sys_role_menu` VALUES (1585193266508439618, 1551458068714385410, 305);
INSERT INTO `sys_role_menu` VALUES (1585193266508439619, 1551458068714385410, 50);
INSERT INTO `sys_role_menu` VALUES (1585193266508439620, 1551458068714385410, 306);
INSERT INTO `sys_role_menu` VALUES (1585193266508439621, 1551458068714385410, 51);
INSERT INTO `sys_role_menu` VALUES (1585193266508439622, 1551458068714385410, 307);
INSERT INTO `sys_role_menu` VALUES (1585193266508439623, 1551458068714385410, 52);
INSERT INTO `sys_role_menu` VALUES (1585193266508439624, 1551458068714385410, 308);
INSERT INTO `sys_role_menu` VALUES (1585193266562965505, 1551458068714385410, 53);
INSERT INTO `sys_role_menu` VALUES (1585193266562965506, 1551458068714385410, 309);
INSERT INTO `sys_role_menu` VALUES (1585193266562965507, 1551458068714385410, 54);
INSERT INTO `sys_role_menu` VALUES (1585193266562965508, 1551458068714385410, 55);
INSERT INTO `sys_role_menu` VALUES (1585193266562965509, 1551458068714385410, 56);
INSERT INTO `sys_role_menu` VALUES (1585193266562965510, 1551458068714385410, 57);
INSERT INTO `sys_role_menu` VALUES (1585193266562965511, 1551458068714385410, 58);
INSERT INTO `sys_role_menu` VALUES (1585193266562965512, 1551458068714385410, 59);
INSERT INTO `sys_role_menu` VALUES (1585193266562965513, 1551458068714385410, 60);
INSERT INTO `sys_role_menu` VALUES (1585193266562965514, 1551458068714385410, 61);
INSERT INTO `sys_role_menu` VALUES (1585193266562965515, 1551458068714385410, 62);
INSERT INTO `sys_role_menu` VALUES (1585193266562965516, 1551458068714385410, 63);
INSERT INTO `sys_role_menu` VALUES (1585193266562965517, 1551458068714385410, 64);
INSERT INTO `sys_role_menu` VALUES (1585193266562965518, 1551458068714385410, 65);
INSERT INTO `sys_role_menu` VALUES (1585193266562965519, 1551458068714385410, 66);
INSERT INTO `sys_role_menu` VALUES (1585193266562965520, 1551458068714385410, 67);
INSERT INTO `sys_role_menu` VALUES (1585193266562965521, 1551458068714385410, 68);
INSERT INTO `sys_role_menu` VALUES (1585193266562965522, 1551458068714385410, 69);
INSERT INTO `sys_role_menu` VALUES (1585193266562965523, 1551458068714385410, 70);
INSERT INTO `sys_role_menu` VALUES (1585193266562965524, 1551458068714385410, 71);
INSERT INTO `sys_role_menu` VALUES (1585193266562965525, 1551458068714385410, 72);
INSERT INTO `sys_role_menu` VALUES (1585193266562965526, 1551458068714385410, 73);
INSERT INTO `sys_role_menu` VALUES (1585193266562965527, 1551458068714385410, 74);
INSERT INTO `sys_role_menu` VALUES (1585193266562965528, 1551458068714385410, 75);
INSERT INTO `sys_role_menu` VALUES (1585193266562965529, 1551458068714385410, 76);
INSERT INTO `sys_role_menu` VALUES (1585193266562965530, 1551458068714385410, 77);
INSERT INTO `sys_role_menu` VALUES (1585193266562965531, 1551458068714385410, 78);
INSERT INTO `sys_role_menu` VALUES (1585193266562965532, 1551458068714385410, 79);
INSERT INTO `sys_role_menu` VALUES (1585193266562965533, 1551458068714385410, 80);
INSERT INTO `sys_role_menu` VALUES (1585193266562965534, 1551458068714385410, 81);
INSERT INTO `sys_role_menu` VALUES (1585193266562965535, 1551458068714385410, 82);
INSERT INTO `sys_role_menu` VALUES (1585193266562965536, 1551458068714385410, 83);
INSERT INTO `sys_role_menu` VALUES (1585193266562965537, 1551458068714385410, 84);
INSERT INTO `sys_role_menu` VALUES (1585193266562965538, 1551458068714385410, 85);
INSERT INTO `sys_role_menu` VALUES (1585193266562965539, 1551458068714385410, 86);
INSERT INTO `sys_role_menu` VALUES (1585193266562965540, 1551458068714385410, 87);
INSERT INTO `sys_role_menu` VALUES (1585193266562965541, 1551458068714385410, 88);
INSERT INTO `sys_role_menu` VALUES (1585193266562965542, 1551458068714385410, 89);
INSERT INTO `sys_role_menu` VALUES (1585193266562965543, 1551458068714385410, 90);
INSERT INTO `sys_role_menu` VALUES (1585193266562965544, 1551458068714385410, 91);
INSERT INTO `sys_role_menu` VALUES (1585193266562965545, 1551458068714385410, 92);
INSERT INTO `sys_role_menu` VALUES (1585193266562965546, 1551458068714385410, 93);
INSERT INTO `sys_role_menu` VALUES (1585193266562965547, 1551458068714385410, 94);
INSERT INTO `sys_role_menu` VALUES (1585193266562965548, 1551458068714385410, 95);
INSERT INTO `sys_role_menu` VALUES (1585193266562965549, 1551458068714385410, 96);
INSERT INTO `sys_role_menu` VALUES (1585193266562965550, 1551458068714385410, 97);
INSERT INTO `sys_role_menu` VALUES (1585193266562965551, 1551458068714385410, 98);
INSERT INTO `sys_role_menu` VALUES (1585193266562965552, 1551458068714385410, 99);
INSERT INTO `sys_role_menu` VALUES (1585193266562965553, 1551458068714385410, 100);
INSERT INTO `sys_role_menu` VALUES (1585193266562965554, 1551458068714385410, 101);
INSERT INTO `sys_role_menu` VALUES (1585193266562965555, 1551458068714385410, 102);
INSERT INTO `sys_role_menu` VALUES (1585193266562965556, 1551458068714385410, 103);
INSERT INTO `sys_role_menu` VALUES (1585193266562965557, 1551458068714385410, 104);
INSERT INTO `sys_role_menu` VALUES (1585193266562965558, 1551458068714385410, 105);
INSERT INTO `sys_role_menu` VALUES (1585193266562965559, 1551458068714385410, 106);
INSERT INTO `sys_role_menu` VALUES (1585193266562965560, 1551458068714385410, 107);
INSERT INTO `sys_role_menu` VALUES (1585193266562965561, 1551458068714385410, 108);
INSERT INTO `sys_role_menu` VALUES (1585193266562965562, 1551458068714385410, 109);
INSERT INTO `sys_role_menu` VALUES (1585193266562965563, 1551458068714385410, 110);
INSERT INTO `sys_role_menu` VALUES (1585193266562965564, 1551458068714385410, 111);
INSERT INTO `sys_role_menu` VALUES (1585193266562965565, 1551458068714385410, 112);
INSERT INTO `sys_role_menu` VALUES (1585193266562965566, 1551458068714385410, 113);
INSERT INTO `sys_role_menu` VALUES (1585193266562965567, 1551458068714385410, 114);
INSERT INTO `sys_role_menu` VALUES (1585193266562965568, 1551458068714385410, 115);
INSERT INTO `sys_role_menu` VALUES (1585193266562965569, 1551458068714385410, 116);
INSERT INTO `sys_role_menu` VALUES (1585193266562965570, 1551458068714385410, 117);
INSERT INTO `sys_role_menu` VALUES (1585193266562965571, 1551458068714385410, 118);
INSERT INTO `sys_role_menu` VALUES (1585193266562965572, 1551458068714385410, 119);
INSERT INTO `sys_role_menu` VALUES (1585193266562965573, 1551458068714385410, 120);
INSERT INTO `sys_role_menu` VALUES (1585193266562965574, 1551458068714385410, 121);
INSERT INTO `sys_role_menu` VALUES (1585193266562965575, 1551458068714385410, 122);
INSERT INTO `sys_role_menu` VALUES (1585193266562965576, 1551458068714385410, 123);
INSERT INTO `sys_role_menu` VALUES (1585193266562965577, 1551458068714385410, 124);
INSERT INTO `sys_role_menu` VALUES (1585193266562965578, 1551458068714385410, 125);
INSERT INTO `sys_role_menu` VALUES (1585193266562965579, 1551458068714385410, 126);
INSERT INTO `sys_role_menu` VALUES (1585193266562965580, 1551458068714385410, 127);
INSERT INTO `sys_role_menu` VALUES (1585193266562965581, 1551458068714385410, 128);
INSERT INTO `sys_role_menu` VALUES (1585193266562965582, 1551458068714385410, 129);
INSERT INTO `sys_role_menu` VALUES (1585193266562965583, 1551458068714385410, 130);
INSERT INTO `sys_role_menu` VALUES (1585193266562965584, 1551458068714385410, 131);
INSERT INTO `sys_role_menu` VALUES (1585193266562965585, 1551458068714385410, 132);
INSERT INTO `sys_role_menu` VALUES (1585193266562965586, 1551458068714385410, 133);
INSERT INTO `sys_role_menu` VALUES (1585193266562965587, 1551458068714385410, 134);
INSERT INTO `sys_role_menu` VALUES (1585193266562965588, 1551458068714385410, 135);
INSERT INTO `sys_role_menu` VALUES (1585193266562965589, 1551458068714385410, 136);
INSERT INTO `sys_role_menu` VALUES (1585193266562965590, 1551458068714385410, 137);
INSERT INTO `sys_role_menu` VALUES (1585193266562965591, 1551458068714385410, 138);
INSERT INTO `sys_role_menu` VALUES (1585193266562965592, 1551458068714385410, 139);
INSERT INTO `sys_role_menu` VALUES (1585193266562965593, 1551458068714385410, 140);
INSERT INTO `sys_role_menu` VALUES (1585193266562965594, 1551458068714385410, 141);
INSERT INTO `sys_role_menu` VALUES (1585193266630074369, 1551458068714385410, 142);
INSERT INTO `sys_role_menu` VALUES (1585193266630074370, 1551458068714385410, 143);
INSERT INTO `sys_role_menu` VALUES (1585193266630074371, 1551458068714385410, 144);
INSERT INTO `sys_role_menu` VALUES (1585193266630074372, 1551458068714385410, 145);
INSERT INTO `sys_role_menu` VALUES (1585193266630074373, 1551458068714385410, 146);
INSERT INTO `sys_role_menu` VALUES (1585193266630074374, 1551458068714385410, 147);
INSERT INTO `sys_role_menu` VALUES (1585193266630074375, 1551458068714385410, 148);
INSERT INTO `sys_role_menu` VALUES (1585193266630074376, 1551458068714385410, 149);
INSERT INTO `sys_role_menu` VALUES (1585193266630074377, 1551458068714385410, 150);
INSERT INTO `sys_role_menu` VALUES (1585193266630074378, 1551458068714385410, 151);
INSERT INTO `sys_role_menu` VALUES (1585193266630074379, 1551458068714385410, 152);
INSERT INTO `sys_role_menu` VALUES (1585193266630074380, 1551458068714385410, 153);
INSERT INTO `sys_role_menu` VALUES (1585193266630074381, 1551458068714385410, 154);
INSERT INTO `sys_role_menu` VALUES (1585193266630074382, 1551458068714385410, 155);
INSERT INTO `sys_role_menu` VALUES (1585193266630074383, 1551458068714385410, 156);
INSERT INTO `sys_role_menu` VALUES (1585193266630074384, 1551458068714385410, 157);
INSERT INTO `sys_role_menu` VALUES (1585193266630074385, 1551458068714385410, 158);
INSERT INTO `sys_role_menu` VALUES (1585193266630074386, 1551458068714385410, 159);
INSERT INTO `sys_role_menu` VALUES (1585193266630074387, 1551458068714385410, 160);
INSERT INTO `sys_role_menu` VALUES (1585193266630074388, 1551458068714385410, 161);
INSERT INTO `sys_role_menu` VALUES (1585193266630074389, 1551458068714385410, 162);
INSERT INTO `sys_role_menu` VALUES (1585193266630074390, 1551458068714385410, 163);
INSERT INTO `sys_role_menu` VALUES (1585193266630074391, 1551458068714385410, 164);
INSERT INTO `sys_role_menu` VALUES (1585193266630074392, 1551458068714385410, 165);
INSERT INTO `sys_role_menu` VALUES (1585193266630074393, 1551458068714385410, 166);
INSERT INTO `sys_role_menu` VALUES (1585193266630074394, 1551458068714385410, 167);
INSERT INTO `sys_role_menu` VALUES (1585193266630074395, 1551458068714385410, 168);
INSERT INTO `sys_role_menu` VALUES (1585193266630074396, 1551458068714385410, 169);
INSERT INTO `sys_role_menu` VALUES (1585193266630074397, 1551458068714385410, 170);
INSERT INTO `sys_role_menu` VALUES (1585193266630074398, 1551458068714385410, 171);
INSERT INTO `sys_role_menu` VALUES (1585193266630074399, 1551458068714385410, 172);
INSERT INTO `sys_role_menu` VALUES (1585193266630074400, 1551458068714385410, 173);
INSERT INTO `sys_role_menu` VALUES (1585193266630074401, 1551458068714385410, 174);
INSERT INTO `sys_role_menu` VALUES (1585193266630074402, 1551458068714385410, 175);
INSERT INTO `sys_role_menu` VALUES (1585193266630074403, 1551458068714385410, 176);
INSERT INTO `sys_role_menu` VALUES (1585193266630074404, 1551458068714385410, 177);
INSERT INTO `sys_role_menu` VALUES (1585193266630074405, 1551458068714385410, 178);
INSERT INTO `sys_role_menu` VALUES (1585193266630074406, 1551458068714385410, 179);
INSERT INTO `sys_role_menu` VALUES (1585193266630074407, 1551458068714385410, 180);
INSERT INTO `sys_role_menu` VALUES (1585193266630074408, 1551458068714385410, 181);
INSERT INTO `sys_role_menu` VALUES (1585193266630074409, 1551458068714385410, 182);
INSERT INTO `sys_role_menu` VALUES (1585193266630074410, 1551458068714385410, 183);
INSERT INTO `sys_role_menu` VALUES (1585193266630074411, 1551458068714385410, 184);
INSERT INTO `sys_role_menu` VALUES (1585193266630074412, 1551458068714385410, 185);
INSERT INTO `sys_role_menu` VALUES (1585193266630074413, 1551458068714385410, 186);
INSERT INTO `sys_role_menu` VALUES (1585193266630074414, 1551458068714385410, 187);
INSERT INTO `sys_role_menu` VALUES (1585193266630074415, 1551458068714385410, 188);
INSERT INTO `sys_role_menu` VALUES (1585193266630074416, 1551458068714385410, 189);
INSERT INTO `sys_role_menu` VALUES (1585193266630074417, 1551458068714385410, 190);
INSERT INTO `sys_role_menu` VALUES (1585193266630074418, 1551458068714385410, 191);
INSERT INTO `sys_role_menu` VALUES (1585193266630074419, 1551458068714385410, 192);
INSERT INTO `sys_role_menu` VALUES (1585193266630074420, 1551458068714385410, 193);
INSERT INTO `sys_role_menu` VALUES (1585193266630074421, 1551458068714385410, 194);
INSERT INTO `sys_role_menu` VALUES (1585193266630074422, 1551458068714385410, 195);
INSERT INTO `sys_role_menu` VALUES (1585193266630074423, 1551458068714385410, 196);
INSERT INTO `sys_role_menu` VALUES (1585193266692988929, 1551458068714385410, 197);
INSERT INTO `sys_role_menu` VALUES (1585193266692988930, 1551458068714385410, 198);
INSERT INTO `sys_role_menu` VALUES (1585193266692988931, 1551458068714385410, 199);
INSERT INTO `sys_role_menu` VALUES (1585193266692988932, 1551458068714385410, 200);
INSERT INTO `sys_role_menu` VALUES (1585193266692988933, 1551458068714385410, 201);
INSERT INTO `sys_role_menu` VALUES (1585193266692988934, 1551458068714385410, 202);
INSERT INTO `sys_role_menu` VALUES (1585193266692988935, 1551458068714385410, 203);
INSERT INTO `sys_role_menu` VALUES (1585193266692988936, 1551458068714385410, 204);
INSERT INTO `sys_role_menu` VALUES (1585193266692988937, 1551458068714385410, 205);
INSERT INTO `sys_role_menu` VALUES (1585193266692988938, 1551458068714385410, 206);
INSERT INTO `sys_role_menu` VALUES (1585193266692988939, 1551458068714385410, 207);
INSERT INTO `sys_role_menu` VALUES (1585193266692988940, 1551458068714385410, 208);
INSERT INTO `sys_role_menu` VALUES (1585193266692988941, 1551458068714385410, 209);
INSERT INTO `sys_role_menu` VALUES (1585193266692988942, 1551458068714385410, 210);
INSERT INTO `sys_role_menu` VALUES (1585193266692988943, 1551458068714385410, 211);
INSERT INTO `sys_role_menu` VALUES (1585193266692988944, 1551458068714385410, 212);
INSERT INTO `sys_role_menu` VALUES (1585193266692988945, 1551458068714385410, 213);
INSERT INTO `sys_role_menu` VALUES (1585193266692988946, 1551458068714385410, 214);
INSERT INTO `sys_role_menu` VALUES (1585193266692988947, 1551458068714385410, 215);
INSERT INTO `sys_role_menu` VALUES (1585193266692988948, 1551458068714385410, 216);
INSERT INTO `sys_role_menu` VALUES (1585193266692988949, 1551458068714385410, 217);
INSERT INTO `sys_role_menu` VALUES (1585193266692988950, 1551458068714385410, 218);
INSERT INTO `sys_role_menu` VALUES (1585193266692988951, 1551458068714385410, 219);
INSERT INTO `sys_role_menu` VALUES (1585193266692988952, 1551458068714385410, 220);
INSERT INTO `sys_role_menu` VALUES (1585193266692988953, 1551458068714385410, 221);
INSERT INTO `sys_role_menu` VALUES (1585193266692988954, 1551458068714385410, 222);
INSERT INTO `sys_role_menu` VALUES (1585193266692988955, 1551458068714385410, 223);
INSERT INTO `sys_role_menu` VALUES (1585193266692988956, 1551458068714385410, 224);
INSERT INTO `sys_role_menu` VALUES (1585193266692988957, 1551458068714385410, 225);
INSERT INTO `sys_role_menu` VALUES (1585193266692988958, 1551458068714385410, 226);
INSERT INTO `sys_role_menu` VALUES (1585193266692988959, 1551458068714385410, 227);
INSERT INTO `sys_role_menu` VALUES (1585193266692988960, 1551458068714385410, 228);
INSERT INTO `sys_role_menu` VALUES (1585193266692988961, 1551458068714385410, 229);
INSERT INTO `sys_role_menu` VALUES (1585193266692988962, 1551458068714385410, 230);
INSERT INTO `sys_role_menu` VALUES (1585193266692988963, 1551458068714385410, 231);
INSERT INTO `sys_role_menu` VALUES (1585193266692988964, 1551458068714385410, 232);
INSERT INTO `sys_role_menu` VALUES (1585193266692988965, 1551458068714385410, 233);
INSERT INTO `sys_role_menu` VALUES (1585193266692988966, 1551458068714385410, 234);
INSERT INTO `sys_role_menu` VALUES (1585193266692988967, 1551458068714385410, 235);
INSERT INTO `sys_role_menu` VALUES (1585193266692988968, 1551458068714385410, 236);
INSERT INTO `sys_role_menu` VALUES (1585193266692988969, 1551458068714385410, 237);
INSERT INTO `sys_role_menu` VALUES (1585193266692988970, 1551458068714385410, 238);
INSERT INTO `sys_role_menu` VALUES (1585193266692988971, 1551458068714385410, 239);
INSERT INTO `sys_role_menu` VALUES (1585193266692988972, 1551458068714385410, 240);
INSERT INTO `sys_role_menu` VALUES (1585193266692988973, 1551458068714385410, 241);
INSERT INTO `sys_role_menu` VALUES (1585193266692988974, 1551458068714385410, 242);
INSERT INTO `sys_role_menu` VALUES (1585193266692988975, 1551458068714385410, 243);
INSERT INTO `sys_role_menu` VALUES (1585193266692988976, 1551458068714385410, 244);
INSERT INTO `sys_role_menu` VALUES (1585193266692988977, 1551458068714385410, 245);
INSERT INTO `sys_role_menu` VALUES (1585193266692988978, 1551458068714385410, 246);
INSERT INTO `sys_role_menu` VALUES (1585193266692988979, 1551458068714385410, 247);
INSERT INTO `sys_role_menu` VALUES (1585193266692988980, 1551458068714385410, 248);
INSERT INTO `sys_role_menu` VALUES (1585193266692988981, 1551458068714385410, 249);
INSERT INTO `sys_role_menu` VALUES (1585193266692988982, 1551458068714385410, 250);
INSERT INTO `sys_role_menu` VALUES (1585193266692988983, 1551458068714385410, 251);
INSERT INTO `sys_role_menu` VALUES (1585193266760097794, 1551458068714385410, 252);
INSERT INTO `sys_role_menu` VALUES (1585193266760097795, 1551458068714385410, 253);
INSERT INTO `sys_role_menu` VALUES (1585193266760097796, 1551458068714385410, 254);
INSERT INTO `sys_role_menu` VALUES (1585193266760097797, 1551458068714385410, 255);
INSERT INTO `sys_role_menu` VALUES (1585193266760097798, 1551458068714385410, 1558006778241753089);
INSERT INTO `sys_role_menu` VALUES (1585193266760097799, 1551458068714385410, 6414357916883420801);
INSERT INTO `sys_role_menu` VALUES (1585193266760097800, 1551458068714385410, 8000986046974593578);
INSERT INTO `sys_role_menu` VALUES (1585193266760097801, 1551458068714385410, 5197311839117522988);
INSERT INTO `sys_role_menu` VALUES (1585193266760097802, 1551458068714385410, 6494634442879742520);
INSERT INTO `sys_role_menu` VALUES (1585193266760097803, 1551458068714385410, 6659298240010835782);
INSERT INTO `sys_role_menu` VALUES (1585193266760097804, 1551458068714385410, 5027224879967170377);
INSERT INTO `sys_role_menu` VALUES (1585193266760097805, 1551458068714385410, 5465134665687105949);
INSERT INTO `sys_role_menu` VALUES (1585193266760097806, 1551458068714385410, 6535734057645557495);
INSERT INTO `sys_role_menu` VALUES (1585193266760097807, 1551458068714385410, 6134354751483903106);
INSERT INTO `sys_role_menu` VALUES (1585193266760097808, 1551458068714385410, 8424802293696759825);
INSERT INTO `sys_role_menu` VALUES (1585193266760097809, 1551458068714385410, 6122993178045887049);
INSERT INTO `sys_role_menu` VALUES (1585193266760097810, 1551458068714385410, 7529678467290942551);
INSERT INTO `sys_role_menu` VALUES (1585193266760097811, 1551458068714385410, 7209147716433162075);
INSERT INTO `sys_role_menu` VALUES (1585193266760097812, 1551458068714385410, 8655981106868069232);
INSERT INTO `sys_role_menu` VALUES (1585193266760097813, 1551458068714385410, 7337865129337018225);
INSERT INTO `sys_role_menu` VALUES (1585193266760097814, 1551458068714385410, 5196743176498588333);
INSERT INTO `sys_role_menu` VALUES (1585193266760097815, 1551458068714385410, 4794640277308881312);
INSERT INTO `sys_role_menu` VALUES (1585193266760097816, 1551458068714385410, 1574929774689878018);
INSERT INTO `sys_role_menu` VALUES (1585193266760097817, 1551458068714385410, 6947194724669964805);
INSERT INTO `sys_role_menu` VALUES (1585193266760097818, 1551458068714385410, 7119232719243003213);
INSERT INTO `sys_role_menu` VALUES (1585193266760097819, 1551458068714385410, 4678695510847297372);
INSERT INTO `sys_role_menu` VALUES (1585193266760097820, 1551458068714385410, 8502207026072268708);
INSERT INTO `sys_role_menu` VALUES (1585193266760097821, 1551458068714385410, 7294415501551357401);
INSERT INTO `sys_role_menu` VALUES (1585193266760097822, 1551458068714385410, 8175022271486866402);
INSERT INTO `sys_role_menu` VALUES (1585193266760097823, 1551458068714385410, 6996269839693777638);
INSERT INTO `sys_role_menu` VALUES (1585193266760097824, 1551458068714385410, 1552209067446579201);
INSERT INTO `sys_role_menu` VALUES (1585193266760097825, 1551458068714385410, 8422209827465537355);
INSERT INTO `sys_role_menu` VALUES (1585193266760097826, 1551458068714385410, 6405406552095053594);
INSERT INTO `sys_role_menu` VALUES (1585193266760097827, 1551458068714385410, 8903136733033804117);
INSERT INTO `sys_role_menu` VALUES (1585193266760097828, 1551458068714385410, 5382528335658667107);
INSERT INTO `sys_role_menu` VALUES (1585193266760097829, 1551458068714385410, 6046397975825687693);
INSERT INTO `sys_role_menu` VALUES (1585193266760097830, 1551458068714385410, 5999927780812383399);
INSERT INTO `sys_role_menu` VALUES (1585193266760097831, 1551458068714385410, 5274675770695528925);
INSERT INTO `sys_role_menu` VALUES (1585193266760097832, 1551458068714385410, 6813707923637624035);
INSERT INTO `sys_role_menu` VALUES (1585193266760097833, 1551458068714385410, 8287951873357908072);
INSERT INTO `sys_role_menu` VALUES (1585193266760097834, 1551458068714385410, 4700141466883779130);
INSERT INTO `sys_role_menu` VALUES (1585193266760097835, 1551458068714385410, 4834264129878073409);
INSERT INTO `sys_role_menu` VALUES (1585193266760097836, 1551458068714385410, 7845222658049694795);
INSERT INTO `sys_role_menu` VALUES (1585193266760097837, 1551458068714385410, 8946168826263694974);
INSERT INTO `sys_role_menu` VALUES (1585193266760097838, 1551458068714385410, 5162865219757254273);
INSERT INTO `sys_role_menu` VALUES (1585193266760097839, 1551458068714385410, 9090782745729954273);
INSERT INTO `sys_role_menu` VALUES (1585193266760097840, 1551458068714385410, 7909153361025684710);
INSERT INTO `sys_role_menu` VALUES (1585193266760097841, 1551458068714385410, 1342445437296771074);
INSERT INTO `sys_role_menu` VALUES (1585193266760097842, 1551458068714385410, 1264622039642256721);
INSERT INTO `sys_role_menu` VALUES (1585193266760097843, 1551458068714385410, 1264622039642256731);
INSERT INTO `sys_role_menu` VALUES (1585193266760097844, 1551458068714385410, 1264622039642256741);
INSERT INTO `sys_role_menu` VALUES (1585193266760097845, 1551458068714385410, 1574930637298827265);
INSERT INTO `sys_role_menu` VALUES (1585193266760097846, 1551458068714385410, 5474507168841280952);
INSERT INTO `sys_role_menu` VALUES (1585193266760097847, 1551458068714385410, 1574930880073531393);

-- ----------------------------
-- Table structure for sys_sms
-- ----------------------------
DROP TABLE IF EXISTS `sys_sms`;
CREATE TABLE `sys_sms`  (
                            `id` bigint(20) NOT NULL COMMENT '主键',
                            `phone_numbers` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '手机号',
                            `validate_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '短信验证码',
                            `template_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '短信模板ID',
                            `biz_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '回执id，可根据该id查询具体的发送状态',
                            `status` tinyint(4) NOT NULL COMMENT '发送状态（字典 0 未发送，1 发送成功，2 发送失败，3 失效）',
                            `source` tinyint(4) NOT NULL COMMENT '来源（字典 1 app， 2 pc， 3 其他）',
                            `invalid_time` datetime(0) NULL DEFAULT NULL COMMENT '失效时间',
                            `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                            `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                            `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                            `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                            PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '短信信息发送表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_sms
-- ----------------------------

-- ----------------------------
-- Table structure for sys_timers
-- ----------------------------
DROP TABLE IF EXISTS `sys_timers`;
CREATE TABLE `sys_timers`  (
                               `id` bigint(20) NOT NULL COMMENT '定时器id',
                               `timer_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '任务名称',
                               `action_class` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '执行任务的class的类名（实现了TimerTaskRunner接口的类的全称）',
                               `cron` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '定时任务表达式',
                               `job_status` tinyint(4) NULL DEFAULT 0 COMMENT '状态（字典 1运行  2停止）',
                               `remark` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注信息',
                               `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                               `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                               `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                               `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                               PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_timers
-- ----------------------------
INSERT INTO `sys_timers` VALUES (1288760324837851137, '定时同步缓存常量', 'vip.xiaonuo.sys.modular.timer.tasks.RefreshConstantsTaskRunner', '0 0/1 * * * ?', 1, '定时同步sys_config表的数据到缓存常量中', '2020-07-30 16:56:20', 1265476890672672808, '2020-07-30 16:58:52', 1265476890672672808);
INSERT INTO `sys_timers` VALUES (1304971718170832898, '定时打印一句话', 'vip.xiaonuo.sys.modular.timer.tasks.SystemOutTaskRunner', '0 0 * * * ? *', 2, '定时打印一句话', '2020-09-13 10:34:37', 1265476890672672808, '2020-09-23 20:37:48', 1265476890672672808);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
                             `id` bigint(20) NOT NULL COMMENT '主键',
                             `account` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '账号',
                             `pwd_hash_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
                             `nick_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '昵称',
                             `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '姓名',
                             `avatar` bigint(20) NULL DEFAULT NULL COMMENT '头像',
                             `birthday` date NULL DEFAULT NULL COMMENT '生日',
                             `sex` tinyint(4) NOT NULL COMMENT '性别(字典 1男 2女 3未知)',
                             `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
                             `phone` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机',
                             `tel` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '电话',
                             `last_login_ip` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最后登陆IP',
                             `last_login_time` datetime(0) NULL DEFAULT NULL COMMENT '最后登陆时间',
                             `admin_type` tinyint(4) NOT NULL DEFAULT 0 COMMENT '管理员类型（0超级管理员 1非管理员）',
                             `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '状态（字典 0正常 1冻结 2删除）',
                             `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                             `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                             `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                             `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                             PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统用户表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1265476890672672808, 'superAdmin', '207cf410532f92a47dee245ce9b11ff71f578ebd763eb3bbea44ebd043d018fb', '超级管理员', '超级管理员', NULL, '2020-03-18', 1, 'superAdmin@qq.com', '001757f43bd02871093cd7cbfed021f5', '1234567890', '127.0.0.1', '2022-10-26 16:54:14', 1, 0, '2020-05-29 16:39:28', -1, '2022-10-26 16:54:14', -1);
INSERT INTO `sys_user` VALUES (1275735541155614721, 'yubaoshan', '207cf410532f92a47dee245ce9b11ff71f578ebd763eb3bbea44ebd043d018fb', '俞宝山', '俞宝山', NULL, '1992-10-03', 1, 'await183@qq.com', '1f6e48e5610ba731115d118365dede4a', '', '127.0.0.1', '2022-08-17 14:37:51', 2, 0, '2020-06-24 18:20:30', 1265476890672672808, '2022-08-17 14:37:51', -1);
INSERT INTO `sys_user` VALUES (1280709549107552257, 'xuyuxiang', '207cf410532f92a47dee245ce9b11ff71f578ebd763eb3bbea44ebd043d018fb', '就是那个锅', '徐玉祥', NULL, '2020-07-01', 1, NULL, 'd5437be867777b7e27893bac3a3605a2', NULL, '127.0.0.1', '2022-07-25 16:25:33', 2, 0, '2020-07-08 11:45:26', 1265476890672672808, '2022-07-25 16:25:33', -1);
INSERT INTO `sys_user` VALUES (1471457941179072513, 'dongxiayu', '207cf410532f92a47dee245ce9b11ff71f578ebd763eb3bbea44ebd043d018fb', '阿董', '董夏雨', NULL, '2021-12-16', 1, NULL, '24c5ce408af1c56078c4d58098ebc218', NULL, NULL, NULL, 2, 0, '2021-12-16 20:31:26', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_user` VALUES (1540220849259393026, 'baowenlong', '207cf410532f92a47dee245ce9b11ff71f578ebd763eb3bbea44ebd043d018fb', '保山', '保温', NULL, NULL, 2, NULL, '64705d329344df804b7dd6f105ec88f4', NULL, '127.0.0.1', '2022-06-24 14:37:10', 2, 2, '2022-06-24 14:30:41', 1265476890672672808, '2022-06-24 14:37:50', 1265476890672672808);
INSERT INTO `sys_user` VALUES (1551457178297200641, 'Admin', '207cf410532f92a47dee245ce9b11ff71f578ebd763eb3bbea44ebd043d018fb', NULL, '测试用户', NULL, NULL, 1, NULL, '66664efc7eecc63d93f7c1ab56c690be', NULL, '127.0.0.1', '2022-07-25 16:33:33', 2, 0, '2022-07-25 14:39:51', 1265476890672672808, '2022-07-25 16:33:33', -1);
INSERT INTO `sys_user` VALUES (1551462379850723329, 'Admin1', '12f3bca18e6c1e61b6eaac788a7eb8fdbe795a235d2864f85269e9757b587e23', NULL, 'Admin', NULL, NULL, 1, NULL, '4884cc97e8656d2e7842ead0fb22fa1b', NULL, NULL, NULL, 2, 0, '2022-07-25 15:00:31', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_user` VALUES (1560148932795891714, ' admin', '207cf410532f92a47dee245ce9b11ff71f578ebd763eb3bbea44ebd043d018fb', '管理员', '管理员', NULL, NULL, 1, NULL, 'dc9456435c3f298f662811b8f9b57d5a', NULL, NULL, NULL, 2, 0, '2022-08-18 14:17:46', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_user` VALUES (1560149290020569089, 'manager', '207cf410532f92a47dee245ce9b11ff71f578ebd763eb3bbea44ebd043d018fb', '经理', '经理', NULL, NULL, 1, NULL, 'dc9456435c3f298f662811b8f9b57d5a', NULL, '127.0.0.1', '2022-08-22 09:30:25', 2, 0, '2022-08-18 14:19:12', 1265476890672672808, '2022-08-22 09:30:25', -1);
INSERT INTO `sys_user` VALUES (1560149788173860866, 'gleader', '207cf410532f92a47dee245ce9b11ff71f578ebd763eb3bbea44ebd043d018fb', NULL, '小组长 ', NULL, NULL, 1, NULL, 'dc9456435c3f298f662811b8f9b57d5a', NULL, '127.0.0.1', '2022-08-22 11:56:27', 2, 0, '2022-08-18 14:21:10', 1265476890672672808, '2022-08-22 11:56:27', -1);
INSERT INTO `sys_user` VALUES (1560149962497523714, 'wperson', '207cf410532f92a47dee245ce9b11ff71f578ebd763eb3bbea44ebd043d018fb', NULL, '生产人员', NULL, NULL, 1, NULL, 'dc9456435c3f298f662811b8f9b57d5a', NULL, '127.0.0.1', '2022-08-22 11:55:52', 2, 0, '2022-08-18 14:21:52', 1265476890672672808, '2022-08-22 11:55:52', -1);
INSERT INTO `sys_user` VALUES (1574927397500977154, 'nhszcy', '207cf410532f92a47dee245ce9b11ff71f578ebd763eb3bbea44ebd043d018fb', '南海数字产业', '南海数字产业', NULL, '2022-09-28', 1, NULL, 'fb0e6dadcedfb3eaca597709a5d2304c', NULL, '127.0.0.1', '2022-10-26 16:34:22', 2, 0, '2022-09-28 09:02:07', 1265476890672672808, '2022-10-26 16:34:22', -1);

-- ----------------------------
-- Table structure for sys_user_data_scope
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_data_scope`;
CREATE TABLE `sys_user_data_scope`  (
                                        `id` bigint(20) NOT NULL COMMENT '主键',
                                        `user_id` bigint(20) NOT NULL COMMENT '用户id',
                                        `org_id` bigint(20) NOT NULL COMMENT '机构id',
                                        PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统用户数据范围表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user_data_scope
-- ----------------------------
INSERT INTO `sys_user_data_scope` VALUES (1277459951742840834, 1266277099455635457, 1265476890672672770);
INSERT INTO `sys_user_data_scope` VALUES (1277459952577507330, 1266277099455635457, 1265476890672672773);
INSERT INTO `sys_user_data_scope` VALUES (1277459953424756737, 1266277099455635457, 1265476890672672775);
INSERT INTO `sys_user_data_scope` VALUES (1277459954267811841, 1266277099455635457, 1265476890672672774);
INSERT INTO `sys_user_data_scope` VALUES (1542386685185208321, 1275735541155614721, 1265476890651701250);
INSERT INTO `sys_user_data_scope` VALUES (1542386685248122882, 1275735541155614721, 1265476890672672769);
INSERT INTO `sys_user_data_scope` VALUES (1542386685248122883, 1275735541155614721, 1265476890672672771);
INSERT INTO `sys_user_data_scope` VALUES (1542386685248122884, 1275735541155614721, 1265476890672672772);
INSERT INTO `sys_user_data_scope` VALUES (1542386685315231745, 1275735541155614721, 1265476890672672770);
INSERT INTO `sys_user_data_scope` VALUES (1542386685382340609, 1275735541155614721, 1265476890672672773);
INSERT INTO `sys_user_data_scope` VALUES (1542386685382340610, 1275735541155614721, 1265476890672672775);
INSERT INTO `sys_user_data_scope` VALUES (1542386685449449473, 1275735541155614721, 1265476890672672774);
INSERT INTO `sys_user_data_scope` VALUES (1542404712807006209, 1280709549107552257, 1265476890651701250);
INSERT INTO `sys_user_data_scope` VALUES (1542404712874115073, 1280709549107552257, 1265476890672672769);
INSERT INTO `sys_user_data_scope` VALUES (1542404712874115074, 1280709549107552257, 1265476890672672771);
INSERT INTO `sys_user_data_scope` VALUES (1542404712874115075, 1280709549107552257, 1265476890672672772);
INSERT INTO `sys_user_data_scope` VALUES (1542404712941223937, 1280709549107552257, 1265476890672672770);
INSERT INTO `sys_user_data_scope` VALUES (1542404712941223938, 1280709549107552257, 1265476890672672773);
INSERT INTO `sys_user_data_scope` VALUES (1542404712941223939, 1280709549107552257, 1265476890672672775);
INSERT INTO `sys_user_data_scope` VALUES (1542404713008332802, 1280709549107552257, 1265476890672672774);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
                                  `id` bigint(20) NOT NULL COMMENT '主键',
                                  `user_id` bigint(20) NOT NULL COMMENT '用户id',
                                  `role_id` bigint(20) NOT NULL COMMENT '角色id',
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统用户角色表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1542406846898241538, 1280709549107552257, 1265476890672672817);
INSERT INTO `sys_user_role` VALUES (1542406846961156097, 1280709549107552257, 1265476890672672818);
INSERT INTO `sys_user_role` VALUES (1549008694352695298, 1275735541155614721, 1265476890672672818);
INSERT INTO `sys_user_role` VALUES (1551460683594493953, 1551457178297200641, 1551458068714385410);
INSERT INTO `sys_user_role` VALUES (1551462466786062338, 1551462379850723329, 1551458068714385410);
INSERT INTO `sys_user_role` VALUES (1560148974642462722, 1560148932795891714, 1560148188390817794);
INSERT INTO `sys_user_role` VALUES (1560149318302760961, 1560149290020569089, 1560148282678771714);
INSERT INTO `sys_user_role` VALUES (1585193319432167425, 1574927397500977154, 1551458068714385410);

-- ----------------------------
-- Table structure for sys_vis_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_vis_log`;
CREATE TABLE `sys_vis_log`  (
                                `id` bigint(20) NOT NULL COMMENT '主键',
                                `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
                                `success` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否执行成功（Y-是，N-否）',
                                `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '具体消息',
                                `ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'ip',
                                `location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '地址',
                                `browser` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '浏览器',
                                `os` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作系统',
                                `vis_type` tinyint(4) NOT NULL COMMENT '操作类型（字典 1登入 2登出）',
                                `vis_time` datetime(0) NULL DEFAULT NULL COMMENT '访问时间',
                                `account` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '访问账号',
                                `sign_value` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '签名数据（除ID外）',
                                PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统访问日志表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_vis_log
-- ----------------------------
INSERT INTO `sys_vis_log` VALUES (1585192999054450689, '登出', 'Y', '登出成功', '127.0.0.1', '-', 'Chrome', 'Windows 10 or Windows Server 2016', 2, '2022-10-26 16:53:57', 'nhszcy', 'e230f39717e89ca9dde32ed5039c65bcae842356c5b4103370f895a0c019f884e311fc7ee59b54fa039a097d8b0e62260c99c5145cede079d7c5445b412bb7fb');
INSERT INTO `sys_vis_log` VALUES (1585193071775293442, '登录', 'Y', '登录成功', '127.0.0.1', '-', 'Chrome', 'Windows 10 or Windows Server 2016', 1, '2022-10-26 16:54:14', 'superAdmin', '04c848cdf05408b7c4aee332067744617ed142bde0da5e4b05c522da3ab93111d738d94c84cd7305730c7d5abf1d467b7397bb1df6200a210381ed1e1c4a3e68');

SET FOREIGN_KEY_CHECKS = 1;
