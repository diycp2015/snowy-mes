import { axios } from '@/utils/request'

/**
 * 查询工单任务关系表
 *
 * @author czw
 * @date 2022-06-07 10:17:09
 */
export function workOrderTaskPage (parameter) {
  return axios({
    url: '/workOrderTask/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 工单任务关系表列表
 *
 * @author czw
 * @date 2022-06-07 10:17:09
 */
export function workOrderTaskList (parameter) {
  return axios({
    url: '/workOrderTask/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加工单任务关系表
 *
 * @author czw
 * @date 2022-06-07 10:17:09
 */
export function workOrderTaskAdd (parameter) {
  return axios({
    url: '/workOrderTask/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑工单任务关系表
 *
 * @author czw
 * @date 2022-06-07 10:17:09
 */
export function workOrderTaskEdit (parameter) {
  return axios({
    url: '/workOrderTask/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除工单任务关系表
 *
 * @author czw
 * @date 2022-06-07 10:17:09
 */
export function workOrderTaskDelete (parameter) {
  return axios({
    url: '/workOrderTask/delete',
    method: 'post',
    data: parameter
  })
}

/**
 * 导出工单任务关系表
 *
 * @author czw
 * @date 2022-06-07 10:17:09
 */
export function workOrderTaskExport (parameter) {
  return axios({
    url: '/workOrderTask/export',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}
