/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.invdetail.param;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import vip.xiaonuo.core.pojo.base.param.BaseParam;
import lombok.Data;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.NotBlank;
import java.util.*;

/**
* 出入库明细参数类
 *
 * @author wz
 * @date 2022-06-10 14:27:42
*/
@Data
public class InvDetailParam extends BaseParam {

    /**
     * 主键id
     */
    @NotNull(message = "主键id不能为空，请检查id参数", groups = {edit.class, delete.class, detail.class})
    private Long id;

    /**
     * 库单ID
     */
    @NotNull(message = "库单ID不能为空，请检查invId参数", groups = {add.class, edit.class})
    private String invId;

    /**
     * 产品ID
     */
    @NotNull(message = "产品ID不能为空，请检查proId参数", groups = {add.class, edit.class})
    private String proId;

    /**
     * 库单ID数组
     */
    private String invIds;

    /**
     * 产品ID数组
     */
    private String proIds;


    /**
     * 数量
     */
    @NotNull(message = "数量不能为空，请检查num参数", groups = {add.class, edit.class})
    private Long num;
    private String proName;
    /**
     * 库单编码
     */
    private String code;
    /**
     * 产品单位
     */
    private String unit;
    /**
     * 出入库时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private String time;
    /**
     * 来源
     */
    private Integer category;

    /**
     * 出库类型
     */
    private String outType;
    /**
     * 入库类型
     */
    private String inType;
    /**
     * 出库类型数组
     */
    private String outTypes;
    /**
     * 入库类型数组
     */
    private String inTypes;

    /**
     * 产品编码
     */
    private String proCode;

    /**
     * 仓库id
     */
    @Excel(name = "仓库id")
    private Long warHouId;

    /**
     * 仓库名
     */
    @Excel(name = "仓库名")
    private String warHouName;


}
