/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.workroute.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.xiaonuo.core.pojo.page.PageResult;
import vip.xiaonuo.modular.workroute.entity.WorkRoute;
import vip.xiaonuo.modular.workroute.param.WorkRouteParam;
import java.util.List;

/**
 * 工艺路线service接口
 *
 * @author bwl
 * @date 2022-05-24 15:33:25
 */
public interface WorkRouteService extends IService<WorkRoute> {

    /**
     * 查询工艺路线
     *
     * @author bwl
     * @date 2022-05-24 15:33:25
     */
    PageResult<WorkRoute> page(WorkRouteParam workRouteParam);

    /**
     * 工艺路线列表
     *
     * @author bwl
     * @date 2022-05-24 15:33:25
     */
    List<WorkRoute> list(WorkRouteParam workRouteParam);

    /**
     * 添加工艺路线
     *
     * @author bwl
     * @date 2022-05-24 15:33:25
     */
    void add(WorkRouteParam workRouteParam);

    /**
     * 删除工艺路线
     *
     * @author bwl
     * @date 2022-05-24 15:33:25
     */
    void delete(List<WorkRouteParam> workRouteParamList);

    /**
     * 编辑工艺路线
     *
     * @author bwl
     * @date 2022-05-24 15:33:25
     */
    void edit(WorkRouteParam workRouteParam);

    /**
     * 查看工艺路线
     *
     * @author bwl
     * @date 2022-05-24 15:33:25
     */
     WorkRoute detail(WorkRouteParam workRouteParam);

    /**
     * 导出工艺路线
     *
     * @author bwl
     * @date 2022-05-24 15:33:25
     */
     void export(WorkRouteParam workRouteParam);

}
