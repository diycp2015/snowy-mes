/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.workorder.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;
import vip.xiaonuo.core.pojo.base.entity.BaseEntity;

import java.util.Date;
import java.util.List;

/**
 * 工单表
 *
 * @author czw
 * @date 2022-05-25 10:31:14
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("dw_work_order")
public class WorkOrder extends BaseEntity {


    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;


    /**
     * 工单类型：1：顺序工单；2：流程工单
     */
    @Excel(name = "工单类型")
    private Integer type;


    /**
     * 计划结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm")
    @Excel(name = "计划结束时间", databaseFormat = "yyyy-MM-dd HH:mm:ss", format = "yyyy-MM-dd HH:mm", width = 20)
    private Date plaEndTime;


    /**
     * 计划数
     */
    @Excel(name = "计划数")
    private Integer plaNum;

    /**
     * 计划开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm")
    @Excel(name = "计划开始时间", databaseFormat = "yyyy-MM-dd HH:mm:ss", format = "yyyy-MM-dd HH:mm", width = 20)
    private Date plaStartTime;

    /**
     * 产品id
     */
    @Excel(name = "产品id")
    private Long proId;

    /**
     * 产品型号id
     */
    //@Excel(name = "产品型号id")
    private Long proModelId;

    /**
     * 产品类型id
     */
    //@Excel(name = "产品类型id")
    private Long proTypeId;

    /**
     * 关联单据id
     */
    @Excel(name = "关联单据")
    private Long billId;

    /**
     * 备注
     */
    @Excel(name = "备注")
    private String remarks;

    /**
     * 状态(未开始:-1;执行中:0;已结束:1;已确认:-2)
     */
    @Excel(name = "状态(未开始:-1;执行中:0;已结束:1;已确认:-2)")
    private Integer status;

    /**
     * 工单编号
     */
    @Excel(name = "工单编号")
    private String workOrderNo;

    /**
     * 实际结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm ")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm" ,timezone="GMT+8")
    @Excel(name = "实际结束时间", databaseFormat = "yyyy-MM-dd HH:mm", format = "yyyy-MM-dd", width = 20)
    // @Excel(name = "实际结束时间")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Date factEndTime;

    /**
     * 实际开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm ")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm",timezone="GMT+8")
    @Excel(name = "实际开始时间", databaseFormat = "yyyy-MM-dd HH:mm", format = "yyyy-MM-dd ", width = 20)
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    // @Excel(name = "实际开始时间")
    private Date factStaTime;

    /**
     * 下单人
     */
    @Excel(name = "下单人")
    private Long nextPerson;

    /**
     * 负责人
     */
    @Excel(name = "负责人")
    private Long personCharge;

    /**
     * 下单时间
     *
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm ")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm" ,timezone="GMT+8")
    @Excel(name = "下单时间", databaseFormat = "yyyy-MM-dd HH:mm", format = "yyyy-MM-dd", width = 20)
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Date orderTime;
    /**
     * 备注
     *
     */
    private String enclosure;
    /**
     * 生产进度
     */
    @TableField(exist = false)
    private List<Integer> progressRateList;
}