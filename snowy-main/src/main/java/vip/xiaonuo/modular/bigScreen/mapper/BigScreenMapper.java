package vip.xiaonuo.modular.bigScreen.mapper;


import vip.xiaonuo.modular.bigScreen.param.WorkStepOrderParam;
import vip.xiaonuo.modular.bigScreen.result.RejectsResult;
import vip.xiaonuo.modular.bigScreen.result.WorkOrderAlertResult;
import vip.xiaonuo.modular.bigScreen.result.WorkOrderProResult;
import vip.xiaonuo.modular.bigScreen.result.WorkStepOrderResult;

import java.util.List;

public interface BigScreenMapper {
   List<RejectsResult> selectRejects();
   List<WorkOrderProResult> selectWorkOrderPro();
   List<WorkOrderAlertResult> selectWorkOrderAlert();
   List<WorkStepOrderResult> selectWorkStepOrder(WorkStepOrderParam workStepOrderParam);

}
